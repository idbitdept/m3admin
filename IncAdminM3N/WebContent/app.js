/*
 * File: app.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

// @require @packageOverrides
Ext.Loader.setConfig({

});


Ext.application({
    models: [
        'ApiResponse',
        'mdlOps'
    ],
    stores: [
        'apilog',
        'byProg',
        'byDay',
        'oksnoks',
        'LtrcByDay',
        'Trn',
        'byProgStacked',
        'Users',
        'Programs',
        'Transactions',
        'Fields',
        'ApiResponse',
        'byWhlo',
        'warehouses',
        'itemGroups',
        'itemByitemGroups',
        'stock',
        'lots',
        'strStatus',
        'strCalcMethod',
        'pos',
        'po_pallets',
        'itemsAll',
        'reasonCodes',
        'daycodes',
        'strQI',
        'zoi275',
        'stockByLot',
        'completeOrders',
        'customers',
        'itemsAllAttr',
        'attrValues',
        'qmsValues',
        'complete44Orders',
        'strOps',
        'coLines',
        'tsBD',
        'tsBDPostProd',
        'mws070ItnoCamu',
        'strAllocCodes',
        'locations',
        'qms001',
        'deASNhdr',
        'deASNln',
        'pos-int',
        'pos-int-pallets',
        'ois275',
        'strFacilities',
        'deASNhdrEVO',
        'deASNlnEVO',
        'bucket'
    ],
    views: [
        'LoginViewport',
        'MainView',
        'pnlApilog',
        'ByProgram',
        'byDay',
        'wndResubmit',
        'frmResubmit',
        'OksNoks',
        'LtrcStackedByDay',
        'MyTabPanel',
        'ProgStacked',
        'frmAddUser',
        'wndAddUser',
        'barByWhlo',
        'wndAttrValues',
        'wndQmsValues',
        'wndCOLines',
        'wndTransHistPostProd',
        'wndXMLContents',
        'wndAttrResults',
        'wndBucket'
    ],
    mainView: 'MyApp.view.LoginViewport',
    name: 'MyApp',
    'favIcon - url': 'jsp/favicon.ico',

    requires: [
        'Ext.chart.*'
    ],

    launch: function() {
        // check is user already validly logged in...


        Ext.Ajax.request({
            url: 'jsp/checkLogin.jsp',
            success: function(response){
                var check = Ext.decode(response.responseText);
                if (check.success === true) {
                    var target = MyApp.app.getMainView();
                    target.destroy();
                    MyApp.app.setMainView('MyApp.view.MainView');
                        // call these sequentially...

          Ext.getStore('oksnoks').load({
            params : {},
            success : function() {},
            failure : function() {},
            callback : function() {

                Ext.getStore('LtrcByDay').load({
                    params : {},
                    success : function() {},
                    failure : function() {},
                    callback : function() {

                         Ext.getStore('byWhlo').load({
                            params : {},
                            success : function() {},
                            failure : function() {},
                            callback : function() {

                                Ext.getStore("apilog").load({
                                    params: {
                                    rgdt: Ext.Date.format(new Date(), 'Ymd')
                                    }
                                });
                            }
                         });
                    }
                });
            }
        });

                }


            }, failure: function(response) {
                Ext.getBody().unmask();
            }
        });

        Ext.onReady(function(){
            Ext.util.Observable.observe(Ext.data.Connection, {
                requestexception: function(conn, response, options) {
                    // User's session has expired - "unauthorized" response from server. Log the user out
                    if (response.status === 401) {

                        var href = window.location.origin + window.location.pathname;


                        var curr = document.location.href.split('?')[1];
                        curr = curr.replace('&expired','expired');
                        curr = curr.replace('&logout=true', '');
                        curr = curr.replace('logout=true', '');
                        var expired = "expired=true";
                        if (curr.indexOf("expired=true") > -1) expired = "";

                        if (curr.substring(0, 1) == "&") curr = curr.substring(1);

                        if(! href)
                            window.location.href = href + "?" + expired + "&" + curr;
                        else {
                            href = window.location.origin + window.location.pathname;
                            window.location.href = href + "?" + expired + "&" + curr;
                        }

                        //window.location.href = href + "?expired=true";
                    }
                }
            });
        });
    }

});
