package com.ornua;


import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.ornua.MvxBean;


/**
 * Servlet implementation class excelM3API
 */
@WebServlet("/excelM3API")
public class excelM3API extends HttpServlet {
private static final long serialVersionUID = 1L;
private String minm;
private String trnm;
private String trds;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public excelM3API() {
        super();
        // TODO Auto-generated constructor stub
    }

/**
* @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
*/
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub

minm = request.getParameter("minm"); if(minm==null) minm = "";
trds = request.getParameter("trds"); if(trds==null) trds = "";
trnm = request.getParameter("trnm"); if(trnm==null) trnm = "";
//System.out.println(minm + " " + trnm + " " + trds);
MvxBean KevBean = new MvxBean();

HttpSession session = request.getS	ession(true);
String system = session.getAttribute("system").toString();
String cono = session.getAttribute("zdcono").toString();

String m3user = session.getAttribute("m3user").toString();
String m3pass = session.getAttribute("m3pass").toString();

int port = (Integer)session.getAttribute("port");

KevBean.setSystem(system);
KevBean.setPort(port);
KevBean.setCompany(cono);
KevBean.setUsername(m3user);
KevBean.setPassword(m3pass);

KevBean.setInitialise("MRS001MI");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "I"); //input fields
KevBean.runProgram("LstFields");
XSSFWorkbook workbook = new XSSFWorkbook();
CreationHelper createHelper = workbook.getCreationHelper();
Sheet inSheet = workbook.createSheet("Input");
Sheet outSheet = workbook.createSheet("Output");

// Create a Font for styling header cells
Font headerFont = workbook.createFont();
headerFont.setBold(true);
headerFont.setFontHeightInPoints((short) 14);
headerFont.setColor(IndexedColors.RED.getIndex());

// Create a CellStyle with the font
CellStyle headerCellStyle = workbook.createCellStyle();
headerCellStyle.setFont(headerFont);

// Create a Row
Row progName = inSheet.createRow(0);
Cell cellMinm = progName.createCell(0);
cellMinm.setCellValue(minm);
Cell cellTrnm = progName.createCell(1);
cellTrnm.setCellValue(trnm);
Cell cellTrds = progName.createCell(2);
cellTrds.setCellValue(trds);

Row headerRow = inSheet.createRow(1);
String[] columns = {"Field Name", "Description", "Length", "Type", "Mandatory", "From", "To"};

// Create cells
for(int i = 0; i < columns.length; i++) {
    Cell cell = headerRow.createCell(i);
    cell.setCellValue(columns[i]);
    cell.setCellStyle(headerCellStyle);
}

// Create Cell Style for formatting Date
CellStyle dateCellStyle = workbook.createCellStyle();
dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

int rowNum = 2;
String flnm = ""; String flds = ""; String leng = ""; String frpo = ""; String topo = ""; String type = ""; String mand = "";
while(KevBean.nextRow()) {
flnm = KevBean.getField("FLNM"); flds = KevBean.getField("FLDS"); 
leng = KevBean.getField("LENG"); frpo = KevBean.getField("FRPO"); 
topo = KevBean.getField("TOPO"); type = KevBean.getField("TYPE"); type = type.trim();
if(type.equals("A")) type = "Alphanumeric"; else if (type.equals("N")) type = "Numeric"; else if (type.equals("D")) type = "Date";
mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim(); if(mand.equals("1")) mand = "Yes";
    Row row = inSheet.createRow(rowNum++);
    row.createCell(0).setCellValue(flnm);
    row.createCell(1).setCellValue(flds);

    row.createCell(2).setCellValue(leng);
    row.createCell(3).setCellValue(type);
    row.createCell(4).setCellValue(mand);
    row.createCell(5).setCellValue(frpo);
    row.createCell(6).setCellValue(topo);
}

// Resize all columns to fit the content size
for(int i = 0; i < columns.length; i++) {
    inSheet.autoSizeColumn(i);
}


KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "O"); //output fields

KevBean.runProgram("LstFields");


headerRow = outSheet.createRow(0);

for(int i = 0; i < columns.length; i++) {
    Cell cell = headerRow.createCell(i);
    cell.setCellValue(columns[i]);
    cell.setCellStyle(headerCellStyle);
}




rowNum = 1;
while(KevBean.nextRow()) {
flnm = KevBean.getField("FLNM"); flds = KevBean.getField("FLDS"); 
leng = KevBean.getField("LENG"); frpo = KevBean.getField("FRPO"); 
topo = KevBean.getField("TOPO"); type = KevBean.getField("TYPE"); 
mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim();
    Row row = outSheet.createRow(rowNum++);
    row.createCell(0).setCellValue(flnm);
    row.createCell(1).setCellValue(flds);
    row.createCell(2).setCellValue(frpo);
    row.createCell(3).setCellValue(topo);
    row.createCell(4).setCellValue(leng);
    row.createCell(5).setCellValue(type);
    row.createCell(6).setCellValue(mand);
}

// Resize all columns to fit the content size
for(int i = 0; i < columns.length; i++) {
    outSheet.autoSizeColumn(i);
}

response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "attachment; filename=" + minm + "_" + trnm + ".xlsx");

workbook.write(response.getOutputStream()); // Write workbook to response.
// Closing the workbook
workbook.close();
}

/**
* @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
*/
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub
response.getWriter().append("Served at: ").append(request.getContextPath());
}

/**
* @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
*/
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub
doGet(request, response);
}

}

