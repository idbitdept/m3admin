package com.ornua;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * Servlet implementation class xsdM3API
 */
@WebServlet("/xsdM3API")
public class xsdM3API extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String minm;
	private String trnm;
	private String trds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public xsdM3API() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub

	    minm = request.getParameter("minm"); if(minm==null) minm = "";
	    trds = request.getParameter("trds"); if(trds==null) trds = "";
	    trnm = request.getParameter("trnm"); if(trnm==null) trnm = "";
	    System.out.println(minm + " " + trnm + " " + trds);
	    MvxBean KevBean = new MvxBean();
	
	    HttpSession session = request.getSession(true);
	    String system = session.getAttribute("system").toString();
	    String cono = session.getAttribute("zdcono").toString();
	
	    String m3user = session.getAttribute("m3user").toString();
	    String m3pass = session.getAttribute("m3pass").toString();
	    
	    String uname = session.getAttribute("uname").toString();
	
	    int port = (Integer)session.getAttribute("port");
	
	    KevBean.setSystem(system);
	    KevBean.setPort(port);
	    KevBean.setCompany(cono);
	    KevBean.setUsername(m3user);
	    KevBean.setPassword(m3pass);
	
	    KevBean.setInitialise("MRS001MI");
	
	    KevBean.setField("MINM", minm);
	    KevBean.setField("TRNM", trnm);
	    KevBean.setField("TRTP", "I"); //input fields
	    KevBean.runProgram("LstFields");
	    
	    Element root;
	    Document doc;
	    //int id = 1;
	    
	    Namespace ns1 = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema");
	    //Namespace ns2 = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
	    //Namespace ns3 = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema-instance");
	    //Namespace ns4 = Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema");
	    //Namespace ns5 = Namespace.getNamespace("xs");
	    root = new Element("schema", ns1);
	    //root.addNamespaceDeclaration(ns2);
	    //root.setAttribute("VERSION", "1.0");
	    
	    Comment comment = new Comment("Exported by " + uname + " at " + Calendar.getInstance().getTime() + "m3 api version 13.4java");
		root.addContent(comment);
	    
	    Element masterdata = new Element("element", root.getNamespace());
	    masterdata.setAttribute("name", "masterdata");
	    root.addContent(masterdata);
	    
	    Element complexType = new Element("complexType", root.getNamespace());
	    Element complexType2 = new Element("complexType", root.getNamespace());
	    masterdata.addContent(complexType);
	    
	    Element sequence = new Element("sequence", root.getNamespace());
	    Element sequence2 = new Element("sequence", root.getNamespace());
	    complexType.addContent(sequence);
	   
	    
	    Element reference = new Element("element", root.getNamespace());
	    reference.setAttribute("name", "reference");
	    reference.setAttribute("type", "xs:string");
	    reference.setAttribute("minOccurs", "0");	    
	    sequence.addContent(reference);
	    
	    Element program = new Element("element", root.getNamespace());
	    program.setAttribute("name", "program");
	    program.setAttribute("type", "xs:string");
	    program.setAttribute("fixed", minm);	    
	    sequence.addContent(program);
	    
	    Element transaction = new Element("element", root.getNamespace());
	    transaction.setAttribute("name", "transaction");
	    transaction.setAttribute("type", "xs:string");
	    transaction.setAttribute("fixed", trnm);	    
	    sequence.addContent(transaction);
	    
	    
	    Element rec = new Element("element", root.getNamespace());
	    rec.setAttribute("name", "record");
	    rec.setAttribute("maxOccurs", "unbounded");  
	    sequence.addContent(rec);
	    

	    rec.addContent(complexType2);
	    complexType2.addContent(sequence2);
	    
	    String flnm = ""; String flds = ""; String leng = ""; String type = ""; String mand = "";
	    Element apiField = null;
	    while(KevBean.nextRow()) {
	    	flnm = KevBean.getField("FLNM"); if(flnm==null) flnm = ""; flnm = flnm.trim();
	    	flds = KevBean.getField("FLDS"); if(flds==null) flds = ""; flds = flds.trim();
	    	leng = KevBean.getField("LENG"); if(leng==null) leng = ""; leng = leng.trim();
	    	type = KevBean.getField("TYPE"); if(type==null) type = ""; type = type.trim();  	 
	    	mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim();
	    	
	    	apiField = new Element("element", root.getNamespace());
	    	apiField.setAttribute("name", flnm);
	    	if(type.equals("N"))
	    		apiField.setAttribute("type", "xs:decimal");
	    	else
	    		apiField.setAttribute("type", "xs:string");
	    	if(!mand.equals("1"))
	    		apiField.setAttribute("minOccurs", "0");	    
		    sequence2.addContent(apiField);
		    
		    mand = ""; type = "";
	    }

	    doc = new Document(root);
	    
	    response.setContentType("text/xml");
	    response.setHeader("Content-Disposition", "attachment; filename=" + minm + "_" + trnm + ".xsd");
	    
	    XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(doc, response.getOutputStream());	    
	    
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}