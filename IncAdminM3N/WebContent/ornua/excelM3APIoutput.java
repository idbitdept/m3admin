package com.ornua;


import java.io.*;
import java.util.Enumeration;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import com.ornua.MvxBean;


/**
 * Servlet implementation class excelM3API
 */
@WebServlet("/excelM3APIoutput")
public class excelM3APIoutput extends HttpServlet {
private static final long serialVersionUID = 1L;
private String minm;
private String trnm;
private String trds;

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public excelM3APIoutput() {
        super();
        // TODO Auto-generated constructor stub
    }

/**
* @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
*/
@SuppressWarnings("unchecked")
protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

Object obj=JSONValue.parse(request.getParameter("data"));  
JSONObject jsonObject = (JSONObject) obj;  


String maxRec = request.getParameter("maxRecs"); if(maxRec==null) maxRec = "";
System.out.println("max rec is " + maxRec);
minm = (String)jsonObject.get("minm");
trnm = (String)jsonObject.get("trnm");

MvxBean KevBean = new MvxBean();

HttpSession session = request.getSession(true);
String system = session.getAttribute("system").toString();
String cono = session.getAttribute("zdcono").toString();

String m3user = session.getAttribute("m3user").toString();
String m3pass = session.getAttribute("m3pass").toString();

int port = (Integer)session.getAttribute("port");

KevBean.setSystem(system);
KevBean.setPort(port);
KevBean.setCompany(cono);
KevBean.setUsername(m3user);
KevBean.setPassword(m3pass);

KevBean.setInitialise("MRS001MI");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "O"); 

KevBean.runProgram("LstFields");
String[] fieldNames = new String[200];
XSSFWorkbook workbook = new XSSFWorkbook();
CreationHelper createHelper = workbook.getCreationHelper();
Sheet inSheet = workbook.createSheet("API Output");

// Create a Font for styling header cells
Font headerFont = workbook.createFont();
headerFont.setBold(true);
headerFont.setFontHeightInPoints((short) 14);
headerFont.setColor(IndexedColors.RED.getIndex());

// Create a CellStyle with the font
CellStyle headerCellStyle = workbook.createCellStyle();
headerCellStyle.setFont(headerFont);

// Create a Row
Row progName = inSheet.createRow(0);
Cell cellMinm = progName.createCell(0);
cellMinm.setCellValue(minm);
Cell cellTrnm = progName.createCell(1);
cellTrnm.setCellValue(trnm);
//Cell cellTrds = progName.createCell(2);
//cellTrds.setCellValue(trds);

Row headerRow = inSheet.createRow(1);
int totFields = 0;
while(KevBean.nextRow()) {
	String flnm = KevBean.getField("FLNM");
	  fieldNames[totFields] = flnm;
  Cell cell = headerRow.createCell(totFields++);
  cell.setCellValue(flnm);
  cell.setCellStyle(headerCellStyle);
}

int rowNum = 2;

KevBean.setInitialise(minm);
if(!maxRec.equals("")) {
	KevBean.setField("MRCD", maxRec);
	KevBean.runProgram("SetLstMaxRec");
}

jsonObject.keySet().forEach(keyStr -> {
    Object keyvalue = jsonObject.get(keyStr);
    //System.out.println("key: "+ keyStr + " value: " + keyvalue);
    String kstr = keyStr.toString();
    String kvalue = keyvalue.toString();
  if(Character.isUpperCase(kstr.charAt(0)))
   	   KevBean.setField(kstr, kvalue);
});

KevBean.runProgram(trnm);
while(KevBean.nextRow()) {
	Row thsRow = inSheet.createRow(rowNum++);
	for(int j=0; j<totFields; j++) {
		thsRow.createCell(j).setCellValue(KevBean.getField(fieldNames[j]));
	}
}

response.setContentType("application/vnd.ms-excel");
response.setHeader("Content-Disposition", "attachment; filename=" + minm + "_" + trnm + "_output.xlsx");

workbook.write(response.getOutputStream()); // Write workbook to response.
// Closing the workbook
workbook.close();
}

/**
* @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
*/
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub
response.getWriter().append("Served at: ").append(request.getContextPath());
}

/**
* @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
*/
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
// TODO Auto-generated method stub
doGet(request, response);
}

}

