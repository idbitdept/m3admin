/*
 * File: app/store/locations.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.locations', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.field.Field'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'locations',
            proxy: {
                type: 'ajax',
                url: 'jsp/locations.jsp',
                reader: {
                    type: 'json'
                }
            },
            fields: [
                {
                    name: 'whsl'
                },
                {
                    name: 'slds'
                }
            ]
        }, cfg)]);
    }
});