/*
 * File: app/store/strStatus.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.strStatus', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.field.Integer'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            storeId: 'strStatus',
            autoLoad: false,
            data: [
                {
                    statuscode: 99,
                    statusdesc: 'As is - dont change status'
                },
                {
                    statuscode: 1,
                    statusdesc: '1 - Under Inspection'
                },
                {
                    statuscode: 2,
                    statusdesc: '2 - Approved'
                },
                {
                    statuscode: 3,
                    statusdesc: '3 - Rejected'
                }
            ],
            proxy: {
                type: 'ajax',
                reader: {
                    type: 'json'
                }
            },
            fields: [
                {
                    type: 'int',
                    name: 'statuscode'
                },
                {
                    name: 'statusdesc'
                }
            ]
        }, cfg)]);
    }
});