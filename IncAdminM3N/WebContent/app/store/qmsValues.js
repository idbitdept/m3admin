/*
 * File: app/store/qmsValues.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.store.qmsValues', {
    extend: 'Ext.data.Store',

    requires: [
        'Ext.data.proxy.Ajax',
        'Ext.data.reader.Json',
        'Ext.data.field.Number'
    ],

    constructor: function(cfg) {
        var me = this;
        cfg = cfg || {};
        me.callParent([Ext.apply({
            groupField: 'qrid',
            storeId: 'qmsValues',
            proxy: {
                type: 'ajax',
                url: 'jsp/getQmsValues.jsp',
                reader: {
                    type: 'json'
                }
            },
            fields: [
                {
                    name: 'spec'
                },
                {
                    name: 'qtst'
                },
                {
                    name: 'tx40'
                },
                {
                    name: 'atid'
                },
                {
                    type: 'float',
                    name: 'evmx'
                },
                {
                    type: 'float',
                    name: 'evmn'
                },
                {
                    type: 'float',
                    name: 'evtg'
                },
                {
                    name: 'qtrs'
                },
                {
                    name: 'bano'
                },
                {
                    name: 'itno'
                },
                {
                    name: 'qrid'
                },
                {
                    type: 'int',
                    name: 'tstt'
                },
                {
                    name: 'qse1'
                },
                {
                    type: 'int',
                    name: 'qop1'
                },
                {
                    name: 'ttus'
                },
                {
                    name: 'ttdt'
                },
                {
                    name: 'ttte'
                },
                {
                    name: 'ttdate'
                },
                {
                    type: 'int',
                    name: 'tseq'
                },
                {
                    type: 'int',
                    name: 'tsty'
                },
                {
                    name: 'prvl'
                }
            ]
        }, cfg)]);
    }
});