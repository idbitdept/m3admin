/*
 * File: app/view/OksNoks.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.OksNoks', {
    extend: 'Ext.chart.PolarChart',
    alias: 'widget.oksnoks',

    requires: [
        'MyApp.view.OksNoksViewModel',
        'MyApp.view.OksNoksViewController',
        'Ext.chart.series.Pie',
        'Ext.chart.series.sprite.PieSlice',
        'Ext.chart.legend.Legend'
    ],

    config: {
        plugins: {
            ptype: 'chartitemevents',
            moveEvents: true
        }
    },

    controller: 'oksnoks',
    viewModel: {
        type: 'oksnoks'
    },
    store: 'oksnoks',
    theme: 'yellow',

    series: [
        {
            type: 'pie',
            highlight: true,
            label: {
                field: 'desc',
                display: 'rotate',
                contrast: true,
                font: '12px Arial'
            },
            tooltip: {
                trackMouse: true,
                renderer: function(toolT, storeItem) {
                          toolT.setHtml(storeItem.data.desc + ': ' + storeItem.data.num);
              }
            },
            angleField: 'num',
            radiusField: 'desc',
            listeners: {
                itemclick: 'onPieItemClick'
            }
        }
    ],
    legend: {
        xtype: 'legend',
        docked: 'right'
    }

});