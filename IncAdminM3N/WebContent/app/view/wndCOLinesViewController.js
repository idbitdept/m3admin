/*
 * File: app/view/wndCOLinesViewController.js
 *
 * This file was generated by Sencha Architect version 4.2.8.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 7.2.x Classic library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 7.2.x Classic. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.wndCOLinesViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.wndcolines',

    onButtonClick: function(button, e, eOpts) {
        button.setText('Completing lines...');
        button.disable();

        var pnlCOLines = Ext.getCmp('pnlCOLines');
        var selectedLines = pnlCOLines.getSelectionModel().getSelection();
        var numSelectedLines = selectedLines.length;

        var sdata = new Array();

        for (var i = 0; i < numSelectedLines; i++) {
            sdata.push({
                orno: selectedLines[i].data.orno,
                orst: selectedLines[i].data.orst,
                ponr: selectedLines[i].data.ponr
            });
        }


        Ext.Ajax.setTimeout(1200000);

        Ext.Ajax.request({
            url: 'jsp/closeOrderLines.jsp',
            params: {
                sdata: Ext.encode(sdata)
            },
            success: function(response, opts) {
                var rdata = Ext.decode(response.responseText);

                Ext.Msg.show({
                    title: 'Orders Completed',
                    msg: 'The following Orders were marked completed:' + rdata.results,
                    icon: Ext.Msg.INFO,
                    buttons: Ext.Msg.OK
                });

                button.setText('Close Lines');
                button.enable();
            },
            failure: function(response, opts) {
                Ext.Msg.alert('Unable to Close Lines', 'Please select lines to complete');
                button.setText('Close Lines');
                button.enable();
            }
        });
    }

});
