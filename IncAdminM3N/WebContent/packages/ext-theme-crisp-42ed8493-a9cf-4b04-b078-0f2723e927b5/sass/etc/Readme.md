# ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass/etc"`, these files
need to be used explicitly.
