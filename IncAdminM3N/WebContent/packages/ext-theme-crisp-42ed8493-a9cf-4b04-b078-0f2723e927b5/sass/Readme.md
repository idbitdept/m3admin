# ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass

This folder contains SASS files of various kinds, organized in sub-folders:

    ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass/etc
    ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass/src
    ext-theme-crisp-42ed8493-a9cf-4b04-b078-0f2723e927b5/sass/var
