<%@ page contentType="text/html; charset=utf-8" language="java" import="java.text.SimpleDateFormat,java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="BetsyBean" class="com.ornua.MvxBean" scope="page"/>
<%
BetsyBean.setSystem(system);
BetsyBean.setPort(PORT);
BetsyBean.setLib(LIB);
BetsyBean.setCompany(zdcono);
BetsyBean.setUsername(m3user);
BetsyBean.setPassword(m3pass);

String uname = (String)session.getAttribute("uname"); if(uname==null) uname = "";
String results = "";
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
java.util.Date date = new java.util.Date();

String cono = request.getParameter("cono"); if(cono==null) cono = "";
String divis = request.getParameter("divi"); if(divis==null) divis = "";
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray apiArray = (JSONArray) obj;

Iterator<JSONObject> iterator = apiArray.iterator();
while (iterator.hasNext()) {
	
	//rcvd: selectedRecs[i].get('rcvd')
	 JSONObject apis = (JSONObject) iterator.next();
	 //String rgdt = (String) apis.get("rgdt"); String rgtm = (String) apis.get("rgtm"); rgtm = rgtm.replace(":", "");
	 
	 String minm = (String)apis.get("minm"); minm = minm.trim();
	 String trnm = (String)apis.get("trnm");
	 String rcvd = (String)apis.get("rcvd");
	 
	 String chid = (String)apis.get("chid"); if(chid==null) chid = "";
	 String rgdt = (String)apis.get("rgdt"); if(rgdt==null) rgdt = "";
	 String rgtm = (String)apis.get("rgtm"); if(rgtm==null) rgtm = ""; rgtm = rgtm.replace(":", "");
	 String tmsx = (String)apis.get("tmsx"); if(tmsx==null) tmsx = "";
	 
	 BetsyBean.setInitialise(minm); 
	 BetsyBean.setMessage(minm + " " + rcvd); 
	 String resp = BetsyBean.getMessage(); System.out.println("resp is " + resp);
	 
	KevBean.changeProg("ZOR003MI");
	
	String comment = ""; String ltrc = "";
	
	if(resp.startsWith("OK")) { 
		comment = "Fixed by " + uname;
		ltrc = "2";
	} else {
		comment = "Second NOK by " + uname + " at " + dateFormat.format(date);
		ltrc = "4";
	}
	if(comment.length()>30) comment = comment.substring(0,30);
	
	KevBean.setField("CONO", cono.trim());
	KevBean.setField("DIVI", divis.trim());
	KevBean.setField("MINM", minm.trim());
	KevBean.setField("TRNM", trnm.trim());
	KevBean.setField("CHID", chid.trim());
	KevBean.setField("RGDT", rgdt.trim());
	KevBean.setField("RGTM", rgtm.trim());
	KevBean.setField("TMSX", tmsx.trim()); 
	KevBean.setField("COMT", comment);
	KevBean.setField("LTRC", ltrc); // fixed
	
	//System.out.println(cono + " " + divis + " " + minm + " " + trnm + " ");
	//System.out.println(chid + " " + rgdt + " " + rgtm + " " + tmsx + " ");
	//System.out.println(comment + " " + ltrc);
	String zor = KevBean.runProgram("UpdStatus"); System.out.println("Zor " + zor);
	results+= "{\"ltrc\":\"" + ltrc + "\", \"comt\":\"" + comment + "\" },";
}	 
if(results.length()>2) results = results.substring(0, results.length() - 1);
out.println("{\"success\":true, \"results\":[" + results + "]}");
%>
