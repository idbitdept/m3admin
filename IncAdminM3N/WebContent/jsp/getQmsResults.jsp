<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="QMS302Bean" class="com.ornua.MvxBean" scope="page"/>
<%
QMS302Bean.setSystem(system);
QMS302Bean.setPort(PORT);
QMS302Bean.setLib(LIB);
QMS302Bean.setCompany(zdcono);
QMS302Bean.setUsername(m3user);
QMS302Bean.setPassword(m3pass);

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String qrid = ""; String spec = ""; String qse1 = "";

QMS302Bean.setInitialise("QMS302MI");
QMS302Bean.setField("FACI", zdfaci);
QMS302Bean.setField("ITNO", itno);
QMS302Bean.setField("BANO", bano);
QMS302Bean.runProgram("LstTestQIReq");
if(QMS302Bean.nextRow()) {
	qrid = QMS302Bean.getField("QRID");
	spec = QMS302Bean.getField("SPEC");
	qse1 = QMS302Bean.getField("QSE1");
}

	KevBean.setInitialise("QMS400MI");
	KevBean.setField("FACI", zdfaci);
	KevBean.setField("ITNO", itno);
	KevBean.setField("QRID", qrid);
	KevBean.setField("BANO", bano);
	KevBean.runProgram("LstTestResults");

	String json = "["; String thisBano = "";
	String thisTest = "";
	while(KevBean.nextRow()) {
		thisBano = KevBean.getField("BANO"); if(thisBano==null) thisBano = ""; thisBano = thisBano.trim();
		if(!thisBano.equals(bano)) break;
		
		String qtst = KevBean.getField("QTST");

		json+= "{";
		json+= "\"qtst\":\"" + qtst + "\",";
		json+= "\"tseq\":" + KevBean.getField("TSEQ") + ",";
		json+= "\"tsty\":" + KevBean.getField("TSTY") + ",";
		json+= "\"qse1\":\"" + KevBean.getField("QSE1") + "\",";
		
		json+= "\"qop1\":\"" + KevBean.getField("QOP1") + "\",";
		json+= "\"qtsp\":\"" + KevBean.getField("QTSP") + "\",";
		
		json+= "\"ttus\":\"" + KevBean.getField("TTUS") + "\",";
		json+= "\"ttdt\":\"" + KevBean.getField("TTDT") + "\",";
		json+= "\"ttte\":\"" + KevBean.getField("TTTE") + "\",";
		
		json+= "\"bano\":\"" + bano + "\",";
		json+= "\"itno\":\"" + itno + "\",";
		json+= "\"qrid\":\"" + qrid + "\",";	
		
		json+= "\"qtrs\":" + KevBean.getField("QTRS");
		json+= "},";
	}

	if(json.length()>2) json = json.substring(0, json.length()-1);
	json = json + "]";
	out.print(json); 
%>
