<%@ page contentType="text/html; charset=utf-8" language="java" import="java.math.BigDecimal,java.sql.*,org.json.simple.JSONObject" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%
int liveOrTest = 0;
String uricono = (String)session.getAttribute("uricono");
if(uricono.equals("PRD")) liveOrTest = 1;

String po = request.getParameter("po"); if(po==null) po = "";
String co = request.getParameter("co"); if(co==null) co = "";
String co_line = request.getParameter("co_line"); if(co_line==null) co_line = ""; 
int iLine = 0; try { iLine = Integer.parseInt(co_line); } catch (Exception e) { }

String co_um = request.getParameter("co_um"); if(co_um==null) co_um = "";

String pnli = "";
String itno = "";
String whlo = "";
String suno = "";

String pitd = ""; String pitt = ""; // po item desc 
String pupr = ""; String ppun = ""; // po price and um
String orqt = ""; String unms = ""; // order quantity and basic um
String puun = ""; String cfqa = ""; // PO U/M and confirmed quantity
String pucd = ""; // purchase price quantity
String prdt = ""; // manuf date
KevBean.setInitialise("PPS200MI");
KevBean.setField("CONO", zdcono);
KevBean.setField("PUNO", po);
KevBean.runProgram("LstLine");
if(KevBean.nextRow()) {
	pnli = KevBean.getField("PNLI");
	itno = KevBean.getField("ITNO"); if(itno==null) itno = "";
	whlo = KevBean.getField("WHLO");
	suno = KevBean.getField("SUNO");
	pitt = KevBean.getField("PITT");
	pitd = KevBean.getField("PITD");
	pupr = KevBean.getField("PUPR");
	ppun = KevBean.getField("PPUN");
	cfqa = KevBean.getField("CFQA");
	puun = KevBean.getField("PUUN"); if(puun==null) puun = ""; puun = puun.trim();
	orqt = KevBean.getField("ORQT");
	unms = KevBean.getField("UNMS");
	pucd = KevBean.getField("PUCD"); if(pucd==null) pucd = ""; //purchase price quantity
}

KevBean.setInitialise("MMS200MI");
KevBean.setField("CONO", zdcono);
KevBean.setField("ITNO", itno); String itmBasicUM = "";
if(KevBean.runProgram("GetItmBasic").startsWith("OK")) {
	itmBasicUM = KevBean.getField("UNMS"); 
	if(itmBasicUM==null) itmBasicUM = ""; itmBasicUM = itmBasicUM.trim();
	System.out.println("Basic um is " + itmBasicUM + " unms is " + unms);
}

String multiplier = "1"; String numDecs = "0"; double dMultiplier = 1.0; int iNumDecs = 0;
boolean matchUM = false; String alun, tx40 = "";

if(!itmBasicUM.equalsIgnoreCase("KG")) {
	KevBean.setInitialise("MMS200MI");
	KevBean.setField("CONO", zdcono);
	KevBean.setField("FITN", itno);
	KevBean.setField("TITN", itno);
	KevBean.setField("FAUT", "1");
	KevBean.setField("TAUT", "1");
	KevBean.runProgram("LstItmAltUnitMs");
	
	
	while(KevBean.nextRow()) {
		alun = (String)KevBean.getField("ALUN"); if(alun==null) alun = ""; alun = alun.trim();
		tx40 = (String)KevBean.getField("TX40"); if(tx40==null) tx40 = ""; tx40 = tx40.trim();
		
		System.out.println("alun and tx 40 " + alun + " " + tx40 + " puun " + puun);
		if( (alun.equals(puun)) || (tx40.equalsIgnoreCase(puun)) ) { // Bag = BAG
			System.out.println("we re in 1 "); matchUM = true;
			multiplier = (String)KevBean.getField("COFA"); System.out.println("CAll " + multiplier);
	
			//int iend = multiplier.indexOf(".");
			//if (iend != -1) 
			//	multiplier = multiplier.substring(0 , iend);
			
			dMultiplier = Double.parseDouble(multiplier);
			
			//iMultiplier = 25;
			
			numDecs = KevBean.getField("DCCD"); if(numDecs==null) numDecs = ""; if(numDecs.equals("")) numDecs = "1";
			iNumDecs = Integer.parseInt(numDecs);
		}
	}
}


if(!matchUM) { // if there's not an alternative for the PO UM (e.g. PO UM is KG, basic UM is KG, therefore NO alternate UM!)
	// if basic UM of the item in m3 is KG, then use the Dublin UM (Bag) to match an alternate...
	// if basic UM = PUUN..?
			System.out.println("no match go again");


		if(itmBasicUM.equalsIgnoreCase(unms)) {
			

			KevBean.setInitialise("MMS200MI");
			KevBean.setField("CONO", zdcono);
			KevBean.setField("FITN", itno);
			KevBean.setField("TITN", itno);
			KevBean.setField("FAUT", "1");
			KevBean.setField("TAUT", "1");
			KevBean.runProgram("LstItmAltUnitMs");
			while(KevBean.nextRow()) {
				alun = (String)KevBean.getField("ALUN"); if(alun==null) alun = ""; alun = alun.trim();
				tx40 = (String)KevBean.getField("TX40"); if(tx40==null) tx40 = ""; tx40 = tx40.trim();
				
				System.out.println("alun and tx 40 " + alun + " " + tx40 + " dub " + co_um);
				String dubRelate = ""; if(co_um.equalsIgnoreCase("BAG")) dubRelate = "EA"; if(co_um.equalsIgnoreCase("CTN")) dubRelate = "EA";
				if( (alun.equals(co_um)) || (tx40.equalsIgnoreCase(co_um)) || (alun.equalsIgnoreCase(dubRelate)) ) { // Bag = BAG
					System.out.println("we re in 2"); matchUM = true;
					multiplier = (String)KevBean.getField("COFA"); System.out.println("Call 2 " + multiplier);

					//int iend = multiplier.indexOf(".");
					//if (iend != -1) 
					//	multiplier = multiplier.substring(0 , iend);
					
					dMultiplier = Double.parseDouble(multiplier);
					
					numDecs = KevBean.getField("DCCD"); if(numDecs==null) numDecs = ""; if(numDecs.equals("")) numDecs = "1";
					iNumDecs = Integer.parseInt(numDecs);
				}
			}
		}
	}
    String json = "[";
    
	String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	String Gis05Uname = "GIS5263";
	String Gis05Pword = "GIS5263";
	String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=IDB_General";
	
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	double avgCawe = 0.0;

	try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);
		
		if(!itno.equals("")) {
			ps = conn.prepareStatement("update po_workbench set po_itno = ? where co = ? and liveOrTest = ?");
		    ps.setString(1, itno);
			ps.setString(2, po);
			ps.setInt(3, liveOrTest);
		    ps.executeUpdate();
		}
		
		ps = conn.prepareStatement("SELECT avg(pallet_cawe) as avg_pallet_cawe from po_workbench where co = ? and liveOrTest = ?");
	    ps.setString(1, po);
	    ps.setInt(2, liveOrTest);
	    rs = ps.executeQuery();
	    if(rs.next()) avgCawe = rs.getDouble("avg_pallet_cawe");
				

    	String COLIF, ECOLI, FAT, FDM, LIST, MOISTURE, MOULD, PH, SALMONELLA, SALT, STAPHS, YEAST = "";
    	
	    ps = conn.prepareStatement("SELECT * from po_workbench where co = ? and co_line = ? and liveOrTest = ?");
	    ps.setString(1, po);
	    ps.setInt(2, iLine);
	    ps.setInt(3, liveOrTest);
	    rs = ps.executeQuery();
	    while(rs.next()) {
	    	
	    	prdt = rs.getString("prdt"); if(prdt==null) prdt = "";
	    	
	    	COLIF = rs.getString("COLIF"); if(COLIF==null) COLIF = "";
	    	ECOLI = rs.getString("ECOLI"); if(ECOLI==null) ECOLI = "";
	    	FAT = rs.getString("FAT"); if(FAT==null) FAT = "";
	    	FDM = rs.getString("FDM"); if(FDM==null) FDM = "";
	    	LIST = rs.getString("LIST"); if(LIST==null) LIST = "";
	    	MOISTURE = rs.getString("MOISTURE"); if(MOISTURE==null) MOISTURE = "";
	    	MOULD = rs.getString("MOULD"); if(MOULD==null) MOULD = "";
	    	PH = rs.getString("PH"); if(PH==null) PH = "";
	    	SALMONELLA = rs.getString("SALMONELLA"); if(SALMONELLA==null) SALMONELLA = "";
	    	SALT = rs.getString("SALT"); if(SALT==null) SALT = "";
	    	STAPHS = rs.getString("STAPHS"); if(STAPHS==null) STAPHS = "";
	    	YEAST = rs.getString("YEAST"); if(YEAST==null) YEAST = "";
	    	
	    	json += "{";
	    	
	    	json += "\"qty\":" + rs.getInt("pallet_qty") + ",";
	    	
	    	//json += "\"qty_leek\":" + (rs.getInt("pallet_qty") * dMultiplier) + ",";
	    	
	    	
	    	BigDecimal bdQty = new BigDecimal(rs.getDouble("pallet_qty"));
	    	BigDecimal bdMultiplier = new BigDecimal(dMultiplier);
	    	BigDecimal bdQtyLeek = bdQty.multiply(bdMultiplier);
	    	bdQtyLeek = bdQtyLeek.setScale(3, BigDecimal.ROUND_HALF_DOWN);
	    	bdQtyLeek = bdQtyLeek.stripTrailingZeros();
	    	

	    	json += "\"qty_leek\":" + bdQtyLeek.toPlainString() + ",";
	    	
	    	
	    	json += "\"cawe\":" + rs.getDouble("pallet_cawe") + ",";
	    	json += "\"avg_cawe\":" + avgCawe + ",";
	    	
	    	json += "\"daycode\":\"" + rs.getString("daycode") + "\",";
	    	
	    	String hmark = rs.getString("healthmark"); if(hmark==null) hmark="";
    		if(hmark.startsWith("IRL-")) 
    			if(hmark.length()>=8)
    				hmark = "IE" + hmark.substring(4,8) + "EC";
    		if(hmark.startsWith("NI")) hmark = "UK" + hmark;
    		
	    	json += "\"healthmark\":\"" + hmark + "\",";
	    	
	    	json += "\"po_itno\":\"" + itno + "\",";
	    	
	    	json += "\"co_line\":" + iLine + ",";
	   
	    	json += "\"prdt\":\"" + prdt + "\",";
	    		
	    	json += "\"um\":\"" + rs.getString("co_um").trim() + "\",";
	    	
	    	json += "\"qty_um\":\"" + rs.getInt("pallet_qty") + " "  + rs.getString("co_um").trim() + "\",";
	    	json += "\"cawe_um\":\"" + rs.getDouble("pallet_cawe") + " MT\",";
	    	json += "\"avg_cawe_um\":\"" + avgCawe + " MT\",";
	    	
	    	json += "\"po\":\"" + po + "\",";
	    	json += "\"co\":\"" + rs.getString("po") + "\",";
	    	
	    	json += "\"puun\":\"" + puun + "\",";
	    	json += "\"cfqa\":\"" + cfqa + "\",";
	    	
	    	
	    	json += "\"po_qty\":\"" + cfqa + " " + puun + "\",";
	    	
	    	
	    	json += "\"resp\":\"" + " " + "\",";
	    	// fields from API
	    	json += "\"pnli\":" + pnli + ",";
	    	json += "\"itno\":\"" + itno + "\",";
	    	json += "\"whlo\":\"" + whlo + "\",";
	    	json += "\"suno\":\"" + suno + "\",";
	    	json += "\"item\":\"" + pitt + " " + " (" + itno + ")\",";
	    	json += "\"pitt\":\"" + pitt + "\",";
	    	json += "\"pitd\":\"" + pitd + "\",";
	    	
	    	json += "\"pupr\":\"" + pupr + "\",";
	    	json += "\"ppun\":\"" + ppun + "\",";
	    	json += "\"orqt\":" + orqt + ",";
	    	json += "\"unms\":\"" + unms + "\",";
	    	
	    	json += "\"multiplier\":" + dMultiplier + ",";
	    	json += "\"numDecs\":" + iNumDecs + ",";
	    	
	    	json += "\"orqt_um\":\"" + orqt + " " + unms + "\",";
	    	json += "\"price\":\"" + pupr + " per " + pucd + "/" + ppun + "\",";    
	    	
	    	
	    	// now attributes
	    	json += "\"COLIF\":\"" + COLIF + "\",";
	    	json += "\"ECOLI\":\"" + ECOLI + "\",";
	    	json += "\"FAT\":\"" + FAT + "\",";
	    	json += "\"FDM\":\"" + FDM + "\",";
	    	json += "\"LIST\":\"" + LIST + "\",";
	    	json += "\"MOISTURE\":\"" + MOISTURE + "\",";
	    	json += "\"MOULD\":\"" + MOULD + "\",";
	    	json += "\"PH\":\"" + PH + "\",";
	    	json += "\"SALMONELLA\":\"" + SALMONELLA + "\",";
	    	json += "\"SALT\":\"" + SALT + "\",";
	    	json += "\"STAPHS\":\"" + STAPHS + "\",";
	    	json += "\"YEAST\":\"" + YEAST + "\",";
	    	//
	    	
	    	json += "\"serial\":\"" + rs.getString("serial") + "\"";
	        	    		  	        	    		 
	    	json += "},";
	    }
	} catch (Exception e) {
		System.err.println("cakes " + e);
	}

   
          if(rs!=null) rs.close();  
          if(ps!=null) ps.close();  
          if(conn!=null) conn.close();  
          
          if(json.length()>2) json = json.substring(0, json.length() - 1);
          
          json = json + "]";
          
          out.print(json);

%>
