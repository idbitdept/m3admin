<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.HashMap, java.util.Hashtable, java.util.Map, javax.naming.Context, javax.naming.NamingEnumeration,javax.naming.NamingException, javax.naming.directory.Attribute, javax.naming.directory.Attributes, javax.naming.directory.SearchControls,javax.naming.directory.SearchResult,javax.naming.ldap.InitialLdapContext,javax.naming.ldap.LdapContext,javax.naming.directory.*" %>
<%@ include file="Connections/INCm3login.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%!
private static Map authenticate(String user, String pass, String prefix) {

	String ldapHost = ""; String searchBase = "";
	if(prefix.equalsIgnoreCase("IDB")) prefix = prefix.toUpperCase();
	if(prefix.equalsIgnoreCase("AFL")) prefix = prefix.toUpperCase();
	if(prefix.equalsIgnoreCase("ORNUA")) prefix = "Ornua";
	
	System.out.println("Username/pass/prefix " + user + " " + pass + " " + prefix);
	
	if(prefix.equals("IDB")) {
    //Dublin and some places beyond the Pale
		ldapHost = "ldap://domain.idb.ie";
		searchBase = "dc=idb,dc=ie";
//Leek – Foods & Ingredients
	} else if(prefix.equals("AFL")) {
		ldapHost = "ldap://domain.afl.local";
		searchBase = "dc=afl,dc=local";
//Viva L’Espana, Nantwich and soon to be Foods NA and Allemagne deux points
	} else if(prefix.equals("Ornua")) {
		ldapHost = "ldap://domain.ornua.corp";
		//ldapHost = "ldap://ornua.corp";
		//ldapHost = "ldap://DOMAIN.ORNUA.CORP";
		searchBase = "dc=ornua,dc=corp";
		//searchBase = "dc=ORNUA,dc=CORP";
//Ledbury
	} else if(prefix.equals("MCC")) {
		ldapHost = "ldap://domain.mcc.local";
		searchBase = "dc=mcc,dc=local";
//Foods NA – soon to be retired well earned
	} else if(prefix.equals("IDBUSA")) {
		ldapHost = "ldap://domain.idbusa.local";
		searchBase = "dc=idbusa,dc=local";
//Ingredients North America Hilbert
	} else if(prefix.equals("thielcheese")) {
		ldapHost = "ldap://domain.thielcheese.com";
		searchBase = "dc=thielcheese,dc=com";
//Ingredient North America Byron
	} else if(prefix.equals("Meadow")) {
		ldapHost = "ldap://domain.meadow.local";
		searchBase = "dc=meadow,dc=local";
//Allemagne deux points
	} else if(prefix.equals("idb-deutschland")) {
		ldapHost = "ldap://domain.idb-deutschland.de";
		searchBase = "dc=idb-deutschland,dc=de";
	}
	
    String domain = ldapHost.replace("ldap://domain.","");
    //domain = ldapHost.replace("ldap://DOMAIN.","");
    // todo or should it just be idb.ie - check other users?
    		
    System.out.println("Ldaphost is " + ldapHost + " searchBase is " + searchBase + " domain " + domain);
    String pager = "";
	
    String returnedAtts[] = {"sn", "givenName", "mail", "description", "memberOf", "pager"};
    String searchFilter = "(&(objectClass=user)(sAMAccountName=" + user + "))";

    //Create the search controls
    SearchControls searchCtls = new SearchControls();
    searchCtls.setReturningAttributes(returnedAtts);

    //Specify the search scope
    searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);

    Hashtable env = new Hashtable();
    env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    env.put(Context.PROVIDER_URL, ldapHost);
    env.put(Context.SECURITY_AUTHENTICATION, "simple");
    env.put(Context.SECURITY_PRINCIPAL, user + "@" + domain);
    env.put(Context.SECURITY_CREDENTIALS, pass);

   LdapContext ctxGC = null;
int totalResults = 0;
    try {
      ctxGC = new InitialLdapContext(env, null);
      //Search objects in GC using filters
      NamingEnumeration answer = ctxGC.search(searchBase, searchFilter, searchCtls);
      while (answer.hasMoreElements()) {
        SearchResult sr = (SearchResult) answer.next();
        Attributes attrs = sr.getAttributes();
        Map amap = null;
        if (attrs != null) {
          amap = new HashMap();
          NamingEnumeration ne = attrs.getAll();
          while (ne.hasMore()) {
            Attribute attr = (Attribute) ne.next();
            amap.put(attr.getID(), attr.get());
	          	for (NamingEnumeration e = attr.getAll();e.hasMore();totalResults++) {
				String grp = e.next().toString();

				/*
				m3 user is deualpo
exclsDEUALPO
prefix is idb-deutschland
Username/pass/prefix DEUALPO 2023B3rl!n idb-deutschland
				*/
			}
         }
          ne.close();
        }
          return amap;
      }
    }  catch (NamingException ex) {
	    System.out.print("Wrong name " + ex);
      ex.printStackTrace();
    }

    return null;
	}
%>


<%
String uname = request.getParameter("uname"); if(uname==null) uname = ""; uname = uname.trim(); //uname = uname.toUpperCase();
String pword = request.getParameter("pword"); if(pword==null) pword = ""; pword = pword.trim(); 

String actWeight = ""; String multLot = "";

String port = request.getParameter("port"); if(port==null) port = ""; port = port.trim();
String uricono = ""; String uridivi = ""; String urienv = ""; String usebano = "";
String uri = request.getParameter("uri");
if(uri==null) uri = "";
if(!uri.equals("")) {
	String[] bits = uri.split("&");
	for(int i=0;i<bits.length;i++) {
		String bit = bits[i];
		if(bit.startsWith("port")) port = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("m3user")) m3user = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("m3pass")) m3pass = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("system")) system = bit.substring(bit.lastIndexOf("=") + 1);
		
		if(bit.startsWith("cono")) uricono = bit.substring(bit.lastIndexOf("=") + 1); session.setAttribute("uricono", uricono);
		if(bit.startsWith("divi")) uridivi = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("env")) urienv = bit.substring(bit.lastIndexOf("=") + 1);
		
		if(bit.startsWith("usebano")) usebano = bit.substring(bit.lastIndexOf("=") + 1);
		IDBCDTA602_STRING = "jdbc:as400:" + system + "/" + LIB;
	}
}

session.setAttribute("usebano", usebano);

if(urienv==null) urienv = ""; urienv = urienv.toUpperCase(); urienv = urienv.trim();
uricono = uricono.toUpperCase(); uridivi = uridivi.toUpperCase();

if( (!uricono.equals("")) && (!uridivi.equals("")) ) {
	m3user = "M3MEC" + urienv;
}


boolean gotUser = false;

String prefix = "";
String afterPrefixUsername = "";

System.out.println("m3 user is " + uname);
if(uname.contains("\\")) { 
	String[] userBits = uname.split("\\\\");
	afterPrefixUsername = userBits[1];
	prefix = userBits[0];
} else {
	afterPrefixUsername = uname;
}
afterPrefixUsername = afterPrefixUsername.toUpperCase();

String excls = "";

if( (!uricono.equals("")) && (!uridivi.equals("")) && (!urienv.equals("")) ) {
	
	String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	String Gis05Uname = "GIS5263";
	String Gis05Pword = "GIS5263";
	String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=IDB_General";

	try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);
	    PreparedStatement ps = conn.prepareStatement("select * from PortalCompany where env = ? and cono = ? and divi = ?");
	    ps.setString(1, urienv);
	    ps.setString(2, uricono);
	    ps.setString(3, uridivi);
	    //System.out.println(urienv + " " + uricono + " " + uridivi);
	    ResultSet rs = ps.executeQuery();
	    if(rs.next()) {
	    	m3pass = rs.getString("m3pass");
	    	m3user = rs.getString("m3user");
	    	PORT = rs.getInt("port");
	    	system = rs.getString("system");
	    	ldapPrefix = rs.getString("ldapPrefix"); if(ldapPrefix==null) ldapPrefix = "";
	    	if(prefix.equals("")) prefix = ldapPrefix.trim();
	    	
	    	actWeight = rs.getString("actWeight"); if(actWeight==null) actWeight = "";
	    	multLot = rs.getString("multLot"); if(multLot==null) multLot = ""; 
	    	session.setAttribute("actWeight", actWeight);
	    	session.setAttribute("multLot", multLot);
	    	
	    	IDBCDTA602_STRING = "jdbc:as400:" + system + "/" + LIB;
	    	gotUser = true;
			// get exclusions	    	
	    	ps = conn.prepareStatement("select program, whlo from VizExcl where env = ? and cono = ? and divi = ? and usid = ? order by whlo");
		    ps.setString(1, urienv);
		    ps.setString(2, uricono);
		    ps.setString(3, uridivi);
		    ps.setString(4, afterPrefixUsername.toUpperCase().trim());
		    //System.out.println(urienv + " " + uricono + " " + uridivi);
		    ResultSet rsExcl = ps.executeQuery();
		    while(rsExcl.next()) {
		    	excls+= "'" + rsExcl.getString("program").trim() + "',";
		    }
		    if(excls.length()>2) excls = excls.substring(0, excls.length()-1);
		    if(excls.length()>2) excls = "(" + excls + ")";
		    System.out.println("excls" + excls + afterPrefixUsername);
			session.setAttribute("excls", excls);
			
			
	    }
	} catch (Exception e) {
		System.err.println("cakes" + e);
	}
	
}


if(gotUser) {
	
	m3user = m3user.trim(); m3pass = m3pass.trim(); urienv = urienv.trim();

session.setAttribute("usebano", usebano);
session.setAttribute("m3user", m3user);
session.setAttribute("m3pass", m3pass);

//System.out.println("setting m3user and pass as " + m3user + " " + m3pass);
session.setAttribute("system", system);
session.setAttribute("port", PORT);
session.setAttribute("env", urienv);


		
		KevBean.setSystem(system);
		KevBean.setPort(PORT);
		KevBean.setLib(LIB);
		KevBean.setCompany(uricono);
		KevBean.setUsername(m3user);
		KevBean.setPassword(m3pass);
		
		String conm = ""; String emal = ""; String dept = ""; String resp = "";
		
	if( system.startsWith("m3") || uricono.equals("902") || uricono.equals("802") || uricono.equals("800") ) { // use LDAP

				System.out.println("prefix is " + prefix);
				session.setAttribute("uname", afterPrefixUsername);
				//ldap them first..
				HashMap hash = (HashMap)authenticate(afterPrefixUsername, pword, prefix.trim());

	if(hash==null) {
		out.println("{\"success\":false,\"msg\":\"Invalid Username/Password. You may need to specify your domain prefix e.g. idb or afl in your username \"}");
} else {

	// also save the users own deets..
	
	session.setAttribute("localUname",  afterPrefixUsername);
	session.setAttribute("localPword", pword);
	session.setAttribute("localDom", prefix.trim());
	String usfn = (String)hash.get("sn") + " " + (String)hash.get("givenName"); if(usfn==null) usfn = "";
	session.putValue("email", (String)hash.get("mail"));
	session.putValue("description", (String)hash.get("description"));
	session.putValue("fname", (String)hash.get("sn"));
	session.putValue("sname", (String)hash.get("givenName"));
	session.putValue("longName", usfn + " (Ldap Auth)");
	session.putValue("pager", (String)hash.get("pager"));
	System.out.println("Pager session: " + (String)session.getAttribute("pager"));
	
	session.setAttribute("usfn",usfn);
				
	if(m3user.contains("\\")) { // in case username has a domain suffix
		m3user = m3user.substring(m3user.lastIndexOf("\\")+1);
	}

				KevBean.setInitialise("MNS150MI");
				KevBean.setField("USID", m3user.toUpperCase()); // now get m3 details
				
				System.out.println("m3user is " + m3user);
				resp = KevBean.runProgram("GetUserData"); 
				System.out.println("resp2 is " + resp);
				
				if(resp.startsWith("O")) {
					
					String tx40 = KevBean.getField("TX40");		session.setAttribute("tx40",tx40);	
					String apiname = KevBean.getField("NAME");		session.setAttribute("apiname",apiname);
					
					String zdcono = KevBean.getField("CONO"); 	session.setAttribute("zdcono", zdcono);
					String zddivi = KevBean.getField("DIVI"); 	session.setAttribute("zddivi", zddivi);
					String zdfaci = KevBean.getField("FACI"); 	session.setAttribute("zdfaci", zdfaci);
					
					String zdwhlo = KevBean.getField("WHLO"); 	session.setAttribute("zdwhlo",zdwhlo);
					String zdlanc = KevBean.getField("LANC"); 	session.setAttribute("zdlanc",zdlanc);
					String zddtfm = KevBean.getField("DTFM"); 	session.setAttribute("zddtfm",zddtfm);
					String tizo = KevBean.getField("TIZO");		session.setAttribute("tizo",tizo);		
				}

				out.println("{\"success\":true,\"conm\":\"" + conm + "\",\"emal\":\"" + emal + "\",\"dept\":\"" + dept + "\"}");
}
		
	} else if(system.equals("joyce.idb.ie")) {
		KevBean.setInitialise("MNS150MI");		
		KevBean.setField("USID", uname);
		
		System.out.println("Uname is " + uname);
		resp = KevBean.runProgram("GetUserData"); 
		System.out.println("Resp is " + resp);
		if(resp.startsWith("O")) {
			String frf6 = "";
			if( (uricono.equals("902")) || (uricono.equals("900")) ) { // 13.1 germany MNS150 User Def fields not available..
				//System.out.println("IN GERMANY");
				frf6 = pword;
			} else { 
				frf6 = KevBean.getField("FRF6"); 
			}
			
			if(frf6==null) frf6 = ""; frf6 = frf6.trim();
			if(frf6.equals("")) frf6 = KevBean.getField("TFNO");

			if( (frf6.trim().equalsIgnoreCase(pword.trim())) || (!pword.equals("")) ) {
				System.out.println("we re in x");
				conm = KevBean.getField("CONM"); emal = KevBean.getField("EMAL"); dept = KevBean.getField("DEPT");
				session.setAttribute("uname",uname);
				session.setAttribute("emal",emal);
				session.setAttribute("dept",dept);
				session.setAttribute("conm",conm);
				
				String usfn = KevBean.getField("NAME");	session.setAttribute("usfn",usfn);

				if(m3user.contains("\\")) { // in case username has a domain suffix
					m3user = m3user.substring(m3user.lastIndexOf("\\")+1);
				}
				//KevBean.setInitialise("GENERAL"); // try this...
				KevBean.setInitialise("MNS150MI");
				KevBean.setField("USID", m3user); // now get m3 details
				
				System.out.println("m3user is " + m3user);
				resp = KevBean.runProgram("GetUserData"); 
				System.out.println("resp2 is " + resp);
				
				if(resp.startsWith("O")) {
					
					String tx40 = KevBean.getField("TX40");		session.setAttribute("tx40",tx40);	
					String apiname = KevBean.getField("NAME");		session.setAttribute("apiname",apiname);
					
					String zdcono = KevBean.getField("CONO"); 	session.setAttribute("zdcono", zdcono);
					String zddivi = KevBean.getField("DIVI"); 	session.setAttribute("zddivi", zddivi);
					String zdfaci = KevBean.getField("FACI"); 	session.setAttribute("zdfaci", zdfaci);
					
					String zdwhlo = KevBean.getField("WHLO"); 	session.setAttribute("zdwhlo",zdwhlo);
					String zdlanc = KevBean.getField("LANC"); 	session.setAttribute("zdlanc",zdlanc);
					String zddtfm = KevBean.getField("DTFM"); 	session.setAttribute("zddtfm",zddtfm);
					String tizo = KevBean.getField("TIZO");		session.setAttribute("tizo",tizo);		
				}

				out.println("{\"success\":true,\"conm\":\"" + conm + "\",\"emal\":\"" + emal + "\",\"dept\":\"" + dept + "\"}");
					
			} else
				out.println("{\"success\":false,\"msg\":\"Password for user " + uname + " is incorrect\"}");
			
			
			
		} else
			out.println("{\"success\":false,\"msg\":\"" + resp + "\"}");	
		
	} else { // end if its joyce...
		KevBean.setInitialise("MVXDTAMI");		
		KevBean.setField("USID", uname);
		resp = KevBean.runProgram("GETCMNUSRA1"); 
		if(resp.startsWith("O")) {
			String fax = KevBean.getField("TFNO");
			
			if(pword.trim().equalsIgnoreCase(fax.trim())) {			
				session.setAttribute("zdcono", uricono);
				session.setAttribute("zddivi", uridivi);
				session.setAttribute("uname", uname); session.setAttribute("apiname",uname);
				
				String tx40 = KevBean.getField("TX40"); if(tx40==null) tx40 = ""; session.setAttribute("tx40",tx40); session.setAttribute("usfn",uname);				
				String whlo = KevBean.getField("WHLO"); if(whlo==null) whlo = "";	session.setAttribute("zdwhlo",whlo);
				String lanc = KevBean.getField("LANC"); 	session.setAttribute("zdlanc",lanc);
				String dtfn = KevBean.getField("DTFN"); 	session.setAttribute("zddtfm",dtfn);
				
				String tizo = KevBean.getField("TIZO");		session.setAttribute("tizo",tizo);
				
				out.println("{\"success\":true,\"conm\":\"" + KevBean.getField("USTP") + "\",\"emal\":\"" + tx40 + "\",\"dept\":\"" + KevBean.getField("DEPT") + "\"}");
			} else out.println("{\"success\":false,\"msg\":\"Password for user " + uname + " is incorrect\"}");
		} else out.println("{\"success\":false,\"msg\":\"User " + uname + " does not exist\"}");

		
	}
} else { 
	out.print("{\"success\":false,\"msg\":\"" + m3user + " (" + uricono + "/" + uridivi + ") not found - check web address - please contact ICT\"}");
} 
            
%>
