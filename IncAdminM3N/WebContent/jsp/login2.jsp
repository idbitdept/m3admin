<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3login.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%
String uname = request.getParameter("uname"); if(uname==null) uname = ""; uname = uname.trim(); uname = uname.toUpperCase();
String pword = request.getParameter("pword"); if(pword==null) pword = ""; pword = pword.trim(); 

String port = request.getParameter("port"); if(port==null) port = ""; port = port.trim();
String uricono = ""; String uridivi = ""; String urienv = ""; String usebano = "";
String uri = request.getParameter("uri");
if(uri==null) uri = "";
if(!uri.equals("")) {
	String[] bits = uri.split("&");
	for(int i=0;i<bits.length;i++) {
		String bit = bits[i];
		if(bit.startsWith("port")) port = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("m3user")) m3user = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("m3pass")) m3pass = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("system")) system = bit.substring(bit.lastIndexOf("=") + 1);
		
		if(bit.startsWith("cono")) uricono = bit.substring(bit.lastIndexOf("=") + 1); session.setAttribute("uricono", uricono);
		if(bit.startsWith("divi")) uridivi = bit.substring(bit.lastIndexOf("=") + 1);
		if(bit.startsWith("env")) urienv = bit.substring(bit.lastIndexOf("=") + 1);
		
		if(bit.startsWith("usebano")) usebano = bit.substring(bit.lastIndexOf("=") + 1);
		IDBCDTA602_STRING = "jdbc:as400:" + system + "/" + LIB;
	}
}

session.setAttribute("usebano", usebano);

if(urienv==null) urienv = ""; urienv = urienv.toUpperCase(); urienv = urienv.trim();
uricono = uricono.toUpperCase(); uridivi = uridivi.toUpperCase();

if( (!uricono.equals("")) && (!uridivi.equals("")) ) {
	m3user = "M3MEC" + urienv;
}

// todo - if env, cono and divi are set, get the values from the gis5 mssql table

boolean gotUser = false;


if( (!uricono.equals("")) && (!uridivi.equals("")) && (!urienv.equals("")) ) {
	
	String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	String Gis05Uname = "GIS5263";
	String Gis05Pword = "GIS5263";
	String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=IDB_General";

	try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);
	    PreparedStatement ps = conn.prepareStatement("select * from PortalCompany where env = ? and cono = ? and divi = ?");
	    ps.setString(1, urienv);
	    ps.setString(2, uricono);
	    ps.setString(3, uridivi);
	    //System.out.println(urienv + " " + uricono + " " + uridivi);
	    ResultSet rs = ps.executeQuery();
	    if(rs.next()) {
	    	m3pass = rs.getString("m3pass");
	    	m3user = rs.getString("m3user");
	    	PORT = rs.getInt("port");
	    	system = rs.getString("system");
	    	ldapPrefix = rs.getString("ldapPrefix"); if(ldapPrefix==null) ldapPrefix = "";
	    	IDBCDTA602_STRING = "jdbc:as400:" + system + "/" + LIB;
	    	gotUser = true;
	    }
	} catch (Exception e) {
		System.err.println("cakes" + e);
	}
	
}


if(gotUser) {
	
	m3user = m3user.trim(); m3pass = m3pass.trim(); urienv = urienv.trim();

session.setAttribute("usebano", usebano);
session.setAttribute("m3user", m3user);
session.setAttribute("m3pass", m3pass);

//System.out.println("setting m3user and pass as " + m3user + " " + m3pass);
session.setAttribute("system", system);
session.setAttribute("port", PORT);
session.setAttribute("env", urienv);


		
		KevBean.setSystem(system);
		KevBean.setPort(PORT);
		KevBean.setLib(LIB);
		KevBean.setCompany(uricono);
		KevBean.setUsername(m3user);
		KevBean.setPassword(m3pass);
		
		
		// prob can get rid of this...?
		/*
		KevBean.setInitialise("GENERAL");
		KevBean.setField("USID", m3user);
		String getuserinfo = KevBean.runProgram("GetUserInfo");
		if (getuserinfo.startsWith("O")) {
		}
		*/
		String conm = ""; String emal = ""; String dept = ""; String resp = "";
	//if( (system.equals("joyce.idb.ie")) || (system.equals("m3dev.ornua.com")) ) {
	if( (system.equals("joyce.idb.ie")) || (system.startsWith("m3")) ) {
		KevBean.setInitialise("MNS150MI");		
		KevBean.setField("USID", uname);
		
		System.out.println("Uname is " + uname);
		resp = KevBean.runProgram("GetUserData"); 
		System.out.println("Resp is " + resp);
		if(resp.startsWith("O")) {
			String frf6 = "";
			if( (uricono.equals("902")) || (uricono.equals("900")) ) { // 13.1 germany MNS150 User Def fields not available..
				//System.out.println("IN GERMANY");
				frf6 = pword;
			} else { 
				frf6 = KevBean.getField("FRF6"); 
			}
			
			if(frf6==null) frf6 = ""; frf6 = frf6.trim();
			if(frf6.equals("")) frf6 = KevBean.getField("TFNO");

			if( (frf6.trim().equalsIgnoreCase(pword.trim())) || (!pword.equals("")) ) {
				//System.out.println("we re in");
				conm = KevBean.getField("CONM"); emal = KevBean.getField("EMAL"); dept = KevBean.getField("DEPT");
				session.setAttribute("uname",uname);
				session.setAttribute("emal",emal);
				session.setAttribute("dept",dept);
				session.setAttribute("conm",conm);
				
				String usfn = KevBean.getField("NAME");	session.setAttribute("usfn",usfn);

				if(m3user.contains("\\")) { // in case username has a domain suffix
					m3user = m3user.substring(m3user.lastIndexOf("\\")+1);
				}
				//KevBean.setInitialise("GENERAL"); // try this...
				KevBean.setInitialise("MNS150MI");
				KevBean.setField("USID", m3user); // now get m3 details
				
				System.out.println("m3user is " + m3user);
				resp = KevBean.runProgram("GetUserData"); 
				System.out.println("resp2 is " + resp);
				
				if(resp.startsWith("O")) {
					
					String tx40 = KevBean.getField("TX40");		session.setAttribute("tx40",tx40);	
					String apiname = KevBean.getField("NAME");		session.setAttribute("apiname",apiname);
					
					String zdcono = KevBean.getField("CONO"); 	session.setAttribute("zdcono", zdcono);
					String zddivi = KevBean.getField("DIVI"); 	session.setAttribute("zddivi", zddivi);
					String zdfaci = KevBean.getField("FACI"); 	session.setAttribute("zdfaci", zdfaci);
					
					String zdwhlo = KevBean.getField("WHLO"); 	session.setAttribute("zdwhlo",zdwhlo);
					String zdlanc = KevBean.getField("LANC"); 	session.setAttribute("zdlanc",zdlanc);
					String zddtfm = KevBean.getField("DTFM"); 	session.setAttribute("zddtfm",zddtfm);
					String tizo = KevBean.getField("TIZO");		session.setAttribute("tizo",tizo);
			
				}
				
				


				
				out.println("{\"success\":true,\"conm\":\"" + conm + "\",\"emal\":\"" + emal + "\",\"dept\":\"" + dept + "\"}");
					
			} else
				out.println("{\"success\":false,\"customErrorMessage\":\"Password for user " + uname + " is incorrect\"}");
			
			
			
		} else
			out.println("{\"success\":false,\"msg\":\"" + resp + "\"}");	
		
	} else { // end if its joyce...
		KevBean.setInitialise("MVXDTAMI");		
		KevBean.setField("USID", uname);
		resp = KevBean.runProgram("GETCMNUSRA1"); 
		if(resp.startsWith("O")) {
			String fax = KevBean.getField("TFNO");
			
			if(pword.trim().equalsIgnoreCase(fax.trim())) {			
				session.setAttribute("zdcono", uricono);
				session.setAttribute("zddivi", uridivi);
				session.setAttribute("uname", uname); session.setAttribute("apiname",uname);
				
				String tx40 = KevBean.getField("TX40"); if(tx40==null) tx40 = ""; session.setAttribute("tx40",tx40); session.setAttribute("usfn",uname);				
				String whlo = KevBean.getField("WHLO"); if(whlo==null) whlo = "";	session.setAttribute("zdwhlo",whlo);
				String lanc = KevBean.getField("LANC"); 	session.setAttribute("zdlanc",lanc);
				String dtfn = KevBean.getField("DTFN"); 	session.setAttribute("zddtfm",dtfn);
				
				String tizo = KevBean.getField("TIZO");		session.setAttribute("tizo",tizo);
				
				out.println("{\"success\":true,\"conm\":\"" + KevBean.getField("USTP") + "\",\"emal\":\"" + tx40 + "\",\"dept\":\"" + KevBean.getField("DEPT") + "\"}");
			} else out.println("{\"success\":false,\"customErrorMessage\":\"Password for user " + uname + " is incorrect\"}");
		} else out.println("{\"success\":false,\"customErrorMessage\":\"User " + uname + " does not exist\"}");

		
	}
} else { 
	out.print("{\"success\":false,\"customErrorMessage\":\"" + m3user + " (" + uricono + "/" + uridivi + ") not found - check web address - please contact ICT\"}");
} 
            
%>
