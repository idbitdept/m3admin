<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS422MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS410MIBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "MHS850MI";
String localTrans = "AddCOPick";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
LocalBean.setInitialise(localProg);
//make sure the users has the same cono/divi/faci settings as the API account

String uname = (String)session.getAttribute("uname");
String orno = request.getParameter("orno"); if(orno==null) orno = "";

long orsl = 0; long ponr = 0;

MWS422MIBean.setSystem(system);
MWS422MIBean.setPort(PORT);
MWS422MIBean.setLib(LIB);
MWS422MIBean.setCompany(zdcono);
MWS422MIBean.setUsername(m3user);
MWS422MIBean.setPassword(m3pass);

KevBean.setInitialise("MMS850MI");
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray ordersArray = (JSONArray) obj;
String publishResults = "<ul>Results of Order Line Completions";

Iterator<JSONObject> iterator = ordersArray.iterator();
while (iterator.hasNext()) {
	 JSONObject orders = (JSONObject) iterator.next();
	 orno = (String) orders.get("orno");
	 //orsl = (Long) orders.get("orsl");
	 ponr = (Long)orders.get("ponr");

	KevBean.setInitialise("MWS411MI");
	KevBean.setField("RORC", "3");
	KevBean.setField("RIDN", orno);
	KevBean.runProgram("LstDelLnByOrd"); String delNo = ""; String[] delnos = new String[20]; int i = 0;
	while(KevBean.nextRow()) {
		String thisDelNo = (String)KevBean.getField("DLIX");
		if(!delNo.equals(thisDelNo)) delnos[i++] = thisDelNo;
		delNo = thisDelNo;
	}
	for(int v=0;v<delnos.length;v++) {
		if(delnos[v]==null) break; else {
			
			MWS422MIBean.setInitialise("MWS422MI");
			MWS422MIBean.setField("DLIX", delnos[v]);
			System.out.println("List pick " + MWS422MIBean.runProgram("LstPickDetail"));
			while(MWS422MIBean.nextRow()) {
				String ordLine = MWS422MIBean.getField("RIDL");
				//System.out.println("Order line " + ordLine + " ponr " + ponr);
				if(ordLine.trim().equals(ponr+"")) {
					
					LocalBean.setField("PRFL", "*EXE");
					LocalBean.setField("CONO", zdcono);
					LocalBean.setField("WHLO", MWS422MIBean.getField("WHLO"));
					LocalBean.setField("ITNO", MWS422MIBean.getField("ITNO"));
					
					LocalBean.setField("E0PA", "5013546007355");
					LocalBean.setField("E065", "31");
	
					LocalBean.setField("CUNO", MWS422MIBean.getField("CUNO"));
					LocalBean.setField("QTYP", "0");
					LocalBean.setField("QTYO", MWS422MIBean.getField("ALQT"));
					
					LocalBean.setField("RIDN", MWS422MIBean.getField("RIDN"));
					
					LocalBean.setField("RIDL", ordLine);
					LocalBean.setField("RIDI", delnos[v]);
					LocalBean.setField("PLSX", MWS422MIBean.getField("PLSX"));
					LocalBean.setField("OEND", "1");
					
					LocalBean.setField("PACT", "PALLET");
					LocalBean.setField("ISMD", "0");
					String resp = LocalBean.runProgram(localTrans);
					if(resp.startsWith("NOK            Not allowed")) {
						 //keepGoing = false;
						 resp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
						 break;
					 }
					publishResults = publishResults + "<li> Order " + orno + "/" + ordLine + ": " + resp + "</li>"; 
				}
			}
		}
	}	
}
LocalBean.closeConnection();
MWS422MIBean.closeConnection();
MWS410MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

%>
	