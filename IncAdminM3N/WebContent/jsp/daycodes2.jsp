<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

KevBean.setInitialise("MMS060MI");
KevBean.setField("MRCD", "5000");
KevBean.runProgram("SetLstMaxRec");
KevBean.setField("CONO", zdcono);
KevBean.setField("WHLO", whlo);
KevBean.setField("ITNO", itno);
KevBean.runProgram("List");

String json = "["; String bref = ""; String bre2 = ""; String julday = "";
String healthmark = ""; int cnt = 0;

	while(KevBean.nextRow()) {
		bref = KevBean.getField("BREF"); if(bref==null) bref = ""; bref = bref.trim();
		bre2 = KevBean.getField("BRE2"); if(bre2==null) bre2 = ""; bre2 = bre2.trim();
		if(bref.length()==7) { // its a valid daycode

			if( (!julday.equals(bref.substring(0,4)) || (!bre2.equals(healthmark)) ) ) {	
				json+= "{";
				json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
				json+= "\"bref\":\"" + bref.substring(0,4) + "\",";
				json+= "\"bre2\":\"" + bre2 + "\"";
				json+= "},";
				cnt++;
			}
			julday = bref.substring(0,4);
			healthmark = bre2;
		}
	}
	if(cnt>0) {
		json+= "{";
		json+= "\"whsl\":\"All\",";
		json+= "\"bref\":\"All\",";
		json+= "\"bre2\":\"All\"";
		json+= "},";
	}
		


if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
