<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.text.SimpleDateFormat" %>
<%@ include file="Connections/INCm3.jsp" %>

<%
String excls = (String)session.getAttribute("excls"); if(excls==null) excls = "";
String where = "";
if(!excls.equals("")) where += " and XMMINM not in " + excls;
String query = "";
if(system.startsWith("m3")) query = " select XMWHLO, count(*) as num from CUSJDTA.ZMILOG " + 
		" where XMCONO = ? and XMDIVI = ? and XMLTRC = ? " + 
		" and XMWHLO >= '001' and XMWHLO <= 'ZZZ'" + where + 
		" group by XMWHLO " +
		" order by XMWHLO ";
else query = " select XMWHLO, count(*) as num from " + libby + ".zmilog " + 
" where xmcono = ? and xmdivi = ? and xmltrc = ? " + 
" group by XMWHLO " +
" order by XMWHLO ";

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);
PreparedStatement ps = conn.prepareStatement(query);

ps.setInt(1, icono);
ps.setString(2, divi);
ps.setInt(3, 1); // only show NOKs
ResultSet rs = ps.executeQuery();

    String json = "["; String whlo = "";
 
          while (rs.next()) {
        	  whlo = rs.getString("XMWHLO"); if(whlo==null) whlo = ""; whlo = whlo.trim(); if(whlo.equals("")) whlo = "Not given";
			json+= "{";
			json+= "\"whlo\":\"" + whlo + "\",\"num\":" + rs.getInt("num");
			json+="},"; 
          }

          if(rs!=null) rs.close();  
          if(ps!=null) ps.close();  
          if(conn!=null) conn.close();  
          
			if(json.length()>2) json = json.substring(0, json.length() - 1);
          json = json + "]";        
          out.print(json);

%>
