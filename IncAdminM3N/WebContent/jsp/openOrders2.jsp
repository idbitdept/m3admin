<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String cuno = request.getParameter("cuno"); if(cuno==null) cuno = "";
KevBean.setInitialise("OIS100MI");
KevBean.setField("MRCD", "500");
KevBean.runProgram("SetLstMaxRec");

KevBean.setField("CONO", zdcono);
KevBean.setField("CUNO", cuno); 
KevBean.setField("ORSL", "44");
KevBean.setField("ORST", "66");
KevBean.runProgram("LstHead");

String json = "[";
while(KevBean.nextRow()) {
	String status = KevBean.getField("STAT"); int iStatus = Integer.parseInt(status);
	String lowStatus = KevBean.getField("ORSL"); int iLowStatus = Integer.parseInt(lowStatus);
	String highStatus = KevBean.getField("ORST"); int iHighStatus = Integer.parseInt(highStatus);
	
	if( (iHighStatus>44) && (iLowStatus<66)  ) {
		json+= "{";
		json+= "\"orno\":\"" + KevBean.getField("ORNO") + "\",";
		json+= "\"ortp\":\"" + KevBean.getField("ORTP") + "\",";
		json+= "\"rldt\":" + KevBean.getField("RLDT") + ",";
		json+= "\"stat\":" + status + ",";
		json+= "\"cuor\":\"" + KevBean.getField("CUOR") + "\",";
		json+= "\"yref\":\"" + KevBean.getField("YREF") + "\",";
		
		json+= "\"orsl\":" + lowStatus + ",";
		json+= "\"orst\":" + highStatus + ",";
		json+= "\"dlix\":" + KevBean.getField("DLIX") + "";
		json+= "},";
	}
}

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
