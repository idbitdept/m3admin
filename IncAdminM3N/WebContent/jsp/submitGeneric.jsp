<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String maxRec = request.getParameter("maxRecs"); if(maxRec==null) maxRec = "";
System.out.println("MaxRecs is " + maxRec);

String json = "{\"fields\": [{ ";


String minm = request.getParameter("minm");
String trnm = request.getParameter("trnm");

KevBean.setInitialise("MRS001MI");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "O"); 

KevBean.runProgram("LstFields");

String[] fieldNames = new String[200]; int i = 0; int t = 0;
while(KevBean.nextRow()) {
	String flnm = KevBean.getField("FLNM");
	fieldNames[i++] = flnm;
	json+= "\"name\":\"" + flnm + "\",";
}
if(json.length()>1) json = json.substring(0, json.length()-1);
json += "}],";

KevBean.setInitialise(minm);
if(!maxRec.equals("")) {
	KevBean.setField("MRCD", maxRec);
	KevBean.runProgram("SetLstMaxRec");
}

Enumeration paramNames = request.getParameterNames();

while(paramNames.hasMoreElements()) {
   String paramName = (String)paramNames.nextElement();
   if(Character.isUpperCase(paramName.charAt(0))) {
	   KevBean.setField(paramName, request.getParameter(paramName));
   }
}

String resp = KevBean.runProgram(trnm); if(resp==null) resp = "";

json+= "\"data\": [ "; boolean isRecs = false;

if(resp.startsWith("NOK")) {
	json = "{\"success\":false,\"msg\":\"" + resp + "\"}";
} else { 
	
	// would do the excel export here...
	boolean openBrack = false;
	while(KevBean.nextRow()) {
		//System.out.println("Next row..");
		isRecs = true; json+= "{"; openBrack = true;
		for(t=0;t<i;t++)
			json+= "\"" + fieldNames[t] + "\":\"" + KevBean.getField(fieldNames[t]) + "\",";
		if(json.length()>1) json = json.substring(0, json.length()-1);
		json+= "},";
	} 
	if(!isRecs) {
		//System.out.println("is rics not");
	
		json+= "{ "; openBrack = true;
		for(t=0;t<i;t++)
			json+= "\"" + fieldNames[t] + "\":\"" + KevBean.getField(fieldNames[t]) + "\",";
		if(json.length()>1) json = json.substring(0, json.length()-1);
		json+= "},";
	}
	if(!openBrack) json+= "{";
	if(json.length()>1) json = json.substring(0, json.length()-1);
	json+="],\"success\":true}";
}
out.println(json);
%>
