<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>

<%
// use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "ATS101MI";
String localTrans = "SetAttrValue";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setPassword(localPword); 

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String aloc = request.getParameter("aloc"); if(aloc==null) aloc = "";
String stas = request.getParameter("stas"); if(stas==null) stas = "";
String uname = (String)session.getAttribute("uname");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>Attribute results";

int lotNum = 0; String resp = ""; boolean keepGoing = true;
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject camus = (JSONObject) iterator.next();
	 camu = (String) camus.get("camu");
	 stas = (String) camus.get("stas");
	 bano = (String) camus.get("bano");
	 whsl = (String) camus.get("whsl");
	 aloc = (String) camus.get("aloc");
	 try {
		 itno = (String) camus.get("itno");
	 } catch (Exception e) {
		 // use header itno
	 }
	 
	 // get the atnr for the pallet
	 KevBean.setInitialise("MDBREADMI");
	 KevBean.setField("MRCD", "1"); // we only need the first record
	 KevBean.runProgram("SetLstMaxRec");
	 KevBean.setField("CAMU", camu);
	 KevBean.runProgram("LstMITLOCZ7");

	 if(!KevBean.nextRow()) {
		 publishResults += "<li> Pallet " + camu + ": No Attribute number found</li>";
	 } else {
		 String atnr = KevBean.getField("ATNR");
		 
		 Enumeration<String> parameterNames = request.getParameterNames();
		 while (parameterNames.hasMoreElements()) {
			 if(!keepGoing) break;
		     String paramName = parameterNames.nextElement();
		     if( (!paramName.equals("sdata")) && (!paramName.equals("whlo")) && (!paramName.equals("itno")) ) {
			     String[] paramValues = request.getParameterValues(paramName);
		         String paramValue = paramValues[0]; paramValue = paramValue.trim();
		         if(!paramValue.equals("")) {
		        	 LocalBean.setInitialise(localProg);
		    		 LocalBean.setField("CONO", zdcono);
		    		 LocalBean.setField("ATNR", atnr);
			         LocalBean.setField("ATID", paramName);
			         LocalBean.setField("ATVA", paramValue);
					 String attrResp = LocalBean.runProgram("SetAttrValue");
					 if(attrResp.startsWith("NOK            Not allowed")) {
						 keepGoing = false;
						 attrResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
						 publishResults += "<li> Pallet " + camu + ": Attribute: " + paramName + ": " + attrResp + "</li>";
						 break;
					 }
					 if(attrResp.endsWith("is invalid")) attrResp+=" (Attribute may be held at Lot - not pallet - level)";
					 publishResults += "<li> Pallet " + camu + ": Attribute: " + paramName + ": " + attrResp + "</li>";
					 System.out.println("Pub results now " + publishResults);
		         }
		     }
		 }
 	}
	lotNum++;
}

publishResults = publishResults + "</ul>";
//System.out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
%>