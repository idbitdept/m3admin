<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,java.net.*,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<%@ include file="Connections/connections.jsp" %>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MDBReadBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="DubBean" class="com.ornua.MvxBean" scope="page"/>
<%
MDBReadBean.setSystem(system);
MDBReadBean.setPort(PORT);
MDBReadBean.setLib(LIB);
MDBReadBean.setCompany(zdcono);
MDBReadBean.setUsername(m3user);
MDBReadBean.setPassword(m3pass);
MDBReadBean.setInitialise("MDBReadMI");
MDBReadBean.setField("MRCD", "2");
MDBReadBean.runProgram("SetLstMaxRec");

DubBean.setSystem("beckett.idb.ie");
DubBean.setPort(MM_PORT);
DubBean.setLib(lib);
DubBean.setCompany(MM_CONO);
DubBean.setUsername(MY_USERNAME);
DubBean.setPassword(MY_PASSWORD);
DubBean.setInitialise("MHS850MI");
//use logged-on user's account
//String localUname = (String)session.getAttribute("localUname");
String localUname = (String)session.getAttribute("pager");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

System.out.println(localUname  + " " + localPword + " " + localDom);
System.out.println(localDom + "\\" + localUname);

String localProg = "PPS001MI";
String localTrans = "Receipt";

System.out.println(system + " " + PORT);

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
//LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setUsername(localUname); // doesnt seem to want domain... 
System.out.println("Local uname is " + localUname);
LocalBean.setPassword(localPword); 

/*
// use local or api account to do mdbread?
MDBReadBean.setSystem(system);
MDBReadBean.setPort(PORT);
MDBReadBean.setLib(LIB);
MDBReadBean.setCompany(zdcono);
MDBReadBean.setUsername(localUname);
MDBReadBean.setPassword(localPword);
*/

int liveOrTest = 0;
if(uricono.equals("PRD")) liveOrTest = 1;

//LocalBean.changeProg("PPS001MI");
LocalBean.setInitialise(localProg);
String json = "[";

String po = request.getParameter("po"); if(po==null) po = "";
String pnli = request.getParameter("pnli"); if(pnli==null) pnli = "";
String zcnc = request.getParameter("zcnc"); if(zcnc==null) zcnc = "";
String doNum = request.getParameter("doNum"); if(doNum==null) doNum = "";
String doLine = request.getParameter("doLine"); if(doLine==null) doLine = "";
String dlix = request.getParameter("dlix"); if(dlix==null) dlix	 = "";
String trsh = request.getParameter("trsh"); if(trsh==null) trsh = ""; trsh = trsh.trim();
String fromWhs = request.getParameter("fromWhs"); if(fromWhs==null) fromWhs = ""; fromWhs = fromWhs.trim();
String poItno = request.getParameter("poItno"); if(poItno==null) poItno = ""; poItno = poItno.trim();
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";

String hdrBrefTxt = request.getParameter("hdrBrefTxt"); if(hdrBrefTxt==null) hdrBrefTxt = "";
hdrBrefTxt = hdrBrefTxt.trim();
if(hdrBrefTxt.length()<9) hdrBrefTxt = " " + hdrBrefTxt;
// if trsh 66 (69?) update dublin DOReceipt...

boolean chkCompletePO = Boolean.parseBoolean(request.getParameter("chkCompletePO"));
int totValPallets = Integer.parseInt(request.getParameter("totValPallets"));

boolean allOKs = true; String closeMsg = "";

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);
//System.out.println("Today is " + today);
String bano = ""; 
String mdbBano = ""; String mdbCamu = ""; String currPrdt = "";
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray pallets = (JSONArray) obj;

int i = 0; int actReceipts = 0;
Iterator<JSONObject> iterator = pallets.iterator();
while (iterator.hasNext()) {
	 JSONObject pallet = (JSONObject) iterator.next();
	 
	 long qtyDE = 0; long qtyIE = 0;
	 try{
	 	qtyDE = (Long)pallet.get("qtyDE");
	 } catch (Exception e) {
		 System.err.println("betss" +e);
	 }
	 try {
	 	qtyIE = (Long)pallet.get("qtyIE");
	 } catch (Exception e) {
		 System.err.println("betss" +e); 
	 }
	 
	 //System.out.println("i is " + i + " qty: " + qty); 
	 i++;
	 if(qtyDE>0) {
		 actReceipts++;
	 
	 String prdt = (String)pallet.get("prdt"); if(prdt==null) prdt = ""; prdt = prdt.trim();
	 String bre2 = (String)pallet.get("healthmark"); if(bre2==null) bre2 = ""; bre2 = bre2.trim();
	 String bref = (String)pallet.get("daycode"); if(bref==null) bref = ""; bref = bref.trim();
	 String itno = (String)pallet.get("itno"); if(itno==null) itno = "";
	 String brem = (String)pallet.get("brem"); if(brem==null) brem = "";
	 String brefTxt = (String)pallet.get("brefTxt"); if(brefTxt==null) brefTxt = "";
	 if(brefTxt.equals("")) brefTxt = hdrBrefTxt;
	 
		 LocalBean.setField("CONO", uricono); //System.out.println("FR cono" + uricono);
		 LocalBean.setField("TRDT", today.trim()); //System.out.println("FR today"  + today);
		 //LocalBean.setField("RESP", "IDBKEWO"); //hmm
		 LocalBean.setField("RESP", localUname.toUpperCase()); //hmm
		 //LocalBean.setField("RESP", (String)session.getAttribute("uname"));
		 LocalBean.setField("PUNO", po.trim()); //System.out.println("FR po " + po);
		 LocalBean.setField("PNLI", pnli.trim()); System.out.println("DE: " + po + "/" + pnli);
		 LocalBean.setField("WHSL", whsl); //System.out.println("FR pnli" + pnli);
		 //LocalBean.setField("PNLS", "0");
		 LocalBean.setField("RVQA", qtyDE + "");
		 //LocalBean.setField("WHLO", "IDB"); // get from order line
		 LocalBean.setField("SUDO", doNum); 
		 LocalBean.setField("PRDT", prdt.trim()); //System.out.println("prdt"  + prdt.trim());
		 LocalBean.setField("BREF", bref); //LocalBean.setField("BREF", brefTxt);
		 //LocalBean.setField("CAMU", bref); // let m3 generate CAMU
		 String bre2prdt = bre2.trim() + "/" + prdt.trim();
		 LocalBean.setField("BRE2", bre2prdt); // alex POLENZ add /yyyyMMdd manuf date
		 LocalBean.setField("BREM", brem + brefTxt); //defaults to ZCNC on Line page, can be overridden // changed to container + temperature
		 
		 System.out.println("ZCNC" + zcnc);
		 // generate unique lot by manufacture date..
		 
		 if(prdt.equals(currPrdt)) LocalBean.setField("BANO", mdbBano);
		 else LocalBean.setField("BANO", "");
		 currPrdt = prdt.trim();

		 
		 /*
		 if( (chkCompletePO) && (actReceipts==totValPallets) && (allOKs) ) {
			 LocalBean.setField("OEND", "1");
			 closeMsg = " PO now marked complete";
		 }
		 */
	
		 String msg = LocalBean.runProgram(localTrans);		 
		 System.out.println("message is " + msg);
		 
		 boolean ok = false; 
		 if(msg.startsWith("OK")) {
			 ok = true;
			 
			 // get lot and pallet (bano, camu)
			 int matches = 0;
			 mdbBano = ""; mdbCamu = "";

			 MDBReadBean.setInitialise("MDBReadMI");
			 MDBReadBean.setField("MRCD", "2");
			 MDBReadBean.runProgram("SetLstMaxRec");
			 MDBReadBean.setField("ITNO", itno);
			 MDBReadBean.setField("BRE2", bre2prdt);
			 MDBReadBean.setField("BREF", bref);
			 String mdbResp = MDBReadBean.runProgram("LstMITLOCV8");
			 
			 System.out.println("mdbResp in processReceiptDE: " + mdbResp);
			 while(MDBReadBean.nextRow()) {
				 String thisBref = MDBReadBean.getField("BREF").trim();
				 String thisBre2 = MDBReadBean.getField("BRE2").trim();
				 String thisItno = MDBReadBean.getField("ITNO").trim();
				 
				 System.out.println("Check " + thisBref + thisBre2 + thisItno);
				 System.out.println("Against " + bref + bre2 + itno);
				 if( 
					 (bref.trim().equals(thisBref)) &&
					 (bre2prdt.equals(thisBre2)) &&
					 (itno.trim().equals(thisItno))
				) {
					 mdbBano = MDBReadBean.getField("BANO");
					 mdbCamu = MDBReadBean.getField("CAMU");
					 matches++;
				 }
			 }
			 
			 /* old method - loading IRL Camu -> DE Camu. Alex needs DE Camu to be m3 generated 9 char, so relocate IRL Camu -> DE Bref. Have DE Brem share ZCNC + temperature

			 MMS060.setField("ITNO", itno);
			 MMS060.setField("WHLO", "IDB"); //todo lookup
			 MMS060.setField("CAMU", bref);
			 String mdbResp = MMS060.runProgram("LstContainer");
			 
			 while(MMS060.nextRow()) {
				 String thisBref = MMS060.getField("BREF").trim();
				 String thisBre2 = MMS060.getField("BRE2").trim();
				 String thisItno = MMS060.getField("ITNO").trim();
				 String thisCamu = MMS060.getField("CAMU").trim();
				 
				 //System.out.println("Check " + thisBref + thisBre2 + thisItno);
				 //System.out.println("Against " + bref + bre2 + itno);
				 if( 
					 (bre2.trim().equals(thisBre2)) &&
					 (itno.trim().equals(thisItno)) &&
					 (bref.trim().equals(thisCamu))
				) {
					 mdbBano = MMS060.getField("BANO");
					 mdbCamu = MMS060.getField("CAMU");
					 matches++;
				 }
			 }
			 */
			 
			msg+= " Lot: " + mdbBano + " Container: " + mdbCamu;
			//msg+= " Container: " + bref;
			
			if(matches>1)
				msg+= " Warning - duplicate receipt ";
			 
			 // do DOReceipt also if status 66/69
			 if( trsh.equals("66") || trsh.equals("69") ) {
				 DubBean.setField("PRFL", "*EXE");
				 DubBean.setField("CONO", MM_CONO + "");
				 DubBean.setField("WHLO", "092");
				 DubBean.setField("E0PA", "1");
				 DubBean.setField("E065", "WMS");
				 DubBean.setField("TWHL", fromWhs);
				 DubBean.setField("ITNO", poItno);
				 DubBean.setField("WHSL", "WHS");
				 DubBean.setField("CAMU", bref); // lot ref 1 in DE is CAMU in IRL // update, now both same...
				 DubBean.setField("QTY", qtyIE + "");
				 DubBean.setField("RIDN", doNum);
				 DubBean.setField("RIDO", "5");
				 DubBean.setField("RIDL", Integer.parseInt(doLine)*100 + ""); 
				 DubBean.setField("RIDI", dlix);
				 DubBean.setField("USD2", bre2);
				 DubBean.setField("BRE2", bre2);
				 DubBean.setField("BREF", brem);
				 DubBean.setField("USID", (String)session.getAttribute("uname"));
				 //
				//todo put these next 2 lines back...!
				 //String dubMsg = DubBean.runProgram("AddDOReceipt");
				 //msg+= " Dub: " + dubMsg;
			 }
			
			
		 } else {
			allOKs = false;
		 }
		
		
		String msgn = ""; //String msg = ""; 
		
		
		//json+= "{\"msg\":\"" + msgn + "\",\"msgn\":\"" + msg + closeMsg +
		//	" Lot: " + mdbBano + " Container: " + mdbCamu + "\", \"ok\":" + ok + 	
		//	"},";
	 	//msg = "";
	 	json+= "{\"msg\":\"" + msg + "\",\"msgn\":\"" + msg + "\", \"ok\":" + ok + "},";
	 } else {
		 json+= "{\"msg\":\"\",\"msgn\":\"\", \"ok\":true},";
	 }
} // end looping through the JSON array

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json); 
//System.out.print(json);

%>