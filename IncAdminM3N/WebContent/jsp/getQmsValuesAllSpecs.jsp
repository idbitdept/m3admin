<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="QMS400Bean" class="com.ornua.MvxBean" scope="page"/>
<%

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";

QMS400Bean.setSystem(system);
QMS400Bean.setPort(PORT);
QMS400Bean.setLib(LIB);
QMS400Bean.setCompany(zdcono);
QMS400Bean.setUsername(m3user);
QMS400Bean.setPassword(m3pass);
QMS400Bean.setInitialise("QMS400MI");

KevBean.setInitialise("QMS302MI");
KevBean.setField("FACI", zdfaci);
KevBean.setField("ITNO", itno);
KevBean.setField("BANO", bano);
KevBean.runProgram("LstTestQIReq");

	String json = "["; String thisBano = "";
	while(KevBean.nextRow()) {
		thisBano = KevBean.getField("BANO"); if(thisBano==null) thisBano = ""; thisBano = thisBano.trim();
		if(!thisBano.equals(bano)) break;
		
		String tsty = KevBean.getField("TSTY");
		String frti = KevBean.getField("FRTI"); if(frti==null) frti = "";
		int ifrti = 1;
		try {
			ifrti = Integer.parseInt(frti);
		} catch (Exception e) {
			System.err.println("Frti convert problem " + frti);
		}
		if(ifrti==0) ifrti = 1; // should always be one test (surely?)
		for (int i=0;i<ifrti;i++) {
			json+= "{";
			json+= "\"spec\":\"" + KevBean.getField("SPEC") + "\",";
			json+= "\"qtst\":\"" + KevBean.getField("QTST") + "\",";
			json+= "\"tx40\":\"" + KevBean.getField("TX40") + "\",";
			json+= "\"qse1\":\"" + KevBean.getField("QSE1") + "\",";
			
			json+= "\"qrid\":\"" + KevBean.getField("QRID") + "\",";
			
			json+= "\"tsty\":" + tsty + ",";

			
			json+= "\"frti\":" + frti + ",";
			json+= "\"currFrti\":" + (i+1) + ",";
			
			json+= "\"bano\":\"" + bano + "\",";
			json+= "\"itno\":\"" + itno + "\",";
			
			json+= "\"atid\":\"" + KevBean.getField("ATID") + "\",";
			json+= "\"evmx\":" + KevBean.getField("EVMX") + ",";
			json+= "\"evmn\":" + KevBean.getField("EVMN") + ",";
			json+= "\"evtg\":" + KevBean.getField("EVTG") + ",";
			
			json+= "\"tstt\":" + KevBean.getField("TSTT") + ",";
			
			// REPLACE QTRS (result) and QOP1 (operator?) with a call to QMS400MI GetTestResults/QTRS for each one..
			// can also get TTUS, TTDT, TTTE (tested by, date, time...)
			// could show both expected operator for the test AND current operator given..
			
			QMS400Bean.setField("FACI", zdfaci);
			QMS400Bean.setField("ITNO", itno);
			QMS400Bean.setField("QRID", KevBean.getField("QRID"));
			QMS400Bean.setField("BANO", bano);
			QMS400Bean.setField("TSTY", tsty);
			QMS400Bean.setField("QTST", KevBean.getField("QTST"));
			QMS400Bean.setField("TSEQ", (i+1)+"");
			
			String qop1 = "";
			if(QMS400Bean.runProgram("GetTestResults").startsWith("OK")) {
				qop1 = QMS400Bean.getField("QOP1"); if(qop1==null) qop1 = ""; if(qop1.equals("")) qop1 = "0";
				int iqop1 = 0; 
				try {
					iqop1 = Integer.parseInt(qop1);
				} catch (Exception e) {
					System.err.println("Qop1 error " + e);
				}
				json+= "\"qop1\":" + iqop1 + ",";
				json+= "\"prvl\":\"" + QMS400Bean.getField("PRVL") + "\",";
				
				json+= "\"ttus\":\"" + QMS400Bean.getField("TTUS") + "\",";
				json+= "\"ttdt\":\"" + QMS400Bean.getField("TTDT") + "\",";
				json+= "\"ttte\":\"" + QMS400Bean.getField("TTTE") + "\",";	
				json+= "\"ttdate\":\"" + QMS400Bean.getField("TTDT") + "/" + QMS400Bean.getField("TTTE") + "\",";
				
				if(tsty.equals("0"))
					json+= "\"qtrs\":\"" + QMS400Bean.getField("QTRS") + "\"";
				else
					json+= "\"qtrs\":\"" + QMS400Bean.getField("QLCD") + "\"";
					
			} else {
				qop1 = KevBean.getField("QOP1"); if(qop1==null) qop1 = ""; if(qop1.equals("")) qop1 = "0";
				int iqop1 = 0; 
				try {
					iqop1 = Integer.parseInt(qop1);
				} catch (Exception e) {
					System.err.println("Qop1 error " + e);
				}
				json+= "\"qop1\":" + iqop1 + ",";
				json+= "\"prvl\":\"\",";
				json+= "\"ttus\":\"\",";
				json+= "\"ttdt\":\"\",";
				json+= "\"ttte\":\"\",";
				json+= "\"ttdate\":\"\",";
				if(tsty.equals("0"))
					json+= "\"qtrs\":\"" + KevBean.getField("QTRS") + "\"";
				else
					json+= "\"qtrs\":\"" + KevBean.getField("QLCD") + "\"";
			}

			json+= "},";
		} //end frti frequency
	}

	if(json.length()>2) json = json.substring(0, json.length()-1);
	json = json + "]";
	out.print(json);
%>
