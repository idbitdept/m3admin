<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "ATS101MI";
String localTrans = "SetAttrValue";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
//make sure the users has the same cono/divi/faci settings as the API account

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String atvl = request.getParameter("atvl"); if(atvl==null) atvl = "";
String atid = request.getParameter("atid"); if(atid==null) atid = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";

KevBean.setInitialise("MDBREADMI");
KevBean.setField("MRCD", "1"); // we only need the first record
KevBean.runProgram("SetLstMaxRec");
KevBean.setField("CAMU", camu);
KevBean.runProgram("LstMITLOCZ7");

String attrResp = "";

if(!KevBean.nextRow()) {
	attrResp = "Pallet " + camu + ": No Attribute number found";
} else {
	String atnr = KevBean.getField("ATNR");
	LocalBean.setInitialise(localProg);
	LocalBean.setField("CONO", zdcono);
	LocalBean.setField("ATNR", atnr);
	LocalBean.setField("ATID", atid);
	LocalBean.setField("ATVA", atvl);
	 attrResp = LocalBean.runProgram(localTrans);
	 if(attrResp.startsWith("NOK            Not allowed")) 
		 attrResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;

}
out.println("{\"success\":true,\"resp\":\"" + attrResp + "\",\"ok\":" + attrResp.startsWith("OK") + "}");

%>
	