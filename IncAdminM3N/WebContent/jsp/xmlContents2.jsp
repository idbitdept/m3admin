<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@ page import="java.util.List" %>
<%@ page import="org.jdom.*" %>
<%@ page import="org.jdom.input.SAXBuilder" %>
<%@ page import="java.io.*" %>
<%@ page import="org.jdom.output.*" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String sndd = request.getParameter("sndd"); if(sndd==null) sndd = "";
String rgdt = request.getParameter("rgdt"); if(rgdt==null) rgdt = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

String[] snddBits = sndd.split(" ");
String direction = snddBits[1]; String dir = "in";
if(direction==null) direction = "";
if(direction.equals("Sent")) dir = "out";
else if(direction.equals("Received")) dir = "in";

String justFileName = snddBits[snddBits.length-1];

String as2Fol = "";

String []justFileNameBits = justFileName.split(Pattern.quote("\\"));
if(justFileNameBits.length>1) {
	as2Fol = justFileNameBits[0];
	justFileName = justFileNameBits[1];
}
if(divi.equals("B20")) as2Fol = "094"; // b20 doesnt use 3 char grouping, only FM relevant

System.out.println("AS2 fol " + as2Fol);
if(as2Fol.equals("")) {
	/*KevBean.setInitialise("OPS500MI");
	KevBean.setField("CONO", zdcono);
	KevBean.setField("FWHL", whlo);
	KevBean.setField("TWHL", whlo);
	KevBean.runProgram("LstStore");
	if(KevBean.nextRow()) as2Fol = KevBean.getField("WF03");*/
	
	KevBean.setInitialise("MDBREADMI");
	KevBean.setField("WHLO", whlo);
	if(KevBean.runProgram("GetOSTORE00").startsWith("O")) as2Fol = KevBean.getField("WF03");
}

String yyyy = rgdt.substring(0,4);
String mm   = rgdt.substring(4,6);
String dd   = rgdt.substring(6,8);

//System.out.println("yyyy: " + yyyy + " mm: " + mm + " dd: " + dd + " Dir: " + dir + " whlo: " + whlo);

String fileName = ""; String fileNameNotYetSentAS2 = "";
File file = null;
BufferedReader reader = null;
String filecontents = "";
String line = null;

try {	
	fileName = GISroot + "as2/evolve/archive/" + dir + "/" + as2Fol + "/"+ yyyy + "/" + mm + "/" + justFileName;
	file = new File(fileName);
	if(!file.exists()) // not yet sent by AS2, capture in outbound directory..
		fileName = GISroot + "as2/evolve/" + dir + "bound/" + "/" + as2Fol + "/" + justFileName;
	
	System.out.println("Filename: " + fileName);
		file = new File(fileName);
		
		try {
			SAXBuilder builder = new SAXBuilder();
			Document document = (Document) builder.build(file);
			%>
			<textarea rows="150" cols="100" style="border:none;">
			<%
			new XMLOutputter(Format.getPrettyFormat()).output(document, out);
			%>
			</textarea>
			<%
			} catch (Exception e) {		
		        reader = new BufferedReader(new FileReader(file));	
		        //... Loop as long as there are input lines.
		        line = null;
		        while ((line=reader.readLine()) != null) {
					out.print(line.replaceAll(" ", "&nbsp;") + "<br/>");
				}
		        reader.close(); 
			}
			
} catch (Exception e) {
	System.out.print("Couldnt do it " + e);
	out.print("File could not be found - if it is needed, please contact Techhelp");
}

%>
