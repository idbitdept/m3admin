<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/connections.jsp" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String ridl = request.getParameter("ridl"); if(ridl==null) ridl = ""; ridl = ridl.trim(); 
int iridl = Integer.parseInt(ridl);
String po = request.getParameter("po"); if(po==null) po = ""; po = po.trim();
String json = "";

boolean foundPOHead = true;

String pust = "";
String pusl = ""; int ipusl = 0;
String pitt = "";

KevBean.setInitialise("PPS200MI");
KevBean.setField("CONO", uricono);
KevBean.setField("PUNO", po);
KevBean.setField("PNLI", ridl);
String resp = KevBean.runProgram("GetLine");
if(resp.startsWith("OK")) {
	pusl = KevBean.getField("PUSL"); if(pusl==null) pusl = ""; 
	pitt = KevBean.getField("PITT"); if(pitt==null) pitt = "";
	try {
		ipusl = Integer.parseInt(pusl);
	} catch (Exception e) {
		System.err.println("Error converting ipusl: " + e);
	}
} else {
	if(resp.startsWith("NOK            Purchase order number " + po + " does not exist") || resp.startsWith("NOK            Purchase order number must be entered")) foundPOHead = false;
}

System.out.println("Check existing line, pusl is " + ipusl);
if(ipusl>=15 && ipusl <75) { // that line is fine... if they match, less than 75 is ok
	out.println("{\"success\":true, \"ridl\":" + iridl + ",\"pitt\":\"" + pitt + "\",\"foundPOHead\":" + foundPOHead + "}");
} else {
	KevBean.setField("CONO", uricono);
	KevBean.setField("PUNO", po);
	KevBean.runProgram("LstLine"); boolean foundLine = false;
	while(KevBean.nextRow()) {
		String lnPusl = KevBean.getField("PUSL"); if(lnPusl==null) lnPusl = "";
		pitt = KevBean.getField("PITT"); if(pitt==null) pitt = "";
		int iLnPusl = 0;
		try {
			iLnPusl = Integer.parseInt(lnPusl);
		} catch (Exception e) {
			System.err.println("Cant turn lstLine Pusl into number... " + e);
		}
		if(iLnPusl<=35 && iLnPusl >=15) {
			foundLine = true;
			out.println("{\"success\":true, \"ridl\":" + KevBean.getField("PNLI") + ",\"pitt\":\"" + pitt + "\",\"foundPOHead\":" + foundPOHead + "}");
			break;
		} else {
			System.out.println("Line: " + KevBean.getField("PNLI") + " is status: " + iLnPusl);
		}
	}
	if(!foundLine) out.println("{\"success\":false, \"ridl\":0, \"pitt\":\"\",\"foundPOHead\":" + foundPOHead + "}");
}
%>
