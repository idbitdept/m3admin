<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,org.jdom.*,org.jdom.output.XMLOutputter,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.setInitialise("MRS001MI");
KevBean.runProgram("SetLstMaxRec");

String program = request.getParameter("minm");
String transaction = request.getParameter("trnm");
String transDesc = request.getParameter("trds"); if(transDesc==null) transDesc = "";
String direction = "I";
String directionDesc = "INPUT"; if(!direction.equals("I")) directionDesc = "OUTPUT";

KevBean.setField("MINM", program);
KevBean.setField("TRNM", transaction);
KevBean.setField("TRTP", direction);
KevBean.runProgram("LstFields");

int childCount = 0;

while(KevBean.nextRow()) {
	childCount++;
}

KevBean.setField("MINM", program);
KevBean.setField("TRNM", transaction);
KevBean.setField("TRTP", direction); 

KevBean.runProgram("LstFields");

Element root;
Document doc;
int id = 1;

Namespace ns1 = Namespace.getNamespace("http://www.stercomm.com/SI/Map");
Namespace ns2 = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

root = new Element("Mapper", ns1);
root.addNamespaceDeclaration(ns2);
root.setAttribute("VERSION", "1.0");

Element mapDetails = new Element("MapDetails", root.getNamespace()); 
root.addContent(mapDetails);
Element versionControl = new Element("VersionControl", root.getNamespace());
mapDetails.addContent(versionControl);
versionControl.addContent(new Element("SerializationVersion", root.getNamespace()).setText("33056"));
versionControl.addContent(new Element("MajorVersion", root.getNamespace()).setText("0"));
versionControl.addContent(new Element("MinorVersion", root.getNamespace()).setText("0"));
versionControl.addContent(new Element("CompiledDate", root.getNamespace()).setText("319424482"));
Element summary = new Element("Summary", root.getNamespace());
mapDetails.addContent(summary);

summary.addContent(new Element("Author", root.getNamespace()).setText("Kevin Woods"));
summary.addContent(new Element("Description", root.getNamespace()).setText("KW M3 B2Bi Map Generator"));
summary.addContent(new Element("MapFunction", root.getNamespace()).setText("24"));

Element flags = new Element("Flags", root.getNamespace());
mapDetails.addContent(flags);

flags.addContent(new Element("SystemTemplate", root.getNamespace()).setText("0"));
flags.addContent(new Element("UseBigDecimal", root.getNamespace()).setText("0"));
flags.addContent(new Element("InitializeExtendedRuleVariables", root.getNamespace()).setText("0"));
flags.addContent(new Element("ErrorForNotUsed", root.getNamespace()).setText("0"));
flags.addContent(new Element("SuspendGroupProcessing", root.getNamespace()).setText("0"));
flags.addContent(new Element("SWIFTValidation", root.getNamespace()));
flags.addContent(new Element("UseConfigurableTrimming", root.getNamespace()).setText("0"));
flags.addContent(new Element("CompatibleRuleExecution", root.getNamespace()).setText("1"));
flags.addContent(new Element("KeepTrailingZeroes", root.getNamespace()).setText("0"));

Element eDIAssociations_IN = new Element("EDIAssociations_IN", root.getNamespace());
mapDetails.addContent(eDIAssociations_IN);

eDIAssociations_IN.addContent(new Element("AgencyID", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("VersionID", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("BindingID", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("FunctionGroupID", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("AgencyDescription", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("VersionDescription", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("BindingDescription", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("FunctionalGroupDescription", root.getNamespace()));
eDIAssociations_IN.addContent(new Element("Release", root.getNamespace()).setText("65535"));

Element eDIAssociations_OUT = new Element("EDIAssociations_OUT", root.getNamespace());
mapDetails.addContent(eDIAssociations_OUT);

eDIAssociations_OUT.addContent(new Element("AgencyID", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("VersionID", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("BindingID", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("FunctionGroupID", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("AgencyDescription", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("VersionDescription", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("BindingDescription", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("FunctionalGroupDescription", root.getNamespace()));
eDIAssociations_OUT.addContent(new Element("Release", root.getNamespace()).setText("65535"));

root.addContent(new Element("TemplateOption", root.getNamespace()).setText("2"));
root.addContent(new Element("ExtendedRuleLibraries", root.getNamespace()));

Element input = new Element("INPUT", root.getNamespace());
root.addContent(input);

Element posSyntax = new Element("PosSyntax", root.getNamespace());
input.addContent(posSyntax);

posSyntax.addContent(new Element("DelimiterUsed", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("Delimiter1", root.getNamespace()));
posSyntax.addContent(new Element("Delimiter2Used", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("Delimiter2", root.getNamespace()));
posSyntax.addContent(new Element("RecordLengthUsed", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("RecordLength", root.getNamespace()).setText("0"));
posSyntax.addContent(new Element("CharacterEncoding", root.getNamespace()));
posSyntax.addContent(new Element("DecimalCharUsed", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("DecimalChar", root.getNamespace()));
posSyntax.addContent(new Element("UseBytePositions", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("RepeatSuppressionCharUsed", root.getNamespace()).setText("no"));
posSyntax.addContent(new Element("RepeatSuppressionChar", root.getNamespace()));

Element group = new Element("Group", root.getNamespace());
posSyntax.addContent(group);

group.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
group.addContent(new Element("Name", root.getNamespace()).setText(directionDesc));
group.addContent(new Element("Description", root.getNamespace()).setText(program.trim() + "_" + transaction.trim() + direction));
group.addContent(new Element("Active", root.getNamespace()).setText("1"));
group.addContent(new Element("ChildCount", root.getNamespace()).setText("1"));
group.addContent(new Element("Note", root.getNamespace()).setText(program.trim() + "_" + transaction.trim() + "_" + directionDesc));
group.addContent(new Element("Min", root.getNamespace()).setText("0"));
group.addContent(new Element("Max", root.getNamespace()).setText("1"));
group.addContent(new Element("PromoteGroup", root.getNamespace()).setText("no"));

group.addContent(new Element("GroupChoiceType", root.getNamespace()).setText("0"));
group.addContent(new Element("OrderingType", root.getNamespace()).setText("0"));
group.addContent(new Element("OrderingTag", root.getNamespace()));
group.addContent(new Element("UsageRelatedFieldName", root.getNamespace()));

Element posRecord = new Element("PosRecord", root.getNamespace());
group.addContent(posRecord);

posRecord.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
posRecord.addContent(new Element("Name", root.getNamespace()).setText(program.trim() + "_" + transaction.trim()));
posRecord.addContent(new Element("Description", root.getNamespace()).setText(transDesc));

posRecord.addContent(new Element("Active", root.getNamespace()).setText("1"));
posRecord.addContent(new Element("ChildCount", root.getNamespace()).setText(Integer.toString(childCount)));

posRecord.addContent(new Element("Note", root.getNamespace()).setText(program.trim() + "-" + transaction.trim() + ". Direction: " + direction + 
			". Generated by KW M3/B2Bi API Mapper using account " + m3user + ", company " + icono + " on " + system));
posRecord.addContent(new Element("Min", root.getNamespace()).setText("0"));
posRecord.addContent(new Element("Max", root.getNamespace()).setText("1"));

posRecord.addContent(new Element("LoopCtl", root.getNamespace()).setText("normal"));
posRecord.addContent(new Element("OrderingType", root.getNamespace()).setText("0"));

posRecord.addContent(new Element("OrderingTag", root.getNamespace()));
posRecord.addContent(new Element("UsageRelatedFieldName", root.getNamespace()));

Element blockSig = new Element("BlockSig", root.getNamespace());
posRecord.addContent(blockSig);

blockSig.addContent(new Element("Tag", root.getNamespace()).setText(transaction));

blockSig.addContent(new Element("TagPos", root.getNamespace()).setText("0"));
blockSig.addContent(new Element("KeyFieldID", root.getNamespace()).setText("0"));
blockSig.addContent(new Element("KeyFieldData", root.getNamespace()).setText("65535"));
blockSig.addContent(new Element("KeyFieldAction", root.getNamespace()).setText("65535"));
blockSig.addContent(new Element("Tag", root.getNamespace()));

blockSig.addContent(new Element("TagPos", root.getNamespace()).setText("0"));
blockSig.addContent(new Element("KeyFieldID", root.getNamespace()).setText("0"));
blockSig.addContent(new Element("KeyFieldData", root.getNamespace()).setText("0"));
blockSig.addContent(new Element("KeyFieldAction", root.getNamespace()).setText("0"));

posRecord.addContent(new Element("TagLength", root.getNamespace()).setText("15")); 
//int z = 0;

while(KevBean.nextRow()) {
	
	//if(z==2) break;z++;
	Element field = new Element("Field", root.getNamespace());
	posRecord.addContent(field);
	
	field.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
	field.addContent(new Element("Name", root.getNamespace()).setText(transaction.trim() + "_" + KevBean.getField("FLNM")));
	field.addContent(new Element("Description", root.getNamespace()).setText(KevBean.getField("FLDS")));
	field.addContent(new Element("Active", root.getNamespace()).setText("1"));
	field.addContent(new Element("ChildCount", root.getNamespace()).setText("1"));
	field.addContent(new Element("Note", root.getNamespace()).setText("Entry date " + KevBean.getField("RGDT") + " at " + KevBean.getField("RGTM") + ". Changed by " + KevBean.getField("CHID")));
	String mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim(); if(mand.equals("1")) mand = "yes"; else mand = "no";
	field.addContent(new Element("Mandatory", root.getNamespace()).setText(mand)); // could set this if we want...
	
	field.addContent(new Element("NotUsed", root.getNamespace()).setText("no"));
	field.addContent(new Element("FieldNumber", root.getNamespace()).setText("0"));
	field.addContent(new Element("StoreGroup", root.getNamespace()).setText("65535"));
	field.addContent(new Element("StoreField", root.getNamespace()).setText("65535"));
	field.addContent(new Element("BusinessName", root.getNamespace()));

	Element storeLimit = new Element("StoreLimit", root.getNamespace());
	field.addContent(storeLimit);
	
	storeLimit.addContent(new Element("MaxLen", root.getNamespace()).setText(KevBean.getField("LENG")));
	storeLimit.addContent(new Element("MinLen", root.getNamespace()).setText("0"));
	
	storeLimit.addContent(new Element("Signed", root.getNamespace()).setText("no"));
	
	String type = KevBean.getField("TYPE"); if(type==null) type = ""; type = type.trim();
	if(type.equals("N")) type = "numeric"; else type = "string";
	String format = ""; if(type.equals("numeric")) format = "R0";
	storeLimit.addContent(new Element("DataType", root.getNamespace()).setText(type));
	storeLimit.addContent(new Element("ImpliedDecimalPos", root.getNamespace()).setText("0"));
	storeLimit.addContent(new Element("ImplicitDecimal", root.getNamespace()).setText("no"));
	storeLimit.addContent(new Element("AllowSignedDecimal", root.getNamespace()).setText("1"));
	storeLimit.addContent(new Element("Format", root.getNamespace()).setText(format));
	storeLimit.addContent(new Element("BinaryOutput", root.getNamespace()).setText("0"));
	storeLimit.addContent(new Element("BinaryWidth", root.getNamespace()).setText("0"));

	String frpo = KevBean.getField("FRPO"); if(frpo==null) frpo = ""; frpo = frpo.trim();
	int ifrpo = Integer.parseInt(frpo); 
	field.addContent(new Element("StartPos", root.getNamespace()).setText(Integer.toString(--ifrpo)));
	field.addContent(new Element("Length", root.getNamespace()).setText(KevBean.getField("LENG")));
	
	field.addContent(new Element("Justify", root.getNamespace()).setText("left"));
	field.addContent(new Element("PadChar", root.getNamespace()).setText("SP"));
	field.addContent(new Element("PadHighByte", root.getNamespace()).setText("0"));
	field.addContent(new Element("Binary", root.getNamespace()).setText("0"));	
}

// do the output
Element output = new Element("OUTPUT", root.getNamespace());
root.addContent(output);

Element posSyntaxOut = new Element("PosSyntax", root.getNamespace());
output.addContent(posSyntaxOut);

posSyntaxOut.addContent(new Element("DelimiterUsed", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("Delimiter1", root.getNamespace()));
posSyntaxOut.addContent(new Element("Delimiter2Used", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("Delimiter2", root.getNamespace()));
posSyntaxOut.addContent(new Element("RecordLengthUsed", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("RecordLength", root.getNamespace()).setText("0"));
posSyntaxOut.addContent(new Element("CharacterEncoding", root.getNamespace()));
posSyntaxOut.addContent(new Element("DecimalCharUsed", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("DecimalChar", root.getNamespace()));
posSyntaxOut.addContent(new Element("UseBytePositions", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("RepeatSuppressionCharUsed", root.getNamespace()).setText("no"));
posSyntaxOut.addContent(new Element("RepeatSuppressionChar", root.getNamespace()));

Element groupOut = new Element("Group", root.getNamespace());
posSyntaxOut.addContent(groupOut);

groupOut.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
groupOut.addContent(new Element("Name", root.getNamespace()).setText("OUTPUT"));

groupOut.addContent(new Element("Description", root.getNamespace()).setText(program.trim() + "_" + transaction.trim() + "OUT"));
groupOut.addContent(new Element("Active", root.getNamespace()).setText("1"));
groupOut.addContent(new Element("ChildCount", root.getNamespace()).setText("1"));
groupOut.addContent(new Element("Note", root.getNamespace()).setText(program.trim() + "_" + transaction.trim() + "_" + "OUTPUT"));
groupOut.addContent(new Element("Min", root.getNamespace()).setText("0"));
groupOut.addContent(new Element("Max", root.getNamespace()).setText("1"));
groupOut.addContent(new Element("PromoteGroup", root.getNamespace()).setText("no"));

groupOut.addContent(new Element("GroupChoiceType", root.getNamespace()).setText("0"));
groupOut.addContent(new Element("OrderingType", root.getNamespace()).setText("0"));
groupOut.addContent(new Element("OrderingTag", root.getNamespace()));
groupOut.addContent(new Element("UsageRelatedFieldName", root.getNamespace()));

Element posRecordOut = new Element("PosRecord", root.getNamespace());
groupOut.addContent(posRecordOut);


posRecordOut.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
posRecordOut.addContent(new Element("Name", root.getNamespace()).setText(program.trim() + "_" + transaction.trim()));
posRecordOut.addContent(new Element("Description", root.getNamespace()).setText(transDesc));

posRecordOut.addContent(new Element("Active", root.getNamespace()).setText("1"));
posRecordOut.addContent(new Element("ChildCount", root.getNamespace()).setText(Integer.toString(childCount)));

posRecordOut.addContent(new Element("Note", root.getNamespace()));
posRecordOut.addContent(new Element("Min", root.getNamespace()).setText("0"));
posRecordOut.addContent(new Element("Max", root.getNamespace()).setText("1"));

posRecordOut.addContent(new Element("LoopCtl", root.getNamespace()).setText("normal"));
posRecordOut.addContent(new Element("OrderingType", root.getNamespace()).setText("0"));

posRecordOut.addContent(new Element("OrderingTag", root.getNamespace()));
posRecordOut.addContent(new Element("UsageRelatedFieldName", root.getNamespace()));

Element blockSigOut = new Element("BlockSig", root.getNamespace());
posRecordOut.addContent(blockSigOut);

String outputTag = "OK";
if(transaction.startsWith("Lst")) outputTag = "REP";
blockSigOut.addContent(new Element("Tag", root.getNamespace()).setText(outputTag));

blockSigOut.addContent(new Element("TagPos", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldID", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldData", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldAction", root.getNamespace()).setText("65535"));
blockSigOut.addContent(new Element("Tag", root.getNamespace()));

blockSigOut.addContent(new Element("TagPos", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldID", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldData", root.getNamespace()).setText("0"));
blockSigOut.addContent(new Element("KeyFieldAction", root.getNamespace()).setText("0"));

posRecordOut.addContent(new Element("TagLength", root.getNamespace()).setText("15")); // 0 if dummy...




// now the output fields

direction = "O";
directionDesc = "OUTPUT";


KevBean.setField("MINM", program);
KevBean.setField("TRNM", transaction);
KevBean.setField("TRTP", direction); 

KevBean.runProgram("LstFields");

childCount = 0;

while(KevBean.nextRow()) {
	childCount++;
}

KevBean.setField("MINM", program);
KevBean.setField("TRNM", transaction);
KevBean.setField("TRTP", direction); 

KevBean.runProgram("LstFields");

while(KevBean.nextRow()) {
	
	//if(z==2) break;z++;
	Element field = new Element("Field", root.getNamespace());
	posRecordOut.addContent(field);
	
	field.addContent(new Element("ID", root.getNamespace()).setText(Integer.toString(id++)));
	field.addContent(new Element("Name", root.getNamespace()).setText(transaction.trim() + "_" + KevBean.getField("FLNM")));
	field.addContent(new Element("Description", root.getNamespace()).setText(KevBean.getField("FLDS")));
	field.addContent(new Element("Active", root.getNamespace()).setText("1"));
	field.addContent(new Element("ChildCount", root.getNamespace()).setText("1"));
	field.addContent(new Element("Note", root.getNamespace()).setText("Entry date " + KevBean.getField("RGDT") + " at " + KevBean.getField("RGTM") + ". Changed by " + KevBean.getField("CHID")));
	String mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim(); if(mand.equals("1")) mand = "yes"; else mand = "no";
	field.addContent(new Element("Mandatory", root.getNamespace()).setText(mand)); // could set this if we want...
	
	field.addContent(new Element("NotUsed", root.getNamespace()).setText("no"));
	field.addContent(new Element("FieldNumber", root.getNamespace()).setText("0"));
	field.addContent(new Element("StoreGroup", root.getNamespace()).setText("65535"));
	field.addContent(new Element("StoreField", root.getNamespace()).setText("65535"));
	field.addContent(new Element("BusinessName", root.getNamespace()));

	Element storeLimit = new Element("StoreLimit", root.getNamespace());
	field.addContent(storeLimit);
	
	storeLimit.addContent(new Element("MaxLen", root.getNamespace()).setText(KevBean.getField("LENG")));
	storeLimit.addContent(new Element("MinLen", root.getNamespace()).setText("0"));
	
	storeLimit.addContent(new Element("Signed", root.getNamespace()).setText("no"));
	
	String type = KevBean.getField("TYPE"); if(type==null) type = ""; type = type.trim();
	if(type.equals("N")) type = "numeric"; else type = "string";
	String format = ""; if(type.equals("numeric")) format = "R0";
	storeLimit.addContent(new Element("DataType", root.getNamespace()).setText(type));
	storeLimit.addContent(new Element("ImpliedDecimalPos", root.getNamespace()).setText("0"));
	storeLimit.addContent(new Element("ImplicitDecimal", root.getNamespace()).setText("no"));
	storeLimit.addContent(new Element("AllowSignedDecimal", root.getNamespace()).setText("1"));
	storeLimit.addContent(new Element("Format", root.getNamespace()).setText(format));
	storeLimit.addContent(new Element("BinaryOutput", root.getNamespace()).setText("0"));
	storeLimit.addContent(new Element("BinaryWidth", root.getNamespace()).setText("0"));

	String frpo = KevBean.getField("FRPO"); if(frpo==null) frpo = ""; frpo = frpo.trim();
	int ifrpo = Integer.parseInt(frpo); 
	field.addContent(new Element("StartPos", root.getNamespace()).setText(Integer.toString(--ifrpo)));
	field.addContent(new Element("Length", root.getNamespace()).setText(KevBean.getField("LENG")));
	
	field.addContent(new Element("Justify", root.getNamespace()).setText("left"));
	field.addContent(new Element("PadChar", root.getNamespace()).setText("SP"));
	field.addContent(new Element("PadHighByte", root.getNamespace()).setText("0"));
	field.addContent(new Element("Binary", root.getNamespace()).setText("0"));	
}
//


// finally, add syntax tokens		
Element syntaxTokens = new Element("SyntaxTokens", root.getNamespace());
root.addContent(syntaxTokens);

// first Token
Element token = new Element ("Token", root.getNamespace());
syntaxTokens.addContent(token);
token.addContent(new Element("Code", root.getNamespace()).setText("A"));

Element range1 = new Element("Range", root.getNamespace());
token.addContent(range1);
range1.addContent(new Element("Start", root.getNamespace()).setText("Z"));
range1.addContent(new Element("End", root.getNamespace()).setText("A"));

Element range2 = new Element("Range", root.getNamespace());
token.addContent(range2);
range2.addContent(new Element("Start", root.getNamespace()).setText("z"));
range2.addContent(new Element("End", root.getNamespace()).setText("a"));

token.addContent(new Element("Char", root.getNamespace()).setText("SP"));


// second Token
Element token2 = new Element ("Token", root.getNamespace());
syntaxTokens.addContent(token2);
token2.addContent(new Element("Code", root.getNamespace()).setText("N"));

Element rangeT2 = new Element("Range", root.getNamespace());
token2.addContent(rangeT2);
rangeT2.addContent(new Element("Start", root.getNamespace()).setText("9"));
rangeT2.addContent(new Element("End", root.getNamespace()).setText("0"));

token2.addContent(new Element("Char", root.getNamespace()).setText("."));
token2.addContent(new Element("Char", root.getNamespace()).setText("-"));
token2.addContent(new Element("Char", root.getNamespace()).setText("+"));

// third Token
Element token3 = new Element ("Token", root.getNamespace());
syntaxTokens.addContent(token3);
token3.addContent(new Element("Code", root.getNamespace()).setText("J"));

Element rangeT31 = new Element("Range", root.getNamespace());
token3.addContent(rangeT31);
rangeT31.addContent(new Element("Start", root.getNamespace()).setText("Z"));
rangeT31.addContent(new Element("End", root.getNamespace()).setText("A"));

Element rangeT32 = new Element("Range", root.getNamespace());
token3.addContent(rangeT32);
rangeT32.addContent(new Element("Start", root.getNamespace()).setText("z"));
rangeT32.addContent(new Element("End", root.getNamespace()).setText("a"));

Element rangeT33 = new Element("Range", root.getNamespace());
token3.addContent(rangeT33);
rangeT33.addContent(new Element("Start", root.getNamespace()).setText("9"));
rangeT33.addContent(new Element("End", root.getNamespace()).setText("0"));

Element rangeT34 = new Element("Range", root.getNamespace());
token3.addContent(rangeT34);
rangeT34.addContent(new Element("Start", root.getNamespace()).setText("/"));
rangeT34.addContent(new Element("End", root.getNamespace()).setText("!"));

Element rangeT35 = new Element("Range", root.getNamespace());
token3.addContent(rangeT35);
rangeT35.addContent(new Element("Start", root.getNamespace()).setText("@"));
rangeT35.addContent(new Element("End", root.getNamespace()).setText(":"));

Element rangeT36 = new Element("Range", root.getNamespace());
token3.addContent(rangeT36);
rangeT36.addContent(new Element("Start", root.getNamespace()).setText("`"));
rangeT36.addContent(new Element("End", root.getNamespace()).setText("["));

Element rangeT37 = new Element("Range", root.getNamespace());
token3.addContent(rangeT37);
rangeT37.addContent(new Element("Start", root.getNamespace()).setText("~"));
rangeT37.addContent(new Element("End", root.getNamespace()).setText("{"));

Element rangeT38 = new Element("Range", root.getNamespace());
token3.addContent(rangeT38);
rangeT38.addContent(new Element("Start", root.getNamespace()).setText("0xDF"));
rangeT38.addContent(new Element("End", root.getNamespace()).setText("0xA1"));


token3.addContent(new Element("Char", root.getNamespace()).setText("SP"));		

doc = new Document(root);
//File f=new File("IDB/Documents");
//if(!f.isDirectory()) if(!f.mkdir()) System.err.print("Cant make folder");

String filename = "/Users/IDB/Downloads/BigMap-" + String.valueOf(System.currentTimeMillis()) + ".mxl";
filename = "\\\\b2bitst.ornua.com\\maps\\source\\" + program.trim() + "_" + transaction.trim() + ".mxl";
try {
	FileOutputStream out1 = new FileOutputStream(filename);
	XMLOutputter serializer = new XMLOutputter();
	serializer.output(doc, out1);
	out1.flush(); out1.close();
	
	String resp = "xxx";
	
	out.println("{\"success\":false,\"msg\":\"" + resp + "\"}");
} catch (Exception e) {
	System.err.println("Exception writing file out..." + e);
}	
%>
