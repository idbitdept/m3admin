<%@ page contentType="text/html; charset=utf-8" language="java" errorPage="" %>
<%@ page import="java.util.List" %>
<%@ page import="org.jdom.*" %>
<%@ page import="org.jdom.input.SAXBuilder" %>
<%@ page import="java.io.*" %>
<%@ page import="org.jdom.output.*" %>
<%@ page import="java.util.regex.Pattern" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String sndd = request.getParameter("sndd"); if(sndd==null) sndd = "";
String rgdt = request.getParameter("rgdt"); if(rgdt==null) rgdt = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

String[] snddBits = sndd.split(" ");
String direction = snddBits[1]; String dir = "in";
if(direction==null) direction = "";
if(direction.equals("Sent") || direction.equals("receipt")) dir = "out";
else if(direction.equals("Received")) dir = "in";

String justFileName = snddBits[snddBits.length-1];

String raw = request.getParameter("raw"); if(raw==null) raw = "";

String as2Fol = "";

String []justFileNameBits = justFileName.split(Pattern.quote("\\"));
if(justFileNameBits.length>1) {
	as2Fol = justFileNameBits[0];
	justFileName = justFileNameBits[1];
}
if(divi.equals("B20")) as2Fol = "094"; // b20 doesnt use 3 char grouping, only FM relevant

if(as2Fol.equals("")) {
	/*KevBean.setInitialise("OPS500MI");
	KevBean.setField("CONO", zdcono);
	KevBean.setField("FWHL", whlo);
	KevBean.setField("TWHL", whlo);
	KevBean.runProgram("LstStore");
	if(KevBean.nextRow()) as2Fol = KevBean.getField("WF03");*/
	
	KevBean.setInitialise("MDBREADMI");
	KevBean.setField("WHLO", whlo);
	if(KevBean.runProgram("GetOSTORE00").startsWith("O")) as2Fol = KevBean.getField("WF03");
}

String yyyy = rgdt.substring(0,4);
String mm   = rgdt.substring(4,6);
String dd   = rgdt.substring(6,8);

//System.out.println("yyyy: " + yyyy + " mm: " + mm + " dd: " + dd + " Dir: " + dir + " whlo: " + whlo);

String fileName = ""; String fileNameNotYetSentAS2 = "";
File file = null;
BufferedReader reader = null;
String filecontents = "";
String line = null;

String fileNameHtm = "";
if (justFileName.length() > 4) fileNameHtm = justFileName.substring(0, justFileName.length() - 4) + ".htm";
//System.out.println("Just file name: " + justFileName + " filenameHtm: " + fileNameHtm);

try {	
	
	if(!raw.equals("true")) {
		fileName = GISserver + "as2/" + as2org + "/archive/" + dir + "/" + as2Fol + "/"+ yyyy + "/" + mm + "/" + fileNameHtm;
		file = new File(fileName);
		System.out.println("filename 1: " + fileName);
	}
	
	if(file==null || !file.exists())
		fileName = GISserver + "as2/" + as2org + "/archive/" + dir + "/" + as2Fol + "/"+ yyyy + "/" + mm + "/" + justFileName;
	file = new File(fileName);
	System.out.println("filename 2: " + fileName);
	if(!file.exists()) // not yet sent by AS2, capture in outbound directory..
		fileName = GISserver + "as2/" + as2org + "/" + dir + "bound" + "/" + as2Fol + "/" + justFileName;
	
	System.out.println("Filename 3: " + fileName);
		file = new File(fileName);
		
		if(fileName.endsWith(".xml")) {
		
			try {
				SAXBuilder builder = new SAXBuilder();
				Document document = (Document) builder.build(file);
				%>
				<textarea rows="150" cols="100" style="border:none;">
				<%
				new XMLOutputter(Format.getPrettyFormat()).output(document, out);
				%>
				</textarea>
				<%
			} catch (Exception e) {		
		        reader = new BufferedReader(new FileReader(file));	
		        //... Loop as long as there are input lines.
		        line = null;
		        while ((line=reader.readLine()) != null) {
					out.print(line.replaceAll(" ", "&nbsp;") + "<br/>");
				}
		        reader.close(); 
			}
		} else if(fileName.endsWith(".htm")) { // could be text files etc
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) out.print(line);
		} else { // if(fileName.endsWith(".txt")) { // could be text edi files etc
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) out.print(line + "<br/>");
		}
			
} catch (Exception e) {
	System.out.print("Couldnt do it " + e);
	out.print("File could not be found - if it is needed, please contact Techhelp");
}

%>
