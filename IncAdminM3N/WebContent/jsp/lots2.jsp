<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

String banoOrBref  = request.getParameter("banoOrBref"); if(banoOrBref==null) banoOrBref = "";
int ibanoOrBref = 0; // 0 is lot 1 is lot ref 1
try { 
	ibanoOrBref = Integer.parseInt(banoOrBref);
} catch (Exception e) {
	System.err.println("Error converting lot or lot ref 1 " + e);
}

KevBean.setInitialise("MMS060MI");
KevBean.setField("MRCD", "5000");
KevBean.runProgram("SetLstMaxRec");
KevBean.setField("CONO", zdcono);
KevBean.setField("WHLO", whlo);
KevBean.setField("ITNO", itno);
KevBean.runProgram("List");

String json = "["; String bano = ""; String bref = "";
if (ibanoOrBref==0) {
	while(KevBean.nextRow()) {
		String newbano = KevBean.getField("BANO");
		if(!bano.equals(newbano)) {
			json+= "{";
			json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
			json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
			json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
			json+= "\"bano\":\"" + newbano + "\"";
			json+= "},";
		}
		bano = newbano;
	}
} else {
	while(KevBean.nextRow()) {
		String newbref = KevBean.getField("BREF");
		if(!bref.equals(newbref)) {
			json+= "{";
			json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
			json+= "\"bref\":\"" + newbref + "\",";
			json+= "\"bano\":\"" + KevBean.getField("BANO") + "\"";
			json+= "},";
		}
		bref = newbref;
	}
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
