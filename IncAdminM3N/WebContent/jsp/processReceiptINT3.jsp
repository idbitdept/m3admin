<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,java.net.*,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>

<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "PPS001MI";
String localTrans = "Receipt";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setPassword(localPword); 

LocalBean.setInitialise(localProg);
String json = "[";

boolean chkCompletePO = Boolean.parseBoolean(request.getParameter("chkCompletePO"));
int totValPallets = Integer.parseInt(request.getParameter("totValPallets"));

boolean allOKs = true; String closeMsg = "";

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray pallets = (JSONArray) obj;

int i = 0; int actReceipts = 0; boolean keepGoing = true;
Iterator<JSONObject> iterator = pallets.iterator(); 
while (iterator.hasNext()) {
	if(!keepGoing) break;
	 JSONObject pallet = (JSONObject) iterator.next();
	 
	 long dlqa = (Long)pallet.get("dlqa");
	 //System.out.println("i is " + i + " qty: " + qty); 
	 i++;
	 if(dlqa>0) {
		 actReceipts++;

		 LocalBean.setField("CONO", zdcono);
		 LocalBean.setField("TRDT", today.trim());
		 LocalBean.setField("RESP", "IDBKEWO"); //hmm
		 LocalBean.setField("PUNO", (String)pallet.get("ridn")); 
		 LocalBean.setField("PNLI", (Long)pallet.get("ridl") + "");
		 
		 LocalBean.setField("RVQA", dlqa + "");
		 
		 String scawe = "";
		 try {
			 Double dcawe = (Double)pallet.get("cawe");
			 scawe = dcawe + "";
		 } catch (Exception e) {
			 Long lcawe = (Long)pallet.get("cawe");
			 scawe = lcawe + "";
		 }
		 LocalBean.setField("CAWE", scawe);
		 
		 LocalBean.setField("WHLO", (String)pallet.get("whlo")); 
		 LocalBean.setField("WHSL", (String)pallet.get("whsl")); 
		 LocalBean.setField("SUDO", (String)pallet.get("sudo")); 
		 
		 LocalBean.setField("CAMU", (String)pallet.get("camu")); 
		 LocalBean.setField("BANO", (String)pallet.get("bano")); 
		 
		 LocalBean.setField("BREF", (String)pallet.get("bref")); 
		 LocalBean.setField("BRE2", (String)pallet.get("bre2")); 
		 
		 LocalBean.setField("PRDT", (String)pallet.get("mfdt"));

		 String msg = LocalBean.runProgram("Receipt");		 
		 System.out.println("message is " + msg);
		 
		 boolean ok = false; 
		 if(msg.startsWith("OK")) {
			 ok = true;
		 } else {
			allOKs = false;
			
			if(msg.startsWith("NOK            Not allowed")) {
				 keepGoing = false;
				 msg += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
				 break;
			 }
		 }
		
		
		String msgn = ""; //String msg = ""; 
		
	 	json+= "{\"msg\":\"" + msg + "\",\"msgn\":\"" + msg + "\"},";
	 } else {
		 json+= "{\"msg\":\"\",\"msgn\":\"\", \"ok\":true},";
	 }
} // end looping through the JSON array

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json); 
//System.out.print(json);

%>