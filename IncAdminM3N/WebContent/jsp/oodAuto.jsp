<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.List,java.sql.*,java.util.Properties,javax.activation.*,javax.mail.*,javax.mail.internet.*" %>
<%@ include file="Connections/INCm3login.jsp" %>
<jsp:useBean id="MWS070MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MMS060MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MMS850MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MNS150MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MMS005MIBean" class="com.ornua.MvxBean" scope="page"/>

<%
String emal = request.getParameter("emal"); if(emal==null) emal = ""; if(emal.equals("")) emal = "Kevin.Woods@ornua.com";
String urienv = request.getParameter("env"); if(urienv==null) urienv = "";
String uridivi = request.getParameter("divi"); if(uridivi==null) uridivi = "";
String uricono = request.getParameter("cono"); if(uricono==null) uricono = "";
String faci = "";
	
	String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	String Gis05Uname = "GIS5263";
	String Gis05Pword = "GIS5263";
	String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=IDB_General";
	try {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);
	    PreparedStatement ps = conn.prepareStatement("select * from PortalCompany where env = ? and cono = ? and divi = ?");
	    ps.setString(1, urienv);
	    ps.setString(2, uricono);
	    ps.setString(3, uridivi);
	    //System.out.println(urienv + " " + uricono + " " + uridivi);
	    ResultSet rs = ps.executeQuery();
	    if(rs.next()) {
	    	m3pass = rs.getString("m3pass");
	    	m3user = rs.getString("m3user");
	    	PORT = rs.getInt("port");
	    	system = rs.getString("system");
	    }
	} catch (Exception e) {
		System.err.println("cakes" + e);
	}

	MNS150MIBean.setSystem(system);
	MNS150MIBean.setPort(PORT);
	MNS150MIBean.setCompany(uricono);
	MNS150MIBean.setUsername(m3user);
	MNS150MIBean.setPassword(m3pass);
	MNS150MIBean.setInitialise("MNS150MI");
	
	MMS005MIBean.setSystem(system);
	MMS005MIBean.setPort(PORT);
	MMS005MIBean.setCompany(uricono);
	MMS005MIBean.setUsername(m3user);
	MMS005MIBean.setPassword(m3pass);
	MMS005MIBean.setInitialise("MMS005MI");
	
	String myM3User = m3user;
	if(m3user.contains("\\")) { // in case username has a domain suffix
		myM3User = m3user.substring(m3user.lastIndexOf("\\")+1);
	}
	
	MNS150MIBean.setField("USID", myM3User);
	String resp = MNS150MIBean.runProgram("GetUserData");
	String zdcono = MNS150MIBean.getField("CONO");
	String zddivi = MNS150MIBean.getField("DIVI");
	String zdfaci = MNS150MIBean.getField("FACI");
	
MMS060MIBean.setSystem(system);
MMS060MIBean.setPort(PORT);
MMS060MIBean.setCompany(zdcono);
MMS060MIBean.setUsername(m3user);
MMS060MIBean.setPassword(m3pass);
MMS060MIBean.setInitialise("MMS060MI");

MMS850MIBean.setSystem(system);
MMS850MIBean.setPort(PORT);
MMS850MIBean.setCompany(zdcono);
MMS850MIBean.setUsername(m3user);
MMS850MIBean.setPassword(m3pass);
MMS850MIBean.setInitialise("MMS850MI");

MWS070MIBean.setSystem(system);
MWS070MIBean.setPort(PORT);
MWS070MIBean.setCompany(zdcono);
MWS070MIBean.setUsername(m3user);
MWS070MIBean.setPassword(m3pass);
MWS070MIBean.setInitialise("MWS070MI");

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);

String publishResults = "<ul>Results of OOD update on expired stock";

MWS070MIBean.setField("MRCD", "0"); // change to 0 (unlimited) or big number for live
MWS070MIBean.runProgram("SetLstMaxRec");

MWS070MIBean.setField("TTYP", "96");
//MWS070MIBean.setField("XQT0", "1");
MWS070MIBean.setField("RESP", "M3ADMIN");
MWS070MIBean.setField("FTRD", today);
MWS070MIBean.runProgram("LstTransByUser");
while(MWS070MIBean.nextRow()) {
	// get the current remark
	String stas = MWS070MIBean.getField("STAS"); if(stas==null) stas = "";
	String trqt = MWS070MIBean.getField("TRQT"); if(trqt==null) trqt = "";
	
	if( (stas.equals("3")) && (!trqt.contains("-")) ) {

		String bano = MWS070MIBean.getField("BANO"); if(bano==null) bano = "";
		String whsll = "";
		whsll = MWS070MIBean.getField("WHSL"); if(whsll==null) whsll = ""; whsll = whsll.trim();
		String whlt = MWS070MIBean.getField("WHLT"); if(whlt==null) whlt = "";
		String camu = MWS070MIBean.getField("CAMU"); if(camu==null) camu = "";
		
		MMS060MIBean.setField("CONO", zdcono);
		MMS060MIBean.setField("WHLO", MWS070MIBean.getField("WHLO"));
		MMS060MIBean.setField("ITNO", MWS070MIBean.getField("ITNO"));
		MMS060MIBean.setField("WHSL", whsll); System.out.println("Whsl for lot " + bano + " is " + whsll + " and whlt is " + whlt);
		MMS060MIBean.setField("BANO", bano);
		MMS060MIBean.setField("CAMU", camu);
		MMS060MIBean.setField("REPN", MWS070MIBean.getField("REPN"));
		MMS060MIBean.runProgram("Get");
		String brem = MMS060MIBean.getField("BREM"); if(brem==null) brem = "";
		System.out.println("Brem is " + brem);
		String newBrem = "OOD";
		if( (!brem.equals("")) && (!brem.startsWith("OOD")) && (!brem.contains(" OOD")) ) newBrem = brem + " - OOD";
		else if(brem.equals("")) newBrem = "OOD";
		else newBrem = brem;
		if(newBrem.length()>20) newBrem = newBrem.substring(0,20);
		System.out.println("New brem is " + newBrem);
		
		String whlo = MWS070MIBean.getField("WHLO");
		MMS005MIBean.setField("WHLO", whlo);
		String whloResp = MMS005MIBean.runProgram("GetWarehouse");
		String whloFaci = MMS005MIBean.getField("FACI");
		
		if(zdfaci.trim().equals(whloFaci.trim())) {
		
		MMS850MIBean.setField("PRFL", "*EXE");
		MMS850MIBean.setField("CONO", zdcono);
		MMS850MIBean.setField("E0PA", "WS");
		MMS850MIBean.setField("E065", "WMS");
		MMS850MIBean.setField("WHLO", MWS070MIBean.getField("WHLO").trim());
		System.out.println("setting whsl " + whsll);
		MMS850MIBean.setField("WHSL", whsll); 
		System.out.println("setting whsl " + whsll);
		MMS850MIBean.setField("ITNO", MWS070MIBean.getField("ITNO"));
		MMS850MIBean.setField("REPN", MWS070MIBean.getField("REPN"));
		MMS850MIBean.setField("BREF", MWS070MIBean.getField("BREF"));
		MMS850MIBean.setField("BRE2", MWS070MIBean.getField("BRE2"));
		
		MMS850MIBean.setField("BANO", bano.trim()); System.out.println("Set bano " + bano);
		MMS850MIBean.setField("CAMU", camu.trim());
		
		MMS850MIBean.setField("BREM", newBrem);
		MMS850MIBean.setField("REPN", MWS070MIBean.getField("REPN").trim());
		MMS850MIBean.setField("STAS", stas);
		MMS850MIBean.setField("CALT", "1");
		// not setting qty, date etc, m3 should default these, but test
		

			String reclass = MMS850MIBean.runProgram("AddReclass");
			publishResults+="<li>Warehouse: " + whlo  + ", Lot: " + bano + ", Container: " + camu + ", Remark: " + newBrem + ", M3: " + reclass + "</li>";
		}
	}
}
	
MMS850MIBean.closeConnection();
MMS060MIBean.closeConnection();
MWS070MIBean.closeConnection();
MNS150MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

//now send a mail..

	String host = "SMTP.ORNUA.COM";
	Properties props = new Properties();
	props.setProperty("mail.smtp.host", host);
	props.setProperty("mail.smtp.port", "25");
	Session smtpSession = Session.getDefaultInstance(props);

	try {
     Message message = new MimeMessage(smtpSession);
		message.setFrom(new InternetAddress("Max.Stride@ornua.com"));
		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emal));
		message.setSubject("Hi - Your OOD remarks report - OOD appended to remarks on new status 3 stock passing expiry date (" + urienv + "/" + zdcono + "/" + zddivi + "/" + zdfaci + ")");
		message.setContent(publishResults, "text/html; charset=utf-8");
		message.setSentDate(new java.util.Date()); 
		Transport.send(message);
		//System.out.println("Sent message successfully....");

	} catch (MessagingException e) {
		System.err.println(" Couldnt send oodAuto email " + e);
	}
%>
	