<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.setInitialise("MDBREADMI");
KevBean.setField("STCO", "RSCD");
KevBean.runProgram("SelCSYTAB20");
String stky = ""; String tx40 = "";

String json = "[";
while(KevBean.nextRow()) {
	stky = KevBean.getField("STKY"); if(stky==null) stky = ""; stky = stky.trim();
	tx40 = KevBean.getField("TX40"); if(tx40==null) tx40 = ""; tx40 = tx40.trim();
	json+= "{";
	json+= "\"stky\":\"" + stky + "\",";
	json+= "\"tx40\":\"" + tx40 + " (" + stky + ")" + "\"";
	json+= "},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
