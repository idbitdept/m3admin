<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
//KevBean.changeProg("MRS001MI");

KevBean.setInitialise("MRS001MI");
KevBean.setField("MRCD", "200"); // kw, sometimes more than 100 fields, imagine!
KevBean.runProgram("SetLstMaxRec");

String trnm = request.getParameter("trnm");
String minm = request.getParameter("minm");

String rcvd = request.getParameter("rcvd");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "I"); //input fields

KevBean.runProgram("LstFields");

// build up array of relevant data...
String flnm = ""; String flds = ""; String leng = ""; String frpo = ""; String topo = ""; String type = ""; String mand = "";
String json = "{\"success\":true, \"results\":[ ";
while(KevBean.nextRow()) {
	flnm = KevBean.getField("FLNM"); flds = KevBean.getField("FLDS"); leng = KevBean.getField("LENG"); frpo = KevBean.getField("FRPO"); topo = KevBean.getField("TOPO");
	type = KevBean.getField("TYPE"); mand = KevBean.getField("MAND"); if(mand==null) mand = ""; mand = mand.trim();
	//mod - MRS001 mandatorys are not always really mandatory. Sometimes no means yes. Allow anything in.
	if(trnm.startsWith("AddCOPick") && flnm.startsWith("USD1")) flds = "Cust Req Del Date"; 
	json += "{\"flnm\":\"" + flnm + "\", \"flds\":\"" + flds + "\", \"leng\":" + leng + ", \"frpo\":" + frpo + ", \"topo\":" + topo + ", \"type\":\"" + type + "\", \"mand\":\"" + mand +"\"},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json += "]}";
out.println(json);
%>
