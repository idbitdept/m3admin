<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String cuno = request.getParameter("cuno"); if(cuno==null) cuno = "";
String cunm = request.getParameter("cunm"); if(cunm==null) cunm = "";

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray cunoArray = (JSONArray) obj;

String json = "["; 

Iterator<JSONObject> iterator = cunoArray.iterator();
while (iterator.hasNext()) {
	 JSONObject cunos = (JSONObject) iterator.next();
	 cuno = (String) cunos.get("cuno");
	 cunm = (String) cunos.get("cunm");
	 
	 if(cuno.equals("All")) {
		 KevBean.setInitialise("MDBREADMI");
		 KevBean.setField("DIVI", divi);
		 KevBean.setField("ORSL", "44");
		 KevBean.setField("ORST", "44");
		 KevBean.runProgram("LstOOHEADX5");
		 while(KevBean.nextRow()) {
			json+= "{";
			json+= "\"orno\":\"" + KevBean.getField("ORNO") + "\",";
			json+= "\"ortp\":\"\",";
			json+= "\"rldt\":" + KevBean.getField("RLDT") + ",";
			json+= "\"stat\":" + KevBean.getField("ORST") + ",";
			json+= "\"cuor\":\"" + KevBean.getField("CUOR") + "\",";
			json+= "\"yref\":\"" + KevBean.getField("YREF") + "\",";
			
			json+= "\"cuno\":\"" + KevBean.getField("CUNO") + "\",";
			json+= "\"cunm\":\"\",";
			
			json+= "\"orsl\":" + KevBean.getField("ORSL") + ",";
			json+= "\"orst\":" + KevBean.getField("ORST") + ",";
			json+= "\"dlix\":0";
			json+= "},";
			}
		 //System.out.println("All done");
		 break; 
	 } else {
	 
	KevBean.setInitialise("OIS100MI");
	KevBean.setField("MRCD", "2000"); // kw change from 500 to 2000 as now mins 33
	KevBean.runProgram("SetLstMaxRec");
	
	KevBean.setField("CONO", zdcono);
	KevBean.setField("CUNO", cuno); 
	KevBean.setField("ORSL", "44"); 
	KevBean.setField("ORST", "44");
	KevBean.runProgram("LstHead");
	
	while(KevBean.nextRow()) {
		String status = KevBean.getField("STAT"); int iStatus = Integer.parseInt(status);
		String lowStatus = KevBean.getField("ORSL"); int iLowStatus = Integer.parseInt(lowStatus);
		String highStatus = KevBean.getField("ORST"); int iHighStatus = Integer.parseInt(highStatus);
		String faci = KevBean.getField("FACI"); if(faci==null) faci = "";
		
		if(faci.equals(zdfaci)) {
		
			json+= "{";
			json+= "\"orno\":\"" + KevBean.getField("ORNO") + "\",";
			json+= "\"ortp\":\"" + KevBean.getField("ORTP") + "\",";
			json+= "\"rldt\":" + KevBean.getField("RLDT") + ",";
			json+= "\"stat\":" + status + ",";
			json+= "\"cuor\":\"" + KevBean.getField("CUOR") + "\",";
			json+= "\"yref\":\"" + KevBean.getField("YREF") + "\",";
			
			json+= "\"cuno\":\"" + cuno + "\",";
			json+= "\"cunm\":\"" + cunm + "\",";
			
			json+= "\"orsl\":" + lowStatus + ",";
			json+= "\"orst\":" + highStatus + ",";
			json+= "\"dlix\":" + KevBean.getField("DLIX") + "";
			json+= "},";
		}
	  }
	}
}

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
