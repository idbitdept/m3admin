<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import = "javax.servlet.http.*" %>
<%@ page import = "org.apache.commons.fileupload.*" %>
<%@ page import = "org.apache.commons.fileupload.disk.*" %>
<%@ page import = "org.apache.commons.fileupload.servlet.*" %>
<%@ page import = "org.apache.commons.io.output.*" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.CellType" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>

<%@ page import = "org.json.simple.JSONObject" %>


<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String uname = (String)session.getAttribute("uname");

String minm = request.getParameter("minm"); if(minm==null) minm = "";
String trnm = request.getParameter("trnm"); if(trnm==null) trnm = "";
String resp = "<ul>";
int currentCell = 0;

   File file ;
   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   ServletContext context = pageContext.getServletContext();
   String filePath = context.getInitParameter("file-upload");
   System.out.println("File path is " + filePath);

   // Verify the content type
   String contentType = request.getContentType();
   String fileName = "";

   if ((contentType.indexOf("multipart/form-data") >= 0)) {
      DiskFileItemFactory factory = new DiskFileItemFactory();
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
      // Location to save data that is larger than maxMemSize.
      
      //factory.setRepository(new File("/Users/IDB/Documents/excel/"));
      factory.setRepository(new File("C:\\kw\\excelUploads/"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );

      try { 
         // Parse the request to get file items
         List fileItems = upload.parseRequest(request);
         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         while ( i.hasNext () ) {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () ) {
               // Get the uploaded file parameters
               String fieldName = fi.getFieldName();
               fileName = fi.getName();
               boolean isInMemory = fi.isInMemory();
               long sizeInBytes = fi.getSize();
               // Write the file
               if( fileName.lastIndexOf("\\") >= 0 ) {
                  file = new File( filePath + 
                  fileName.substring( fileName.lastIndexOf("\\"))) ;
               } else {
                  file = new File( filePath + 
                  fileName.substring(fileName.lastIndexOf("\\")+1)) ;
               }
               fi.write( file ) ;
               System.out.println("Uploaded Filename: " + filePath + fileName + "<br />");
            }
         }

         try {

        if(fileName.contains("\\")) {
        	fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
        }
        FileInputStream FIS = new FileInputStream(new File(filePath + fileName));
 
      //FileOutputStream fileOut = new FileOutputStream(new File(filePath + fileName));
        //Create Workbook instance holding reference to .xlsx file

        XSSFWorkbook workbook = new XSSFWorkbook(FIS);
        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        int currentRow = 0; String[] apiFields = new String[100]; //hardly be more than 100 input fields..
        //Iterate through each rows one by one

        Iterator<Row> rowIterator = sheet.iterator();
        
     // change while to if to get the first row...
        if (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            KevBean.setInitialise(minm);

            Iterator<Cell> cellIterator = row.cellIterator();
            currentCell = 0;
 
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();  
                if(currentRow==0) {
	                cell.setCellType(CellType.STRING);
	                apiFields[currentCell] = cell.getStringCellValue();
	                System.out.println("setting pos " + currentCell + " as " + cell.getStringCellValue());
                }
              currentCell++; 
            }
          currentRow++;
        } 

        int lastHeaderCell = currentCell;
        currentCell = 0;

        // now do second row...

 
        while (rowIterator.hasNext()) {

            Row row = rowIterator.next();
            currentCell = 0;
          for(int cn=0; cn<lastHeaderCell; cn++) {

        	Cell cell = row.getCell(cn, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
              String apiValue = "";                   
              cell.setCellType(CellType.STRING);
              apiValue = cell.getStringCellValue();
              if(apiValue==null) apiValue = "";
              if(!apiValue.equals("")) {
	              System.out.println("About to set " + apiFields[currentCell] + " with value " + apiValue);
	              KevBean.setField(apiFields[currentCell].toUpperCase(), apiValue); 
              }
              currentCell++;             
          }

            String mvxResp = KevBean.runProgram(trnm);
            //if(currentRow>0) resp += "<\\li>" + mvxResp + "<\\/li>"; 
            if(currentRow>0) resp += "<li>" + mvxResp + "</li>"; 

            Cell celly = row.createCell(currentCell);
            celly.setCellType(CellType.STRING);

            if(currentRow==0) {             
                celly.setCellValue("M3 Response");
            } else {
                celly.setCellValue(mvxResp);
            }
          currentRow++;
        } 

      sheet.autoSizeColumn(currentCell);
      Row footerRow = sheet.createRow(currentRow++);
              Cell celly = footerRow.createCell(0); celly.setCellValue("Program " + minm + "/" + trnm);
           footerRow = sheet.createRow(currentRow++);
              celly = footerRow.createCell(0); celly.setCellValue("Uploaded by "  + uname);
           footerRow = sheet.createRow(currentRow++);
              celly = footerRow.createCell(0); celly.setCellValue("At " + new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));

      FIS.close();
      
      	FileOutputStream fileOut = new FileOutputStream(new File(filePath + uname + "_" + System.currentTimeMillis() + "_" + fileName));
    	workbook.write(fileOut);
    	fileOut.close();
        workbook.close();
        
    } catch (Exception e) {
      e.printStackTrace();
      //resp += "<\\li>" + KevBean.runProgram(trnm) + "<\\/li>";
      resp += "<li>" + KevBean.runProgram(trnm) + "</li>";
    } 



         resp += "</ul>";

         //out.println("{\"success\":true,\"file\":\"" + fileName + "\",\"resp\":\"" + JSONObject.escape(resp) + "\"}");
out.println("{\"success\":true,\"file\":\"" + fileName + "\",\"resp\":\"" + resp + "\"}");
         //out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

         // now, process it

      } catch(Exception ex) {
         System.out.println(ex);
      }
   }
%>