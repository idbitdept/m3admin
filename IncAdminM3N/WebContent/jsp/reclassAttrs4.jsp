<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,javax.mail.internet.MimeMessage.*,javax.mail.*,javax.mail.internet.*,java.util.Properties" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="ATSBean010" class="com.ornua.MvxBean" scope="page"/>
<%
// use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "ATS101MI";
String localTrans = "SetAttrValue";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setPassword(localPword); 

ATSBean010.setSystem(system);
ATSBean010.setPort(PORT);
ATSBean010.setLib(LIB);
ATSBean010.setCompany(zdcono);
ATSBean010.setUsername(m3user);
ATSBean010.setPassword(m3pass);
ATSBean010.setInitialise("ATS010MI");


String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String aloc = request.getParameter("aloc"); if(aloc==null) aloc = "";
String stas = request.getParameter("stas"); if(stas==null) stas = "";
String uname = (String)session.getAttribute("uname");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>Attribute results";
String publishResultsTable = "<table>";
publishResultsTable += "<table border=1 width=\"800\"><tr><th>Item</th><th>Lot</th><th>Container</th>";

Enumeration<String> parameterNames = request.getParameterNames();
while (parameterNames.hasMoreElements()) {
    String paramName = parameterNames.nextElement();
    if( (!paramName.equals("sdata")) && (!paramName.equals("whlo")) && (!paramName.equals("itno")) ) {
	     String[] paramValues = request.getParameterValues(paramName);
        String paramValue = paramValues[0]; paramValue = paramValue.trim();
        if(!paramValue.equals("")) {
        	publishResultsTable += "<th>" + paramName + "</th>";
        }
    }
}
publishResultsTable+="</tr>";

int lotNum = 0; String resp = ""; boolean keepGoing = true;
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject camus = (JSONObject) iterator.next();
	 camu = (String) camus.get("camu");
	 stas = (String) camus.get("stas");
	 bano = (String) camus.get("bano");
	 whsl = (String) camus.get("whsl");
	 aloc = (String) camus.get("aloc");
	 try {
		 String palItno = (String) camus.get("itno");
		 if(palItno!=null) itno = palItno;
	 } catch (Exception e) {
		 // use header itno
	 }
	 publishResultsTable += "<tr><td>" + itno + "</td><td>" + bano + "</td><td>" + camu + "</td>";
	 
	 // get the atnr for the pallet
	 /*
	 KevBean.setInitialise("MDBREADMI");
	 KevBean.setField("MRCD", "1"); // we only need the first record
	 KevBean.runProgram("SetLstMaxRec");
	 KevBean.setField("CAMU", camu);
	 KevBean.runProgram("LstMITLOCZ7");
	*/
	 KevBean.setInitialise("MMS060MI");
	 //KevBean.setField("MRCD", "1"); // we only need the first record
	 //KevBean.runProgram("SetLstMaxRec");
	 KevBean.setField("CONO", zdcono);
	 KevBean.setField("WHLO", whlo);
	 KevBean.setField("CAMU", camu);
	 KevBean.setField("BANO", bano);
	 KevBean.setField("ITNO", itno);
	 KevBean.setField("WHSL", whsl);
	
	 //if(!KevBean.nextRow()) {
	 if(!KevBean.runProgram("Get").startsWith("OK")) {
		 publishResults += "<li> Pallet " + camu + ": No Attribute number found</li>";
		 //todo reflect this in the table
	 } else {
		 String atnr = KevBean.getField("ATNR");
		 String atnb = KevBean.getField("ATNB");
		 
		 //System.out.println("Atnr: " + atnr);
		 //System.out.println("Atnb: " + atnb);
		 
		 parameterNames = request.getParameterNames();
		 while (parameterNames.hasMoreElements()) {
			 if(!keepGoing) break;
		     String paramName = parameterNames.nextElement();
		     if( (!paramName.equals("sdata")) && (!paramName.equals("whlo")) && (!paramName.equals("itno")) ) {
			     String[] paramValues = request.getParameterValues(paramName);
		         String paramValue = paramValues[0]; paramValue = paramValue.trim();
		         if(!paramValue.equals("")) {
		        	 
		        	String cobt = "";
	        		ATSBean010.setField("ATID", paramName.trim()); //System.out.println(paramName);
	        		if(ATSBean010.runProgram("GetAttribute").startsWith("OK")) {
	        			cobt = ATSBean010.getField("COBT");
	        		}
		        	 
		        	 LocalBean.setInitialise(localProg);
		        	 LocalBean.setField("CONO", zdcono);
		        	 if(cobt.trim().equals("2"))
		    		 	LocalBean.setField("ATNR", atnb);
		        	 else
		        		 LocalBean.setField("ATNR", atnr);
		        	 
			         LocalBean.setField("ATID", paramName);
			         LocalBean.setField("ATVA", paramValue);
					 String attrResp = LocalBean.runProgram("SetAttrValue");
					 if(attrResp.startsWith("NOK            Not allowed")) {
						 keepGoing = false;
						 attrResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
						 publishResults += "<li> Pallet " + camu + ": Attribute: " + paramName + ": " + attrResp + "</li>";
						 //break;
					 }
					 if(attrResp.endsWith("is invalid")) { 
						 attrResp += " (Attribute may be held at different level - check)";
					 }
					 publishResults += "<li> Pallet " + camu + ": Attribute: " + paramName + ": " + attrResp + "</li>";
					 if(attrResp.startsWith("N"))
						 publishResultsTable += "<td style=\"color: red;\">" + paramValue + "<br/>" + attrResp + "</style></td>";
					else
					 	publishResultsTable += "<td>" + paramValue + "</td>";
					 //todo reflect OK/NOK with ermsg
		         }
		     }
		 }
		 publishResultsTable += "</tr>";
 	}
	lotNum++;
}

publishResults = publishResults + "</ul>";
publishResultsTable += "</table>";
//System.out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
out.println("{\"success\":true, \"results\":\"" + JSONObject.escape(publishResultsTable) + "\"}");

String to = (String)session.getAttribute("email"); if(to==null) to = "";
if(!to.equals("")) { // should we bother with CRS111 email lookup if not found on LDAP? you'd imagine it should be on LDAP...
	String host = "SMTP.ORNUA.COM";
	Properties props = new Properties();
	props.setProperty("mail.smtp.host", host);
	props.setProperty("mail.smtp.port", "25");
	Session sess = Session.getDefaultInstance(props);
	
	try {
	   MimeMessage message = new MimeMessage(sess);
	   message.setFrom(new InternetAddress("noreply@ornua.com"));
	   message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	   message.setSubject("Your Attributes Submission for " + zdcono + "/" + divi + " (" + zdfaci + ")");
	   message.setContent(publishResultsTable, "text/html");
	   
	   Transport.send(message);
	   System.out.println("Sent message successfully....");
	} catch (MessagingException mex) {
	   System.out.println("MAx " + mex);
	}
}
%>