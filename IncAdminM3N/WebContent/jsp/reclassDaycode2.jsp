<%@ page contentType="text/html; charset=utf-8" language="java" import="javax.activation.DataHandler.*,javax.mail.internet.*,javax.mail.internet.MimeMessage.*,javax.mail.*,java.util.Properties,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,org.jdom.*,org.jdom.output.XMLOutputter,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="MMS470MIBean" class="com.ornua.MvxBean" scope="page"/>	
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
// todo VJ spotted another bug - we need to update reclassDaycode.jsp to produce one reclass file per pallet - as per reclass.jsp
//use logged-on user's account
// todo VJ is spotting lots of bugs today :) - change filename to use legacy whs code
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "MMS850MI";
String localTrans = "AddReclass";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setPassword(localPword); 

String grpLot = request.getParameter("grpLot"); if(grpLot==null) grpLot = ""; if(grpLot.equals("")) grpLot = "0";
String htmlTable = "<table border=1><tr><th>Old item</th><th>New item</th><th>Lot</th><th>Pallet</th><th>Daycode</th><th>Healthmark</th></tr>";

boolean atLeastOneDiffItem = false;
String nc = request.getParameter("nc"); if(nc==null) nc = "";
String newBrem = request.getParameter("newBrem"); if(newBrem==null) newBrem = ""; newBrem = newBrem.trim();
if(!nc.equals("")) { 
	MMS470MIBean.setSystem(system);
	MMS470MIBean.setPort(PORT);
	MMS470MIBean.setLib(LIB);
	MMS470MIBean.setCompany(zdcono);
	MMS470MIBean.setUsername(m3user);
	MMS470MIBean.setPassword(m3pass);
	MMS470MIBean.setInitialise("MMS470MI");
}

boolean atLeast1ok = false; boolean keepGoing = true;
String newitno = request.getParameter("newitno"); if(newitno==null) newitno = "";

String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String aloc = request.getParameter("aloc"); if(aloc==null) aloc = "";
String brem = request.getParameter("brem"); if(brem==null) brem = "";

String bref = request.getParameter("bref"); if(bref==null) bref = "";
String bre2 = request.getParameter("bre2"); if(bre2==null) bre2 = "";
String stqt = request.getParameter("stqt"); if(stqt==null) stqt = "";
String unms = request.getParameter("unms"); if(unms==null) unms = "";

String stas = request.getParameter("stas"); if(stas==null) stas = "";

String newBre2 = request.getParameter("newBre2"); if(newBre2==null) newBre2 = ""; newBre2 = newBre2.trim();

String newstat = request.getParameter("newStat"); if(newstat==null) newstat = ""; newstat = newstat.trim(); if(newstat.equals("")) newstat = "99";
String calcMethod = request.getParameter("calcMethod"); if(calcMethod==null) calcMethod = ""; calcMethod = calcMethod.trim(); if(calcMethod.equals("99")) calcMethod = "";
String qi = request.getParameter("qi"); if(qi==null) qi = ""; qi = qi.trim(); if(qi.equals("99")) qi = "";

String overrideAlloc = request.getParameter("overrideAlloc"); if(overrideAlloc==null) overrideAlloc = ""; overrideAlloc = overrideAlloc.trim(); if(overrideAlloc.equals("99")) overrideAlloc = "";

String rscd = request.getParameter("rscd"); if(rscd==null) rscd = "";
String uname = (String)session.getAttribute("uname"); String lastBano = "";

Element root;
Document doc;
root = new Element("STOCK_RECLASS");
Element whouse = new Element("IDB_WAREHOUSE").setText(whlo);
root.addContent(whouse);

//KevBean.setInitialise("MMS850MI");
LocalBean.setInitialise(localProg);
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>Results of Reclassifications";
String res = "[ ";

int lotNum = 0; String resp = ""; String newBano = ""; String nitno = ""; String itno = "";
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject camus = (JSONObject) iterator.next();
	 camu = (String) camus.get("camu");
	 stas = (String) camus.get("stas");
	 if(!newstat.equals("99")) stas = newstat; //System.out.println("stas is " + stas);
	 bano = (String) camus.get("bano");
	 whsl = (String) camus.get("whsl");
	 aloc = (String) camus.get("aloc");
	 brem = (String) camus.get("brem");
	 
	 bref = (String) camus.get("bref"); if(bref==null) bref = "";
	 bre2 = (String) camus.get("bre2"); if(bre2==null) bre2 = "";
	 
	 stqt = (String) camus.get("stqt"); if(stqt==null) stqt = "";
	 unms = (String) camus.get("unms"); if(unms==null) unms = "";
	 itno = (String) camus.get("itno"); if(itno==null) itno = "";
	 
	 boolean diffItem = true; if(itno.equals(newitno) || newitno.equals("")) diffItem = false;
	 
	 if(diffItem) atLeastOneDiffItem = true;

	 LocalBean.setField("PRFL", "*EXE");
	 LocalBean.setField("CONO", zdcono);
	
	if(system.startsWith("m3")) {
		LocalBean.setField("E0PA", "CUL");
		LocalBean.setField("E065", "31");
	} else {
		LocalBean.setField("E0PA", "1");
		LocalBean.setField("E065", "WMS");
	}

	LocalBean.setField("WHLO", whlo);
	LocalBean.setField("ITNO", itno);
	LocalBean.setField("CAMU", camu);
	LocalBean.setField("WHSL", whsl);
	LocalBean.setField("STAS", stas);
	LocalBean.setField("BANO", bano);
	LocalBean.setField("NITN", newitno);
	
	//
	LocalBean.setField("NBAN", newBano);
	//KevBean.setField("NBAN", bano);
	//31 July, 2019, KW in Leek, UK, raining, 7pm. Ahead of a trip to the cock inn for drinks and the indian for a great night
	// Karel the legend Ternest requests QRBS to be removed. Can't recall exactly why it was put in..
	//KevBean.setField("QRBS", "1");
			
	// oct 4 2019 use qi from screen
	System.out.println("Lot seq: " + lotNum + " qi: " + qi);
	LocalBean.setField("QRBS", qi);
			
	if(overrideAlloc.equals(""))
		LocalBean.setField("ALOC", aloc);
	else
		LocalBean.setField("ALOC", overrideAlloc);
	
	LocalBean.setField("RESP", uname); System.out.println("RECLASS DONE BY " + uname + " in " + env + " " + divi + " " + zdcono);	
	LocalBean.setField("RSCD", rscd);
	
	if(newBrem.equals(""))
		LocalBean.setField("BREM", brem);
	else
		LocalBean.setField("BREM", newBrem);
	// kw pass in bref and bre2 as given.. sometimes they get lost...
	LocalBean.setField("BREF", bref);
	if(newBre2.equals(""))
		LocalBean.setField("BRE2", bre2);
	else
		LocalBean.setField("BRE2", newBre2);

	LocalBean.setField("CALT", calcMethod);
	
	//System.out.println(resp + " l " + lotNum + " " + grpLot);
	resp = LocalBean.runProgram(localTrans); System.out.println("Resp:" + resp);
	if(resp.startsWith("NOK            Not allowed")) {
		 keepGoing = false;
		 resp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;			 
		 //break;
	 }
	if(resp.startsWith("OK")) { 
		atLeast1ok = true;
		htmlTable += "<tr><td>" + itno + "</td><td>" + newitno + "</td><td>" + bano + "</td><td>" + camu + "</td><td>" + bref + "</td><td>" + bre2 + "</td></tr>";
		if(diffItem) {
			Element pallet = new Element("PALLET");
			Element frmItno = new Element("FROM_ITEM").setText(itno);
			pallet.addContent(frmItno);
			Element toItno = new Element("TO_ITEM").setText(newitno);
			pallet.addContent(toItno);
			pallet.addContent(new Element("PALLET_CODE").setText(camu));
			pallet.addContent(new Element("LOT_CODE").setText(bano));
			pallet.addContent(new Element("HEALTHMARK").setText(bre2));
			pallet.addContent(new Element("PRODUCTION_CODE").setText(bref));
			pallet.addContent(new Element("RECEIVED_QUANTITY").setText(stqt));
			pallet.addContent(new Element("UNIT_OF_MEASURE").setText(unms));
			root.addContent(pallet);
		}
	}
	
	String addReclassStart = "";
	if(resp.length()>0) addReclassStart = resp.substring(0,1);
	
	System.out.println("Duff: " + lotNum + grpLot + resp);
	if( (lotNum==0) & (grpLot.equals("true")) && ( (resp.startsWith("O")) || (resp.startsWith("mvxRecv")) ) ) {
	//if( (resp.startsWith("OK")) && (lotNum==0) & (grpLot.equals("true")) ) {
		nitno = ""; if(!newitno.equals("")) nitno = newitno; else nitno = itno;
		qi = ""; // if moving multiple lots to a single one, take QMS tests from first lot
		//calcMethod = "";
		System.out.println("qi now: " + qi + " resp: " + resp + " lotNum: " + lotNum + " grpLot: " + grpLot);
		KevBean.setInitialise("MMS060MI");
		KevBean.setField("WHLO", whlo);
		KevBean.setField("CAMU", camu);
		KevBean.setField("ITNO", nitno);
		//System.out.println("WHLO " + whlo + " CAMU " + camu + " nitno " + nitno);
		KevBean.runProgram("LstContainer");
		if(KevBean.nextRow()) {		
			newBano = KevBean.getField("BANO");
			System.out.println("Run " + lotNum + ": New bano is " + newBano);
		}
		//KevBean.setInitialise("MMS850MI");
	}
	
	String ncResp = "";
	if( (resp.startsWith("OK")) && (!nc.equals("")) ) {
		MMS470MIBean.setField("CONO", zdcono);
		MMS470MIBean.setField("PANR", camu);
		MMS470MIBean.setField("DLRM", nc);
		ncResp = " - NC update: " + MMS470MIBean.runProgram("ChangePackStk");
	}
	
	res+="{\"resp\":\"" + addReclassStart + "\", \"newStat\":\"" + newstat + "\"},";

	publishResults = publishResults + "<li> Pallet " + camu + ": " + resp + ncResp + "</li>";
	
	lotNum++;
}


	// get last lot...
	
	if(!calcMethod.equals("1")) {
	
		KevBean.setInitialise("MMS060MI");
		KevBean.setField("WHLO", whlo);
		KevBean.setField("CAMU", camu);
		KevBean.setField("ITNO", nitno);
		KevBean.runProgram("LstContainer");
		if(KevBean.nextRow()) {		
			lastBano = KevBean.getField("BANO");
		}
	}
		if(lastBano==null) lastBano = ""; if(lastBano.equals("")) lastBano = bano;
	//

publishResults = publishResults + "</ul>";
if(res.length()>2) res = res.substring(0, res.length()-1);
res+=" ]";
out.println("{\"success\":true, \"res\":" + res + ",\"results\":\"" + publishResults + "\", \"lastBano\":\"" + lastBano + "\"}");

if(atLeastOneDiffItem && atLeast1ok) {
	
	String toWhsEmail = ""; String as2 = ""; String wf11 = "";
	KevBean.setInitialise("MDBREADMI");
	KevBean.setField("WHLO", whlo);
	if(KevBean.runProgram("GetOSTORE00").startsWith("O")) {
		toWhsEmail = KevBean.getField("WF07"); if(toWhsEmail==null) toWhsEmail = ""; toWhsEmail = toWhsEmail.trim();
		as2 = KevBean.getField("WF03"); if(as2==null) as2 = ""; as2 = as2.trim();
		wf11 = KevBean.getField("WF11"); if(wf11==null) wf11 = ""; wf11 = wf11.trim(); if(wf11.length()==1) wf11 = "00" + wf11; else if(wf11.length()==2) wf11 = "0" + wf11;
		//System.out.println("send to warehouse email: " + toWhsEmail);
	}
	
	if(!as2.equals("")) { // send XML to folder...
		doc = new Document(root);
		String filename = "STOCK_RECLASS_" + wf11 + "_" + itno + "_" + newitno + "_" + System.currentTimeMillis() + ".xml";
		String folder = GISroot + "as2/evolve/outbound/" + as2 + "/";
		try {
			FileOutputStream out1 = new FileOutputStream(folder + filename);
			XMLOutputter serializer = new XMLOutputter();
			serializer.output(doc, out1);
			out1.flush(); out1.close();
			
			
		} catch (Exception e) {
			System.err.println("Exception writing file out..." + e);
		}
	}
	
	String from = "";
	if(email.equals("")) {
		KevBean.setInitialise("CRS111MI");
		KevBean.setField("EMTP", "04");
		KevBean.setField("EMKY", (String)session.getAttribute("uname"));
		String crs111Resp = KevBean.runProgram("Get");
		if(crs111Resp.startsWith("OK")) {
			email = KevBean.getField("EMAL"); if(email==null) email = "";
			session.setAttribute("email", email);
		}
	}
	
	if(email.equals("")) from = "noreply@ornua.com"; else from = email;
	
	if(!email.equals("") || !toWhsEmail.equals("")) {
		
		    String host = "SMTP.ORNUA.COM";
		    Properties props = new Properties();
		    props.setProperty("mail.smtp.host", host);
		    props.setProperty("mail.smtp.port", "25");
		    Session sess = Session.getDefaultInstance(props);
		
		    try {
		       MimeMessage message = new MimeMessage(sess);
		       message.setFrom(new InternetAddress(from));
		       if(!email.equals(""))
		        	message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
		       if(!toWhsEmail.equals(""))
		       		message.addRecipient(Message.RecipientType.TO, new InternetAddress(toWhsEmail));
		       //System.out.println("From: " + from + " to1: " + email + " to2: " + toWhsEmail);
		       message.setSubject("Item code changes on Ornua stock in your warehouse");
		       //System.out.println(htmlTable);
		       message.setContent(
		               "<h1>Item code has been changed on these pallets:</h1><br/>" + htmlTable + "</table>",
		              "text/html");
		       
		       Transport.send(message);
		       System.out.println("Sent message successfully....");
		    } catch (MessagingException mex) {
		       System.out.println("MAx " + mex);
		    }
		}
	}
%>
	