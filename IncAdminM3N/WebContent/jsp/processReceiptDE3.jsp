<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,java.net.*,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<%@ include file="Connections/connections.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="MDBReadBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="DubBean" class="com.ornua.MvxBean" scope="page"/>
<%

MDBReadBean.setSystem(system);
MDBReadBean.setPort(PORT);
MDBReadBean.setLib(LIB);
MDBReadBean.setCompany(zdcono);
MDBReadBean.setUsername(m3user);
MDBReadBean.setPassword(m3pass);
MDBReadBean.setInitialise("MDBReadMI");
MDBReadBean.setField("MRCD", "2");
MDBReadBean.runProgram("SetLstMaxRec");

// todo remove hardcode
int iDubCono = 602;
DubBean.setSystem("beckett.idb.ie");
DubBean.setPort(6602);
DubBean.setLib("IDBCDTA" + iDubCono);
DubBean.setCompany(iDubCono);
DubBean.setUsername("IDBWEBUAT");
DubBean.setPassword("GISTODB2");
DubBean.setInitialise("MHS850MI");

int liveOrTest = 0;
if(uricono.equals("PRD")) liveOrTest = 1;

KevBean.changeProg("PPS001MI");
String json = "[";

String po = request.getParameter("po"); if(po==null) po = "";
String pnli = request.getParameter("pnli"); if(pnli==null) pnli = "";
String zcnc = request.getParameter("zcnc"); if(zcnc==null) zcnc = "";
String doNum = request.getParameter("doNum"); if(doNum==null) doNum = "";
String doLine = request.getParameter("doLine"); if(doLine==null) doLine = "";
String dlix = request.getParameter("dlix"); if(dlix==null) dlix	 = "";
String trsh = request.getParameter("trsh"); if(trsh==null) trsh = ""; trsh = trsh.trim();
String fromWhs = request.getParameter("fromWhs"); if(fromWhs==null) fromWhs = ""; fromWhs = fromWhs.trim();
String poItno = request.getParameter("poItno"); if(poItno==null) poItno = ""; poItno = poItno.trim();

// if trsh 66 (69?) update dublin DOReceipt...

boolean chkCompletePO = Boolean.parseBoolean(request.getParameter("chkCompletePO"));
int totValPallets = Integer.parseInt(request.getParameter("totValPallets"));

boolean allOKs = true; String closeMsg = "";

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);
//System.out.println("Today is " + today);
String bano = ""; 
String mdbBano = ""; String mdbCamu = ""; String currPrdt = "";
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray pallets = (JSONArray) obj;

int i = 0; int actReceipts = 0;
Iterator<JSONObject> iterator = pallets.iterator();
while (iterator.hasNext()) {
	 JSONObject pallet = (JSONObject) iterator.next();
	 
	 long qtyDE = (Long)pallet.get("qtyDE");
	 long qtyIE = (Long)pallet.get("qtyIE");
	 
	 //System.out.println("i is " + i + " qty: " + qty); 
	 i++;
	 if(qtyDE>0) {
		 actReceipts++;
	 
	 String prdt = (String)pallet.get("prdt"); if(prdt==null) prdt = ""; prdt = prdt.trim();
	 String bre2 = (String)pallet.get("healthmark"); if(bre2==null) bre2 = ""; bre2 = bre2.trim();
	 String bref = (String)pallet.get("daycode"); if(bref==null) bref = ""; bref = bref.trim();
	 String itno = (String)pallet.get("itno"); if(itno==null) itno = "";
	 String brem = (String)pallet.get("brem"); if(brem==null) brem = "";
	 String brefTxt = (String)pallet.get("brefTxt"); if(brefTxt==null) brefTxt = "";
	 
	 
	 
		 KevBean.setField("CONO", uricono); System.out.println("FR cono" + uricono);
		 KevBean.setField("TRDT", today.trim()); System.out.println("FR today"  + today);
		 KevBean.setField("RESP", "IDBKEWO"); //hmm
		 //KevBean.setField("RESP", (String)session.getAttribute("uname"));
		 KevBean.setField("PUNO", po.trim()); System.out.println("FR po " + po);
		 KevBean.setField("PNLI", pnli.trim()); System.out.println("FR pnli" + pnli);
		 //KevBean.setField("PNLS", "0");
		 KevBean.setField("RVQA", qtyDE + "");
		 //KevBean.setField("WHLO", "IDB"); // get from order line
		 KevBean.setField("SUDO", doNum); 
		 KevBean.setField("PRDT", prdt.trim()); System.out.println("prdt"  + prdt.trim());
		 KevBean.setField("BREF", brefTxt);
		 KevBean.setField("CAMU", bref);
		 KevBean.setField("BRE2", bre2);
		 KevBean.setField("BREM", brem); //defaults to ZCNC on Line page, can be overridden
		 
		 System.out.println("ZCNC" + zcnc);
		 // generate unique lot by manufacture date..
		 
		 if(prdt.equals(currPrdt)) KevBean.setField("BANO", mdbBano);
		 else KevBean.setField("BANO", "");
		 currPrdt = prdt.trim();

		 
		 /*
		 if( (chkCompletePO) && (actReceipts==totValPallets) && (allOKs) ) {
			 KevBean.setField("OEND", "1");
			 closeMsg = " PO now marked complete";
		 }
		 */
	
		 String msg = KevBean.runProgram("Receipt");		 
		 System.out.println("message is " + msg);
		 
		 boolean ok = false; 
		 if(msg.startsWith("OK")) {
			 ok = true;
			 
			 // do DOReceipt also if status 66/69
			 if( trsh.equals("66") || trsh.equals("69") ) {
				 DubBean.setField("PRFL", "*EXE");
				 DubBean.setField("CONO", iDubCono + "");
				 DubBean.setField("WHLO", "092");
				 DubBean.setField("E0PA", "1");
				 DubBean.setField("E065", "WMS");
				 DubBean.setField("TWHL", fromWhs);
				 DubBean.setField("ITNO", poItno);
				 DubBean.setField("WHSL", "WHS");
				 DubBean.setField("CAMU", bref); // lot ref 1 in DE is CAMU in IRL
				 DubBean.setField("QTY", qtyIE + "");
				 DubBean.setField("RIDN", doNum);
				 DubBean.setField("RIDO", "5");
				 DubBean.setField("RIDL", Integer.parseInt(doLine)*100 + ""); 
				 DubBean.setField("RIDI", dlix);
				 DubBean.setField("USD2", bre2);
				 DubBean.setField("BRE2", bre2);
				 DubBean.setField("BREF", brem);
				 DubBean.setField("USID", (String)session.getAttribute("uname"));
				 //

				 String dubMsg = DubBean.runProgram("AddDOReceipt");
				 msg+= " Dub: " + dubMsg;
			 }
			
			 // get lot and pallet (bano, camu)
			 int matches = 0;
			 
			 /*
			 mdbBano = ""; mdbCamu = "";
			 MDBReadBean.setInitialise("MDBReadMI");
			 MDBReadBean.setField("MRCD", "2");
			 MDBReadBean.runProgram("SetLstMaxRec");
			 MDBReadBean.setField("ITNO", itno);
			 MDBReadBean.setField("BRE2", bre2);
			 MDBReadBean.setField("BREF", bref);
			 String mdbResp = MDBReadBean.runProgram("LstMITLOCV8");
			 
			 while(MDBReadBean.nextRow()) {
				 String thisBref = MDBReadBean.getField("BREF").trim();
				 String thisBre2 = MDBReadBean.getField("BRE2").trim();
				 String thisItno = MDBReadBean.getField("ITNO").trim();
				 
				 //System.out.println("Check " + thisBref + thisBre2 + thisItno);
				 //System.out.println("Against " + bref + bre2 + itno);
				 if( 
					 (bref.trim().equals(thisBref)) &&
					 (bre2.trim().equals(thisBre2)) &&
					 (itno.trim().equals(thisItno))
				) {
					 mdbBano = MDBReadBean.getField("BANO");
					 mdbCamu = MDBReadBean.getField("CAMU");
					 matches++;
				 }
			 }
			 */
			 
			msg+= " Lot: " + mdbBano + " Container: " + mdbCamu;
			msg+= " Container: " + bref;
			/*
			if(matches>1)
				msg+= " Warning - duplicate receipt ";
			*/
			 
			 
		 } else {
			allOKs = false;
		 }
		
		
		String msgn = ""; //String msg = ""; 
		
		
		//json+= "{\"msg\":\"" + msgn + "\",\"msgn\":\"" + msg + closeMsg +
		//	" Lot: " + mdbBano + " Container: " + mdbCamu + "\", \"ok\":" + ok + 	
		//	"},";
	 	//msg = "";
	 	json+= "{\"msg\":\"" + msg + "\",\"msgn\":\"" + msg + "\"},";
	 } else {
		 json+= "{\"msg\":\"\",\"msgn\":\"\", \"ok\":true},";
	 }
} // end looping through the JSON array

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json); 
//System.out.print(json);

%>