<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="QMS201" class="com.ornua.MvxBean" scope="page"/>
<%
KevBean.setInitialise("QMS200MI");

QMS201.setSystem(system);
QMS201.setPort(PORT);
QMS201.setLib(LIB);
QMS201.setCompany(zdcono);
QMS201.setUsername(m3user);
QMS201.setPassword(m3pass);
QMS201.setInitialise("QMS201MI");

String itno = request.getParameter("itno"); if(itno==null) itno = ""; itno = itno.trim();
//KevBean.setField("CONO", zdcono);
KevBean.setField("ITNO", itno);
String json = "{\"success\":true, \"results\":[ ";
KevBean.runProgram("LstSpecsByItem");
String spec = ""; String qse1 = ""; String thisItno = "";

while(KevBean.nextRow()) {
	thisItno = KevBean.getField("ITNO"); if(thisItno==null) thisItno = null; thisItno = thisItno.trim();
	if(!thisItno.equals(itno)) break;
	spec = KevBean.getField("SPEC"); if(spec==null) spec = "";
	qse1 = KevBean.getField("QSE1"); if(qse1==null) qse1 = "";
	QMS201.setField("SPEC", spec);
	QMS201.setField("QSE1", qse1);
	QMS201.setField("ITNO", itno);
	QMS201.runProgram("LstSpecTest");
	

	while(QMS201.nextRow()) {
		
		String frti = QMS201.getField("FRTI"); if(frti==null) frti = "";
		int ifrti = 1;
		try {
			ifrti = Integer.parseInt(frti);
		} catch (Exception e) {
			// not much to be done
			System.err.println("frti error " + e);
		}
		for (int t=1;t<(ifrti+1);t++) {
			String desc = QMS201.getField("TX40") + " (" + QMS201.getField("QTST") + ")";
			if(t>1) desc += " - " + t;
			json += "{";
			json += "\"spec\":\"" + spec + "\",";
			json += "\"qtst\":\"" + QMS201.getField("QTST") + "\",";
			
			json += "\"tseq\":" + t + ",";
			
			json += "\"qop1\":" + QMS201.getField("QOP1") + ",";
			
			json += "\"tsty\":" + QMS201.getField("TSTY") + ",";
			json += "\"atid\":\"" + QMS201.getField("ATID") + "\",";
			json += "\"desc\":\"" + desc + "\"";
			json += "},";
		}
	}
} 
if(json.length()>2) json = json.substring(0, json.length()-1);
json += "]}";
out.println(json);
%>
