<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String itno = request.getParameter("itno"); if(itno==null) itno = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String qtst = request.getParameter("qtst"); if(qtst==null) qtst = "";
String qtrs = request.getParameter("qtrs"); if(qtrs==null) qtrs = ""; //number result

String qse1 = request.getParameter("qse1"); if(qse1==null) qse1 = "";
String spec = request.getParameter("spec"); if(spec==null) spec = "";

String tsty = request.getParameter("tsty"); if(tsty==null) tsty = "";
String qrid = request.getParameter("qrid"); if(qrid==null) qrid = "";

String newQop1 = request.getParameter("newQop1"); if(newQop1==null) newQop1 = "";
String oldQop1 = request.getParameter("oldQop1"); if(oldQop1==null) oldQop1 = "";

String currFrti = request.getParameter("currFrti"); if(currFrti==null) currFrti = "";
if(currFrti.equals("")) currFrti = "1"; //sequence no, default 1

KevBean.setInitialise("QMS400MI");
KevBean.setField("FACI", zdfaci);
KevBean.setField("ITNO", itno);
KevBean.setField("BANO", bano);
KevBean.setField("QRID", qrid);
KevBean.setField("QTST", qtst);

KevBean.setField("TSTY", tsty);

if(tsty.equals("0")) {
	KevBean.setField("QTRS", qtrs);
	KevBean.setField("QOP1", newQop1);
} else {
	KevBean.setField("QLCD", qtrs);
}


//KevBean.setField("TSTY", "0"); //revisit, test type.. 
KevBean.setField("TSEQ", currFrti);
KevBean.setField("VLEN", "1");
KevBean.setField("UPCT", "1");

String qmsResp = KevBean.runProgram("UpdTestResult");

int inewStat = -1;
if(qmsResp.startsWith("OK")) {
	KevBean.setInitialise("QMS302MI");
	KevBean.setField("FACI", zdfaci);
	KevBean.setField("ITNO", itno);
	KevBean.setField("BANO", bano);
	KevBean.setField("SPEC", spec);
	KevBean.setField("QSE1", qse1);
	KevBean.setField("QTE1", qse1);
	KevBean.setField("QTST", qtst);
	
	KevBean.setField("QRID", qrid);
	KevBean.setField("TSTY", tsty);
	String qms302Resp = KevBean.runProgram("GetTestQIReq");
	if(qms302Resp.startsWith("OK")) {
		String newStat = KevBean.getField("TSTT");
		inewStat = Integer.parseInt(newStat);
	}
}
out.println("{\"success\":true,\"newstat\":" + inewStat + ",\"qmsResp\":\"" + qmsResp + "\",\"ok\":" + qmsResp.startsWith("OK") + "}");
%>
	