<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.setInitialise("QMS400MI");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray apiArray = (JSONArray) obj;

String minm = ""; boolean multiples = false;
Iterator<JSONObject> iterator = apiArray.iterator();
while (iterator.hasNext()) {
	 JSONObject apis = (JSONObject) iterator.next();
	 
	 String itno = (String)apis.get("itno"); if(itno==null) itno = "";
	 String bano = (String)apis.get("bano"); if(bano==null) bano = "";
	 String qtst = (String)apis.get("qtst");if(qtst==null) qtst = "";
	 String qtrs = (String)apis.get("qtrs");if(qtrs==null) qtrs = "";
	 String qse1 = (String)apis.get("qse1"); if(qse1==null) qse1 = "";
	 String spec = (String)apis.get("spec"); if(spec==null) spec = "";
	 String tsty = (String)apis.get("tsty"); if(tsty==null) tsty = "";
	 String qrid = (String)apis.get("qrid"); if(qrid==null) qrid = "";
	 String newQop1 = (String)apis.get("newQop1"); if(newQop1==null) newQop1 = "";
	 String oldQop1 = (String)apis.get("oldQop1"); if(oldQop1==null) oldQop1 = "";
	 String currFrti = (String)apis.get("currFrti"); if(currFrti==null) currFrti = "";
	 if(currFrti.equals("")) currFrti = "1"; //sequence no, default 1
	 
KevBean.setField("FACI", zdfaci);
KevBean.setField("ITNO", itno);
KevBean.setField("BANO", bano);
KevBean.setField("QRID", qrid);
KevBean.setField("QTST", qtst);
KevBean.setField("TSTY", tsty);

if(tsty.equals("0")) {
	KevBean.setField("QTRS", qtrs);
	KevBean.setField("QOP1", newQop1);
} else {
	KevBean.setField("QLCD", qtrs);
}

//KevBean.setField("TSTY", "0"); //revisit, test type.. 
KevBean.setField("TSEQ", currFrti);
KevBean.setField("VLEN", "1");
KevBean.setField("UPCT", "1");
String qmsResp = KevBean.runProgram("UpdTestResult");

}
out.println("{\"success\":true,\"ok\":true}");
%>
	