<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="MMS470MIBean" class="com.ornua.MvxBean" scope="page"/>	
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "MMS850MI";
String localTrans = "AddReclass";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
LocalBean.setUsername(localDom + "\\" + localUname); 
LocalBean.setPassword(localPword); 
String grpLot = request.getParameter("grpLot"); if(grpLot==null) grpLot = ""; if(grpLot.equals("")) grpLot = "0";

String nc = request.getParameter("nc"); if(nc==null) nc = "";
String newBrem = request.getParameter("newBrem"); if(newBrem==null) newBrem = ""; newBrem = newBrem.trim();
if(!nc.equals("")) { 
	MMS470MIBean.setSystem(system);
	MMS470MIBean.setPort(PORT);
	MMS470MIBean.setLib(LIB);
	MMS470MIBean.setCompany(zdcono);
	MMS470MIBean.setUsername(m3user);
	MMS470MIBean.setPassword(m3pass);
	MMS470MIBean.setInitialise("MMS470MI");
}

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String newitno = request.getParameter("newitno"); if(newitno==null) newitno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String aloc = request.getParameter("aloc"); if(aloc==null) aloc = "";
String brem = request.getParameter("brem"); if(brem==null) brem = "";

String bref = request.getParameter("bref"); if(bref==null) bref = "";
String bre2 = request.getParameter("bre2"); if(bre2==null) bre2 = "";

String stas = request.getParameter("stas"); if(stas==null) stas = "";

String newstat = request.getParameter("newStat"); if(newstat==null) newstat = ""; newstat = newstat.trim(); if(newstat.equals("")) newstat = "99";
String calcMethod = request.getParameter("calcMethod"); if(calcMethod==null) calcMethod = ""; calcMethod = calcMethod.trim(); if(calcMethod.equals("99")) calcMethod = "";
String qi = request.getParameter("qi"); if(qi==null) qi = ""; qi = qi.trim(); if(qi.equals("99")) qi = "";

String overrideAlloc = request.getParameter("overrideAlloc"); if(overrideAlloc==null) overrideAlloc = ""; overrideAlloc = overrideAlloc.trim(); if(overrideAlloc.equals("99")) overrideAlloc = "";


String rscd = request.getParameter("rscd"); if(rscd==null) rscd = "";

String uname = (String)session.getAttribute("uname"); String lastBano = "";


LocalBean.setInitialise(localProg);
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>Results of Reclassifications";
String res = "[ ";

int lotNum = 0; String resp = ""; String newBano = ""; String nitno = ""; boolean keepGoing = true;
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	//if(!keepGoing) break;
	 JSONObject camus = (JSONObject) iterator.next();
	 camu = (String) camus.get("camu");
	 stas = (String) camus.get("stas");
	 if(!newstat.equals("99")) stas = newstat; //System.out.println("stas is " + stas);
	 bano = (String) camus.get("bano");
	 whsl = (String) camus.get("whsl");
	 aloc = (String) camus.get("aloc");
	 brem = (String) camus.get("brem");
	 
	 bref = (String) camus.get("bref"); if(bref==null) bref = "";
	 bre2 = (String) camus.get("bre2"); if(bre2==null) bre2 = "";

	 LocalBean.setField("PRFL", "*EXE");
	 LocalBean.setField("CONO", zdcono);
	
	if(system.startsWith("m3")) {
		LocalBean.setField("E0PA", "CUL");
		LocalBean.setField("E065", "31");
	} else {
		LocalBean.setField("E0PA", "1");
		LocalBean.setField("E065", "WMS");
	}
	
	
	
	
	LocalBean.setField("WHLO", whlo);
	LocalBean.setField("ITNO", itno);
	LocalBean.setField("CAMU", camu);
	LocalBean.setField("WHSL", whsl);
	LocalBean.setField("STAS", stas);
	LocalBean.setField("BANO", bano);
	LocalBean.setField("NITN", newitno);
	
	//
	LocalBean.setField("NBAN", newBano);
	//KevBean.setField("NBAN", bano);
	//31 July, 2019, KW in Leek, UK, raining, 7pm. Ahead of a trip to the cock inn for drinks and the indian for a great night
	// Karel the legend Ternest requests QRBS to be removed. Can't recall exactly why it was put in..
	//KevBean.setField("QRBS", "1");
			
	// oct 4 2019 use qi from screen
	LocalBean.setField("QRBS", qi);
			
	if(overrideAlloc.equals(""))
		LocalBean.setField("ALOC", aloc);
	else
		LocalBean.setField("ALOC", overrideAlloc);
	
	LocalBean.setField("RESP", uname);
	LocalBean.setField("RSCD", rscd);
	
	if(newBrem.equals(""))
		LocalBean.setField("BREM", brem);
	else
		LocalBean.setField("BREM", newBrem);
	// kw pass in bref and bre2 as given.. sometimes they get lost...
	LocalBean.setField("BREF", bref);
	LocalBean.setField("BRE2", bre2);

	LocalBean.setField("CALT", calcMethod);
	
	//System.out.println(resp + " l " + lotNum + " " + grpLot);
	resp = LocalBean.runProgram(localTrans);
	if(resp.startsWith("NOK            Not allowed")) {
		 keepGoing = false;
		 resp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;			 
		 //break;
	 }
	
	String addReclassStart = "";
	if(resp.length()>0) addReclassStart = resp.substring(0,1);
	
	if( (resp.startsWith("OK")) && (lotNum==0) & (grpLot.equals("true")) ) {
		qi = "";
		nitno = ""; if(!newitno.equals("")) nitno = newitno; else nitno = itno;
		KevBean.setInitialise("MMS060MI");
		KevBean.setField("WHLO", whlo);
		KevBean.setField("CAMU", camu);
		KevBean.setField("ITNO", nitno);
		//System.out.println("WHLO " + whlo + " CAMU " + camu + " nitno " + nitno);
		KevBean.runProgram("LstContainer");
		if(KevBean.nextRow()) {		
			newBano = KevBean.getField("BANO");
		}
		//KevBean.setInitialise(localProg);
	}
	
	String ncResp = "";
	if( (resp.startsWith("OK")) && (!nc.equals("")) ) {
		MMS470MIBean.setField("CONO", zdcono);
		MMS470MIBean.setField("PANR", camu);
		MMS470MIBean.setField("DLRM", nc);
		ncResp = " - NC update: " + MMS470MIBean.runProgram("ChangePackStk");
	}
	
	res+="{\"resp\":\"" + addReclassStart + "\", \"newStat\":\"" + newstat + "\"},";

	publishResults = publishResults + "<li> Pallet " + camu + ": " + resp + ncResp + "</li>";
	
	lotNum++;
}


	// get last lot...
	
	if(!calcMethod.equals("1")) {
	
		KevBean.setInitialise("MMS060MI");
		KevBean.setField("WHLO", whlo);
		KevBean.setField("CAMU", camu);
		KevBean.setField("ITNO", nitno);
		KevBean.runProgram("LstContainer");
		if(KevBean.nextRow()) {		
			lastBano = KevBean.getField("BANO");
		}
	}
		if(lastBano==null) lastBano = ""; if(lastBano.equals("")) lastBano = bano;
	//

publishResults = publishResults + "</ul>";
if(res.length()>2) res = res.substring(0, res.length()-1);
res+=" ]";
out.println("{\"success\":true, \"res\":" + res + ",\"results\":\"" + publishResults + "\", \"lastBano\":" + lastBano + "}");

%>