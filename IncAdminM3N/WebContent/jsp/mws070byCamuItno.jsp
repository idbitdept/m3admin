<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.*,java.text.DecimalFormat" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="OIS100MIBean" class="com.ornua.MvxBean" scope="page"/>
<%
OIS100MIBean.setSystem(system);
OIS100MIBean.setPort(PORT);
OIS100MIBean.setLib(LIB);
OIS100MIBean.setCompany(zdcono);
OIS100MIBean.setUsername(m3user);
OIS100MIBean.setPassword(m3pass);

String json = "[";

String camu = request.getParameter("camu"); if(camu==null) camu = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String itno = request.getParameter("itno"); if(itno==null) itno = "";

String lastCamuDate = ""; String lastCamuTime = ""; 
String thisCuno = ""; String cunm = "";

KevBean.setInitialise("MDBREADMI");

if(!camu.equals("")) {
KevBean.setField("CAMU", camu);
KevBean.setField("ITNO", itno);
KevBean.runProgram("LstMITTRAU4K1");
} else {
	KevBean.setField("BANO", bano);
	KevBean.runProgram("LstMITTRA25");
}

while(KevBean.nextRow()) {
	if(!camu.trim().equals(KevBean.getField("CAMU").trim())) break;
	String trdt = KevBean.getField("TRDT"); if(trdt==null) trdt = ""; lastCamuDate = trdt;
	String trtm = KevBean.getField("TRTM"); if(trtm==null) trtm = ""; lastCamuTime = trtm;
	String trqt = KevBean.getField("TRQT"); if(trqt==null) trqt = ""; if(trqt.equals("")) trqt = "0";
	
	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
    String ftrqt = trqt;
    
    try {
    	ftrqt = decimalFormat.format(Double.valueOf(trqt));
    } catch (Exception e){
    	; // nothing to do...
    }
    
	String unms = KevBean.getField("UNMS"); if(unms==null) unms = "";
	String resp = KevBean.getField("RESP"); if(resp==null) resp = "";
	
	String dat = trdt + " " + trtm; 
	String qty = ftrqt + " " + unms;
	String ttid = KevBean.getField("TTID"); if(ttid==null) ttid = "";
	String trtp = KevBean.getField("TRTP"); if(trtp==null) trtp = "";
	String ttyp = KevBean.getField("TTYP"); if(ttyp==null) ttyp = "";
	String transTypeFull = resp + ": " + ttid + " " + trtp + " " + ttyp;
	String rscd = KevBean.getField("RSCD"); if(rscd==null) rscd = "";
	if(!rscd.equals("")) rscd = " (Reason: " + rscd + ")";
	transTypeFull+= rscd;
	
	
	String ridn = KevBean.getField("RIDN"); if(ridn==null) ridn = "";
	String ridl = KevBean.getField("RIDL"); if(ridl==null) ridl = "";
	String rido = KevBean.getField("RIDO"); if(rido==null) rido = "";
	String ridi = KevBean.getField("RIDI"); if(ridi==null) ridi = "";
	
	String ridnl = "";
	if(!ridn.equals("")) ridnl = ridn + "/" + ridl;
	
	String rftx = KevBean.getField("RFTX"); if(rftx==null) rftx = "";
	
	json+="{";
	
	json+="\"leaf\": true,";
    json+="\"expanded\": false,";
    
	if(ttyp.equals("98")) { // reclass of item code
		json+= "\"itds\":\"" + rftx.split(" ")[0] + "\",";
		json+= "\"bano\":\"" + rftx.split(" ")[1] + "\",";
	} else {
		json+= "\"bano\":\"" + KevBean.getField("BANO") + "\",";
	}
	json+= "\"itno\":\"" + transTypeFull + "\",";
	json+= "\"ridnl\":\"" + ridnl + "\",";
	json+= "\"dat\":\"" + dat + "\",";
	json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
	json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
	
	json+= "\"camu\":\"" + KevBean.getField("CAMU") + "\",";
	json+= "\"resp\":\"" + KevBean.getField("RESP") + "\",";
	
	json+= "\"ttid\":\"" + ttid + "\",";
	json+= "\"trtp\":\"" + trtp + "\",";
	json+= "\"ttyp\":\"" + ttyp + "\",";
	
	json+= "\"ridn\":\"" + ridn + "\",";
	json+= "\"ridl\":\"" + ridl + "\",";
	json+= "\"ridi\":\"" + ridi	 + "\",";
	json+= "\"cawe\":\"" + KevBean.getField("CAWE") + "\",";
	json+= "\"rftx\":\"" + rftx + "\",";
	json+= "\"whlo\":\"" + KevBean.getField("WHLO") + "\",";
	json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
		
	json+= "\"rscd\":\"" + rscd + "\",";
	
	if(ttid.equals("OID")) {
		OIS100MIBean.setInitialise("OIS100MI");	
		OIS100MIBean.setField("CONO", zdcono);
		OIS100MIBean.setField("ORNO", ridn);
		OIS100MIBean.runProgram("GetHead");
		String cuno = OIS100MIBean.getField("CUNO");
		String cuor = OIS100MIBean.getField("CUOR");
		String oref = OIS100MIBean.getField("OREF");
		String rldt = OIS100MIBean.getField("RLDT");
		
		OIS100MIBean.setField("CONO", zdcono);
		OIS100MIBean.setField("ORNO", ridn);
		OIS100MIBean.setField("PONR", ridl);
		OIS100MIBean.runProgram("GetLine");
		String whlo = OIS100MIBean.getField("WHLO");
		json+= "\"powhs\":\"" + whlo + "\",";
		String orqt = OIS100MIBean.getField("ORQT");
		String alun = OIS100MIBean.getField("ALUN");
		json+= "\"poqty\":\"" + orqt + " " + alun + "\",";
		json+= "\"podat\":\"" + rldt + "\",";
		json+= "\"suno\":\"" + cuno + "\",";
		
		if(!cuno.equals(thisCuno)) {
			OIS100MIBean.setInitialise("CRS610MI");
			OIS100MIBean.setField("CONO", zdcono);
			OIS100MIBean.setField("CUNO", cuno);
			OIS100MIBean.runProgram("GetBasicData");
			cunm = OIS100MIBean.getField("CUNM");
		}
		thisCuno = cuno;
		
		json+= "\"sunm\":\"" + cunm + "\",";
		
	} else {
		json+= "\"sunm\":\"" + rftx + "\",";
		
	}

	json+= "\"qty\":\"" + qty + "\"";
	json+="},";
}


// now do the same for the LOT only, for non-CAMU transactions AFTER the last one on this container
// reason for this is the CAMU might move to non-container managed world (e.g. 101->106 foods -> Culina)
System.out.println("Last: " + lastCamuDate + " : " + lastCamuTime);


KevBean.setInitialise("MDBReadMI");
KevBean.setField("BANO", bano);
KevBean.runProgram("LstMITTRA25");

thisCuno = ""; cunm = "";
while(KevBean.nextRow()) {
	String lotCamu = KevBean.getField("CAMU"); if(lotCamu==null) lotCamu = "";
	String lotBano = KevBean.getField("BANO");
	if(!lotBano.equals(bano)) break;
	if(lotCamu.equals("")) {
		String trdt = KevBean.getField("TRDT"); if(trdt==null) trdt = "";
		String trtm = KevBean.getField("TRTM"); if(trtm==null) trtm = "";
		String trqt = KevBean.getField("TRQT"); if(trqt==null) trqt = ""; if(trqt.equals("")) trqt = "0";
		
		System.out.println("CurrDate: " + (trdt+trtm));
		System.out.println("OldDate: " + (lastCamuDate+lastCamuTime));
		
		if(Long.parseLong(trdt+trtm) >= Long.parseLong(lastCamuDate + lastCamuTime) ) {
		
			DecimalFormat decimalFormat = new DecimalFormat("0.#####");
		    String ftrqt = trqt;
		    
		    try {
		    	ftrqt = decimalFormat.format(Double.valueOf(trqt));
		    } catch (Exception e){
		    	; // nothing to do...
		    }
		    
			String unms = KevBean.getField("UNMS"); if(unms==null) unms = "";
			String resp = KevBean.getField("RESP"); if(resp==null) resp = "";
			
			String dat = trdt + " " + trtm; 
			String qty = ftrqt + " " + unms;
			String ttid = KevBean.getField("TTID"); if(ttid==null) ttid = "";
			String trtp = KevBean.getField("TRTP"); if(trtp==null) trtp = "";
			String ttyp = KevBean.getField("TTYP"); if(ttyp==null) ttyp = "";
			String transTypeFull = resp + ": " + ttid + " " + trtp + " " + ttyp;
			String rscd = KevBean.getField("RSCD"); if(rscd==null) rscd = "";
			if(!rscd.equals("")) rscd = " (Reason: " + rscd + ")";
			transTypeFull+= rscd;
			
			
			String ridn = KevBean.getField("RIDN"); if(ridn==null) ridn = "";
			String ridl = KevBean.getField("RIDL"); if(ridl==null) ridl = "";
			String rido = KevBean.getField("RIDO"); if(rido==null) rido = "";
			String ridi = KevBean.getField("RIDI"); if(ridi==null) ridi = "";
			
			String ridnl = "";
			if(!ridn.equals("")) ridnl = ridn + "/" + ridl;
			
			String rftx = KevBean.getField("RFTX"); if(rftx==null) rftx = "";
			
			json+="{";
			
			json+="\"leaf\": true,";
		    json+="\"expanded\": false,";
		    
			if(ttyp.equals("98")) { // reclass of item code
				json+= "\"itds\":\"" + rftx.split(" ")[0] + "\",";
				json+= "\"bano\":\"" + rftx.split(" ")[1] + "\",";
			} else {
				json+= "\"bano\":\"" + KevBean.getField("BANO") + "\",";
			}
			json+= "\"itno\":\"" + transTypeFull + "\",";
			json+= "\"ridnl\":\"" + ridnl + "\",";
			json+= "\"dat\":\"" + dat + "\",";
			json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
			json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
			
			json+= "\"camu\":\"" + KevBean.getField("CAMU") + "\",";
			json+= "\"resp\":\"" + KevBean.getField("RESP") + "\",";
			
			json+= "\"ttid\":\"" + ttid + "\",";
			json+= "\"trtp\":\"" + trtp + "\",";
			json+= "\"ttyp\":\"" + ttyp + "\",";
			
			json+= "\"ridn\":\"" + ridn + "\",";
			json+= "\"ridl\":\"" + ridl + "\",";
			json+= "\"ridi\":\"" + ridi	 + "\",";
			json+= "\"cawe\":\"" + KevBean.getField("CAWE") + "\",";
			json+= "\"rftx\":\"" + rftx + "\",";
			json+= "\"whlo\":\"" + KevBean.getField("WHLO") + "\",";
			json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
				
			json+= "\"rscd\":\"" + rscd + "\",";
			
			if(ttid.equals("OID")) {
				OIS100MIBean.setInitialise("OIS100MI");	
				OIS100MIBean.setField("CONO", zdcono);
				OIS100MIBean.setField("ORNO", ridn);
				OIS100MIBean.runProgram("GetHead");
				String cuno = OIS100MIBean.getField("CUNO");
				String cuor = OIS100MIBean.getField("CUOR");
				String oref = OIS100MIBean.getField("OREF");
				String rldt = OIS100MIBean.getField("RLDT");
				
				OIS100MIBean.setField("CONO", zdcono);
				OIS100MIBean.setField("ORNO", ridn);
				OIS100MIBean.setField("PONR", ridl);
				OIS100MIBean.runProgram("GetLine");
				String whlo = OIS100MIBean.getField("WHLO");
				json+= "\"powhs\":\"" + whlo + "\",";
				String orqt = OIS100MIBean.getField("ORQT");
				String alun = OIS100MIBean.getField("ALUN");
				json+= "\"poqty\":\"" + orqt + " " + alun + "\",";
				json+= "\"podat\":\"" + rldt + "\",";
				json+= "\"suno\":\"" + cuno + "\",";
				
				if(!thisCuno.equals(cuno)) {
					OIS100MIBean.setInitialise("CRS610MI");
					OIS100MIBean.setField("CONO", zdcono);
					OIS100MIBean.setField("CUNO", cuno);
					OIS100MIBean.runProgram("GetBasicData");
					cunm = OIS100MIBean.getField("CUNM");
				} 
				thisCuno = cuno;
				json+= "\"sunm\":\"" + cunm  + "\",";
				
			} else {
				json+= "\"sunm\":\"" + rftx + "\",";
				
			}
		
			json+= "\"qty\":\"" + qty + "\"";
			json+="},";
		}
	}
}


if(json.length()>2) json = json.substring(0, json.length() - 1);
json = json + "]";
out.print(json);
%>

