<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,java.net.*,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%
int liveOrTest = 0;
String uricono = (String)session.getAttribute("uricono");
if(uricono.equals("PRD")) liveOrTest = 1;

KevBean.changeProg("PPS001MI");
String json = "[";

boolean chkWeights = 	Boolean.parseBoolean(request.getParameter("chkWeights"));
boolean chkBano = 		Boolean.parseBoolean(request.getParameter("chkBano"));
boolean chkAttributes = Boolean.parseBoolean(request.getParameter("chkAttributes")); 
boolean chkCompletePO = Boolean.parseBoolean(request.getParameter("chkCompletePO"));
int totValPallets = Integer.parseInt(request.getParameter("totValPallets"));

String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";

boolean allOKs = true; String closeMsg = "";

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);
//System.out.println("Today is " + today);
String bano = "";
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray pallets = (JSONArray) obj;

String puno = ""; 
int i = 0; int actReceipts = 0;
Iterator<JSONObject> iterator = pallets.iterator();
while (iterator.hasNext()) {
	 JSONObject pallet = (JSONObject) iterator.next();
	 
	 long qty = (Long)pallet.get("qty");
	 //System.out.println("i is " + i + " qty: " + qty); 
	 i++;
	 if(qty>0) {
		 actReceipts++;
	 puno = (String)pallet.get("puno"); if(puno==null) puno = ""; puno = puno.trim();
	 String suno = (String)pallet.get("suno"); if(suno==null) suno = ""; suno = suno.trim();
	 String itno = (String)pallet.get("itno"); if(itno==null) itno = ""; itno = itno.trim();
	 String whlo = (String)pallet.get("whlo"); if(whlo==null) whlo = ""; whlo = whlo.trim();
	 
	 String prdt = (String)pallet.get("prdt"); if(prdt==null) prdt = ""; prdt = prdt.trim();
	 
	 long numDecs = 0; long multiplier = 1;
	 
	 try {
		 numDecs = (Long)pallet.get("numDecs");
		 multiplier = (Long)pallet.get("multiplier");
	 } catch (Exception e) {
		 System.err.println("no wories " + e);
	 }
	 
	 //	System.out.println(" decs are " + numDecs + " mult is " + multiplier);
	
	 long pnli = (Long)pallet.get("pnli"); 
	 double actCawe = 0.0;
	 try {
	 	actCawe = (Double)pallet.get("actCawe");
	 } catch (java.lang.ClassCastException e) {
		 Long lActCawe = (Long)pallet.get("actCawe");
		 actCawe = lActCawe.doubleValue();
	 }
	 
	 double avgCawe = 0.0;
	 try {
		 avgCawe = (Double)pallet.get("avgCawe");
	 } catch (java.lang.ClassCastException e) {
		 Long lAvgCawe = (Long)pallet.get("avgCawe");
		 avgCawe = lAvgCawe.doubleValue();
	 }

	 String bre2 = (String)pallet.get("healthmark"); if(bre2==null) bre2 = "";
	 String bref = (String)pallet.get("daycode"); if(bref==null) bref = "";
	 // need to get the manuf date from the daycode.. calculation..
	 
	 String brem = (String)pallet.get("serial"); if(brem==null) brem = "";
	 String sudo = (String)pallet.get("sudo"); if(sudo==null) sudo = ""; // sudo is dub CO no
	 
	 
		 KevBean.setField("CONO", zdcono);
		 KevBean.setField("TRDT", today); 
		 KevBean.setField("RESP", "IDBKEWO"); //hmm
		 KevBean.setField("PUNO", puno);
		 KevBean.setField("PNLI", pnli + "");
		 KevBean.setField("PNLS", "0");
		 KevBean.setField("RVQA", qty*multiplier + "");
		 KevBean.setField("WHLO", whlo);
		 KevBean.setField("SUDO", sudo);
		 
		 KevBean.setField("PRDT", prdt);
		 
		 KevBean.setField("BREF", bref);
		 KevBean.setField("BRE2", bre2);
		 KevBean.setField("BREM", brem);
		 
		 if(chkBano) KevBean.setField("BANO", bano);
		 //KevBean.setField("ITNO", itno); // itno not needed, fancy!
		 
		 KevBean.setField("WHSL", whsl);
		 
		 double dd = 1d;
		 if(numDecs==0)  dd = 1d;
		 else if(numDecs==1)  dd = 10d;
		 else if(numDecs==2)  dd = 100d;
		 else if(numDecs==3)  dd = 1000d;
		 else if(numDecs==4)  dd = 10000d;
		 else if(numDecs==5)  dd = 100000d;
		 else if(numDecs==6)  dd = 1000000d;
		 else if(numDecs==7)  dd = 10000000d;
		 
		 if((chkWeights) && (avgCawe>0.0)) {
			 avgCawe = avgCawe * 1000; //dub is MT evolve is KG for CW
			 avgCawe = (double)Math.round(avgCawe * dd) / dd;
			 KevBean.setField("CAWE", avgCawe + "");
		 } else if (actCawe>0.0) {
			 System.out.println("actual pre weight..." + actCawe);
			 actCawe = actCawe * 1000; //dub is MT evolve is KG for CW
			 actCawe = (double)Math.round(actCawe * dd) / dd;
			 KevBean.setField("CAWE", actCawe + "");		 
		 }
		 
		 if( (chkCompletePO) && (actReceipts==totValPallets) && (allOKs) ) {
			 KevBean.setField("OEND", "1");
			 closeMsg = " PO now marked complete";
		 }
	
		 String msg = KevBean.runProgram("Receipt");		 
		 //System.out.println("message is " + msg);
		 
		 boolean ok = false; 
		 if(msg.startsWith("OK")) {
			 ok = true;
		 } else {
			allOKs = false;
		 }
		
		
		String msgn = ""; //String msg = ""; 
		
		try {
			msgn = KevBean.getField("MSGN");
			if(msg.startsWith("OK")) {
				bano = KevBean.getField("BANO");
			} else {
				bano = "";
			}
			
		} catch (Exception e) {
			System.err.println("Plough on " + e);
		}
		
		json+= "{\"msg\":\"" + msgn + "\",\"msgn\":\"" + msg + closeMsg + " Lot: " + bano + "\", \"ok\":" + ok + "},";
	 	msg = "";
	 } else {
		 json+= "{\"msg\":\"\",\"msgn\":\"\", \"ok\":true},";
	 }
} // end looping through the JSON array

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json); 
//System.out.print(json);

int po_open = 8; 
if(chkAttributes) po_open = 9; // if 8 or 9 its done, but if 9 also process qms	

Connection conn = null;
PreparedStatement ps = null;
String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
String Gis05Uname = "GIS5263";
String Gis05Pword = "GIS5263";
String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=IDB_General";
Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);

	ps = conn.prepareStatement("update po_workbench set po_open = ? where co = ? and po_open = 1 and liveOrTest = ?");
	ps.setInt(1, po_open);
	ps.setString(2, puno);
	ps.setInt(3, liveOrTest);
   	ps.executeUpdate();
   
   String triggerPageResponse = "";
	try {
		URL urlL = new URL("http://b2bi" + uricono.toLowerCase() + ".ornua.com:4133/po_workbench_" + divi);
		URLConnection uc = urlL.openConnection();			
		
	  BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
	  StringBuffer buffer = new StringBuffer();
	  String line = "";
	  // changing the following while to an if - we just need the first line...
	  if ((line = in.readLine()) != null) buffer.append(line);
	  triggerPageResponse = buffer.toString();
	  
	  in.close();
	   
 } catch (Exception e) {
	  System.err.println("cant get url " + e);
 }

%>