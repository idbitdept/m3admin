<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Properties,javax.activation.*,javax.mail.*,javax.mail.internet.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS422MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS410MIBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "MHS850MI";
String localTrans = "AddCOPick";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
LocalBean.setInitialise(localProg);
//make sure the users has the same cono/divi/faci settings as the API account

String uname = (String)session.getAttribute("uname"); if(uname==null) uname = "";
String orno = request.getParameter("orno"); if(orno==null) orno = "";
long orsl = 0;

MWS422MIBean.setSystem(system);
MWS422MIBean.setPort(PORT);
MWS422MIBean.setLib(LIB);
MWS422MIBean.setCompany(zdcono);
MWS422MIBean.setUsername(m3user);
MWS422MIBean.setPassword(m3pass);


KevBean.setInitialise("MMS850MI");
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray ordersArray = (JSONArray) obj;
String publishResults = "<ul>Results of Order Completions from " + uname + " at " + new java.util.Date();

Iterator<JSONObject> iterator = ordersArray.iterator();
while (iterator.hasNext()) {
	 JSONObject orders = (JSONObject) iterator.next();
	 orno = (String) orders.get("orno");
	 orsl = (Long) orders.get("orsl");

	KevBean.setInitialise("MWS411MI");
	KevBean.setField("RORC", "3");
	KevBean.setField("RIDN", orno);
	KevBean.runProgram("LstDelLnByOrd"); String delNo = ""; String[] delnos = new String[20]; int i = 0;
	while(KevBean.nextRow()) {
		String thisDelNo = (String)KevBean.getField("DLIX");
		//System.out.println("Del n is " + thisDelNo);
		if(!delNo.equals(thisDelNo)) delnos[i++] = thisDelNo;
		delNo = thisDelNo;
	}
	//System.out.println("del nos length" + delnos.length);
	for(int v=0;v<delnos.length;v++) {
		if(delnos[v]==null) break; else {
			//System.out.println("V is " + v);
			// if its a 33, release it for pick
			if(orsl==33) {
				MWS410MIBean.setSystem(system);
				MWS410MIBean.setPort(PORT);
				MWS410MIBean.setLib(LIB);
				MWS410MIBean.setCompany(zdcono);
				MWS410MIBean.setUsername(m3user);
				MWS410MIBean.setPassword(m3pass);
				MWS410MIBean.setInitialise("MWS410MI");
				MWS410MIBean.setField("CONO", zdcono);
				MWS410MIBean.setField("DLIX", delnos[v]);
				System.out.println("release for pick: " + MWS410MIBean.runProgram("RelForPick"));
				Thread.sleep(12000); // sleep 12 seconds
			}			
			
			MWS422MIBean.setInitialise("MWS422MI");
			MWS422MIBean.setField("DLIX", delnos[v]); //System.out.println("chuck " + delnos[v]);
			System.out.println("List pick " + MWS422MIBean.runProgram("LstPickDetail"));
			while(MWS422MIBean.nextRow()) {
				//System.out.println("Anything in here?");
				
				LocalBean.setField("PRFL", "*EXE");
				LocalBean.setField("CONO", zdcono);
				LocalBean.setField("WHLO", MWS422MIBean.getField("WHLO"));
				LocalBean.setField("ITNO", MWS422MIBean.getField("ITNO"));
				
				LocalBean.setField("E0PA", "5013546007355");
				LocalBean.setField("E065", "31");

				LocalBean.setField("CUNO", MWS422MIBean.getField("CUNO"));
				LocalBean.setField("QTYP", "0");
				LocalBean.setField("QTYO", MWS422MIBean.getField("ALQT"));
				
				LocalBean.setField("RIDN", MWS422MIBean.getField("RIDN"));
				String ordLine = MWS422MIBean.getField("RIDL");
				LocalBean.setField("RIDL", ordLine);
				LocalBean.setField("RIDI", delnos[v]);
				LocalBean.setField("PLSX", MWS422MIBean.getField("PLSX"));
				LocalBean.setField("OEND", "1");
				
				LocalBean.setField("PACT", "PALLET");
				LocalBean.setField("ISMD", "0");
				String resp = LocalBean.runProgram(localTrans);
				if(resp.startsWith("NOK            Not allowed")) {
					 //keepGoing = false;
					 resp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
					 break;
				 }
				publishResults += "<li> Order " + orno + "/" + ordLine + ": " + resp + "</li>"; 
			}
		}
	}	
}
LocalBean.closeConnection();
MWS422MIBean.closeConnection();
MWS410MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

// now send a mail..

if(email.equals("")) {
	KevBean.setInitialise("CRS111MI");
	KevBean.setField("EMTP", "04");
	KevBean.setField("EMKY", uname.trim());
	if(KevBean.runProgram("Get").startsWith("OK")) {
		email = KevBean.getField("EMAL");
		session.setAttribute("email", email);
	}
}
if(email.equals("")) email = "Kevin.Woods@ornua.com";

if(!email.equals("")) {
	String host = "SMTP.ORNUA.COM";
	Properties props = new Properties();
	props.setProperty("mail.smtp.host", host);
	props.setProperty("mail.smtp.port", "25");
	Session smtpSession = Session.getDefaultInstance(props);
	
	try {
        Message message = new MimeMessage(smtpSession);
   		message.setFrom(new InternetAddress("noreply@ornua.com"));
   		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
   		message.setSubject("Hi " + uname + " - Your close deliveries report (" + zdcono + "/" + divi + ")");
   		message.setContent(publishResults, "text/html; charset=utf-8");
   		message.setSentDate(new java.util.Date()); 
		Transport.send(message);
   		System.out.println("Sent message successfully....");

	} catch (MessagingException e) {
		System.err.println("cant send mail " + e);
	}
}
%>
	