<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
//KevBean.changeProg("MRS001MI");

KevBean.setInitialise("MRS001MI");

String trnm = request.getParameter("trnm");
String minm = request.getParameter("minm");
String trtp = request.getParameter("trtp");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", trtp); 

KevBean.runProgram("LstFields");


String json = "[";
while(KevBean.nextRow()) {
	json+= "{";
	json+= "\"flnm\":\"" + KevBean.getField("FLNM") + "\",";
	json+= "\"flds\":\"" + KevBean.getField("FLDS") + "\",";
	json+= "\"leng\":" + KevBean.getField("LENG") + ",";
	json+= "\"frpo\":" + KevBean.getField("FRPO") + ",";
	json+= "\"topo\":" + KevBean.getField("TOPO") + ",";
	json+= "\"mand\":\"" + KevBean.getField("MAND") + "\",";
	json+= "\"type\":\"" + KevBean.getField("TYPE") + "\"";  
	json+= "},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
