<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.*,java.text.DecimalFormat" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String json = "{\"res\":[ ";

String ridn = request.getParameter("ridn"); if(ridn==null) ridn = "";
String ridl = request.getParameter("ridl"); if(ridl==null) ridl = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String ttyp = request.getParameter("ttyp"); if(ttyp==null) ttyp = "";

KevBean.setInitialise("MWS070MI");

//KevBean.setField("CONO", zdcono);
if(ridn.equals("")) ttyp = "17";
KevBean.setField("TTYP", ttyp);
KevBean.setField("RIDN", ridn);
KevBean.setField("RIDL", ridl);
KevBean.setField("CAMU", camu);
KevBean.setField("BANO", bano);

KevBean.runProgram("LstTransByOrder");

while(KevBean.nextRow()) {

	String trdt = KevBean.getField("TRDT"); if(trdt==null) trdt = "";
	String trtm = KevBean.getField("TRTM"); if(trtm==null) trtm = "";
	String trqt = KevBean.getField("TRQT"); if(trqt==null) trqt = ""; if(trqt.equals("")) trqt = "0";
	
	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
    String ftrqt = trqt;
    
    try {
    	ftrqt = decimalFormat.format(Double.valueOf(trqt));
    } catch (Exception e){
    	; // nothing to do...
    }
    
	String unms = KevBean.getField("UNMS"); if(unms==null) unms = "";
	String resp = KevBean.getField("RESP"); if(resp==null) resp = "";
	
	String dat = trdt + " " + trtm; 
	String qty = ftrqt + " " + unms;
	
	json+="{";
       
    json+= "\"leaf\":false,";
	json+= "\"expandable\":false,";
	json+= "\"dat\":\"" + dat + "\",";
	json+= "\"bre2\":\"" + resp + "\",";
	json+= "\"qty\":\"" + qty + "\"";
	json+="},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json+="],\"success\":true}";

out.println(json);
%>
