<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.text.SimpleDateFormat" %>
<%@ include file="Connections/INCm3.jsp" %>

<%
String excls = (String)session.getAttribute("excls"); if(excls==null) excls = "";
String where = "";
if(!excls.equals("")) where += " and XMMINM not in " + excls;

SimpleDateFormat olddate = new SimpleDateFormat("yyyyMMdd");
SimpleDateFormat newdate = new SimpleDateFormat("dd-MMM-yyyy");

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);
PreparedStatement ps = conn.prepareStatement("select XMRGDT, count(*) as num from " + libby + ".zmilog where xmcono = ? and xmdivi = ? " + where + " group by XMRGDT");
ps.setInt(1, icono);
ps.setString(2, divi);
ResultSet rs = ps.executeQuery();
    

    String json = "[";


          while (rs.next()) {        	  
	       	  json = json + "{\"trans\":\"" + rs.getString("XMRGDT") + "\",\"dateformatted\":\"" + newdate.format(olddate.parse(rs.getString("XMRGDT"))) + "\",\"num\":" + rs.getInt("num") + "},";
          }
   
          if(rs!=null) rs.close();  
          if(ps!=null) ps.close();  
          if(conn!=null) conn.close();  
          
          if(json.length()>2) json = json.substring(0, json.length() - 1);
          
          json = json + "]";
          
          out.print(json);
          System.out.print(json);

%>
