<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Properties,javax.activation.*,javax.mail.*,javax.mail.internet.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="MWS422MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MHS850MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS410MIBean" class="com.ornua.MvxBean" scope="page"/>
<%

String uname = (String)session.getAttribute("uname"); if(uname==null) uname = "";
String orno = request.getParameter("orno"); if(orno==null) orno = "";
long orsl = 0;

MWS422MIBean.setSystem(system);
MWS422MIBean.setPort(PORT);
MWS422MIBean.setLib(LIB);
MWS422MIBean.setCompany(zdcono);
MWS422MIBean.setUsername(m3user);
MWS422MIBean.setPassword(m3pass);

MHS850MIBean.setSystem(system);
MHS850MIBean.setPort(PORT);
MHS850MIBean.setLib(LIB);
MHS850MIBean.setCompany(zdcono);
MHS850MIBean.setUsername(m3user);
MHS850MIBean.setPassword(m3pass);

KevBean.setInitialise("MMS850MI");
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray ordersArray = (JSONArray) obj;
String publishResults = "<ul>Results of Order Completions from " + uname + " at " + new java.util.Date();

Iterator<JSONObject> iterator = ordersArray.iterator();
while (iterator.hasNext()) {
	 JSONObject orders = (JSONObject) iterator.next();
	 orno = (String) orders.get("orno");
	 orsl = (Long) orders.get("orsl");

	KevBean.setInitialise("MWS411MI");
	KevBean.setField("RORC", "3");
	KevBean.setField("RIDN", orno);
	KevBean.runProgram("LstDelLnByOrd"); String delNo = ""; String[] delnos = new String[20]; int i = 0;
	while(KevBean.nextRow()) {
		String thisDelNo = (String)KevBean.getField("DLIX");
		//System.out.println("Del n is " + thisDelNo);
		if(!delNo.equals(thisDelNo)) delnos[i++] = thisDelNo;
		delNo = thisDelNo;
	}
	System.out.println("del nos length" + delnos.length);
	for(int v=0;v<delnos.length;v++) {
		if(delnos[v]==null) break; else {
			System.out.println("V is " + v);
			// if its a 33, release it for pick
			if(orsl==33) {
				MWS410MIBean.setSystem(system);
				MWS410MIBean.setPort(PORT);
				MWS410MIBean.setLib(LIB);
				MWS410MIBean.setCompany(zdcono);
				MWS410MIBean.setUsername(m3user);
				MWS410MIBean.setPassword(m3pass);
				MWS410MIBean.setInitialise("MWS410MI");
				MWS410MIBean.setField("CONO", zdcono);
				MWS410MIBean.setField("DLIX", delnos[v]);
				System.out.println("release for pick: " + MWS410MIBean.runProgram("RelForPick"));
				Thread.sleep(12000); // sleep 12 seconds
			}			
			
			MWS422MIBean.setInitialise("MWS422MI");
			MWS422MIBean.setField("DLIX", delnos[v]); System.out.println("chuck " + delnos[v]);
			System.out.println("List pick " + MWS422MIBean.runProgram("LstPickDetail"));
			while(MWS422MIBean.nextRow()) {
				System.out.println("Anything in here?");
				MHS850MIBean.setInitialise("MHS850MI");
				MHS850MIBean.setField("PRFL", "*EXE");
				MHS850MIBean.setField("CONO", zdcono);
				MHS850MIBean.setField("WHLO", MWS422MIBean.getField("WHLO"));
				MHS850MIBean.setField("ITNO", MWS422MIBean.getField("ITNO"));
				
				MHS850MIBean.setField("E0PA", "5013546007355");
				MHS850MIBean.setField("E065", "31");

				MHS850MIBean.setField("CUNO", MWS422MIBean.getField("CUNO"));
				MHS850MIBean.setField("QTYP", "0");
				MHS850MIBean.setField("QTYO", MWS422MIBean.getField("ALQT"));
				
				MHS850MIBean.setField("RIDN", MWS422MIBean.getField("RIDN"));
				String ordLine = MWS422MIBean.getField("RIDL");
				MHS850MIBean.setField("RIDL", ordLine);
				MHS850MIBean.setField("RIDI", delnos[v]);
				MHS850MIBean.setField("PLSX", MWS422MIBean.getField("PLSX"));
				MHS850MIBean.setField("OEND", "1");
				
				MHS850MIBean.setField("PACT", "PALLET");
				MHS850MIBean.setField("ISMD", "0");
				String resp = MHS850MIBean.runProgram("AddCOPick");
				publishResults += "<li> Order " + orno + "/" + ordLine + ": " + resp + "</li>"; 
			}
		}
	}	
}
MHS850MIBean.closeConnection();
MWS422MIBean.closeConnection();
MWS410MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

// now send a mail..
String emal = "Kevin.Woods@ornua.com"; 
KevBean.setInitialise("CRS111MI");
KevBean.setField("EMTP", "04");
KevBean.setField("EMKY", uname.trim());
if(KevBean.runProgram("Get").startsWith("OK")) {
	emal = KevBean.getField("EMAL");
	System.out.println("Foudn emal " + emal);
}

	Properties props = new Properties();
	props.put("mail.smtp.auth", "true");
	props.put("mail.smtp.starttls.enable", "true");
	props.put("mail.smtp.host", "in-v3.mailjet.com");
	props.put("mail.smtp.port", "587");

	Session smtpSession = Session.getInstance(props,
	  new javax.mail.Authenticator() {
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(smtpUsername, smtpPassword);
		}
	  });

	try {
        Message message = new MimeMessage(smtpSession);
   		message.setFrom(new InternetAddress("noreply@ornua.com"));
   		message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emal));
   		message.setSubject("Hi " + uname + " - Your close deliveries report (" + zdcono + "/" + divi + ")");
   		message.setContent(publishResults, "text/html; charset=utf-8");
   		message.setSentDate(new java.util.Date()); 
		Transport.send(message);
   System.out.println("Sent message successfully....");

	} catch (MessagingException e) {
		throw new RuntimeException(e);
	}

%>
	