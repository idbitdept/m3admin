<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String camu = request.getParameter("camu"); if(camu==null) camu = "";
String aloc = request.getParameter("aloc"); if(aloc==null) aloc = "";
String stas = request.getParameter("stas"); if(stas==null) stas = "";
String uname = (String)session.getAttribute("uname");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>Attribute results";

int lotNum = 0; String resp = "";
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject camus = (JSONObject) iterator.next();
	 camu = (String) camus.get("camu");
	 stas = (String) camus.get("stas");
	 bano = (String) camus.get("bano");
	 whsl = (String) camus.get("whsl");
	 aloc = (String) camus.get("aloc");
	 
	 // get the atnr for the pallet
	 KevBean.setInitialise("MDBREADMI");
	 KevBean.setField("MRCD", "1"); // we only need the first record
	 KevBean.runProgram("SetLstMaxRec");
	 KevBean.setField("CAMU", camu);
	 KevBean.runProgram("LstMITLOCZ7");

	 if(!KevBean.nextRow()) {
		 publishResults += "<li> Pallet " + camu + ": No Attribute number found</li>";
	 } else {
		 String atnr = KevBean.getField("ATNR");
		 KevBean.setInitialise("ATS101MI");
		 	 
		 Enumeration<String> parameterNames = request.getParameterNames();
		 while (parameterNames.hasMoreElements()) {
		     String paramName = parameterNames.nextElement();
		     if( (!paramName.equals("sdata")) && (!paramName.equals("whlo")) && (!paramName.equals("itno")) ) {
			     String[] paramValues = request.getParameterValues(paramName);
		         String paramValue = paramValues[0]; paramValue = paramValue.trim();
		         if(!paramValue.equals("")) {
		    		 KevBean.setField("CONO", zdcono);
		    		 KevBean.setField("ATNR", atnr);
			         KevBean.setField("ATID", paramName);
			         KevBean.setField("ATVA", paramValue);
					 String attrResp = KevBean.runProgram("SetAttrValue");
					 publishResults += "<li> Pallet " + camu + ": Attribute: " + paramName + ": " + attrResp + "</li>";
		         }
		     }
		 }
	 }
	lotNum++;
}

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
%>