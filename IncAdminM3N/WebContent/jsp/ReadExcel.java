package src;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) {
		System.out.println("ReadExcel lets go...");
		

		
		
		
		try {
	        FileInputStream file = new FileInputStream(new File("/Users/idbkewo/git/redoqubenumbersNEON/RedoNumbers/src/APIs.xlsx"));

	        //Create Workbook instance holding reference to .xlsx file
	        XSSFWorkbook workbook = new XSSFWorkbook(file);

	        //Get first/desired sheet from the workbook
	        XSSFSheet sheet = workbook.getSheetAt(0);
	        int currentRow = 0; String[] apiFields = new String[100]; //hardly be more than 100 input fields..
	        //Iterate through each rows one by one
	        Iterator<Row> rowIterator = sheet.iterator();
	        while (rowIterator.hasNext()) {
	            Row row = rowIterator.next();
	            //For each row, iterate through all the columns
	            Iterator<Cell> cellIterator = row.cellIterator();
	            int currentCell = 0;
	            while (cellIterator.hasNext()) {
	                Cell cell = cellIterator.next();  
	                if(currentRow==0) {
	                	try {
	                		apiFields[currentCell] = cell.getStringCellValue();
	                	} catch (Exception e) {
	                		apiFields[currentCell] = String.valueOf(cell.getNumericCellValue());
	                	}
	                } else {
		                switch(cell.getCellType()) {
		                case NUMERIC:
		                    String dd = String.valueOf(cell.getNumericCellValue());
		                    System.out.println("About to set " + apiFields[currentCell] + " with value " + dd.replaceAll("([0-9])\\.0+([^0-9]|$)", "$1$2"));	                    
		                    break;
		                case STRING:
		                    //data.add(celldata.getNumericCellValue());
		                	System.out.print("About to set " + apiFields[currentCell] + " with value " + cell.getStringCellValue() + "\t");
		                    break;
		                case BOOLEAN:
		                    //data.add(celldata.getBooleanCellValue());
		                    System.out.print(cell.getBooleanCellValue() + "\t");
		                    break;
						default:
							break;
		                }
	                }
	                
	                
	                
	               currentCell++; 
	            }
	            System.out.println("");
	            currentRow++;
	        }
	        file.close();
	        workbook.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }	
	}
}
