<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%


String minm = request.getParameter("minm");
String trnm = request.getParameter("trnm");
KevBean.setInitialise(minm);

Enumeration paramNames = request.getParameterNames();

while(paramNames.hasMoreElements()) {
   String paramName = (String)paramNames.nextElement();
   if(Character.isUpperCase(paramName.charAt(0))) {
	   KevBean.setField(paramName, request.getParameter(paramName));
   }
}
boolean duplicate = false;
if(minm.trim().equalsIgnoreCase("PPS001MI") && trnm.trim().equalsIgnoreCase("Receipt") ) {
	// do duplicate check before posting...
	String camu = request.getParameter("CAMU"); if(camu==null) camu = "";
	String rvqa = request.getParameter("RVQA"); if(rvqa==null) rvqa = "";
	System.out.println("Dup test " + camu);
	if(!camu.equals("")) {
		
		com.ornua.MvxBean DupBean = new com.ornua.MvxBean();
		DupBean.setSystem(system);
		DupBean.setPort(PORT);
		//DupBean.setCompany(uricono);
		DupBean.setUsername(m3user);
		DupBean.setPassword(m3pass);
		DupBean.setInitialise("MDBREADMI");
		
		DupBean.setField("CAMU", camu); //todo put M7 MITLOC MDBRead into PRD
		String dupResp = DupBean.runProgram("GetMITLOCZ7");
		if( dupResp.startsWith("O") ) { //todo more checks
			String checkQty = DupBean.getField("STQT"); if(checkQty==null) checkQty = ""; 
			//hard stop on any sort of duplicate (CAMU) on admin - pass control over to users
			//separately putting in change to MEC + Portal to append qty to lot if duplicate but different qty - if same qty then hard stop
			//if(checkQty.trim().equals(rvqa.trim())) {
				duplicate = true;
				out.println("{\"success\":true,\"resp\":\"NOK Duplicate pallet " + camu + "\",\"comments\":\"\",\"newltrc\":1,\"ok\":false}");
			//}
		} else duplicate = false;
	}
}

if(!duplicate) {
	String resp = KevBean.runProgram(trnm);
	// add a sample comment to show version control
	
	String apidate = request.getParameter("rgdt"); 
	KevBean.changeProg("ZOR003MI");
	KevBean.setField("CONO", request.getParameter("cono"));
	KevBean.setField("DIVI", request.getParameter("divi"));
	KevBean.setField("MINM", minm);
	KevBean.setField("TRNM", trnm);
	KevBean.setField("CHID", request.getParameter("chid"));
	KevBean.setField("RGDT", apidate);
	String rgtm = request.getParameter("rgtm");rgtm = rgtm.replace(":", "");
	KevBean.setField("RGTM", rgtm);
	KevBean.setField("TMSX", request.getParameter("tmsx"));
	System.out.println("cake " + request.getParameter("tmsx"));
	
	String comments = ""; int newltrc = 0;
	
	if(resp.startsWith("OK")) { // run big phillity's API
		comments = "Fixed by " + (String)session.getAttribute("uname");
		newltrc = 2;
		KevBean.setField("COMT", comments);
		KevBean.setField("LTRC", "2"); // fixed
	} else {
		comments = "Second NOK by " + (String)session.getAttribute("uname");
		newltrc = 4;
		KevBean.setField("COMT", comments);
		KevBean.setField("LTRC", "4");
	}
	System.out.println("Second submit?" + KevBean.runProgram("UpdStatus"));
	
	out.println("{\"success\":true,\"resp\":\"" + resp + "\",\"comments\":\"" + comments + "\",\"newltrc\":" + newltrc + ",\"ok\":" + resp.startsWith("OK") + "}");
}
%>
