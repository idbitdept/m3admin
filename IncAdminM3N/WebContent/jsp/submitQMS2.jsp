<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "QMS400MI";
String localTrans = "UpdTestResult";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
// make sure the users has the same cono/divi/faci settings as the API account

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String qrid = request.getParameter("qrid"); if(qrid==null) qrid = "";

String uname = (String)session.getAttribute("uname");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>QMS results";
boolean newBano = false;

int lotNum = 0; String resp = ""; boolean keepGoing = true;
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject lots = (JSONObject) iterator.next();
	 if(!bano.equals((String) lots.get("bano"))) {
		 newBano = true;
		 qrid = "";
	 } else newBano = false;
	 
	 bano = (String) lots.get("bano");
	 whsl = (String) lots.get("whsl");
	 try {
		 if( (String) lots.get("itno")!=null) itno = (String)lots.get("itno");
		 System.out.println("line itno: " + itno);
	 } catch (Exception e) {
		 System.out.println("Use header itno" + itno);
	 }

	 LocalBean.setInitialise(localProg);
	 	 
	 Enumeration<String> parameterNames = request.getParameterNames();
	 while (parameterNames.hasMoreElements()) {
		 if(!keepGoing) break;
	     String paramName = parameterNames.nextElement();
	     if( (!paramName.equals("sdata")) && (!paramName.equals("itno")) && (!paramName.equals("whsl")) && (!paramName.startsWith("qop1:")) && (!paramName.startsWith("tsty:")) ) {
		     String[] paramValues = request.getParameterValues(paramName);
	         String paramValue = paramValues[0]; paramValue = paramValue.trim();
	         String[] qtstTseq = paramName.split("  -");
	         if(!paramValue.equals("")) {
	        	 
	        	 if(newBano) {
		        	 // get the most recent QRID for this lot...
		        	 KevBean.setInitialise("QMS302MI");
		        	 KevBean.setField("FACI", zdfaci);
		        	 KevBean.setField("ITNO", itno);
		        	 KevBean.setField("BANO", bano);
		        	 KevBean.runProgram("LstTestQIReq");
		        	 while(KevBean.nextRow()) {
		        		 if(!KevBean.getField("BANO").trim().equals(bano)) { break; }
		        		 if(KevBean.getField("QTST").trim().equals(qtstTseq[0].trim())) {
		        			 qrid = KevBean.getField("QRID");
		        		 }
		        	 }
	        	 }
	        	 
	        	 LocalBean.setField("FACI", zdfaci);
	        	 LocalBean.setField("ITNO", itno);
	        	 LocalBean.setField("BANO", bano);
	        	 LocalBean.setField("QRID", qrid);
	    		 
	        	 LocalBean.setField("QTST", qtstTseq[0]);
	        	 LocalBean.setField("TSEQ", qtstTseq[1]);
	    		 
	    		 String tsty = request.getParameter("tsty:" + paramName);
	    		 String qop1 = request.getParameter("qop1:" + paramName);
	    		 LocalBean.setField("TSTY", tsty);
	    		 LocalBean.setField("QOP1", qop1);
		         
		         if(tsty.equals("0"))
		        	 LocalBean.setField("QTRS", paramValue);
		         else
		        	 LocalBean.setField("QLCD", paramValue);
		         
		         LocalBean.setField("VLEN", "1");
		         LocalBean.setField("UPCT", "1");
		         
				 String qmsResp = LocalBean.runProgram(localTrans);
				 if(qmsResp.startsWith("NOK            Not allowed")) {
					 keepGoing = false;
					 qmsResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
					 publishResults += "<li> Lot " + bano + ": Test: " + qtstTseq[0] + " (" + qtstTseq[1] + ")" + ": " + qmsResp + "</li>";
					 break;
				 }
				 String thisSeq = " (" + qtstTseq[1] + ")";
				 if(qtstTseq[1].equals("1")) thisSeq = "";
				 publishResults += "<li> Lot " + bano + ": Test: " + qtstTseq[0] + thisSeq + ": " + qmsResp + "</li>";
	         }
	     }
	}
}

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
%>