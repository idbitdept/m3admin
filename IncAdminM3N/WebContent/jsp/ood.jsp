<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.List,java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="MWS070MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MMS060MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MMS850MIBean" class="com.ornua.MvxBean" scope="page"/>

<%

String uname = (String)session.getAttribute("uname");

MMS060MIBean.setSystem(system);
MMS060MIBean.setPort(PORT);
MMS060MIBean.setLib(LIB);
MMS060MIBean.setCompany(zdcono);
MMS060MIBean.setUsername(m3user);
MMS060MIBean.setPassword(m3pass);
MMS060MIBean.setInitialise("MMS060MI");

MMS850MIBean.setSystem(system);
MMS850MIBean.setPort(PORT);
MMS850MIBean.setLib(LIB);
MMS850MIBean.setCompany(zdcono);
MMS850MIBean.setUsername(m3user);
MMS850MIBean.setPassword(m3pass);
MMS850MIBean.setInitialise("MMS850MI");

MWS070MIBean.setSystem(system);
MWS070MIBean.setPort(PORT);
MWS070MIBean.setLib(LIB);
MWS070MIBean.setCompany(zdcono);
MWS070MIBean.setUsername(m3user);
MWS070MIBean.setPassword(m3pass);
MWS070MIBean.setInitialise("MWS070MI");

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);

String publishResults = "<ul>Results of OOD update on expired stock";

MWS070MIBean.setField("MRCD", "0"); // change to 0 (unlimited) or big number for live
MWS070MIBean.runProgram("SetLstMaxRec");

MWS070MIBean.setField("TTYP", "96");
MWS070MIBean.setField("TTYP", "96");
//MWS070MIBean.setField("XQT0", "1");
MWS070MIBean.setField("RESP", "M3ADMIN");
MWS070MIBean.setField("FTRD", today);
MWS070MIBean.runProgram("LstTransByUser");
while(MWS070MIBean.nextRow()) {
	// get the current remark
	String stas = MWS070MIBean.getField("STAS"); if(stas==null) stas = "";
	String trqt = MWS070MIBean.getField("TRQT"); if(trqt==null) trqt = "";
	String bano = MWS070MIBean.getField("BANO"); if(bano==null) bano = "";
	String camu = MWS070MIBean.getField("CAMU"); if(camu==null) camu = "";
	if( (stas.equals("3")) && (!trqt.contains("-")) ) {
		MMS060MIBean.setField("CONO", zdcono);
		MMS060MIBean.setField("WHLO", MWS070MIBean.getField("WHLO"));
		MMS060MIBean.setField("ITNO", MWS070MIBean.getField("ITNO"));
		MMS060MIBean.setField("WHSL", MWS070MIBean.getField("WHSL"));
		MMS060MIBean.setField("BANO", bano);
		MMS060MIBean.setField("CAMU", camu);
		MMS060MIBean.setField("REPN", MWS070MIBean.getField("REPN"));
		MMS060MIBean.runProgram("Get");
		String brem = MMS060MIBean.getField("BREM"); if(brem==null) brem = "";
		String newBrem = "OOD";
		if( (!brem.equals("")) && (!brem.contains(" OOD ")) ) newBrem = brem + " - OOD";
		if(newBrem.length()>20) newBrem = newBrem.substring(0,20);
		
		MMS850MIBean.setField("PRFL", "*EXE");
		MMS850MIBean.setField("CONO", zdcono);
		MMS850MIBean.setField("E0PA", "WS");
		MMS850MIBean.setField("E065", "WMS");
		MMS850MIBean.setField("WHLO", MWS070MIBean.getField("WHLO"));
		MMS850MIBean.setField("WHSL", MWS070MIBean.getField("WHSL"));
		MMS850MIBean.setField("ITNO", MWS070MIBean.getField("ITNO"));
		MMS850MIBean.setField("REPN", MWS070MIBean.getField("REPN"));
		MMS850MIBean.setField("BREF", MWS070MIBean.getField("BREF"));
		MMS850MIBean.setField("BRE2", MWS070MIBean.getField("BRE2"));
		
		MMS850MIBean.setField("BANO", bano);
		MMS850MIBean.setField("CAMU", camu);
		
		MMS850MIBean.setField("BREM", newBrem);
		MMS850MIBean.setField("REPN", MWS070MIBean.getField("REPN"));
		MMS850MIBean.setField("STAS", stas);
		MMS850MIBean.setField("CALT", "1");
		// not setting qty, date etc, m3 should default these, but test
		String reclass = MMS850MIBean.runProgram("AddReclass");
		publishResults+="<li>Lot: " + bano + ", Container: " + camu + ", Remark: " + newBrem + ", M3: " + reclass + "</li>";
	}
}
	
MMS850MIBean.closeConnection();
MMS060MIBean.closeConnection();
MWS070MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");
%>
	