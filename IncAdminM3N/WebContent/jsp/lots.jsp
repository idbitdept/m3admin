<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

String searchPo = request.getParameter("searchPo"); if(searchPo==null) searchPo = "";
String searchPoLine = "10";
String[] poBits = searchPo.split("/");
if(poBits.length==2) {
	searchPo = poBits[0];
	searchPoLine = poBits[1];
}


String json = "["; String bano = ""; String bref = "";

if(searchPo.equals("")) {
	KevBean.setInitialise("MMS060MI");
	//KevBean.setField("MRCD", "5000");
	KevBean.setField("MRCD", "6000");
	KevBean.runProgram("SetLstMaxRec");
	KevBean.setField("CONO", zdcono);
	KevBean.setField("WHLO", whlo);
	KevBean.setField("ITNO", itno);
	//System.out.println("caeke");
	KevBean.runProgram("List");
	//System.out.println("cakesf");
	while(KevBean.nextRow()) {
		String newbano = KevBean.getField("BANO");
		if(!bano.equals(newbano)) {
			json+= "{";
			json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";
			json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
			json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
			json+= "\"bano\":\"" + newbano + "\"";
			json+= "},";
		}
		bano = newbano;
	}
} else { // search by PO 
	System.out.println("Search by PO?");
	KevBean.setInitialise("MDBReadMI");
	//KevBean.setField("MRCD", "5000");
	KevBean.setField("MRCD", "100");
	KevBean.runProgram("SetLstMaxRec");
	KevBean.setField("RORC", "2"); // came in on PO
	KevBean.setField("RORN", searchPo);
	KevBean.setField("RORL", searchPoLine);
	KevBean.setField("RORX", "0");
	KevBean.setField("ITNO", itno);
	KevBean.runProgram("LstMILOMA30");
	
	while(KevBean.nextRow()) {
		String sItno = KevBean.getField("ITNO");
		String sRorn = KevBean.getField("RORN");
		String sRorl = KevBean.getField("RORL");
		if(!itno.equals(sItno) || !searchPo.equals(sRorn) || !searchPoLine.equals(sRorl)) break;
			json+= "{";
			json+= "\"whsl\":\"\",";
			json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
			json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
			json+= "\"bano\":\"" + KevBean.getField("BANO") + "\"";
			json+= "},";
	}
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
