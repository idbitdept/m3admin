<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%

//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String minm = request.getParameter("minm");
String trnm = request.getParameter("trnm");

String localProg = minm;
String localTrans = trnm;

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
 
LocalBean.setPassword(localPword); 
System.out.println("User: " + connAPI + " pass: " + localPword + " system: " + system);
// make sure the users has the same cono/divi/faci settings as the API account

if(system.startsWith("beckett")) LocalBean = KevBean; // messy doing beckett local account.. it'll be gone shortly anyway...

String maxRec = request.getParameter("maxRecs"); if(maxRec==null) maxRec = "";
//System.out.println("MaxRecs is " + maxRec);

String json = "{\"fields\": [{ ";

KevBean.setInitialise("MRS001MI");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "O"); 

KevBean.runProgram("LstFields");

String[] fieldNames = new String[200]; int i = 0; int t = 0;
while(KevBean.nextRow()) {
	String flnm = KevBean.getField("FLNM");
	fieldNames[i++] = flnm;
	json+= "\"name\":\"" + flnm + "\",";
}
if(json.length()>1) json = json.substring(0, json.length()-1);
json += "}],";

LocalBean.setInitialise(minm);
if(!maxRec.equals("")) {
	LocalBean.setField("MRCD", maxRec);
	LocalBean.runProgram("SetLstMaxRec");
}

Enumeration paramNames = request.getParameterNames();

while(paramNames.hasMoreElements()) {
   String paramName = (String)paramNames.nextElement();
   if(Character.isUpperCase(paramName.charAt(0))) {
	   LocalBean.setField(paramName, request.getParameter(paramName));
   }
}

String resp = LocalBean.runProgram(trnm); if(resp==null) resp = "";

json+= "\"data\": [ "; boolean isRecs = false;

if(resp.startsWith("NOK            Not allowed")) {
	 resp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
}


if(resp.startsWith("NOK")) {
	json = "{\"success\":false,\"msg\":\"" + resp + "\"}";
} else { 
	
	// would do the excel export here...
	boolean openBrack = false;
	while(LocalBean.nextRow()) {
		//System.out.println("Next row..");
		isRecs = true; json+= "{"; openBrack = true;
		for(t=0;t<i;t++)
			json+= "\"" + fieldNames[t] + "\":\"" + LocalBean.getField(fieldNames[t]) + "\",";
		if(json.length()>1) json = json.substring(0, json.length()-1);
		json+= "},";
	} 
	if(!isRecs) {
		//System.out.println("is rics not");
	
		json+= "{ "; openBrack = true;
		for(t=0;t<i;t++)
			json+= "\"" + fieldNames[t] + "\":\"" + LocalBean.getField(fieldNames[t]) + "\",";
		if(json.length()>1) json = json.substring(0, json.length()-1);
		json+= "},";
	}
	if(!openBrack) json+= "{";
	if(json.length()>1) json = json.substring(0, json.length()-1);
	json+="],\"success\":true}";
}
out.println(json);
%>
