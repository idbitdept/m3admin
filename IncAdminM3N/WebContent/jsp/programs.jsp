<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.setInitialise("MRS001MI");

KevBean.runProgram("SetLstMaxRec");
KevBean.runProgram("LstPrograms");

String minm = ""; String excls = (String)session.getAttribute("excls"); if(excls==null) excls = "";

String json = "[";
while(KevBean.nextRow()) {
	minm = KevBean.getField("MINM"); if(minm==null) minm = "";
	json+= "{";
	json+= "\"minm\":\"" + minm + "\",";
	json+= "\"excl\":" + excls.contains(minm) + ",";
	json+= "\"obnm\":\"" + KevBean.getField("OBNM") + "\",";
	json+= "\"chid\":\"" + KevBean.getField("CHID") + "\",";
	json+= "\"mids\":\"" + KevBean.getField("MIDS") + "\",";
	json+= "\"mnid\":\"" + KevBean.getField("MNID") + "\""; 
	json+= "},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
