<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String json = "["; 
// if whlo is 125 (Nantwich), 114 (ONI), 101 (Foods) -> load all items (or none!?) 

if( whlo.equals("125") || whlo.equals("114") || whlo.equals("101") ) {

	KevBean.setInitialise("MDBREADMI");
	KevBean.setField("MRCD", "6000");
	KevBean.runProgram("SetLstMaxRec");
	KevBean.setField("STAT", "20");
	KevBean.runProgram("LstMITMAS20");

	while(KevBean.nextRow()) {
		json+= "{";
		json+= "\"itno\":\"" + KevBean.getField("ITNO") + "\",";
		json+= "\"stock\":true,";
		json+= "\"itds\":\"" + KevBean.getField("ITNO") + " " + KevBean.getField("ITDS") + "\"";
		json+= "},";
	}
} else {
	KevBean.setInitialise("MMS200MI");
	KevBean.setField("MRCD", "0");
	KevBean.runProgram("SetLstMaxRec");
	KevBean.setField("CONO", zdcono);
	KevBean.setField("WHLO", whlo);
	// todo get INVF working (filter out non-zero balances.. would be nice)
	KevBean.runProgram("LstSumWhsBal");

	String stqt = ""; String quqt = ""; String rjqt = ""; 
	double dstqt = 0.0; double dquqt = 0.0; double drjqt = 0.0; boolean stock = false;
	while(KevBean.nextRow()) {
		stqt = KevBean.getField("STQT"); try { dstqt = Double.parseDouble(stqt); } catch (Exception e) {}
		quqt = KevBean.getField("QUQT"); try { dquqt = Double.parseDouble(quqt); } catch (Exception e) {}
		rjqt = KevBean.getField("RJQT"); try { drjqt = Double.parseDouble(rjqt); } catch (Exception e) {}
		
		if(dstqt > 0.0 || dquqt > 0.0 || drjqt > 0.0)
			stock = true; 
		else 
			stock = false;
		
		json+= "{";
		json+= "\"itno\":\"" + KevBean.getField("ITNO") + "\",";
		json+= "\"stock\":" + stock + ",";
		json+= "\"itds\":\"" + KevBean.getField("ITNO") + " " + KevBean.getField("ITDS") + "\"";
		json+= "},";
		//}
	}
}
if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
