<%@ page contentType="text/html; charset=utf-8" language="java" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String json = "[";
json+= "{";
json+= "\"qlcd\":\"ND\",";
json+= "\"tx40\":\"Not Detected\",";
json+= "\"actf\":1,";
json+= "\"txid\":0";
json+= "},";

json+= "{";
json+= "\"qlcd\":\"DETECTED\",";
json+= "\"tx40\":\"Detected\",";
json+= "\"actf\":1,";
json+= "\"txid\":0";
json+= "},";

json+= "{";
json+= "\"qlcd\":\"FAIL\",";
json+= "\"tx40\":\"Fail\",";
json+= "\"actf\":1,";
json+= "\"txid\":0";
json+= "},";

json+= "{";
json+= "\"qlcd\":\"PASS\",";
json+= "\"tx40\":\"Pass\",";
json+= "\"actf\":1,";
json+= "\"txid\":0";
json+= "},";

/*
KevBean.setInitialise("QMS001MI");
KevBean.setField("QLCD", "");
KevBean.runProgram("LstQualitative");

String json = "[";
while(KevBean.nextRow()) {
	json+= "{";
	json+= "\"qlcd\":\"" + KevBean.getField("QLCD") + "\",";
	json+= "\"tx40\":\"" + KevBean.getField("TX40") + " (" + KevBean.getField("QLCD") + ")\",";
	json+= "\"actf\":" + KevBean.getField("ACTF") + ",";
	json+= "\"txid\":" + KevBean.getField("TXID");
	json+= "},";
}
*/
if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
