<%@ page contentType="text/html; charset=utf-8" language="java" import="org.apache.commons.io.*,java.io.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>

<%

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray dispatchArray = (JSONArray) obj;
String publishResults = "<ul>Results of Dispatch Reprocessing";

String dispatchFile = "";
Iterator<JSONObject> iterator = dispatchArray.iterator();
while (iterator.hasNext()) {
	 JSONObject dispatches = (JSONObject) iterator.next();
	 dispatchFile = (String) dispatches.get("dispatch");
	 String dispatchFileName = dispatchFile + ".xml";
		
    File source = new File(dispatchSource+""+dispatchFileName);
	File dest = new File(dispatchDestination+""+dispatchFileName);
    
	try {
	    FileUtils.copyFile(source, dest);
	    publishResults = publishResults + "<li> Dispatch " + dispatchFile + ": Resubmitted</li>";
	} catch (IOException e) {
		//out.println("Error " + e);
		System.out.println("Error " + e);
	    e.printStackTrace();
	    publishResults = publishResults + "<li> Dispatch " + dispatchFile + " could not be resubmitted " + e + "</li>";
	}
}
publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

%>
