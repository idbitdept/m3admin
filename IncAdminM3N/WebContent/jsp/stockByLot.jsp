<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";

String bano = request.getParameter("bano"); if(bano==null) bano = "";

KevBean.setInitialise("MMS060MI");
KevBean.setField("MRCD", "5000");
KevBean.runProgram("SetLstMaxRec");

String json = "["; 


String[] arBanos = new String[100];

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray banoArray = (JSONArray) obj;

Iterator<JSONObject> iterator = banoArray.iterator(); int i = 0;
while (iterator.hasNext()) {
	//i++; System.out.println("I is " + i);
	
	 JSONObject banos = (JSONObject) iterator.next();
	 bano = (String) banos.get("bano");
	 
	 boolean newBano = true;
	 // make sure dont have already
	 //System.out.println(arBanos.length);
	 for (int t=0; t < arBanos.length; t++) {
		 //System.out.println(i + " " + arBanos[i]);
		 if ( (arBanos[t]!=null) && (bano.trim().equals(arBanos[t].trim())) ) { 
			 //System.out.println("already...." +bano);
			 newBano = false; 
			 break; 
			}
	 }
	 arBanos[i++] = bano; //System.out.println("CAke " + arBanos[i]);

	 if(newBano) {
		 //KevBean.setField("CONO", zdcono);
		 KevBean.setField("WHLO", whlo);
		 KevBean.setField("ITNO", itno);
		 KevBean.setField("BANO", bano);
		 KevBean.runProgram("LstLot");
	
		while(KevBean.nextRow()) {
				json+= "{";
				json+= "\"whsl\":\"" + KevBean.getField("WHSL") + "\",";		
				json+= "\"camu\":\"" + KevBean.getField("CAMU") + "\",";
				json+= "\"bref\":\"" + KevBean.getField("BREF") + "\",";
				json+= "\"bre2\":\"" + KevBean.getField("BRE2") + "\",";
				json+= "\"brem\":\"" + KevBean.getField("BREM") + "\",";
				json+= "\"cawe\":\"" + KevBean.getField("CAW2") + "\",";
				json+= "\"repn\":\"" + KevBean.getField("REPN") + "\",";
				json+= "\"unms\":\"" + KevBean.getField("UNMS") + "\",";
				json+= "\"stas\":\"" + KevBean.getField("STAS") + "\",";
				json+= "\"aloc\":\"" + KevBean.getField("ALOC") + "\",";
				json+= "\"stqt\":\"" + KevBean.getField("STQT") + "\",";
				json+= "\"alqt\":\"" + KevBean.getField("ALQT") + "\",";
				json+= "\"atnr\":\"" + KevBean.getField("ATNR") + "\",";
				json+= "\"atnb\":\"" + KevBean.getField("ATNB") + "\",";
		
				json+= "\"bano\":\"" + KevBean.getField("BANO") + "\"";
				json+= "},";
			}
	 }
	}

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
