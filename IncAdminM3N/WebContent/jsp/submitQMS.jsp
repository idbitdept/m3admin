<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,javax.mail.internet.MimeMessage.*,javax.mail.*,javax.mail.internet.*,java.util.Properties" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "QMS400MI";
String localTrans = "UpdTestResult";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
// make sure the users has the same cono/divi/faci settings as the API account

String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whsl = request.getParameter("whsl"); if(whsl==null) whsl = "";
String bano = request.getParameter("bano"); if(bano==null) bano = "";
String qrid = request.getParameter("qrid"); if(qrid==null) qrid = "";
String bref = request.getParameter("bref"); if(bref==null) bref = "";
String bre2 = request.getParameter("bre2"); if(bre2==null) bre2 = "";

String uname = (String)session.getAttribute("uname");

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray camuArray = (JSONArray) obj;
String publishResults = "<ul>QMS results";
String publishResultsTable = "<table border=1 width=\"800\"><tr><th>Item</th><th>Lot</th><th>Production daycode</th><th>Healthmark</th>";
boolean newBano = false;


Enumeration<String> parameterNames = request.getParameterNames();
while (parameterNames.hasMoreElements()) {
    String paramName = parameterNames.nextElement();
    if( (!paramName.equals("sdata")) && (!paramName.equals("itno")) && (!paramName.equals("whsl")) && (!paramName.equals("bref")) && (!paramName.equals("bre2")) && (!paramName.startsWith("qop1:")) && (!paramName.startsWith("tsty:")) ) {
	     String[] paramValues = request.getParameterValues(paramName);
        String paramValue = paramValues[0]; paramValue = paramValue.trim();
        String[] qtstTseq = paramName.split("  -");
        if(!paramValue.equals("")) {
        	publishResultsTable += "<th>" + paramName.replaceAll("-1", "") + "</th>";
    	}
	}
}
    publishResultsTable+="</tr>";


int lotNum = 0; String resp = ""; boolean keepGoing = true;
Iterator<JSONObject> iterator = camuArray.iterator();
while (iterator.hasNext()) {
	 JSONObject lots = (JSONObject) iterator.next();
	 if(!bano.equals((String) lots.get("bano"))) {
		 newBano = true;
		 qrid = "";
	 } else newBano = false;
	 
	 bano = (String) lots.get("bano");
	 whsl = (String) lots.get("whsl");
	 bref = (String) lots.get("bref"); if(bref==null) bref = "";
	 bre2 = (String) lots.get("bre2"); if(bre2==null) bre2 = "";
	 
	 try {
		 if( (String) lots.get("itno")!=null) itno = (String)lots.get("itno");
		 System.out.println("line itno: " + itno);
	 } catch (Exception e) {
		 System.out.println("Use header itno" + itno);
	 }
	 if(newBano) {
	 publishResultsTable += "<tr><td>" + itno + "</td><td>" + bano + "</td><td>" + bref + "</td><td>" + bre2 + "</td>";
	 
	 LocalBean.setInitialise(localProg);
	 	 
	 parameterNames = request.getParameterNames();
	 while (parameterNames.hasMoreElements()) {
		 if(!keepGoing) break;
	     String paramName = parameterNames.nextElement();
	     if( (!paramName.equals("sdata")) && (!paramName.equals("itno")) && (!paramName.equals("bref")) && (!paramName.equals("bre2")) && (!paramName.equals("whsl")) && (!paramName.startsWith("qop1:")) && (!paramName.startsWith("tsty:")) ) {
		     String[] paramValues = request.getParameterValues(paramName);
	         String paramValue = paramValues[0]; paramValue = paramValue.trim();
	         String[] qtstTseq = paramName.split("  -");
	         if(!paramValue.equals("")) {
	        	 
					int thisQrid = 0; int ihighestQrid = 0; String highestQrid = "";
		        	 // get the most recent QRID for this lot...
		        	 // kw update, surely don't do the update at ALL if its the same BANO
		        	 KevBean.setInitialise("QMS302MI");
		        	 KevBean.setField("FACI", zdfaci);
		        	 KevBean.setField("ITNO", itno);
		        	 KevBean.setField("BANO", bano);
		        	 KevBean.runProgram("LstTestQIReq");
		        	 while(KevBean.nextRow()) {
		        		 //System.out.println("Get babno:" + KevBean.getField("BANO") + " bano: " + bano);
		        		 if(!KevBean.getField("BANO").trim().equals(bano)) { break; }
		        		 //System.out.println("Get qtst: " + KevBean.getField("QTST")  + " cake: " + qtstTseq[0].trim());
		        		 if(KevBean.getField("QTST").trim().equals(qtstTseq[0].trim())) {
		        			 qrid = KevBean.getField("QRID");
		        			 int iqrid = 0;
		        			 try { iqrid = Integer.parseInt(qrid); } catch(Exception e) { System.err.println("qrid err " + e); }
		        			 if(iqrid>ihighestQrid) { ihighestQrid = iqrid; highestQrid = qrid; }
		        		 }
		        		 if(qrid.equals("")) qrid = KevBean.getField("QRID"); // if there's no test, use the most recent qrid to force a NOK Test doesnt exist (instead of 'more than one QRID for this..'.)
		        		 //System.out.println("Bano: " + bano + " qrid: " + qrid);
		        	 }
System.out.println("Qrid " + qrid);
System.out.println("Highest Qrid " + highestQrid);
	        	 
		        	 LocalBean.setField("FACI", zdfaci);
		        	 LocalBean.setField("ITNO", itno);
		        	 LocalBean.setField("BANO", bano);
		        	 LocalBean.setField("QRID", highestQrid);
		    		 
		        	 LocalBean.setField("QTST", qtstTseq[0]);
		        	 LocalBean.setField("TSEQ", qtstTseq[1]);
		    		 
		    		 String tsty = request.getParameter("tsty:" + paramName);
		    		 String qop1 = request.getParameter("qop1:" + paramName); if(qop1==null) qop1 = ""; if(qop1.equalsIgnoreCase("None")) qop1 = ""; // not always operators, e.g. ND
		    		 LocalBean.setField("TSTY", tsty);
		    		 LocalBean.setField("QOP1", qop1);
			         
			         if(tsty.equals("0"))
			        	 LocalBean.setField("QTRS", paramValue);
			         else
			        	 LocalBean.setField("QLCD", paramValue);
			         
			         LocalBean.setField("VLEN", "1");
			         LocalBean.setField("UPCT", "1");
			         
					 String qmsResp = LocalBean.runProgram(localTrans);
					 if(qmsResp.startsWith("NOK            Not allowed")) {
						 keepGoing = false;
						 qmsResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;
						 publishResults += "<li> Lot " + bano + ": Test: " + qtstTseq[0] + " (" + qtstTseq[1] + ")" + ": " + qmsResp + "</li>";
						 //break;
					 }
					 String thisSeq = " (" + qtstTseq[1] + ")";
					 if(qtstTseq[1].equals("1")) thisSeq = "";
					 publishResults += "<li> Lot " + bano + ": Test: " + qtstTseq[0] + thisSeq + ": " + qmsResp + "</li>";
					 String op = ""; 
					 if(qop1.equals("1")) op = ">"; else if(qop1.equals("3")) op = "<"; else if(qop1.equals("5")) op = "=";
					 
					 if(qmsResp.startsWith("N"))
						 publishResultsTable += "<td style=\"color: red;\">" + op + " " + paramValue + "<br/>" + qmsResp + "</style></td>";
					else
					 	publishResultsTable += "<td>" + op + " " + paramValue + "</td>";
		         }
        	 }
	     }     
	}
	 publishResultsTable += "</tr>";
}

publishResults = publishResults + "</ul>";
publishResultsTable += "</table>";
out.println("{\"success\":true, \"results\":\"" + JSONObject.escape(publishResultsTable) + "\"}");

if(!email.equals("")) {
	String host = "SMTP.ORNUA.COM";
	Properties props = new Properties();
	props.setProperty("mail.smtp.host", host);
	props.setProperty("mail.smtp.port", "25");
	Session sess = Session.getDefaultInstance(props);
	
	try {
	   MimeMessage message = new MimeMessage(sess);
	   message.setFrom(new InternetAddress("noreply@ornua.com"));
	   message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
	   message.setSubject("Your QMS Test Results Submission for " + zdcono + "/" + divi + " (" + zdfaci + ")");
	   message.setContent(publishResultsTable, "text/html");
	   
	   Transport.send(message);
	   System.out.println("Sent message successfully....");
	} catch (MessagingException mex) {
	   System.out.println("MAx " + mex);
	}
}
%>