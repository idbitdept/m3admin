<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,org.json.simple.JSONObject,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONArray" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String itno = request.getParameter("itno"); if(itno==null) itno = "";

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("itnos"));
String allItnos = "";
if(obj!=null) {
	
	JSONArray itnoArray = (JSONArray) obj;
	Iterator<JSONObject> iterator = itnoArray.iterator();
	
	while (iterator.hasNext()) {
		 JSONObject itnos = (JSONObject) iterator.next();
		 allItnos += "'" + (String) itnos.get("itno") + "',";
	}
	if(allItnos.length()>2) allItnos = allItnos.substring(0, allItnos.length()-1);
}

if(allItnos.equals("")) out.println("[{}]"); else {
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String uricono = (String)session.getAttribute("uricono");
String query = "select DISTINCT MLITNO, MLWHSL, MLWHLO, MLBRE2, substring(MLBREF, 1, 4) as daycode " + 
	//" from M3FDB" + uricono + ".MVXJDTA.MITLOC where MLCONO = ? " + 
	" from MVXJDTA.MITLOC where MLCONO = ? " +
	" and MLITNO in (" + allItnos  + ") " + 
	" and MLWHLO = ? and LEN(MLBREF)>=4 " + 
	//" and MLWHSL NOT LIKE '%=>%' " +
	// attributes and qms can be applied to in transit stock. it cannot be reclassified tho.
	" order by daycode ";
	//System.out.println(query);

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);

PreparedStatement ps = conn.prepareStatement(query); 
ps.setString(1, zdcono);
//ps.setString(2, itno);
ps.setString(2, whlo);
ResultSet rs = ps.executeQuery();

String json = "["; String bref = ""; String bre2 = ""; String julday = "";
String healthmark = ""; int cnt = 0;

while(rs.next()) {
	json+= "{";
	json+= "\"whsl\":\"" + rs.getString("MLWHSL").trim() + "\",";
	json+= "\"itno\":\"" + rs.getString("MLITNO").trim() + "\",";
	json+= "\"bref\":\"" + rs.getString("daycode") + "\",";
	json+= "\"bre2\":\"" + rs.getString("MLBRE2") + "\"";
	json+= "},";
	cnt++;
}
	

	if(cnt>0) {
		json+= "{";
		json+= "\"whsl\":\"All\",";
		json+= "\"itno\":\"All\",";
		json+= "\"bref\":\"All\",";
		json+= "\"bre2\":\"All\"";
		json+= "},";
	}
		


if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
}
%>
