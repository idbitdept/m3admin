<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.setInitialise("MRS001MI");
KevBean.setField("MRCD", "200"); // kw, sometimes more than 100 fields, imagine!
KevBean.runProgram("SetLstMaxRec");

String trnm = request.getParameter("trnm");
String minm = request.getParameter("minm");
String trtp = request.getParameter("trtp");

KevBean.setField("MINM", minm);
KevBean.setField("TRNM", trnm);
KevBean.setField("TRTP", "O"); 

KevBean.runProgram("LstFields");
	
String json = "{\"success\":true, \"results\":[ ";
while(KevBean.nextRow()) {
	json+= "{";
	json+= "\"dataIndex\":\"" + KevBean.getField("FLNM") + "\",";
	json+= "\"header\":\"" + KevBean.getField("FLDS") + " (" + KevBean.getField("FLNM") + ")\",";
	json+= "\"filter\":{ \"type\":\"'string'\"}";
	
	//json+= "\"leng\":" + KevBean.getField("LENG") + ",";
	//json+= "\"frpo\":" + KevBean.getField("FRPO") + ",";
	//json+= "\"topo\":" + KevBean.getField("TOPO") + ",";
	//json+= "\"mand\":\"" + KevBean.getField("MAND") + "\",";
	//json+= "\"type\":\"" + KevBean.getField("TYPE") + "\"";  
	json+= "},";
}

if(json.length()>1) json = json.substring(0, json.length()-1);
json += "]}";
out.println(json);
%>
