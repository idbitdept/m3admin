<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="MWS422MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MHS850MIBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="MWS410MIBean" class="com.ornua.MvxBean" scope="page"/>
<%

String uname = (String)session.getAttribute("uname");
String orno = request.getParameter("orno"); if(orno==null) orno = "";

long orsl = 0; long ponr = 0;

MWS422MIBean.setSystem(system);
MWS422MIBean.setPort(PORT);
MWS422MIBean.setLib(LIB);
MWS422MIBean.setCompany(zdcono);
MWS422MIBean.setUsername(m3user);
MWS422MIBean.setPassword(m3pass);

MHS850MIBean.setSystem(system);
MHS850MIBean.setPort(PORT);
MHS850MIBean.setLib(LIB);
MHS850MIBean.setCompany(zdcono);
MHS850MIBean.setUsername(m3user);
MHS850MIBean.setPassword(m3pass);

KevBean.setInitialise("MMS850MI");
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray ordersArray = (JSONArray) obj;
String publishResults = "<ul>Results of Order Line Completions";

Iterator<JSONObject> iterator = ordersArray.iterator();
while (iterator.hasNext()) {
	 JSONObject orders = (JSONObject) iterator.next();
	 orno = (String) orders.get("orno");
	 //orsl = (Long) orders.get("orsl");
	 ponr = (Long)orders.get("ponr");

	KevBean.setInitialise("MWS411MI");
	KevBean.setField("RORC", "3");
	KevBean.setField("RIDN", orno);
	KevBean.runProgram("LstDelLnByOrd"); String delNo = ""; String[] delnos = new String[20]; int i = 0;
	while(KevBean.nextRow()) {
		String thisDelNo = (String)KevBean.getField("DLIX");
		if(!delNo.equals(thisDelNo)) delnos[i++] = thisDelNo;
		delNo = thisDelNo;
	}
	for(int v=0;v<delnos.length;v++) {
		if(delnos[v]==null) break; else {
			
			MWS422MIBean.setInitialise("MWS422MI");
			MWS422MIBean.setField("DLIX", delnos[v]);
			System.out.println("List pick " + MWS422MIBean.runProgram("LstPickDetail"));
			while(MWS422MIBean.nextRow()) {
				String ordLine = MWS422MIBean.getField("RIDL");
				System.out.println("Order line " + ordLine + " ponr " + ponr);
				if(ordLine.trim().equals(ponr+"")) {
					System.out.println("Ordehhh@=");
					MHS850MIBean.setInitialise("MHS850MI");
					MHS850MIBean.setField("PRFL", "*EXE");
					MHS850MIBean.setField("CONO", zdcono);
					MHS850MIBean.setField("WHLO", MWS422MIBean.getField("WHLO"));
					MHS850MIBean.setField("ITNO", MWS422MIBean.getField("ITNO"));
					
					MHS850MIBean.setField("E0PA", "5013546007355");
					MHS850MIBean.setField("E065", "31");
	
					MHS850MIBean.setField("CUNO", MWS422MIBean.getField("CUNO"));
					MHS850MIBean.setField("QTYP", "0");
					MHS850MIBean.setField("QTYO", MWS422MIBean.getField("ALQT"));
					
					MHS850MIBean.setField("RIDN", MWS422MIBean.getField("RIDN"));
					
					MHS850MIBean.setField("RIDL", ordLine);
					MHS850MIBean.setField("RIDI", delnos[v]);
					MHS850MIBean.setField("PLSX", MWS422MIBean.getField("PLSX"));
					MHS850MIBean.setField("OEND", "1");
					
					MHS850MIBean.setField("PACT", "PALLET");
					MHS850MIBean.setField("ISMD", "0");
					String resp = MHS850MIBean.runProgram("AddCOPick");
					publishResults = publishResults + "<li> Order " + orno + "/" + ordLine + ": " + resp + "</li>"; 
				}
			}
		}
	}	
}
MHS850MIBean.closeConnection();
MWS422MIBean.closeConnection();
MWS410MIBean.closeConnection();

publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

%>
	