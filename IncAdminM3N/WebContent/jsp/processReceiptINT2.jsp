<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List,java.net.*,java.io.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
KevBean.changeProg("PPS001MI");
String json = "[";

boolean chkCompletePO = Boolean.parseBoolean(request.getParameter("chkCompletePO"));
int totValPallets = Integer.parseInt(request.getParameter("totValPallets"));

boolean allOKs = true; String closeMsg = "";

java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyyMMdd");
java.util.Date date = new java.util.Date();
String today = dateFormat.format(date);

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray pallets = (JSONArray) obj;

int i = 0; int actReceipts = 0;
Iterator<JSONObject> iterator = pallets.iterator();
while (iterator.hasNext()) {
	 JSONObject pallet = (JSONObject) iterator.next();
	 
	 long dlqa = (Long)pallet.get("dlqa");
	 //System.out.println("i is " + i + " qty: " + qty); 
	 i++;
	 if(dlqa>0) {
		 actReceipts++;

		 KevBean.setField("CONO", zdcono);
		 KevBean.setField("TRDT", today.trim());
		 KevBean.setField("RESP", "IDBKEWO"); //hmm
		 KevBean.setField("PUNO", (String)pallet.get("ridn")); 
		 KevBean.setField("PNLI", (Long)pallet.get("ridl") + "");
		 
		 KevBean.setField("RVQA", dlqa + "");
		 System.out.println("cae");
		 
		 String scawe = "";
		 try {
			 Double dcawe = (Double)pallet.get("cawe");
			 scawe = dcawe + "";
		 } catch (Exception e) {
			 Long lcawe = (Long)pallet.get("cawe");
			 scawe = lcawe + "";
		 }
		 KevBean.setField("CAWE", scawe);
		 
		 KevBean.setField("WHLO", (String)pallet.get("whlo")); 
		 KevBean.setField("WHSL", (String)pallet.get("whsl")); 
		 KevBean.setField("SUDO", (String)pallet.get("sudo")); 
		 
		 KevBean.setField("CAMU", (String)pallet.get("camu")); 
		 KevBean.setField("BANO", (String)pallet.get("bano")); 
		 
		 KevBean.setField("BREF", (String)pallet.get("bref")); 
		 KevBean.setField("BRE2", (String)pallet.get("bre2")); 
		 
		 KevBean.setField("PRDT", (String)pallet.get("mfdt"));

		 String msg = KevBean.runProgram("Receipt");		 
		 System.out.println("message is " + msg);
		 
		 boolean ok = false; 
		 if(msg.startsWith("OK")) {
			 ok = true;
		 } else {
			allOKs = false;
		 }
		
		
		String msgn = ""; //String msg = ""; 
		
	 	json+= "{\"msg\":\"" + msg + "\",\"msgn\":\"" + msg + "\"},";
	 } else {
		 json+= "{\"msg\":\"\",\"msgn\":\"\", \"ok\":true},";
	 }
} // end looping through the JSON array

if(json.length()>2) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json); 
//System.out.print(json);

%>