<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*"%>
<%@ include file="Connections/INCm3.jsp" %>
<%
String whlo = "150"; // neukirchen (evolve)
// 157 (eurofrigo roermond -> ornua deutschland (neukirchen))

String json = "["; 

String bref = ""; String bre2 = ""; String julday = "";
String healthmark = ""; int cnt = 0;

String uricono = (String)session.getAttribute("uricono");
if(uricono.equals("x902")) {
	zdcono = "501";
	m3userDB = "b2bim3dbdev"; // tst, etc
	m3passDB = "sadfhbgjadseolk78#";
	LIB = "CUSJDTA";
	libby = "CUSJDTA";
	strCon = "jdbc:sqlserver://DEVM3DB01.ORNUA.CORP:51182;databaseName=M3FDBDEV"; // tst, etc
	dispatchSource = "\\\\b2bitst.ornua.com\\evolve\\archive\\in\\xml\\desadv\\";
	dispatchDestination = "\\\\ornua.corp\\m3-dev\\EC_Central\\DEV\\input\\EDIFACT_DESADV\\";
	orderSource = "\\\\b2bitst.ornua.com\\evolve\\archive\\in\\xml\\orders\\";
	orderDestination = "\\\\ornua.corp\\m3-dev\\EC_Central\\DEV\\input\\TRADACOMS_ORDERS\\";
	GISroot = "\\\\b2bitst.ornua.com/";
} else if(uricono.equals("902")) {
	zdcono = "784";
		m3userDB = "b2bim3dbtst"; // tst, etc
		m3passDB = "sadfhbgjadseolk78#";
		LIB = "CUSJDTA";
		libby = "CUSJDTA";
		//strCon = "jdbc:sqlserver://DEVM3DB01.ORNUA.CORP:51182;databaseName=M3FDBTST"; // tst, etc
		strCon = "jdbc:sqlserver://M3BEPRESQL.ORNUA.CORP:1432;databaseName=M3FDBTST"; // tst, etc
		dispatchSource = "\\\\b2bitst.ornua.com\\evolve\\archive\\in\\xml\\desadv\\";
		dispatchDestination = "\\\\ornua.corp\\m3-tst\\EC_Central\\TST\\input\\EDIFACT_DESADV\\";
		orderSource = "\\\\b2bitst.ornua.com\\evolve\\archive\\in\\xml\\orders\\";
		orderDestination = "\\\\ornua.corp\\m3-tst\\EC_Central\\TST\\input\\TRADACOMS_ORDERS\\";
		GISroot = "\\\\b2bitst.ornua.com/";
	} else if(uricono.equals("802")) {
		m3userDB = "b2bim3dbprd"; // tst, etc
		m3passDB = "Uwgfskjdf75#";
		LIB = "CUSJDTA";
		libby = "CUSJDTA";
		//strCon = "jdbc:sqlserver://DEVM3DB01.ORNUA.CORP:51182;databaseName=M3FDBTST"; // tst, etc
		strCon = "jdbc:sqlserver://m3beprdsql.ORNUA.CORP:1432;databaseName=M3FDBPRD"; // tst, etc
		dispatchSource = "\\\\b2biprd.ornua.com\\evolve\\archive\\in\\xml\\desadv\\";
		dispatchDestination = "\\\\ornua.corp\\m3-prd\\EC_Central\\PRD\\input\\EDIFACT_DESADV\\";
		orderSource = "\\\\b2biprd.ornua.com\\evolve\\archive\\in\\xml\\orders\\";
		orderDestination = "\\\\ornua.corp\\m3-prd\\EC_Central\\PRD\\input\\TRADACOMS_ORDERS\\";
		GISroot = "\\\\b2biprd.ornua.com/";
		//System.out.println("dispatch source " + dispatchSource + " dispatch des " + dispatchDestination);
	}

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
System.out.println("System: " + system);
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);
System.out.println(strCon + " " + m3userDB + " " + m3passDB);
String query = " select MGTRNR, MGTRTP, MRPONR, MGRORC, MGRORN, MRTRQT, " +                   
" NLDLIX as OQDLIX, OQPLSX, NLCNCD, NLCSEA, " +  
" MRPONR, MRWHLO, MWWHNM, MRITNO, OQNEWE, OQGRWE, OQRGDT, MRTRSH " +              
" from MVXJDTA.MGHEAD " +                                                      
" inner join MVXJDTA.MGLINE on MGCONO = MRCONO and MGTRNR = MRTRNR " +   
" inner join MVXJDTA.MITWHL on MRCONO = MWCONO and MRWHLO = MWWHLO " +    
" inner join MVXJDTA.MHDISH on MGCONO = OQCONO and MGTRNR = OQRIDN " +   
" inner join MVXJDTA.MHDISL on OQCONO = URCONO and OQRIDN = URRIDN " +   
" and OQDLIX = URDLIX and OQWHLO = ? " + //and MRPONR = URRIDL " + kw could be multiple lines per delivery in m3...                              
" inner join CUSJDTA.ZHCONL on MGCONO = NLCONO and MRTRNR = NLRIDN and MRPONR =  NLRIDL " +               
" where MGCONO = ? and MGTWLO = ? and MRTRSH in (99, 69, 66) " +   
" and MGRIDT > ?  " +                                                
" order by MRTRSH asc, MGRGDT desc ";

PreparedStatement ps = conn.prepareStatement(query);
ps.setString(1, whlo);
ps.setString(2, zdcono);
ps.setString(3, whlo);
ps.setInt(4, 20211115);

System.out.println(whlo + " " + zdcono + " ");
ResultSet rs = ps.executeQuery();


	while(rs.next()) {
		json+= "{";
		
		json+= "\"ridl\":" + rs.getInt("MRPONR") + ",";
		json+= "\"trsh\":" + rs.getInt("MRTRSH") + ",";
		json+= "\"qty\":" + rs.getDouble("MRTRQT") + ",";
		json+= "\"dlix\":" + rs.getInt("OQDLIX") + ",";
		json+= "\"plsx\":" + rs.getInt("OQPLSX") + ",";
		json+= "\"newe\":" + rs.getDouble("OQNEWE") + ",";
		json+= "\"grwe\":" + rs.getDouble("OQGRWE") + ",";		
		json+= "\"rgdt\":" + rs.getInt("OQRGDT") + ",";
	
		json+= "\"po\":\"" + rs.getString("MGRORN") + "\",";
		json+= "\"ridn\":\"" + rs.getString("MGTRNR") + "\",";
		json+= "\"trtp\":\"" + rs.getString("MGTRTP") + "\",";
		json+= "\"do\":\"" + rs.getString("MGTRNR") + "/" + rs.getInt("MRPONR") + "\",";
		json+= "\"itno\":\"" + rs.getString("MRITNO") + "\",";
		json+= "\"whnm\":\"" + rs.getString("MWWHNM").trim() + " - " + rs.getString("MRWHLO") + "\",";
		json+= "\"whlo\":\"" + rs.getString("MRWHLO") + "\",";
		json+= "\"zsea\":\"" + rs.getString("NLCSEA") + "\",";
		json+= "\"zcnc\":\"" + rs.getString("NLCNCD") + "\"";
		json+= "},";
	}

if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
