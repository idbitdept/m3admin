<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.text.SimpleDateFormat,java.util.GregorianCalendar"%>
<%@ include file="Connections/connections.jsp" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
String whlo = "092"; // neukirchen

String ridn = request.getParameter("ridn"); if(ridn==null) ridn = "";
String ridl = request.getParameter("ridl"); if(ridl==null) ridl = ""; ridl = ridl.trim(); if(ridl.equals("")) ridl = "0"; int iridl = 0; try { iridl = Integer.parseInt(ridl); } catch (Exception e) { System.err.println("Num DE: " + e); }
String availRidl = request.getParameter("availRidl"); if(availRidl==null) availRidl = ""; availRidl = availRidl.trim(); if(availRidl.equals("")) availRidl = "0"; int iavailRidl =0; try { iavailRidl = Integer.parseInt(availRidl); }  catch(Exception e) { System.err.println("Num DE2: " + e); }

String ridi = request.getParameter("ridi"); if(ridi==null) ridi = ""; ridi = ridi.trim(); int iridi = Integer.parseInt(ridi);
String po = request.getParameter("po"); if(po==null) po = ""; po = po.trim();
String zcnc = request.getParameter("zcnc"); if(zcnc==null) zcnc = ""; zcnc = zcnc.trim();
String trsh = request.getParameter("trsh"); if(trsh==null) trsh = ""; trsh = trsh.trim();
String frmWhlo = request.getParameter("frmWhlo"); if(frmWhlo==null) frmWhlo = ""; frmWhlo = frmWhlo.trim();

String foundPOHead = request.getParameter("foundPOHead"); if(foundPOHead==null) foundPOHead = ""; 
// prob would be better to get the KG mult on the dub side but lets try this..
// ok, get PO_UM for starters

	String puun = "";
	String itno = "";
	String pitt = "";
	String whloDE = "IDB";
		
System.out.println("IRIDL IS " + iridl);

KevBean.setInitialise("PPS200MI");
KevBean.setField("CONO", uricono);
KevBean.setField("PUNO", po);
		if(iavailRidl==0) availRidl = ridl; // no prompted PO line, so just use DO line...
KevBean.setField("PNLI", availRidl);
String getLine = KevBean.runProgram("GetLine"); System.out.println("GEt line: " + getLine);
if(getLine.startsWith("OK")) {
	puun = KevBean.getField("PUUN"); puun = puun.trim();
	itno = KevBean.getField("ITNO");
	pitt = KevBean.getField("PITT"); if(pitt==null) pitt = "";
	whloDE = KevBean.getField("WHLO");
} else { // just get the item code from the first PO line, (ir)regardless of status
	KevBean.setField("CONO", uricono);
	KevBean.setField("PUNO", po);
	KevBean.runProgram("LstLine");
	if(KevBean.nextRow()) {
		puun = KevBean.getField("PUUN"); puun = puun.trim();
		itno = KevBean.getField("ITNO");
		pitt = KevBean.getField("PITT"); if(pitt==null) pitt = "";
		whloDE = KevBean.getField("WHLO");
	}
}
// hierarchy of line number to retrieve:
// 1) from the prompted RIDL on the portal (tries to match DO -> PO line, if fails, next available open line)
// 2) if that's 0 (no suitable line) just match against the DO line anyway
// 3) if that returns nothing, try a match on first PO line (regardless of status) -> this gets us the Item code and UM 
// important note here is that portal can only suggest a line, user has to be responsible for changing it
// todo if line number is changed on portal screen, re-load lines to reflect item code on that line...		

KevBean.setInitialise("MMS200MI");
KevBean.setField("CONO", uricono);
KevBean.setField("FITN", itno);
KevBean.setField("TITN", itno);
KevBean.setField("FAUT", "1");
KevBean.setField("TAUT", "1");
System.out.println("ALt UM: " + KevBean.runProgram("LstItmAltUnitMs") + " " + uricono + " " + itno);
String mult = "1";
if(puun.equals("KG")) {
	while(KevBean.nextRow()) {
		if(KevBean.getField("ALUN").equals("KTN")) // dub always CTN, test
			mult = KevBean.getField("COFA");
	}
}
double dmult = 1;
try {
	dmult = Double.parseDouble(mult);
} catch (Exception e) {
	System.err.println("cofa " + e);
}

String json = "["; 
String julday = "";
String prdt = "";

Driver drv = (Driver)Class.forName(MY_DRIVER).newInstance();
Connection con = DriverManager.getConnection(MY_STRING,MY_USERNAME,MY_PASSWORD);
//PreparedStatement ps = con.prepareStatement("SELECT MMITNO, MMITDS, MMATMO FROM " + lib + ".mitmas WHERE MMCONO = ? and MMATMO > ''");
String query = "";

if(trsh.equals("99")) { // already received on DO (old method)
	whlo = "092";
query = "select mtbano, mtcamu, mtbref, mtbre2, mttrqt " +                 
				" from " + lib + ".mittra where mtcono = ? " + 
				" and mtridn = ? and mtridl = ? and mtridi = ? " +  
				" and mtwhlo = ? and mtttid = 'MVW' and mttrqt > 0 " +                                               
		" order by mtcamu "; 

} else if(trsh.equals("66")) { // dispatched, not received
	whlo = frmWhlo;
	query = "select mtbano, mtcamu, mtbref, mtbre2, mttrqt " +                 

		" from " + lib + ".mittra where mtcono = ? " + 
		" and mtridn = ? and mtridl = ? and mtridi = ? " +  
		" and mtwhlo = ? and mtttid = 'MVW' and mtwhsl <> 'WHS' " +                                               
" order by mtcamu ";
		} else if(trsh.equals("69")) { // what's dispatched not yet received
			whlo = frmWhlo;
			/*query = "select mqbano as mtbano, mqcamu as mtcamu, mlbref as mtbref, mlbre2 as mlbre2, mqtrqt as mttrqt " +                 

				" from " + lib + ".mitalo where mqcono = ? " + 
				" and mqridn = ? and mqridl = ? and mqridi = ? " +                                           
		" order by mqcamu ";*/
			
			query = " select mqbano as mtbano, mqcamu as mtcamu, mlbref as mtbref, mlbre2 as mtbre2, mqalqt as mttrqt " +                 
					" from " + lib + ".mitalo inner join " + lib + ".mitloc   " +                                 
					" on mqcono = mlcono and mqbano = mlbano      " +                     
					" where mqcono = ? and mlalqt > 0    " +                            
					" and mqridn = ? and mqridl = ? and mqridi = ? and mlwhlo = ? " + 
					" order by mqcamu  ";
			
				}

System.out.println("QURE " + query);

PreparedStatement ps = con.prepareStatement(query);
ps.setString(1, MM_CONO);
ps.setString(2, ridn);
ps.setInt(3, iridl*100);
ps.setInt(4, iridi);
ps.setString(5, whlo); 
System.out.println(MM_CONO + "x " + ridn + "x " + iridl*100 + "x " + iridi + "x " + whlo);

ResultSet rs = ps.executeQuery();

	while(rs.next()) {
		String camu = rs.getString("mtcamu"); if(camu==null) camu = "";
		prdt = ""; int yyyy = 0;
		String dayLet; int dayJul;
		
		dayLet = camu.substring(0,1);
		dayJul = Integer.parseInt(camu.substring(1,4));
		// not likely to be still running in 2022... can replace with api or query
		if(dayLet.equals("P")) yyyy = 2021;
		else if(dayLet.equals("T")) yyyy = 2011;
		else if(dayLet.equals("N")) yyyy = 2012;
		else if(dayLet.equals("B")) yyyy = 2013;
		else if(dayLet.equals("C")) yyyy = 2014;
		else if(dayLet.equals("D")) yyyy = 2015;
		else if(dayLet.equals("G")) yyyy = 2016;
		else if(dayLet.equals("H")) yyyy = 2017;
		else if(dayLet.equals("K")) yyyy = 2018;
		else if(dayLet.equals("L")) yyyy = 2019;
		else if(dayLet.equals("M")) yyyy = 2020;
			
			GregorianCalendar gc = new GregorianCalendar();
	        gc.set(GregorianCalendar.DAY_OF_YEAR, dayJul);
	        gc.set(GregorianCalendar.YEAR, yyyy);
        
	        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
	        fmt.setCalendar(gc);
	        prdt = fmt.format(gc.getTime()); 
	        
		json+= "{";
		
		json+= "\"qty\":" + rs.getDouble("mttrqt") + ",";
		
		if(foundPOHead.equals("true")) {
			json+= "\"mult\":" + dmult + ","; 
			json+= "\"qtyDE\":" + rs.getDouble("mttrqt") * dmult + ",";
		} else {
			json+= "\"mult\":0,"; 
			json+= "\"qtyDE\":0,";
		}
		
		json+= "\"brem\":\"" + zcnc + "\",";
		
		//json+= "\"qtyDE\":49,";
		
		json+= "\"camu\":\"" + camu + "\",";
		json+= "\"bano\":\"" + rs.getString("mtbano") + "\",";
		json+= "\"bref\":\"" + rs.getString("mtbref") + "\",";
		
		json+= "\"whlo\":\"" + whloDE + "\",";
		json+= "\"prdt\":\"" + prdt + "\",";
		json+= "\"itno\":\"" + itno + "\",";
		json+= "\"pitt\":\"" + pitt + "\",";
		
		json+= "\"bre2\":\"" + rs.getString("mtbre2") + "\"";

		
		json+= "},";
	}
		


if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
