<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>

<%
String excls = (String)session.getAttribute("excls"); if(excls==null) excls = "";

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);

String where = "";

String ltrcdesc = request.getParameter("ltrcdesc"); if(ltrcdesc==null) ltrcdesc = "";
if(!ltrcdesc.equals("")) {
	if(ltrcdesc.equals("OK")) where += " and XMLTRC = 0";
	else if(ltrcdesc.equals("NOK")) where += " and XMLTRC = 1";
	else if(ltrcdesc.equals("Fixed")) where += " and XMLTRC = 2";
	else if(ltrcdesc.equals("Cleared")) where += " and XMLTRC = 3";
	else if(ltrcdesc.equals("Second NOK")) where += " and XMLTRC = 4";
}

if(!excls.equals("")) where += " and XMMINM not in " + excls;

String oknoks = request.getParameter("code"); if(oknoks==null) oknoks = ""; oknoks = oknoks.trim();
if(!oknoks.equals("")) where += " and XMLTRC = " + oknoks;

String trans = request.getParameter("trans"); if(trans==null) trans = ""; trans = trans.trim();
if(!trans.equals("")) where += " and XMMINM = '" + trans + "'";

String rgdt = request.getParameter("rgdt"); if(rgdt==null) rgdt = ""; rgdt = rgdt.trim();
if(!rgdt.equals("")) where += " and XMRGDT = '" + rgdt + "'";

String whlo = request.getParameter("whlo"); if(whlo==null) whlo = ""; //whlo = whlo.trim();
if(whlo.equals("Not given")) whlo = "   ";
if(!whlo.equals("")) where += " and XMWHLO = '" + whlo + "'";

String usr3 = request.getParameter("usr3"); if(usr3==null) usr3 = "";
if(usr3.equals("Not given")) usr3 = "   ";
if(!usr3.equals("")) where += " and XMUSR3 = '" + usr3 + "'";

String usr4 = request.getParameter("usr4"); if(usr4==null) usr4 = "";
if(usr4.equals("Not given")) usr4 = "   ";
if(!usr4.equals("")) where += " and XMUSR4 = '" + usr3 + "'";

String usr5 = request.getParameter("usr5"); if(usr5==null) usr5 = "";
if(usr5.equals("Not given")) usr5 = "   ";
if(!usr5.equals("")) where += " and XMUSR5 = '" + usr3 + "'";

String filter = request.getParameter("filter");
if (filter==null) filter = ""; filter = filter.trim();
if(!filter.equals("")) {
	String operator = ""; String value = ""; String property = "";
	JSONParser parser = new JSONParser();
	Object obj = parser.parse(filter);
	JSONArray filts = (JSONArray) obj;
	 Iterator<JSONObject> iterator = filts.iterator();
	 while (iterator.hasNext()) {
		 JSONObject filt = (JSONObject) iterator.next();
		 property = (String) filt.get("property"); if(property.equals("order")) property = "ridn"; property = property.toUpperCase();
		 operator = (String) filt.get("operator"); 
		 if(property.equals("FACI")) {
			 value = "(";
			 JSONArray jaValue2 = (JSONArray) filt.get("value"); //System.out.println("Russ2: " + jaValue2);
			 for (int i = 0 ; i < jaValue2.size(); i++) {
				 String fac  = (String)jaValue2.get(i);
			     value += "'" + fac + "',";
		     }
			 if(value.length()>2) value = value.substring(0, value.length() - 1);
			 value+=")";
		 } else if(property.equals("WHLO")) {
			 value = "(";
			 JSONArray jaValue2 = (JSONArray) filt.get("value");
			 for (int i = 0 ; i < jaValue2.size(); i++) {
				 String fltwhlo  = (String)jaValue2.get(i);
			     value += "'" + fltwhlo + "',";
		     }
			 if(value.length()>2) value = value.substring(0, value.length() - 1);
			 value+=")";
		 } else {
		 	value = (String) filt.get("value"); 
		 }
		 
		 if(operator.equalsIgnoreCase("EQ")) {
			 operator = " = ";
			 where = where + " and XM" + property + " " + operator + " " + value;
		 } else if(operator.equalsIgnoreCase("GT")) {
			 operator = " > ";
			 where = where + " and XM" + property + " " + operator + " " + value;
		 } else if(operator.equalsIgnoreCase("LT")) {
			 operator = " < ";
			 where = where + " and XM" + property + " " + operator + " " + value;
		 } else if(operator.equalsIgnoreCase("IN") && property.equals("FACI")) {
			 where = where + " and SUBSTRING(XMRCVD, 16,3) in " + value;
		 } else if(operator.equalsIgnoreCase("IN") && property.equals("WHLO")) {
			 where = where + " and XMWHLO in " + value;
		 } else
			 where = where + " and XM" + property + " " + operator + " '%" + value + "%'";		 	
	 }
}
//System.out.println("Where: " + where);
String query = "";
if(system.startsWith("m3"))
	query = "select top 500 * from CUSJDTA.ZMILOG where XMCONO = " + icono + " and XMDIVI = '" + divi + "' " + where + " order by XMRGDT desc, XMRGTM desc";
else  // CUSJDTA.ZMILOG 
	query = "select * from " + libby + ".zmilog where xmcono = " + icono + " and xmdivi = '" + divi + "' " + where + " FETCH FIRST 500 ROWS ONLY ";
//System.out.println("Query apilogx " + query);
String json = "[";

if(!where.equals("")) {

	PreparedStatement ps = conn.prepareStatement(query); //
	//ps.setObject(1, field1);
	//ps.setObject(2, field2);
	//System.out.println("Query: " + query);
	ResultSet rs = ps.executeQuery();



          while (rs.next()) {
        	  
        	  String ridn = rs.getString("XMRIDN"); if(ridn==null) ridn = ""; ridn = ridn.trim();
        	  String order = ""; int ridx = 0; if(system.startsWith("joyce")) ridx = rs.getInt("XMRIDX");
        	  int ridl = 0; ridl = rs.getInt("XMRIDL");
        	  if(!ridn.equals("")) order = rs.getString("XMRIDN");
        	  if(ridl!=0) order += "/" + rs.getInt("XMRIDL");
        	  if(ridx!=0) order += "" + ridx;
        	  
        	  String rgtm = rs.getString("XMRGTM");
        	  rgtm = rgtm.trim();
        	  if(rgtm.length()<6) {
        		  int len = rgtm.length();
        		  for (int w=len; w<6;w++) rgtm = "0" + rgtm;
        	  }
        	  rgtm = rgtm.substring(0,2) + ":" + rgtm.substring(2,4) + ":" + rgtm.substring(4,6);
        	  String itno = rs.getString("XMITNO"); if(itno==null) itno = ""; itno = itno.trim();
        	  String minm = rs.getString("XMMINM"); if(minm==null) minm = ""; minm = minm.trim();
        	  String trnm = rs.getString("XMTRNM"); if(trnm==null) trnm = ""; trnm = trnm.trim();
        	  String camu = rs.getString("XMCAMU"); if(camu==null) camu = ""; camu = camu.trim();
        	  //System.out.println("Itno: " + itno + " minm: " + minm + " trnm: " + trnm + " camu: " + camu + itno);
        	  if(itno.equals("") && minm.equals("PPS001MI") && trnm.equals("Receipt") && camu.length() >= 6) itno = camu.substring(0,6);
        	  json = json + "{\"camu\":\"" + camu + "\",\"minm\":\"" + minm + "\",\"trnm\":\"" + trnm + 
        		  "\", \"bre2\":\"" + rs.getString("XMBRE2") + 
        		  "\", \"ridn\":\"" + JSONObject.escape(ridn) + 
        		  "\", \"ridl\":" + rs.getInt("XMRIDL") + "," +
  				  "\"itno\":\"" + itno + "\"," +
				  //"\"bref\":\"" + rs.getString("XMBREF") + "\"," +
				  "\"order\":\"" + JSONObject.escape(order) + "\"," +
				  "\"rcvd\":\"" + JSONObject.escape(rs.getString("XMRCVD")) + "\"," +
				  "\"sndd\":\"" + JSONObject.escape(rs.getString("XMSNDD")) + "\"," +
				  "\"whlo\":\"" + rs.getString("XMWHLO") + "\"," +
				  "\"usr1\":\"" + rs.getString("XMUSR1") + "\"," +
				  "\"usr2\":\"" + rs.getString("XMUSR2") + "\"," +
				  "\"usr3\":\"" + rs.getString("XMUSR3") + "\"," +
				  "\"usr4\":\"" + rs.getString("XMUSR4") + "\"," +
				  "\"usr5\":\"" + rs.getString("XMUSR5") + "\"," +
				  "\"dlix\":" + rs.getInt("XMDLIX") + "," +
				  "\"ltrc\":" + rs.getInt("XMLTRC") + "," +
				  "\"ridx\":" + ridx + "," +
				  
				  "\"cono\":" + rs.getInt("XMCONO") + "," +
				  
					"\"tmsx\":" + rs.getInt("XMTMSX") + "," +

				  "\"divi\":\"" + rs.getString("XMDIVI") + "\"," +
				  
				  "\"chid\":\"" + rs.getString("XMCHID") + "\"," +
			  		
				  
					"\"rgdt\":\"" + rs.getString("XMRGDT") + "\"," +
					"\"rgtm\":\"" + rgtm + "\"," +
				  
				  "\"sudo\":\"" + rs.getString("XMSUDO") + "\"" +
        		  "},";
          }
   
          if(rs!=null) rs.close();  
          if(ps!=null) ps.close();  
          if(conn!=null) conn.close();  
}
          
          if(json.length()>2) json = json.substring(0, json.length() - 1);
          
          json = json + "]";
          
          out.print(json);
          //System.out.print(json);

%>
