<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

String json = "[";


if(system.startsWith("m3")) {
	KevBean.setInitialise("MDBREADMI");
	KevBean.setField("MRCD", "5000");
	KevBean.runProgram("SetLstMaxRec");
	KevBean.setField("DIVI", divi); System.out.println("Setting divi " + divi);
	KevBean.runProgram("LstZMITRN00A1");
	
	while(KevBean.nextRow()) {
		String thisDivi = KevBean.getField("DIVI"); if(thisDivi==null) thisDivi = "";
			if(thisDivi.equals(divi))
		json = json + "{" + 
	  			  "\"cono\":"	+ zdcono + "," +
	  			  "\"minm\":\"" + KevBean.getField("MINM") 	+ "\"," + 
				  "\"trnm\":\"" + JSONObject.escape(KevBean.getField("TRNM")) + "\"," + 
				 "\"chid\":\"M3\"," + 
				  "\"actv\":" + KevBean.getField("ACTV")  	+ 
	  			"},";
	}
				
} else {
	

Driver driver = (Driver)Class.forName(DRIVER).newInstance();
Connection conn = DriverManager.getConnection(strCon, m3userDB, m3passDB);

String query = "select * from " + libby + ".zmitrn where xncono = ? order by XNACTV desc, XNCONO, XNMINM, XNTRNM";

PreparedStatement ps = conn.prepareStatement(query);
ps.setInt(1, icono);
//ps.setObject(2, field2);
ResultSet rs = ps.executeQuery();

     while (rs.next()) {
   	  json = json + "{" + 
   		  			  "\"cono\":" 	+ 	rs.getInt("XNCONO") 	+ "," +
   		  			  "\"minm\":\"" + 	rs.getString("XNMINM") 	+ "\"," + 
	  				  "\"trnm\":\"" + 	JSONObject.escape(rs.getString("XNTRNM")) 	+ "\"," + 
 					  "\"chid\":\"" + 	rs.getString("XNCHID") 	+ "\"," + 
				  "\"actv\":" 	+ 	rs.getInt("XNACTV") 	+ 
   		  			"},";
   		  
     }
   
          if(rs!=null) rs.close();  
          if(ps!=null) ps.close();  
          if(conn!=null) conn.close();  
          

}

if(json.length()>2) json = json.substring(0, json.length() - 1);
json = json + "]";
out.print(json);
%>
