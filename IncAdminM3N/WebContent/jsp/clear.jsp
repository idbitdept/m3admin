<%@ page contentType="text/html; charset=utf-8" language="java" import="java.text.SimpleDateFormat,java.util.Iterator,java.util.Enumeration,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%

KevBean.setInitialise("ZOR003MI");
String json = "";
SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
java.util.Date date = new java.util.Date();
String comment = "Cleared by " + (String)session.getAttribute("uname") + " at " + dateFormat.format(date);

String cono = request.getParameter("cono"); if(cono==null) cono = "";
String divis = request.getParameter("divi"); if(divis==null) divis = "";
JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray apiArray = (JSONArray) obj;

String minm = ""; boolean multiples = false;
Iterator<JSONObject> iterator = apiArray.iterator();
while (iterator.hasNext()) {
	multiples = true;
	 JSONObject apis = (JSONObject) iterator.next();
	 String rgdt = (String) apis.get("rgdt"); String rgtm = (String) apis.get("rgtm"); rgtm = rgtm.replace(":", "");
	 KevBean.setField("CONO", cono);
	 KevBean.setField("DIVI", divis);
	 KevBean.setField("MINM", (String) apis.get("minm"));
	 KevBean.setField("TRNM", (String) apis.get("trnm"));
	 KevBean.setField("CHID", (String) apis.get("chid"));
	 KevBean.setField("RGDT", rgdt);
	 KevBean.setField("RGTM", rgtm);
	 KevBean.setField("TMSX", (Long) apis.get("tmsx") + "");
	 KevBean.setField("COMT", comment.substring(0,30));
	 KevBean.setField("LTRC", "3"); // Cleared
	 String resp = KevBean.runProgram("UpdStatus");
}

String rgdt = request.getParameter("rgdt"); String rgtm = request.getParameter("rgtm");rgtm = rgtm.replace(":", "");
KevBean.setField("CONO", cono);
KevBean.setField("DIVI", divis);
KevBean.setField("MINM", request.getParameter("minm"));
KevBean.setField("TRNM", request.getParameter("trnm"));
KevBean.setField("CHID", request.getParameter("chid"));
KevBean.setField("RGDT", rgdt);
KevBean.setField("RGTM", rgtm);
KevBean.setField("TMSX", request.getParameter("tmsx"));
KevBean.setField("COMT", comment.substring(0,30));
KevBean.setField("LTRC", "3"); // Cleared
String resp = KevBean.runProgram("UpdStatus");

json = "{\"success\":true,\"resp\":\"" + comment + "\",\"ok\":" + resp.startsWith("OK") + "}";
out.println(json);
%>
