<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>
<%@ page import = "javax.servlet.http.*" %>
<%@ page import = "org.apache.commons.fileupload.*" %>
<%@ page import = "org.apache.commons.fileupload.disk.*" %>
<%@ page import = "org.apache.commons.fileupload.servlet.*" %>
<%@ page import = "org.apache.commons.io.output.*" %>

<%@ page import = "java.util.Iterator" %>
<%@ page import = "org.apache.poi.ss.usermodel.Cell" %>
<%@ page import = "org.apache.poi.ss.usermodel.CellType" %>
<%@ page import = "org.apache.poi.ss.usermodel.Row" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFSheet" %>
<%@ page import = "org.apache.poi.xssf.usermodel.XSSFWorkbook" %>

<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<%@ include file="Connections/INCm3.jsp" %>
<%
//todo use user-specific login deets to process ABS100
//todo push process file down to user (prob need to make this a servlet)
String uname = (String)session.getAttribute("uname");

String bank = ""; String hdrDate = ""; String payer = ""; String totRemitted = ""; String currency = "";
String jbno = ""; String jbdt = ""; String jbtm = ""; Double dtotRemitted = 0.0;

String AddStatmtLineResp = ""; String AddStatmtHeadResp = "";

int remLine = 1;

String resp = "";
int currentCell = 0;

   File file ;
   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   ServletContext context = pageContext.getServletContext();
   String filePath = context.getInitParameter("file-upload");
   System.out.println("File path is " + filePath);

   // Verify the content type
   String contentType = request.getContentType();
   String fileName = "";

   if ((contentType.indexOf("multipart/form-data") >= 0)) {
      DiskFileItemFactory factory = new DiskFileItemFactory();
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
      // Location to save data that is larger than maxMemSize.
      //factory.setRepository(new File("/Users/IDB/Documents/excel/"));
      factory.setRepository(new File("C:\\kw\\excelUploads/"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );

      try { 
         // Parse the request to get file items
         List fileItems = upload.parseRequest(request);
         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         while ( i.hasNext () ) {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () ) {
               // Get the uploaded file parameters
               String fieldName = fi.getFieldName();
               fileName = fi.getName();
               boolean isInMemory = fi.isInMemory();
               long sizeInBytes = fi.getSize();
               // Write the file
               if( fileName.lastIndexOf("\\") >= 0 ) {
                  file = new File( filePath + 
                  fileName.substring( fileName.lastIndexOf("\\"))) ;
               } else {
                  file = new File( filePath + 
                  fileName.substring(fileName.lastIndexOf("\\")+1)) ;
               }
               fi.write( file ) ;
               System.out.println("Uploaded Filename: " + filePath + fileName + "<br />");
            }
         }

         try {

        if(fileName.contains("\\")) {
        	fileName = fileName.substring(fileName.lastIndexOf('\\') + 1);
        }
        FileInputStream FIS = new FileInputStream(new File(filePath + fileName));
 
      //FileOutputStream fileOut = new FileOutputStream(new File(filePath + fileName));
        //Create Workbook instance holding reference to .xlsx file

        XSSFWorkbook workbook = new XSSFWorkbook(FIS);
        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        int currentRow = 0;
        //Iterate through each rows one by one
		
        boolean inHeaders = true;
        Iterator<Row> rowIterator = sheet.iterator();

        while (rowIterator.hasNext()) {
			currentRow++;
            Row row = rowIterator.next();

            if(inHeaders) {
	        	Cell hdrDesc = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
	        	Cell hdrVal = row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
	        	
	        	hdrDesc.setCellType(CellType.STRING);
	        	
	        	if(hdrDesc.getStringCellValue().trim().equalsIgnoreCase("Bank")) {
	        		hdrVal.setCellType(CellType.STRING);
	        		bank = hdrVal.getStringCellValue().trim();
	        	} else if(hdrDesc.getStringCellValue().trim().equalsIgnoreCase("Date")) {
	        		hdrVal.setCellType(CellType.STRING);
	        		hdrDate = hdrVal.getStringCellValue().trim();
	        	} else if(hdrDesc.getStringCellValue().trim().equalsIgnoreCase("Payer")) {
	        		hdrVal.setCellType(CellType.STRING);
	        		payer = hdrVal.getStringCellValue().trim();
	        	} else if(hdrDesc.getStringCellValue().trim().contains("otal")) {
	        		hdrVal.setCellType(CellType.NUMERIC);
	        		//totRemitted = hdrVal.getStringCellValue().trim();
	        		dtotRemitted = hdrVal.getNumericCellValue();
	        	} else if(hdrDesc.getStringCellValue().trim().equals("Currency")) {
	        		hdrVal.setCellType(CellType.STRING);
	        		currency = hdrVal.getStringCellValue().trim();
	        	} else if(hdrDesc.getStringCellValue().trim().contains("nvoice")) {
	        		
	        		inHeaders = false; // time to start processing detail...
	        		
	        		//do the header call now
	        		KevBean.setInitialise("ABS100MI");
	        		KevBean.setField("CONO", zdcono);
	        		KevBean.setField("DIVI", divi);
	        		KevBean.setField("BSTY", "3");
	        		KevBean.setField("STMN", hdrDate + payer);
	        		KevBean.setField("BSID", bank);
	        		KevBean.setField("CUCD", currency);
	        		KevBean.setField("BSOB", "0");
	        		KevBean.setField("BSOT", hdrDate);
	        		KevBean.setField("BSCB", dtotRemitted + "");
	        		KevBean.setField("BSCT", hdrDate);
	        		KevBean.setField("BSTD", dtotRemitted + "");
	        		KevBean.setField("BSTC", "0");
	        		AddStatmtHeadResp = KevBean.runProgram("AddStatmtHead");
	        		resp+= "AddStatmtHead " + AddStatmtHeadResp;
	        		if(AddStatmtHeadResp.startsWith("OK")) {
	        			jbno = KevBean.getField("JBNO");
	        			jbdt = KevBean.getField("JBDT");
	        			jbtm = KevBean.getField("JBTM");
	        			
	        			
	        			KevBean.setField("CONO", zdcono);
		        		KevBean.setField("DIVI", divi);
		        		KevBean.setField("BLSN", "10");
		        		KevBean.setField("BLLT", "3");
		        		KevBean.setField("CUCD", currency);
		        		KevBean.setField("ARAT", "1");			
		        		KevBean.setField("BLLF", dtotRemitted + "");
		        		KevBean.setField("CURD", hdrDate);
		        		KevBean.setField("PYNO", payer);
		        		KevBean.setField("JBNO", jbno);
		        		KevBean.setField("JBDT", jbdt);
		        		KevBean.setField("JBTM", jbtm);
						
		        		AddStatmtLineResp = KevBean.runProgram("AddStatmtLine");
		        		resp+= "AddStatmtLine " + AddStatmtLineResp;
	        			
	        		}
	        	
	        	}
	        	
            } else {
            	System.out.println("In a line...");
            	//String lnInvDt = ""; 
            	String lnInvNo = ""; String lnAmt = ""; double dLnAmt = 0.0;
            	//Cell cLnInvDt = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
            	///cLnInvDt.setCellType(CellType.STRING);
	        	Cell cLnInvNo = row.getCell(0, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
	        	cLnInvNo.setCellType(CellType.STRING);
	        	
	        	System.out.println("Now try numeric 1 ");
	        	Cell cLnAmt = row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
	        
	        	try {
	        		cLnAmt.setCellType(CellType.NUMERIC);
	        	} catch (Exception e) {
	        		System.err.println("We tried " + e);
	        	}
	        	System.out.println("set type as num..done ");
	        	
	        	if(!cLnInvNo.getStringCellValue().trim().equalsIgnoreCase("")) {
	        		//lnInvDt = cLnInvDt.getStringCellValue().trim();
	        		lnInvNo = cLnInvNo.getStringCellValue().trim();
	        		try {
	        			lnAmt = cLnAmt.getStringCellValue().trim();
	        		} catch (Exception e) {
	        			System.err.println("Knamnt " + e);
	        		}
	        		try {
	        			dLnAmt = cLnAmt.getNumericCellValue();
	        		} catch (Exception e) {
	        			System.err.println("error rrr" + e);
	        			dLnAmt = Double.parseDouble(lnAmt);
	        		}
	        		
	        		KevBean.setField("DIVI", divi);
	        		KevBean.setField("BLSN", "10");
	        		KevBean.setField("BLSS", remLine++ + "");
	        		KevBean.setField("BLTT", "AR");
	        		KevBean.setField("BLBC", "10");
	        		KevBean.setField("BLNF", dLnAmt + "");
	        		KevBean.setField("CINO", lnInvNo);
					// what about line date!?		
					KevBean.setField("JBNO", jbno);
	        		KevBean.setField("JBDT", jbdt);
	        		KevBean.setField("JBTM", jbtm);
	        						
	        		String AddStatmtDetailResp = KevBean.runProgram("AddStatmtDetail");
	        		resp+= "AddStatmtDetail " + AddStatmtDetailResp;	        						
	        	}
            }
        } 
        
        if(AddStatmtLineResp.startsWith("OK")) {
        	KevBean.setField("DIVI", divi);
        	KevBean.setField("BSHS", "2");
        	KevBean.setField("BSHB", "0");	
			KevBean.setField("JBNO", jbno);
    		KevBean.setField("JBDT", jbdt);
    		KevBean.setField("JBTM", jbtm);
    						
    		String UpdStatmtStatusResp = KevBean.runProgram("UpdStatmtStatus");
    		resp+= "UpdStatmtStatus " + UpdStatmtStatusResp;
        }
        resp+="Bank: " + bank + ", Date: " + hdrDate + ", Payer: " + payer + ", Total: " + dtotRemitted + ", Currency: " + currency;

      FIS.close();
      
    } catch (Exception e) {
      e.printStackTrace();
      resp += "<\\li>Response<\\/li>";
    } 
         out.println("{\"success\":true,\"file\":\"" + fileName + "\",\"resp\":\"" + resp + "\"}");

      } catch(Exception ex) {
         System.out.println(ex);
      }
   }
%>