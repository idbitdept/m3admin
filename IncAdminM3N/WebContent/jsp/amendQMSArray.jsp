<%@ page contentType="text/html; charset=utf-8" language="java" import="java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="LocalBean" class="com.ornua.MvxBean" scope="page"/>
<%
//use logged-on user's account
String localUname = (String)session.getAttribute("localUname");
String localPword = (String)session.getAttribute("localPword");
String localDom = (String)session.getAttribute("localDom");

String localProg = "QMS400MI";
String localTrans = "UpdTestResult";

LocalBean.setSystem(system);
LocalBean.setPort(PORT);
LocalBean.setLib(LIB);
LocalBean.setCompany(zdcono);
String connAPI = localDom + "\\" + localUname; //System.out.println(connAPI);
LocalBean.setUsername(connAPI); 
LocalBean.setPassword(localPword); 
//make sure the users has the same cono/divi/faci settings as the API account
LocalBean.setInitialise(localProg);

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray apiArray = (JSONArray) obj;

String minm = ""; boolean multiples = false;
Iterator<JSONObject> iterator = apiArray.iterator();
while (iterator.hasNext()) {
	 JSONObject apis = (JSONObject) iterator.next();
	 
	 String itno = (String)apis.get("itno"); if(itno==null) itno = "";
	 String bano = (String)apis.get("bano"); if(bano==null) bano = "";
	 String qtst = (String)apis.get("qtst");if(qtst==null) qtst = "";
	 String qtrs = (String)apis.get("qtrs");if(qtrs==null) qtrs = "";
	 String qse1 = (String)apis.get("qse1"); if(qse1==null) qse1 = "";
	 String spec = (String)apis.get("spec"); if(spec==null) spec = "";
	 String tsty = (String)apis.get("tsty"); if(tsty==null) tsty = "";
	 String qrid = (String)apis.get("qrid"); if(qrid==null) qrid = "";
	 String newQop1 = (String)apis.get("newQop1"); if(newQop1==null) newQop1 = "";
	 String oldQop1 = (String)apis.get("oldQop1"); if(oldQop1==null) oldQop1 = "";
	 String currFrti = (String)apis.get("currFrti"); if(currFrti==null) currFrti = "";
	 if(currFrti.equals("")) currFrti = "1"; //sequence no, default 1
	 
	 LocalBean.setField("FACI", zdfaci);
	 LocalBean.setField("ITNO", itno);
	 LocalBean.setField("BANO", bano);
	 LocalBean.setField("QRID", qrid);
	 LocalBean.setField("QTST", qtst);
	 LocalBean.setField("TSTY", tsty);

if(tsty.equals("0")) {
	LocalBean.setField("QTRS", qtrs);
	LocalBean.setField("QOP1", newQop1);
} else {
	LocalBean.setField("QLCD", qtrs);
}

//KevBean.setField("TSTY", "0"); //revisit, test type.. 
LocalBean.setField("TSEQ", currFrti);
LocalBean.setField("VLEN", "1");
LocalBean.setField("UPCT", "1");
String qmsResp = LocalBean.runProgram(localTrans);
if(qmsResp.startsWith("NOK            Not allowed")) 
	 qmsResp += " - if you require access please raise a techhelp ticket for ERP Security team to request SES005 access for your account (" + localDom + "/" + localUname + ") for " + localProg + "/" + localTrans;

}
out.println("{\"success\":true,\"ok\":true}");
%>
	