<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*,java.text.SimpleDateFormat,org.json.simple.JSONObject,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONArray" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>

<%
SimpleDateFormat newdate = new SimpleDateFormat("yyyyMMdd");
SimpleDateFormat newtime = new SimpleDateFormat("HHmmss");

String where = " where 1 = 1 ";



String filter = request.getParameter("filter");
if (filter==null) filter = ""; filter = filter.trim();
if(!filter.equals("")) {
	String operator = ""; String value = ""; String property = ""; long lvalue = 0;
	JSONParser parser = new JSONParser();
	Object obj = parser.parse(filter);
	JSONArray filts = (JSONArray) obj;
	 Iterator<JSONObject> iterator = filts.iterator();
	 while (iterator.hasNext()) {
		 value = ""; lvalue = -1;
		 JSONObject filt = (JSONObject) iterator.next();
		 

		 
		 property = (String) filt.get("property");
		 operator = (String) filt.get("operator"); if(operator.equals("eq")) operator = " = "; else if(operator.equals("gt")) operator = " >"; else if(operator.equals("lt")) operator = " < ";
		 try { value = (String) filt.get("value"); } catch(Exception e) { }
		 try { lvalue = (Long) filt.get("value"); } catch(Exception e) { }
		 
		 if(property.equals("loaded") || property.equals("pickedup")) where += " and " + "Convert(CHAR(8), " + property + ",112)" + " " + operator + " " + value;
		 else {
			 if(operator.equals("like")) value = " '%" + value + "%'";
			 if(!value.equals(""))
			 	where+= " and " + property + " " + operator + value;
			 if(lvalue>-1)
				 	where+= " and " + property + " " + operator + lvalue;
		 }
	 }
}

String json = "[";

String Gis05driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
String Gis05Uname = "GIS5263";
String Gis05Pword = "GIS5263";
String Gis05Uri = "jdbc:sqlserver://b2bisqlprd.ornua.com:1433;databaseName=evo_attrs";

try {
	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	Connection conn = DriverManager.getConnection(Gis05Uri, Gis05Uname, Gis05Pword);
	String query = "select top 150 * from bucket " + where + " order by loaded desc";
	//System.out.println("query: " + query);
    PreparedStatement ps = conn.prepareStatement(query);
    //ps.setString(1, urienv);
    //ps.setString(2, uricono);
   // ps.setString(3, uridivi);
    //System.out.println(urienv + " " + uricono + " " + uridivi);
    int status = 0; boolean overdue = false; //java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
    ResultSet rs = ps.executeQuery();
    while(rs.next()) {
    	status = rs.getInt("STATUS"); //rs.getDate("loaded")
    	
    	//if (rs.getDate("loaded").toInstant().isBefore(thirtyDaysAgo.toInstant()) && status==0) overdue = true;
    	json+="{";
    	json+="\"ITEM_NUMBER\":\"" + rs.getString("ITEM_NUMBER").trim() + "\",";
	      json+="\"HEALTHMARK\":\"" + rs.getString("HEALTHMARK").trim() + "\",";
	      json+="\"PRODUCTION_CODE\":\"" + rs.getString("PRODUCTION_CODE").trim() + "\",";
	      json+="\"MOISTURE\":\"" + (rs.getString("MOISTURE")==null ? "" : rs.getString("MOISTURE").trim() ) + "\",";
	      json+="\"FDM\":\"" + (rs.getString("FDM")==null ? "" : rs.getString("FDM").trim() ) + "\",";
	      json+="\"FAT\":\"" + (rs.getString("FAT")==null ? "" : rs.getString("FAT").trim() ) + "\",";
	      json+="\"SODIUM\":\"" + (rs.getString("SODIUM")==null ? "" : rs.getString("SODIUM").trim() ) + "\",";
	      json+="\"SODIUM_NA\":\"" + (rs.getString("SODIUM_NA")==null ? "" : rs.getString("SODIUM_NA").trim() ) + "\",";
	      json+="\"PH\":\"" + (rs.getString("PH")==null ? "" : rs.getString("PH").trim() ) + "\",";
	      json+="\"ECOLI\":\"" + (rs.getString("ECOLI")==null ? "" : rs.getString("ECOLI").trim() ) + "\",";
	      json+="\"COLIFORMS\":\"" + (rs.getString("COLIFORMS")==null ? "" : rs.getString("COLIFORMS").trim() ) + "\",";
	      json+="\"YEAST\":\"" + (rs.getString("YEAST")==null ? "" : rs.getString("YEAST").trim() ) + "\",";
	      json+="\"MOULD\":\"" + (rs.getString("MOULD")==null ? "" : rs.getString("MOULD").trim() ) + "\",";
	      json+="\"STAPHS\":\"" + (rs.getString("STAPHS")==null ? "" : rs.getString("STAPHS").trim() ) + "\",";
	      json+="\"SALMONELLA\":\"" + (rs.getString("SALMONELLA")==null ? "" : rs.getString("SALMONELLA").trim() ) + "\",";
	      json+="\"LISTERIA\":\"" + (rs.getString("LISTERIA")==null ? "" : rs.getString("LISTERIA").trim() ) + "\",";
	      json+="\"RENNET_TYPE\":\"" + (rs.getString("RENNET_TYPE")==null ? "" : rs.getString("RENNET_TYPE").trim() ) + "\",";
	      json+="\"STARTER_TYPE\":\"" + (rs.getString("STARTER_TYPE")==null ? "" : rs.getString("STARTER_TYPE").trim() ) + "\",";
	      json+="\"BOX_TYPE\":\"" + (rs.getString("BOX_TYPE")==null ? "" : rs.getString("BOX_TYPE").trim() ) + "\",";
	      json+="\"ORIGIN_OF_MILK1\":\"" + (rs.getString("ORIGIN_OF_MILK1")==null ? "" : rs.getString("ORIGIN_OF_MILK1").trim() ) + "\",";
	      json+="\"ORIGIN_OF_MILK2\":\"" + (rs.getString("ORIGIN_OF_MILK2")==null ? "" : rs.getString("ORIGIN_OF_MILK2").trim() ) + "\",";
	      json+="\"ORIGIN_OF_MILK3\":\"" + (rs.getString("ORIGIN_OF_MILK3")==null ? "" : rs.getString("ORIGIN_OF_MILK3").trim() ) + "\",";
	      json+="\"FINISHED_PRODUCT_SPEC\":\"" + (rs.getString("FINISHED_PRODUCT_SPEC")==null ? "" : rs.getString("FINISHED_PRODUCT_SPEC").trim() ) + "\",";
	      json+="\"BEST_BEFORE_DATE\":\"" + (rs.getString("BEST_BEFORE_DATE")==null ? "" : rs.getString("BEST_BEFORE_DATE").trim() ) + "\",";
	      json+="\"MANUFACTURER_LOT_NO\":\"" + (rs.getString("MANUFACTURER_LOT_NO")==null ? "" : rs.getString("MANUFACTURER_LOT_NO").trim() ) + "\",";
	      json+="\"MANUFACTURER_ITEM_CODE\":\"" + (rs.getString("MANUFACTURER_ITEM_CODE")==null ? "" : rs.getString("MANUFACTURER_ITEM_CODE").trim() ) + "\",";
	      json+="\"CHEESE_GRADE\":\"" + (rs.getString("CHEESE_GRADE")==null ? "" : rs.getString("CHEESE_GRADE").trim() ) + "\",";
	      json+="\"GRADE_ACTION_MONTH\":\"" + (rs.getString("GRADE_ACTION_MONTH")==null ? "" : rs.getString("GRADE_ACTION_MONTH").trim() ) + "\",";
	      json+="\"GRADE_COMMENT\":\"" + (rs.getString("GRADE_COMMENT")==null ? "" : rs.getString("GRADE_COMMENT").trim() ) + "\",";
	      json+="\"FACTORY_GRADE_COMMENT\":\"" + (rs.getString("FACTORY_GRADE_COMMENT")==null ? "" : rs.getString("FACTORY_GRADE_COMMENT").trim() ) + "\",";
	      json+="\"FLAVOUR_COMMENT\":\"" + (rs.getString("FLAVOUR_COMMENT")==null ? "" : rs.getString("FLAVOUR_COMMENT").trim() ) + "\",";
	      json+="\"FUNCTIONALITY_COMMENT\":\"" + (rs.getString("FUNCTIONALITY_COMMENT")==null ? "" : rs.getString("FUNCTIONALITY_COMMENT").trim() ) + "\",";
	      json+="\"TEXTURE_COMMENT\":\"" + (rs.getString("TEXTURE_COMMENT")==null ? "" : rs.getString("TEXTURE_COMMENT").trim() ) + "\",";
	      json+="\"PRODUCT_COMMENT\":\"" + (rs.getString("PRODUCT_COMMENT")==null ? "" : rs.getString("PRODUCT_COMMENT").trim() ) + "\",";
	      json+="\"CUSTOMER_COMMENT\":\"" + (rs.getString("CUSTOMER_COMMENT")==null ? "" : rs.getString("PRODUCT_COMMENT").trim() ) + "\",";
	      json+="\"STORE_COMMENT\":\"" + (rs.getString("STORE_COMMENT")==null ? "" : rs.getString("STORE_COMMENT").trim() ) + "\",";
	      json+="\"GENERAL_COMMENT\":\"" + (rs.getString("GENERAL_COMMENT")==null ? "" : rs.getString("GENERAL_COMMENT").trim() ) + "\",";
	      json+="\"SALMONELLA_250G\":\"" + (rs.getString("SALMONELLA_250G")==null ? "" : rs.getString("SALMONELLA_250G").trim() ) + "\",";
	      json+="\"CL_PERFRINGENS\":\"" + (rs.getString("CL_PERFRINGENS")==null ? "" : rs.getString("CL_PERFRINGENS").trim() ) + "\",";
	      json+="\"ENTEROBACTERIAC\":\"" + (rs.getString("ENTEROBACTERIAC")==null ? "" : rs.getString("ENTEROBACTERIAC").trim() ) + "\",";
	      json+="\"B_CEREUS\":\"" + (rs.getString("B_CEREUS")==null ? "" : rs.getString("B_CEREUS").trim() ) + "\",";
	      json+="\"RESERVATION\":\"" + (rs.getString("RESERVATION")==null ? "" : rs.getString("RESERVATION").trim() ) + "\",";
	      json+="\"RESERVE_UNTIL\":\"" + (rs.getString("RESERVE_UNTIL")==null ? "" : rs.getString("RESERVE_UNTIL").trim() ) + "\",";
	      json+="\"GRADE_DATE\":\"" + (rs.getString("GRADE_DATE")==null ? "" : rs.getString("GRADE_DATE").trim() ) + "\",";
	      json+="\"CURD\":\"" + (rs.getString("CURD")==null ? "" : rs.getString("CURD").trim() ) + "\",";
	      json+="\"SPC\":\"" + (rs.getString("SPC")==null ? "" : rs.getString("SPC").trim() ) + "\",";
	      json+="\"FFA\":\"" + (rs.getString("FFA")==null ? "" : rs.getString("FFA").trim() ) + "\",";
	      json+="\"PEROXIDE_VALUE\":\"" + (rs.getString("PEROXIDE_VALUE")==null ? "" : rs.getString("PEROXIDE_VALUE").trim() ) + "\",";
	      json+="\"DP20_1\":\"" + (rs.getString("DP20_1")==null ? "" : rs.getString("DP20_1").trim() ) + "\",";
	      json+="\"DP20_2\":\"" + (rs.getString("DP20_2")==null ? "" : rs.getString("DP20_2").trim() ) + "\",";
	      json+="\"HARDNESS\":\"" + (rs.getString("HARDNESS")==null ? "" : rs.getString("HARDNESS").trim() ) + "\",";
	      json+="\"DIACETYL\":\"" + (rs.getString("DIACETYL")==null ? "" : rs.getString("DIACETYL").trim() ) + "\",";
	      json+="\"TCM\":\"" + (rs.getString("TCM")==null ? "" : rs.getString("TCM").trim() ) + "\",";
	      json+="\"ALLOCATABLE\":\"" + (rs.getString("ALLOCATABLE")==null ? "" : rs.getString("ALLOCATABLE").trim() ) + "\",";
	      json+="\"BONDNUM\":\"" + (rs.getString("BONDNUM")==null ? "" : rs.getString("BONDNUM").trim() ) + "\",";
	      json+="\"AVERAGE_WEIGHT\":\"" + (rs.getString("AVERAGE_WEIGHT")==null ? "" : rs.getString("AVERAGE_WEIGHT").trim() ) + "\",";
	      json+="\"TEMPERATURE\":\"" + (rs.getString("TEMPERATURE")==null ? "" : rs.getString("TEMPERATURE").trim() ) + "\",";
	      json+="\"FOLIC_ACID\":\"" + (rs.getString("FOLIC_ACID")==null ? "" : rs.getString("FOLIC_ACID").trim() ) + "\",";
	      json+="\"MESOPHILIC_ANAEROBIC\":\"" + (rs.getString("MESOPHILIC_ANAEROBIC")==null ? "" : rs.getString("MESOPHILIC_ANAEROBIC").trim() ) + "\",";
	      json+="\"MESOPHILIC_AEROBIC\":\"" + (rs.getString("MESOPHILIC_AEROBIC")==null ? "" : rs.getString("MESOPHILIC_AEROBIC").trim() ) + "\",";
	      json+="\"THERMOPHILES\":\"" + (rs.getString("THERMOPHILES")==null ? "" : rs.getString("THERMOPHILES").trim() ) + "\",";
	      json+="\"THERMOPHILIC_AEROBIC\":\"" + (rs.getString("THERMOPHILIC_AEROBIC")==null ? "" : rs.getString("THERMOPHILIC_AEROBIC").trim() ) + "\",";
	      json+="\"THERMOPHILIC_ANAEROBIC\":\"" + (rs.getString("THERMOPHILIC_ANAEROBIC")==null ? "" : rs.getString("THERMOPHILIC_ANAEROBIC").trim() ) + "\",";
	      json+="\"HEAT_CLASS_WPN\":\"" + (rs.getString("HEAT_CLASS_WPN")==null ? "" : rs.getString("HEAT_CLASS_WPN").trim() ) + "\",";
	      json+="\"DISPERSIBILITY\":\"" + (rs.getString("DISPERSIBILITY")==null ? "" : rs.getString("DISPERSIBILITY").trim() ) + "\",";
	      json+="\"NITRATE\":\"" + (rs.getString("NITRATE")==null ? "" : rs.getString("NITRATE").trim() ) + "\",";
	      json+="\"NITRITE\":\"" + (rs.getString("NITRITE")==null ? "" : rs.getString("NITRITE").trim() ) + "\",";
	      json+="\"AFLATOXIN_M1\":\"" + (rs.getString("AFLATOXIN_M1")==null ? "" : rs.getString("AFLATOXIN_M1").trim() ) + "\",";
	      json+="\"FAECAL_STREPTOCOCCI\":\"" + (rs.getString("FAECAL_STREPTOCOCCI")==null ? "" : rs.getString("FAECAL_STREPTOCOCCI").trim() ) + "\",";
	      json+="\"ENTEROBACTER_SAKAZAKII\":\"" + (rs.getString("ENTEROBACTER_SAKAZAKII")==null ? "" : rs.getString("ENTEROBACTER_SAKAZAKII").trim() ) + "\",";
	      json+="\"TITRATABLE_ACIDITY\":\"" + (rs.getString("TITRATABLE_ACIDITY")==null ? "" : rs.getString("TITRATABLE_ACIDITY").trim() ) + "\",";
	      json+="\"ACIDITY\":\"" + (rs.getString("ACIDITY")==null ? "" : rs.getString("ACIDITY").trim() ) + "\",";
	      json+="\"SCORCHED_PART_PUR\":\"" + (rs.getString("SCORCHED_PART_PUR")==null ? "" : rs.getString("SCORCHED_PART_PUR").trim() ) + "\",";
	      json+="\"WETTABILITY\":\"" + (rs.getString("WETTABILITY")==null ? "" : rs.getString("WETTABILITY").trim() ) + "\",";
	      json+="\"INSOLUBILITY_INDEX\":\"" + (rs.getString("INSOLUBILITY_INDEX")==null ? "" : rs.getString("INSOLUBILITY_INDEX").trim() ) + "\",";
	      json+="\"BULK_DENSITY\":\"" + (rs.getString("BULK_DENSITY")==null ? "" : rs.getString("BULK_DENSITY").trim() ) + "\",";
	      json+="\"COFFEE_STABILITY\":\"" + (rs.getString("COFFEE_STABILITY")==null ? "" : rs.getString("COFFEE_STABILITY").trim() ) + "\",";
	      json+="\"FORTY_MESH\":\"" + (rs.getString("FORTY_MESH")==null ? "" : rs.getString("FORTY_MESH").trim() ) + "\",";
	      json+="\"SIXTY_MESH\":\"" + (rs.getString("SIXTY_MESH")==null ? "" : rs.getString("SIXTY_MESH").trim() ) + "\",";
	      json+="\"EIGHTY_MESH\":\"" + (rs.getString("EIGHTY_MESH")==null ? "" : rs.getString("EIGHTY_MESH").trim() ) + "\",";
	      json+="\"ONE_HUNDRED_MESH\":\"" + (rs.getString("ONE_HUNDRED_MESH")==null ? "" : rs.getString("ONE_HUNDRED_MESH").trim() ) + "\",";
	      json+="\"TWO_HUNDRED_MESH\":\"" + (rs.getString("TWO_HUNDRED_MESH")==null ? "" : rs.getString("TWO_HUNDRED_MESH").trim() ) + "\",";
	      json+="\"SULPHITE_RED_CLOSTRIDIA\":\"" + (rs.getString("SULPHITE_RED_CLOSTRIDIA")==null ? "" : rs.getString("SULPHITE_RED_CLOSTRIDIA").trim() ) + "\",";
	      json+="\"LACTOSE\":\"" + (rs.getString("LACTOSE")==null ? "" : rs.getString("LACTOSE").trim() ) + "\",";
	      json+="\"ASH\":\"" + (rs.getString("ASH")==null ? "" : rs.getString("ASH").trim() ) + "\",";
	      json+="\"VITAMIN_A\":\"" + (rs.getString("VITAMIN_A")==null ? "" : rs.getString("VITAMIN_A").trim() ) + "\",";
	      json+="\"VITAMIN_D3\":\"" + (rs.getString("VITAMIN_D3")==null ? "" : rs.getString("VITAMIN_D3").trim() ) + "\",";
	      json+="\"CALCIUM\":\"" + (rs.getString("CALCIUM")==null ? "" : rs.getString("CALCIUM").trim() ) + "\",";
	      json+="\"ANTIMICROBIAL_RESIDUES\":\"" + (rs.getString("ANTIMICROBIAL_RESIDUES")==null ? "" : rs.getString("ANTIMICROBIAL_RESIDUES").trim() ) + "\",";
	      json+="\"PROTEIN\":\"" + (rs.getString("PROTEIN")==null ? "" : rs.getString("PROTEIN").trim() ) + "\",";
	      json+="\"PROTEIN_MSNF\":\"" + (rs.getString("PROTEIN_MSNF")==null ? "" : rs.getString("PROTEIN_MSNF").trim() ) + "\",";
	      json+="\"PHOSPHATASE_TEST\":\"" + (rs.getString("PHOSPHATASE_TEST")==null ? "" : rs.getString("PHOSPHATASE_TEST").trim() ) + "\",";
	      json+="\"PHOSPHATE\":\"" + (rs.getString("PHOSPHATE")==null ? "" : rs.getString("PHOSPHATE").trim() ) + "\",";
	      json+="\"POTASSIUM\":\"" + (rs.getString("POTASSIUM")==null ? "" : rs.getString("POTASSIUM").trim() ) + "\",";
	      json+="\"MAGNESIUM\":\"" + (rs.getString("MAGNESIUM")==null ? "" : rs.getString("MAGNESIUM").trim() ) + "\",";
	      json+="\"VIT_B12\":\"" + (rs.getString("VIT_B12")==null ? "" : rs.getString("VIT_B12").trim() ) + "\",";
	      json+="\"CHOLINE\":\"" + (rs.getString("CHOLINE")==null ? "" : rs.getString("CHOLINE").trim() ) + "\",";
	      json+="\"COLOUR_TEST\":\"" + (rs.getString("COLOUR_TEST")==null ? "" : rs.getString("COLOUR_TEST").trim() ) + "\",";
	      json+="\"USER_DEFINED5\":\"" + (rs.getString("USER_DEFINED5")==null ? "" : rs.getString("USER_DEFINED5").trim() ) + "\",";
	      json+="\"USER_DEFINED6\":\"" + (rs.getString("USER_DEFINED6")==null ? "" : rs.getString("USER_DEFINED6").trim() ) + "\",";
	      json+="\"FREE_FAT\":\"" + (rs.getString("FREE_FAT")==null ? "" : rs.getString("FREE_FAT").trim() ) + "\",";
	      json+="\"HEAT_STABILITY\":\"" + (rs.getString("HEAT_STABILITY")==null ? "" : rs.getString("HEAT_STABILITY").trim() ) + "\",";
	      json+="\"HEAT_STABILITY_B\":\"" + (rs.getString("HEAT_STABILITY_B")==null ? "" : rs.getString("HEAT_STABILITY_B").trim() ) + "\",";
	      json+="\"HEAT_STABILITY_C\":\"" + (rs.getString("HEAT_STABILITY_C")==null ? "" : rs.getString("HEAT_STABILITY_C").trim() ) + "\",";
	      json+="\"HEAT_STABILITY_D\":\"" + (rs.getString("HEAT_STABILITY_D")==null ? "" : rs.getString("HEAT_STABILITY_D").trim() ) + "\",";
	      
	      json+="\"BANO\":\"" + (rs.getString("BANO")==null ? "" : rs.getString("BANO").trim() ) + "\",";
	      json+="\"CAMU\":\"" + (rs.getString("CAMU")==null ? "" : rs.getString("CAMU").trim() ) + "\",";
	      json+="\"REPN\":\"" + (rs.getString("REPN")==null ? "" : rs.getString("REPN").trim() ) + "\",";
	      json+="\"pickedup\":\"" + (rs.getObject("pickedup")==null ? "" : newdate.format(rs.getDate("pickedup")) + " " + newtime.format(rs.getTime("pickedup")) ) + "\",";
	      json+="\"loaded\":\"" + newdate.format(rs.getDate("loaded")) + " " + newtime.format(rs.getTime("loaded"))  + "\",";
	      json+="\"loadedby\":\"" + (rs.getString("loadedby")==null ? "" : rs.getString("loadedby").trim() ) + "\",";
	      json+="\"overdue\":" + overdue + ",";
	      json+="\"STATUS\":" + status;
    	json+="},";
		
		
    }
} catch (Exception e) {
	System.err.println("cakes" + e);
}
if(json.length()>1) json = json.substring(0, json.length()-1);
json = json + "]";
out.print(json);
%>
