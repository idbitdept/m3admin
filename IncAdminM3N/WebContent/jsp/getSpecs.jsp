<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="ATS100Bean" class="com.ornua.MvxBean" scope="page"/>
<%
String itno = request.getParameter("itno"); if(itno==null) itno = "";
String bano = request.getParameter("firstLot"); if(bano==null) bano = "";
String qrid = "";

ATS100Bean.setSystem(system);
ATS100Bean.setPort(PORT);
ATS100Bean.setLib(LIB);
ATS100Bean.setCompany(zdcono);
ATS100Bean.setUsername(m3user);
ATS100Bean.setPassword(m3pass);
ATS100Bean.setInitialise("QMS100MI");
ATS100Bean.runProgram("SetLstMaxRec   1");

// get qrid for the first lot ...
//System.out.println(zdfaci + " " + itno + " " + bano);
KevBean.setInitialise("QMS302MI");
KevBean.setField("FACI", zdfaci);
KevBean.setField("ITNO", itno);
KevBean.setField("BANO", bano);
KevBean.runProgram("LstTestQIReq");
while(KevBean.nextRow()) {
	 if(!KevBean.getField("BANO").trim().equals(bano)) break;
	 qrid = KevBean.getField("QRID");
	 if(qrid.equals("")) qrid = KevBean.getField("QRID"); // if there's no test, use the most recent qrid to force a NOK Test doesnt exist (instead of 'more than one QRID for this..'.)
	 //System.out.println("Bano: " + bano + " qrid: " + qrid);
}
KevBean.setInitialise("QMS400MI");
KevBean.setField("FACI", zdfaci);
KevBean.setField("QRID", qrid);
KevBean.setField("ITNO", itno);
KevBean.setField("BANO", bano);
KevBean.runProgram("LstTestResults");
String json = "{\"success\":true, \"results\":[ ";

String spec = ""; String qse1 = ""; String thisItno = "";

	while(KevBean.nextRow()) {
		// should we loop through each one to get its full name and list of default values etc..
		String qtst = KevBean.getField("QTST");
		String tsty = KevBean.getField("TSTY");
		ATS100Bean.setInitialise("QMS100MI");
		ATS100Bean.runProgram("SetLstMaxRec   1");
		ATS100Bean.setField("QTST", qtst);
		ATS100Bean.setField("TSTY", tsty);
		String atsResp = ATS100Bean.runProgram("LstTstTemplates");
		String tx40 = ATS100Bean.getField("TX40"); if(tx40==null) tx40 = "";
		json += "{";
		json += "\"qtst\":\"" + KevBean.getField("QTST") + "\",";
		json += "\"desc\":\"" + tx40 + "\",";
		json += "\"tsty\":" + tsty + ",";
		json += "\"tseq\":" + KevBean.getField("TSEQ") + ",";
		json += "\"qop1\":" + KevBean.getField("QOP1");
		json += "},";
	
	} 

if(json.length()>2) json = json.substring(0, json.length()-1);
json += "]}";
out.println(json);
%>
