<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" %>
<%@ include file="Connections/INCm3.jsp" %>
<jsp:useBean id="KevBean" class="com.ornua.MvxBean" scope="session"/>
<jsp:useBean id="ATSBean" class="com.ornua.MvxBean" scope="page"/>
<jsp:useBean id="ATSBean010" class="com.ornua.MvxBean" scope="page"/>

<%
ATSBean.setSystem(system);
ATSBean.setPort(PORT);
ATSBean.setLib(LIB);
ATSBean.setCompany(zdcono);
ATSBean.setUsername(m3user);
ATSBean.setPassword(m3pass);

/*
ATSBean010.setSystem(system);
ATSBean010.setPort(PORT);
ATSBean010.setLib(LIB);
ATSBean010.setCompany(zdcono);
ATSBean010.setUsername(m3user);
ATSBean010.setPassword(m3pass);
ATSBean010.setInitialise("ATS010MI");
*/
String itno = request.getParameter("itno"); if(itno==null) itno = "";
String whlo = request.getParameter("whlo"); if(whlo==null) whlo = "";
String atmo = request.getParameter("atmo"); if(atmo==null) atmo = "";

ATSBean.setInitialise("ATS101MI");
ATSBean.runProgram("SetLstMaxRec"); // got more than 100 records on e.g. CUSTOMER RESERV

KevBean.setInitialise("ATS050MI");
KevBean.setField("ATMO", atmo);
KevBean.runProgram("LstAttribute");

String atid = ""; String atvc = ""; String orip = "";
String json = "{\"success\":true, \"results\":[ ";
while(KevBean.nextRow()) {
	atid = KevBean.getField("ATID");
	atvc = KevBean.getField("ATVC"); if(atvc==null) atvc = "";
	orip = KevBean.getField("ORIP"); if(orip==null) orip = "";
	if(orip.equals("1")) { //included in PO... todo review is this right
		json += "{\"atid\":\"" + atid + "\",\"atvc\":\"" + atvc + "\","; 
		
		/*
		ATSBean010.setField("ATID", atid);
		if(ATSBean010.runProgram("GetAttribute").startsWith("OK")) {
			json+= "\"cobt\":" + ATSBean010.getField("COBT") + ",";
		}
		*/
		// now check if there are default values for this attribute...
		json+= " \"defVals\":[ ";
		if( (atvc.equals("1")) || (atvc.equals("2")) ) { // numerics can have defaults too. e.g. action month 1-12
			ATSBean.setField("CONO", zdcono);
			ATSBean.setField("ATID", atid);	
			ATSBean.setField("ITNO", itno);	
			ATSBean.setField("WHLO", whlo);	
			ATSBean.setField("ATMO", atmo);	
			ATSBean.runProgram("LstAttrPrompt");
			int i = 0;
			while(ATSBean.nextRow()) {
				//if(i++==0) json += "{\"defVal\":\"\", \"defTxt\":\"Blank\"},";
				// todo revisit - combobox cant be emptied once selected when forceselection on...
				if(atvc.equals("2"))
					json += "{\"defVal\":\"" + ATSBean.getField("AALF") + "\", \"defTxt\":\"" + ATSBean.getField("AALF") + "\"},";
				else
					json += "{\"defVal\":\"" + ATSBean.getField("AALF") + "\", \"defTxt\":\"" + ATSBean.getField("TX30") + " (" + ATSBean.getField("AALF") + ")\"},";
			}
			if(json.length()>2) json = json.substring(0, json.length()-1);
		}	
		json += " ]},";
	}
}

if(json.length()>2) json = json.substring(0, json.length()-1);
json += "]}";
out.println(json);
//ATSBean010.closeConnection();
%>
