<%@ page contentType="text/html; charset=utf-8" language="java" import="org.apache.commons.io.*,java.io.*,java.util.Iterator,org.json.simple.parser.JSONParser,org.json.simple.JSONObject,org.json.simple.JSONValue,org.json.simple.JSONArray,java.util.List" %>
<%@ include file="Connections/INCm3.jsp" %>

<%

JSONParser parser = new JSONParser();
Object obj = parser.parse(request.getParameter("sdata"));
JSONArray orderArray = (JSONArray) obj;
String publishResults = "<ul>Results of Order Reprocessing";

String orderFile = "";
Iterator<JSONObject> iterator = orderArray.iterator();
while (iterator.hasNext()) {
	 JSONObject orders = (JSONObject) iterator.next();
	 orderFile = (String) orders.get("order");
	 String orderFileName = orderFile + ".xml";
		
    File source = new File(orderSource+""+orderFileName);
	File dest = new File(orderDestination+""+orderFileName);
    
	try {
	    FileUtils.copyFile(source, dest);
	    publishResults = publishResults + "<li> Order " + orderFile + ": Resubmitted</li>";
	} catch (IOException e) {
		//out.println("Error " + e);
		System.out.println("Error " + e);
	    e.printStackTrace();
	    publishResults = publishResults + "<li> Order " + orderFile + " could not be resubmitted " + e + "</li>";
	}
}
publishResults = publishResults + "</ul>";
out.println("{\"success\":true, \"results\":\"" + publishResults + "\"}");

%>
