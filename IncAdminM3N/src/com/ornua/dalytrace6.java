package com.ornua;

// if it WAS an INT but now its NON-INT
// get unique containers (say there's 2....)
// do for each container....yup thats possible. If one container, do as normal...
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.ornua.MvxBean;

/**
 * Servlet implementation class dalytrace
 */
// Kevin Woods March 2020
@WebServlet("/dalytrace6")
public class dalytrace6 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String camu;
	private XSSFWorkbook workbook;
	private Sheet shtMWS070MI;
	private CellStyle headerCellStyle;
	private int rowNum;
	private int cellNo;
	
	private MvxBean[] arrMWS070MI;
	private MvxBean[] arrMMS200MI;
	
	private MvxBean[] arrMWS070MI25;
	
	
	private String system;
	private String cono;
	private String faci;
	private String m3user;
	private String m3pass;
	private int port;
	
	final String[] columns_MWS070MI_2R = {"Item No", "Item Desc", "Item Type", "Lot", "Container", "PO", "PO Qty", "Supplier No", "Supplier Name"};
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dalytrace6() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub
    	
   rowNum = 0;

    camu = request.getParameter("camu"); if(camu==null) camu = "";
    
	arrMWS070MI = new MvxBean[10];
	arrMMS200MI = new MvxBean[10]; //todo recursion, could use linked list...
	arrMWS070MI25 = new MvxBean[10];
	
    HttpSession session = request.getSession(true);
    system = session.getAttribute("system").toString();
    cono = session.getAttribute("zdcono").toString();
    faci = session.getAttribute("zdfaci").toString();
    m3user = session.getAttribute("m3user").toString();
    m3pass = session.getAttribute("m3pass").toString();
    port = (Integer)session.getAttribute("port");

    workbook = new XSSFWorkbook();
    CreationHelper createHelper = workbook.getCreationHelper();
    shtMWS070MI = workbook.createSheet("BD Trace " + camu);
    
    int width = 15; // Where width is number of caracters 
    shtMWS070MI.setDefaultColumnWidth(width);

    // Lets style it up
    Font headerFont = workbook.createFont();
    headerFont.setBold(true);
    headerFont.setFontHeightInPoints((short) 14);
    headerFont.setColor(IndexedColors.RED.getIndex());

    // and font
    headerCellStyle = workbook.createCellStyle();
    headerCellStyle.setFont(headerFont);

    Row progName = shtMWS070MI.createRow(0);
    Cell apiDesc = progName.createCell(0);
    apiDesc.setCellValue("Daly Recursive Stock trace on pallet " +camu);

    nextLvl(camu, "", 0); // blank bano to start...

    //for(int i = 1; i < columns_MWS070MI_2R.length; i++) 
    //for(int i = 0; i < 1; i++) 
    //	shtMWS070MI.autoSizeColumn(i);


    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment; filename=" + camu + ".xlsx");
    // kw todo could also email this to user based on MNS150 GetUserData (CRS111 Get) email address (or SSO)...
    workbook.write(response.getOutputStream());
    workbook.close();
  }
    
    
    private void nextLvl(String intCamu, String intBano, int intLvl) { // recursion...
    	
    	
    	
    	System.out.println("Camu: " + intCamu + " Lvl: " + intLvl + " row " + rowNum + " Lot: " + intBano);
   	
    	arrMWS070MI[intLvl] = new MvxBean();
    	arrMWS070MI[intLvl].setSystem(system);
    	arrMWS070MI[intLvl].setPort(port);
    	arrMWS070MI[intLvl].setCompany(cono);
    	arrMWS070MI[intLvl].setUsername(m3user);
    	arrMWS070MI[intLvl].setPassword(m3pass);
    	arrMWS070MI[intLvl].setInitialise("MWS070MI");
        
        Row headerRow = shtMWS070MI.createRow(rowNum++);
        String[] columns = {"MO No"};

        for(int i = intLvl; i < (columns.length+intLvl); i++) {
        	//System.out.println("I is x"  + i + " startCell " + intLvl + " len " + columns.length);
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i-intLvl]);
            cell.setCellStyle(headerCellStyle);
        }
  
        String ridn = ""; String finishedItno = "";
        // 17 is qi receipt
        // 10 is a normal receipt with a qi inspection
        // technical want an inspection on blends
        boolean ok17 = false;
        arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("CAMU", intCamu);
        arrMWS070MI[intLvl].setField("BANO", intBano);
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        
        while(arrMWS070MI[intLvl].nextRow()) { //doesnt matter if multiples returned here...	
        	//System.out.println("Type 17");
        	ok17 = true;
        	ridn = arrMWS070MI[intLvl].getField("RIDN");
        	finishedItno = arrMWS070MI[intLvl].getField("ITNO"); 
            Row row = shtMWS070MI.createRow(rowNum++);
            row.createCell(intLvl).setCellValue(ridn);
        }
        
        if(!ok17) {
        	System.out.println("Type 10");
        	arrMWS070MI[intLvl].setField("TTYP", "10"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
                Row row = shtMWS070MI.createRow(rowNum++);
                row.createCell(intLvl).setCellValue(ridn);
            }
        }      
        rowNum++;
        String itno = "";
        
        Row MWS070MI_2R = shtMWS070MI.createRow(rowNum++);

        for(int i = intLvl; i < (columns_MWS070MI_2R.length+intLvl); i++) { //indent levels
            Cell cell = MWS070MI_2R.createCell(i);
            cell.setCellValue(columns_MWS070MI_2R[i-intLvl]);
            cell.setCellStyle(headerCellStyle);
        }
        
        arrMMS200MI[intLvl] = new MvxBean(); // get MO number
        arrMMS200MI[intLvl].setSystem(system);
        arrMMS200MI[intLvl].setPort(port);
        arrMMS200MI[intLvl].setCompany(cono);
        arrMMS200MI[intLvl].setUsername(m3user);
        arrMMS200MI[intLvl].setPassword(m3pass);
        arrMMS200MI[intLvl].setInitialise("MMS200MI");
        
        arrMWS070MI[intLvl].setField("TTYP", "11"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("RIDN", ridn);
        
        // try this...
        //arrMWS070MI[intLvl].setField("BANO", intBano);
        //arrMWS070MI[intLvl].setField("CAMU", intCamu);
        //
        
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        //System.out.println("11: " + ridn);
        
        String existItno = ""; // dont send INTs twice (we get ++ INT records if they have diff bano/camu)
        String itty = ""; String itds = "";
        while(arrMWS070MI[intLvl].nextRow()) { //

        	Row row = null;
        	itno = arrMWS070MI[intLvl].getField("ITNO");
            String iintCamu = arrMWS070MI[intLvl].getField("CAMU"); if(iintCamu==null) iintCamu = "";
            String iintBano = arrMWS070MI[intLvl].getField("BANO"); if(iintBano==null) iintBano = "";

         // get item type from MMS200MI/LstComponent (agreed with BDaly switch from PDS002MI LstComponents as BOM may have changed)
            arrMMS200MI[intLvl].setField("CONO", cono);
            arrMMS200MI[intLvl].setField("ITNO", itno);
            if(arrMMS200MI[intLvl].runProgram("GetItmBasic").startsWith("OK")) {
            	itty = arrMMS200MI[intLvl].getField("ITTY"); if(itty==null) itty = "";
            	itds = arrMMS200MI[intLvl].getField("ITDS"); if(itds==null) itds = "";
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
            	
/*
            	if(itty.equals("PPK")) { // might need other non-INT types here...
            		
            		
            		
            		
            		
            		
            		
            		
                    row = shtMWS070MI.createRow(rowNum++);
                    row.createCell(intLvl).setCellValue(itno);    
                    row.createCell(intLvl+3).setCellValue(iintBano);
                    row.createCell(intLvl+4).setCellValue(iintCamu);
                    
                	row.createCell(intLvl+1).setCellValue(itty);
                	row.createCell(intLvl+2).setCellValue(itds);
                	
                	
            		
            		if(arrMWS070MI25[intLvl]==null) {
            			System.out.println("25 is null, should only do this once...");
	            		arrMWS070MI25[intLvl] = new MvxBean();
	                	arrMWS070MI25[intLvl].setSystem(system);
	                	arrMWS070MI25[intLvl].setPort(port);
	                	arrMWS070MI25[intLvl].setCompany(cono);
	                	arrMWS070MI25[intLvl].setUsername(m3user);
	                	arrMWS070MI25[intLvl].setPassword(m3pass);
            		}
            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
                		
            		arrMWS070MI25[intLvl].setField("TTYP", "25"); // MO Receipt QI
                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
                    if(arrMWS070MI25[intLvl].nextRow()) {
                    	String ridn25 = arrMWS070MI25[intLvl].getField("RIDN"); if(ridn25==null) ridn25 = "";
                    	String ridl25 = arrMWS070MI25[intLvl].getField("RIDL"); if(ridl25==null) ridl25 = "";
                    	String ridnl25 = ""; if(!ridn25.equals("")) ridnl25 = ridn25 + "/" + ridl25;
                    	String trqt25 = arrMWS070MI25[intLvl].getField("TRQT"); if(trqt25==null) trqt25 = "";
                    	String unms25 = arrMWS070MI25[intLvl].getField("UNMS"); if(unms25==null) unms25 = "";
                    	String qty25 = ""; if(!trqt25.equals("")) qty25 = trqt25 + " " + unms25;
                    	
                    	row.createCell(intLvl+5).setCellValue(ridnl25);
                    	row.createCell(intLvl+6).setCellValue(qty25);
                    	
                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
                    	arrMWS070MI25[intLvl].setField("CONO", cono);
                    	arrMWS070MI25[intLvl].setField("PUNO", ridn25);
                    	if(arrMWS070MI25[intLvl].runProgram("GetHeadBasic").startsWith("OK")) {
                    		String suno25 = arrMWS070MI25[intLvl].getField("SUNO"); if(suno25==null) suno25 = "";
                    		if(!suno25.equals("")) {
                    			row.createCell(intLvl+7).setCellValue(suno25);
                    			arrMWS070MI25[intLvl].changeProg("CRS620MI");
                    			arrMWS070MI25[intLvl].setField("CONO", cono);
                    			arrMWS070MI25[intLvl].setField("SUNO", suno25);
                    			if(arrMWS070MI25[intLvl].runProgram("GetBasicData").startsWith("OK")) {
                    				String sunm25 = arrMWS070MI25[intLvl].getField("SUNM"); if(sunm25==null) sunm25 = "";
                    				row.createCell(intLvl+8).setCellValue(sunm25);
                    			}
                    		}
                    	}
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
            	} else if(itty.equals("INT")) {
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		
            		if(!iintCamu.equals("")) {
            			// should only be one INT, don't call two
            			boolean skip = true;
            			if(itno.equals(existItno)) skip = true; else skip = false;
            			existItno = itno;
            			if(!skip) {
                            row = shtMWS070MI.createRow(rowNum++);
                            row.createCell(intLvl).setCellValue(itno);    
                            //row.createCell(intLvl+3).setCellValue(iintBano);
                            //row.createCell(intLvl+4).setCellValue(iintCamu);
                            
                        	row.createCell(intLvl+1).setCellValue(itty);
                        	row.createCell(intLvl+2).setCellValue(itds);
            				nextLvl(iintCamu, iintBano, intLvl+1); //recurse
            			}
            		}
            		
            		
            		
            		
            		
            		
            		
            		
            		
            	}            	
            	
            	
            	*/
            	
            	

            	
            	
            	
            	
            	
            	if(itty.equals("INT")) { 
            		
            		
            		if(!iintCamu.equals("")) {
            			// should only be one INT, don't call two, need to revisit this one...
            			boolean skip = true;
            			if(itno.equals(existItno)) skip = true; else skip = false;
            			existItno = itno;
            			if(!skip) {
                            row = shtMWS070MI.createRow(rowNum++);
                            row.createCell(intLvl).setCellValue(itno);    
                            //row.createCell(intLvl+3).setCellValue(iintBano);
                            //row.createCell(intLvl+4).setCellValue(iintCamu);
                            // can be multiple BANO/CAMU for an INT BOM
                            // might need to break them out...
                            
                        	row.createCell(intLvl+1).setCellValue(itty);
                        	row.createCell(intLvl+2).setCellValue(itds);
            				nextLvl(iintCamu, iintBano, intLvl+1); //recurse
            			}
            		}
            		
            		
            		
            		
            		
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
            	} else if( (itty.equals("PPK")) || (itty.equals("SPK")) ){ // do PO lookup
            		
            		
            		row = shtMWS070MI.createRow(rowNum++);
                    row.createCell(intLvl).setCellValue(itno);    
                    row.createCell(intLvl+3).setCellValue(iintBano);
                    row.createCell(intLvl+4).setCellValue(iintCamu);
                    
                	row.createCell(intLvl+1).setCellValue(itty);
                	row.createCell(intLvl+2).setCellValue(itds);
                	
                	if(!iintCamu.equals("")) {
            		
	            		if(arrMWS070MI25[intLvl]==null) {
	            			System.out.println("25 is null, should only do this once...");
		            		arrMWS070MI25[intLvl] = new MvxBean();
		                	arrMWS070MI25[intLvl].setSystem(system);
		                	arrMWS070MI25[intLvl].setPort(port);
		                	arrMWS070MI25[intLvl].setCompany(cono);
		                	arrMWS070MI25[intLvl].setUsername(m3user);
		                	arrMWS070MI25[intLvl].setPassword(m3pass);
	            		}
	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	                		
	            		arrMWS070MI25[intLvl].setField("TTYP", "25"); // MO Receipt QI
	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
	                    if(arrMWS070MI25[intLvl].nextRow()) {
	                    	String ridn25 = arrMWS070MI25[intLvl].getField("RIDN"); if(ridn25==null) ridn25 = "";
	                    	String ridl25 = arrMWS070MI25[intLvl].getField("RIDL"); if(ridl25==null) ridl25 = "";
	                    	String ridnl25 = ""; if(!ridn25.equals("")) ridnl25 = ridn25 + "/" + ridl25;
	                    	String trqt25 = arrMWS070MI25[intLvl].getField("TRQT"); if(trqt25==null) trqt25 = "";
	                    	String unms25 = arrMWS070MI25[intLvl].getField("UNMS"); if(unms25==null) unms25 = "";
	                    	String qty25 = ""; if(!trqt25.equals("")) qty25 = trqt25 + " " + unms25;
	                    	
	                    	row.createCell(intLvl+5).setCellValue(ridnl25);
	                    	row.createCell(intLvl+6).setCellValue(qty25);
	                    	
	                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
	                    	arrMWS070MI25[intLvl].setField("CONO", cono);
	                    	arrMWS070MI25[intLvl].setField("PUNO", ridn25);
	                    	if(arrMWS070MI25[intLvl].runProgram("GetHeadBasic").startsWith("OK")) {
	                    		String suno25 = arrMWS070MI25[intLvl].getField("SUNO"); if(suno25==null) suno25 = "";
	                    		if(!suno25.equals("")) {
	                    			row.createCell(intLvl+7).setCellValue(suno25);
	                    			arrMWS070MI25[intLvl].changeProg("CRS620MI");
	                    			arrMWS070MI25[intLvl].setField("CONO", cono);
	                    			arrMWS070MI25[intLvl].setField("SUNO", suno25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetBasicData").startsWith("OK")) {
	                    				String sunm25 = arrMWS070MI25[intLvl].getField("SUNM"); if(sunm25==null) sunm25 = "";
	                    				row.createCell(intLvl+8).setCellValue(sunm25);
	                    			}
	                    		}
	                    	}
	                    }
                	}
            		
            		
            	} else { // non INT and PPK
            		row = shtMWS070MI.createRow(rowNum++);
                    row.createCell(intLvl).setCellValue(itno);    
                    row.createCell(intLvl+3).setCellValue(iintBano);
                    row.createCell(intLvl+4).setCellValue(iintCamu);
                    
                	row.createCell(intLvl+1).setCellValue(itty);
                	row.createCell(intLvl+2).setCellValue(itds);
            	}
            	
	
            	
            } else {
            	row.createCell(intLvl+1).setCellValue("Not found");
            }
        }
        System.out.println("End level " + intLvl);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
