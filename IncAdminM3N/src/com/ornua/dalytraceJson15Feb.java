package com.ornua;

// if it WAS an INT but now its NON-INT
// get unique containers (say there's 2....)
// do for each container....yup thats possible. If one container, do as normal...
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ornua.MvxBean;

/**
 * Servlet implementation class dalytrace
 */
// Kevin Woods March 2020
@WebServlet("/dalytraceJson15Feb")
public class dalytraceJson15Feb extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String camu;
	private String lastBano;

	private int rowNum;
	
	private MvxBean[] arrMWS070MI;
	private MvxBean[] arrMMS200MI;
	private MvxBean[] arrMWS070MI25;
		
	private String system;
	private String cono;
	private String faci;
	private String m3user;
	private String m3pass;
	private int port;
	private int highLvl;

	private String json;
	private String strSkipNegs;
	private int numPals;
	private boolean skipNegs;
	private boolean gotHeaderTrans;
	private boolean isFinishedProd;
	
	//private String megabin;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dalytraceJson15Feb() {
        super();
        // TODO Auto-generated constructor stub
    }

    
    private boolean doHdr(String ttyp, String hdrMo, String hdrBano, String hdrCamu) {
    	
    	System.out.println("About to do header on type " + ttyp);
    	System.out.println("MO: " + hdrMo + ", Bano: " + hdrBano + ", Camu: " + hdrCamu);
    	numPals = 0;
    	boolean findAnything = false;
        MvxBean firstLot = new MvxBean();
    	firstLot.setSystem(system);
    	firstLot.setPort(port);
    	firstLot.setCompany(cono);
    	firstLot.setUsername(m3user);
    	firstLot.setPassword(m3pass);
    	firstLot.setInitialise("MWS070MI");
    	firstLot.setField("TTYP", ttyp);
    	firstLot.setField("BANO", hdrBano);
    	firstLot.setField("CAMU", hdrCamu);
    	firstLot.setField("RIDN", hdrMo);
    	firstLot.runProgram("LstTransBalID");
    	while(firstLot.nextRow()) numPals++;
    	if( numPals>0) skipNegs = true; //|| (!strSkipNegs.equals("on")) 
    	
    	
    	if(numPals>0) {
	    	firstLot.setInitialise("MWS070MI");
	    	firstLot.setField("TTYP", ttyp);
	    	firstLot.setField("BANO", hdrBano);
	    	firstLot.setField("CAMU", hdrCamu);
	    	firstLot.setField("RIDN", hdrMo);
	    	firstLot.runProgram("LstTransBalID");
	    	if(!hdrBano.equals(""))
	    		json+="{\"itno\": \"Lot: " + hdrBano + " (Type " + ttyp + ")\",";
	    	else 
	    		json+="{\"itno\": \"MO: " + hdrMo + " (Type " + ttyp + ")\",";
	    	json+="\"summ\": 1,";
	    	json+="\"expanded\": true,";
	    	json+="\"children\": ["; int w=0;
	    	while(firstLot.nextRow()) {
	    		w++;
	    		String trqt = firstLot.getField("TRQT"); double dtrqt = 0.0;
	    		try {
	    			dtrqt = Double.parseDouble(trqt);
	    		} catch (Exception e) {
	    			;
	    		}
	    		System.out.println("Skip dfnegs  " + skipNegs + " trqt " + dtrqt);
	    		
	    		//if(skipNegs && (dtrqt>0) ) {
	    		if(skipNegs && (dtrqt>-1) ) {
	    			
	    			System.out.println("Ca");
	    			findAnything = true;
		    		camu = firstLot.getField("CAMU");
		    		lastBano = firstLot.getField("BANO");

		    		isFinishedProd = true;
		    		json+="{";
		    		json+="\"summ\": 1,";
		    		json+="\"itno\": \"" + "Pallet: " + camu + "\",";
		    		json+="\"itds\": \"" + firstLot.getField("ITNO") + "\",";
		    		json+="\"ridn\": \"" + firstLot.getField("RIDN") + "\",";
		    		if(w==numPals && ttyp.equals("10")) {  			
		    			json+="\"leaf\": false,";
		                //json+="\"expanded\": true";
		                json+="\"expanded\": true,";
		                // comma could be issue if there's no more data...
		    		} else {
		                json+="\"leaf\": true,";
		                json+="\"expanded\": false,";	
		                json+="\"bano\": \"" + lastBano + "\",";
		                json+="\"bref\": \"" + firstLot.getField("BREF") + "\",";
		                json+="\"bre2\": \"" + firstLot.getField("BRE2") + "\",";
		                json+="\"camu\": \"" + camu + "\",";
		                json+="\"dat\": \"\"";
		                json+="},";
		    		}
	    		}
	    	}
    	}
    	return findAnything;
    }
    
    private void doMegabinStuff(String megabin) {
    	System.out.println("About to do megabin on " + megabin); int numMBPals = 0;
    	boolean findAnything = false;
        MvxBean megaMvx = new MvxBean();
        megaMvx.setSystem(system);
        megaMvx.setPort(port);
        megaMvx.setCompany(cono);
        megaMvx.setUsername(m3user);
        megaMvx.setPassword(m3pass);
        megaMvx.setInitialise("MWS070MI");
        megaMvx.setField("TTYP", "97");
        megaMvx.setField("BANO", megabin);
        megaMvx.runProgram("LstTransBalID");
        while(megaMvx.nextRow()) numMBPals++;
        megaMvx.setField("TTYP", "97");
        megaMvx.setField("BANO", megabin);
        megaMvx.runProgram("LstTransBalID");
        
        MvxBean megaMvxMO = new MvxBean(); // this will be the drill-down to MO for each megabin CAMU entry
        megaMvxMO.setSystem(system);
        megaMvxMO.setPort(port);
        megaMvxMO.setCompany(cono);
        megaMvxMO.setUsername(m3user);
        megaMvxMO.setPassword(m3pass);
        megaMvxMO.setInitialise("MWS070MI");
        

        //todo might need to check is there anything before putting result in
		json+="{\"itno\": \"Megabin: " + megabin + "\",";
		json+="\"summ\": 1,";
		json+="\"expanded\": true,";
		json+="\"children\": [";

    	while(megaMvx.nextRow()) {
    		numMBPals++;
    		String trqt = megaMvx.getField("TRQT"); double dtrqt = 0.0;
    		try {
    			dtrqt = Double.parseDouble(trqt);
    		} catch (Exception e) {
    			;
    		}

    		String megaCamu = megaMvx.getField("CAMU");
    		String megaBano = megaMvx.getField("BANO");

    		json+="{";
    		json+="\"summ\": 1,";
    		json+="\"itno\": \"" + "Pallet: " + megaCamu + "\",";
    		json+="\"itds\": \"" + megaMvx.getField("ITNO") + "\",";
    		//json+="\"ridn\": \"" + megaMvx.getField("RIDN") + "\",";
    		// get RIDN from the MO lookup
            //json+="\"leaf\": true,";
            //json+="\"expanded\": false,";	
            json+="\"bano\": \"" + megaBano + "\",";
            json+="\"qty\": \"" + dtrqt + "\",";
            
            json+="\"bref\": \"" + megaMvx.getField("BREF") + "\",";
            json+="\"bre2\": \"" + megaMvx.getField("BRE2") + "\",";
            json+="\"camu\": \"" + megaCamu + "\",";
            json+="\"dat\": \"\",";
            
            int numMegaMOs = 0;
            megaMvxMO.setField("TTYP", "13");
            megaMvxMO.setField("CAMU", megaCamu);
            megaMvxMO.runProgram("LstTransBalID"); String moRidn = "";
            while(megaMvxMO.nextRow()) {
            	moRidn = megaMvxMO.getField("RIDN");
            	numMegaMOs++;
            }
            System.out.println("num mega MOs " + numMegaMOs);
            if(numMegaMOs==1) {
                json+="\"leaf\": true,";
                json+="\"expanded\": false,";	
        		json+="\"ridnl\": \"" + moRidn+ "\"";
            } else if(numMegaMOs>1) {
                megaMvxMO.setField("TTYP", "13");
                megaMvxMO.setField("CAMU", megaCamu);
                megaMvxMO.runProgram("LstTransBalID");
                json+="\"leaf\": false,";
	    		json+="\"expanded\": true,";
	    		json+="\"children\": [";
	    		
	    		while(megaMvxMO.nextRow()) {
	    			json+="{";
					json+="\"itno\": \"" + "MB MO: " + megaMvxMO.getField("RIDN") + "\",";
					json+="\"itds\": \"" + megaMvxMO.getField("ITNO") + "\",";
		    		json+="\"ridn\": \"" + megaMvxMO.getField("RIDN") + "\",";
		            json+="\"bano\": \"" + megaMvxMO.getField("BANO") + "\",";
		            
		            String sMBtrqt = megaMvxMO.getField("TRQT"); double dMBtrqt = 0.0;
		    		try {
		    			dMBtrqt = Double.parseDouble(sMBtrqt);
		    		} catch (Exception e) {
		    			;
		    		}
		            json+="\"qty\": \"" + dMBtrqt + "\",";		            
		            json+="\"bref\": \"" + megaMvxMO.getField("BREF") + "\",";
		            json+="\"bre2\": \"" + megaMvxMO.getField("BRE2") + "\",";
		            json+="\"camu\": \"" + megaMvxMO.getField("CAMU") + "\",";
		            json+="\"dat\": \"\"";
	    			json+="},";
	    		}

	        	if( (json.length()>2) && (json.trim().endsWith("},")) ) 
	    			json = json.substring(0, json.length() - 1);
	        	json+= "]";
	    		
            }// finished with megabin MO
            json+="},";

    	}
    	
    	if( (json.length()>2) && (json.trim().endsWith("},")) ) 
			json = json.substring(0, json.length() - 1);
    	json+= "]";
    }

	
    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub 
    	PrintWriter out = response.getWriter();
    	response.setContentType("text/html;charset=utf-8");  
    	
   rowNum = 0; highLvl = 0;
   json = "";
	json+="{";
	
    HttpSession session = request.getSession(true);
    system = session.getAttribute("system").toString();
    cono = session.getAttribute("zdcono").toString();
    faci = session.getAttribute("zdfaci").toString();
    m3user = session.getAttribute("m3user").toString();
    m3pass = session.getAttribute("m3pass").toString();
    port = (Integer)session.getAttribute("port");
    
	
    camu = request.getParameter("camu"); if(camu==null) camu = "";
    String camuIn = camu;
    //String itno = request.getParameter("itno"); if(itno==null) itno = "";
    String mo = request.getParameter("mo"); if(mo==null) mo = "";
    String bano = request.getParameter("bano"); if(bano==null) bano = "";
    String megabin = request.getParameter("megabin"); if(megabin==null) megabin = "";
    
    strSkipNegs = request.getParameter("skipNegs"); if(strSkipNegs==null) strSkipNegs = "";
    strSkipNegs = strSkipNegs.trim(); if(strSkipNegs.equals("")) strSkipNegs = "1";
    
    isFinishedProd = false; 
    System.out.println("Starting helpMeUnderstand");
	
	json+="\"success\": true,";
    json+="\"text\": \".\",";
    json+="\"children\": [";
    
    boolean justLot = false;
    gotHeaderTrans = false; boolean skipNegs = false;
    // if bano is given (but not lot or MO) - do lookup on BANO, get all pallets, only recurse on last..
    //if((!bano.equals("")) && (camu.equals("")) ) {
    //if(camu.equals("")) {
    numPals = 0; 
    
	if(!megabin.equals("")) {
		// do megabin stuff
		doMegabinStuff(megabin);
	} else {

    	justLot = true;
    	MvxBean firstLot = new MvxBean();
    	firstLot.setSystem(system);
    	firstLot.setPort(port);
    	firstLot.setCompany(cono);
    	firstLot.setUsername(m3user);
    	firstLot.setPassword(m3pass);  	
    	

    	// do 17
    	// do 11
    	// do 10
    	// then do 13?
    	

    	boolean theres17s = doHdr("17", mo, bano, camuIn); 
    	if(theres17s) { 
    		// remove last comma
    		System.out.println("after 17s json is " + json);
    		if( (json.length()>2) && (json.trim().endsWith("},")) ) 
    			json = json.substring(0, json.length() - 1);
    		json+="]},";
    	}
    	boolean theres11s = doHdr("11", mo, bano, camuIn);
    	if(theres11s) { 
    		// remove last comma
    		if( (json.length()>2) && (json.trim().endsWith("},")) ) 
    			json = json.substring(0, json.length() - 1);
    		json+="]},";
    	}
    	boolean theres10s = doHdr("10", mo, bano, camuIn);
    	
    if(!theres10s) json+="{";
    
	arrMWS070MI = new MvxBean[10];
	arrMMS200MI = new MvxBean[10]; //todo recursion, could use linked list...
	arrMWS070MI25 = new MvxBean[10];
	
	System.out.println("Heading into recursive with Camu: " + camu + ", bano: " + lastBano);
	if(theres17s || theres11s || theres10s) nextLvl(camu, lastBano, 0, false); // blank bano to start...    why? 
	
	// get rid of comma if there's no children
	//if(json.endsWith(","))
	//	if(json.length()>2) 
	//		json = json.substring(0, json.length() - 1);

	
	/*
	boolean theres13s = false;
	if(!mo.equals("")) 
		theres13s = doHdr("13", mo, bano, camuIn); 
	if(theres13s) { 
		// remove last comma
		if( (json.length()>2) && (json.trim().endsWith("},")) ) 
			json = json.substring(0, json.length() - 1);
		json+="]},";
	}
	*/
	
	// now do offcuts type 13 aswell?
	if(!mo.equals("")) {
		MvxBean offcut = new MvxBean();
		offcut.setSystem(system);
		offcut.setPort(port);
		offcut.setCompany(cono);
		offcut.setUsername(m3user);
		offcut.setPassword(m3pass);
		offcut.setInitialise("MWS070MI");
		offcut.runProgram("SetLstMaxRec");
		offcut.setField("TTYP", "13");
		offcut.setField("RIDN", mo);
		offcut.runProgram("LstTransBalID");
		int numOffcutPals = 0;
		while(offcut.nextRow()) numOffcutPals++;	
		System.out.println("Ofcuts " + numOffcutPals);
		if(numOffcutPals>0) {
			
			System.out.println(theres17s + " " + theres11s + " " + theres10s);
			if( (!theres17s) && (!theres11s) && (!theres10s)) {
				System.out.println("marriage " + json);
				if(json.endsWith("{")) {
					json = json.substring(0, json.length() - 1);
				}
			}

			boolean theres13s = false;
			offcut.setInitialise("MWS070MI");
			offcut.runProgram("SetLstMaxRec");
			offcut.setField("RIDN", mo);
			offcut.setField("TTYP", "13");
			offcut.runProgram("LstTransBalID");
		
			// remove last [ if there's finished goods
			System.out.println("Num pals off cut " + numPals);
			//if(!isFinishedProd) {
				if(json.trim().endsWith("]")) 
					json = json.substring(0, json.length() - 1);
				
				if(theres17s || theres11s || theres10s) 
					json+=",";
			//} //else json+=",";
			
			json+="{\"itno\": \"WasteStreams on: " + mo + "\",";
			
			json+="\"summ\": 1,";
			json+="\"expanded\": true,";
			json+="\"children\": ["; int w=0;
			while(offcut.nextRow()) {
				String trqt = offcut.getField("TRQT"); double dtrqt = 0.0;
				try {
					dtrqt = Double.parseDouble(trqt);
				} catch (Exception e) {
					;
				}
				System.out.println("Skip dfnegs  " + skipNegs + " trqt " + dtrqt);
				w++;
				//if(skipNegs && (dtrqt>0) ) {
				//if(skipNegs && (dtrqt>-1) ) {
					System.out.println("Ca");
					theres13s = true;
		    		gotHeaderTrans = true;
		    		camu = offcut.getField("CAMU");
		    		bano = offcut.getField("BANO");
		    		
		    		json+="{";
		    		json+="\"summ\": 1,";
		    		json+="\"itno\": \"" + "Offcut: " + camu + "\",";
		    		json+="\"itds\": \"" + offcut.getField("ITNO") + "\",";
		    		json+="\"ridn\": \"" + offcut.getField("RIDN") + "\",";
		    		if(w==numOffcutPals) {  			
		    			json+="\"leaf\": false,";
		                json+="\"bano\": \"" + bano + "\",";
		                json+="\"bref\": \"" + offcut.getField("BREF") + "\",";
		                json+="\"bre2\": \"" + offcut.getField("BRE2") + "\",";
		                json+="\"camu\": \"" + camu + "\",";
		                json+="\"dat\": \"\",";
		                json+="\"expanded\": true";
		    		} else {
		                json+="\"leaf\": true,";
		                json+="\"expanded\": false,";	
		                json+="\"bano\": \"" + bano + "\",";
		                json+="\"bref\": \"" + offcut.getField("BREF") + "\",";
		                json+="\"bre2\": \"" + offcut.getField("BRE2") + "\",";
		                json+="\"camu\": \"" + camu + "\",";
		                json+="\"dat\": \"\"";
		                json+="},";
		    		}
				//}
			}                
		
			if(theres10s || theres11s || theres17s) {
			
		    json+= " } "; // end of first item's children...
		    json+= " ] "; // end of the first item's children
		    
		    //if(isFinishedProd) {
		    	
			    json+="  }  "; //end of everything
			    json+= " ] "; // end of the first item's children
			}
		    //}
		}
    }

	
    System.out.println("High levell is " + highLvl);
    
    
    /*
    for (int w=0; w<highLvl; w++) {
    	System.out.println("removing..." + w);
    	if( (json.length()>2) && (json.trim().endsWith("}]")) ) 
    			json = json.substring(0, json.length() - 2);
    }
    */
    
    //if(json.trim().endsWith("}"))    json = json.substring(0, json.length() - 1);
    //json = json.substring(0, json.length() - 1);
    
    //json+="  }  ";
		//json+="  ]  ";
    if(justLot) {
    	json+= " }] "; //end hte lot/mo
    }
    
	} // end if megabin stuff...
    
    json+= " } "; // end of first item's children...
    json+= " ] "; // end of the first item's children
    json+="  }  "; //end of everything

    out.print(json); out.flush();
  }
    
    private void nextLvl(String intCamu, String intBano, int intLvl, boolean intType) { // recursion...	
    	
    	System.out.println("Camu: " + intCamu + " Lvl: " + intLvl + " row " + rowNum + " Lot: " + intBano);
    	if(intLvl>highLvl) highLvl = intLvl;
   	
    	arrMWS070MI[intLvl] = new MvxBean();
    	arrMWS070MI[intLvl].setSystem(system);
    	arrMWS070MI[intLvl].setPort(port);
    	arrMWS070MI[intLvl].setCompany(cono);
    	arrMWS070MI[intLvl].setUsername(m3user);
    	arrMWS070MI[intLvl].setPassword(m3pass);
    	arrMWS070MI[intLvl].setInitialise("MWS070MI");
  
        String ridn = ""; String finishedItno = "";
        // 17 is qi receipt
        // 10 is a normal receipt with a qi inspection
        // technical want an inspection on blends
        boolean ok17 = false; boolean ok10 = false; boolean ok13 = false; boolean ok11 = false;
        arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("CAMU", intCamu);
        arrMWS070MI[intLvl].setField("BANO", intBano);
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        //System.out.println("trying it with BANO = " + intBano);
        
        String prevBano = ""; String prevBref = ""; String prevBre2 = ""; String dat = ""; String prevCamu = "";
        boolean skipNegs = false;
        while(arrMWS070MI[intLvl].nextRow()) { //doesnt matter if multiples returned here...	
        	System.out.println("Type 17 ok 1 " + intBano);
        	ok17 = true;
        	ridn = arrMWS070MI[intLvl].getField("RIDN");
        	prevBano = arrMWS070MI[intLvl].getField("BANO");
        	prevBref = arrMWS070MI[intLvl].getField("BREF");
        	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
        	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
        	skipNegs = true;
        }
        
        
        // try 17 without pallet	
        
        if(!ok17) {
        	System.out.println("Type 17 take 2 setting " + intCamu + " " + intBano);
        	//arrMWS070MI[intLvl].setInitialise("MWS070MI");
        	arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
            //arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok17 = true;
            	System.out.println("did we find a NEXT next row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            	skipNegs = true;
            }
        }
        
        if(!ok17) {
        	System.out.println("Type 10 cakes, setting " + intCamu + " " + intBano);
        	arrMWS070MI[intLvl].setField("TTYP", "10"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok10 = true;
            	//System.out.println("did we find a next row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }
        
        if(!ok17 && !ok10) {
        	System.out.println("Type 13 cakes, setting " + intCamu + " " + intBano);
        	arrMWS070MI[intLvl].setField("TTYP", "13"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok13 = true;
            	
            	System.out.println("did we find a next 13 row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }
        
        System.out.println(ok17 + " " + ok13 + " " + ok11 + " " + ok10);
        
        if( ok10 || ok17 || ok13 ) {
        	arrMWS070MI[intLvl].setField("TTYP", "11"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok11 = true;
            	//System.out.println("did we find a next row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevCamu = arrMWS070MI[intLvl].getField("CAMU");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }
        
        
        
        //if( (ok10 || ok17 || ok13 || ok11) && (!(intBano.equals(prevBano) && (intCamu.equals(prevCamu))) ) ) {
        if (ok10 || ok17 || ok13 || ok11) {
        //System.out.println("Fuxxle ridn is " + ridn + " for lot " + intBano);
        if(!intType) { // if its INT type, we already have these 
	        json+="\"bano\": \"" + prevBano + "\",";
	        json+="\"bref\": \"" + prevBref + "\",";
	        json+="\"bre2\": \"" + prevBre2 + "\",";
	        json+="\"camu\": \"" + intCamu + "\",";
	        json+="\"dat\": \"" + dat + "\"";
        }
        
        
        
    	json+=",\"children\": [";
        json+="{";
        	json+="\"summ\": 1,";
            json+="\"itno\": \"MO: " + ridn + "\",";
            json+="\"expanded\": true,";
    		json+="\"children\": [";
            // MO should always have children...
        //rowNum++;
        String itno = "";
        
        arrMMS200MI[intLvl] = new MvxBean(); // get item type and desc
        arrMMS200MI[intLvl].setSystem(system);
        arrMMS200MI[intLvl].setPort(port);
        arrMMS200MI[intLvl].setCompany(cono);
        arrMMS200MI[intLvl].setUsername(m3user);
        arrMMS200MI[intLvl].setPassword(m3pass);
        arrMMS200MI[intLvl].setInitialise("MMS200MI");
        
        arrMWS070MI[intLvl].setField("TTYP", "11"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("RIDN", ridn);
        
        // try this...
        /*
        if(intType) {
	        if(intCamu.startsWith("TB")) {
	        //arrMWS070MI[intLvl].setField("BANO", intBano);
	        	System.out.println("Starts with TB + " + intCamu + " " + intBano);
	        	arrMWS070MI[intLvl].setField("CAMU", intBano);
	        } else {
	        	arrMWS070MI[intLvl].setField("BANO", intBano);
	        }
        }
        */
        //
        
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        //arrMWS070MI[intLvl].runProgram("LstTransByOrder");
        //order gives back duplicates and different stuff, need to check...
        //System.out.println("11: " + ridn);
        
        String existItno = ""; // dont send INTs twice (we get ++ INT records if they have diff bano/camu)
        String existBano = ""; // what if we do different INTs where ITNO and BANO diff
        String itty = ""; String itds = "";
        while(arrMWS070MI[intLvl].nextRow()) { //
        	
        	json+="{"; 

        	//Row row = null;
        	itno = arrMWS070MI[intLvl].getField("ITNO");
            String iintCamu = arrMWS070MI[intLvl].getField("CAMU"); if(iintCamu==null) iintCamu = "";
            String iintBano = arrMWS070MI[intLvl].getField("BANO"); if(iintBano==null) iintBano = "";
            String bref = arrMWS070MI[intLvl].getField("BREF"); if(bref==null) bref = "";
            String bre2 = arrMWS070MI[intLvl].getField("BRE2"); if(bre2==null) bre2 = "";
            String trqt = arrMWS070MI[intLvl].getField("TRQT"); if(trqt==null) trqt = "";
            String unms = arrMWS070MI[intLvl].getField("UNMS"); if(unms==null) unms = "";
            
            String moridn25 = arrMWS070MI[intLvl].getField("RIDN"); if(moridn25==null) moridn25 = "";
            String moridl25 = arrMWS070MI[intLvl].getField("RIDL"); if(moridl25==null) moridl25 = "";
            
            String qty11 = ""; 
            if(!trqt.equals("")) { 
            	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
                String ftrqt = decimalFormat.format(Double.valueOf(trqt));
            	qty11 = ftrqt + " " + unms;  
            }

         // get item type from MMS200MI/LstComponent (agreed with BDaly switch from PDS002MI LstComponents as BOM may have changed)
            arrMMS200MI[intLvl].setField("CONO", cono);
            arrMMS200MI[intLvl].setField("ITNO", itno);
            if(arrMMS200MI[intLvl].runProgram("GetItmBasic").startsWith("OK")) {
            	itty = arrMMS200MI[intLvl].getField("ITTY"); if(itty==null) itty = "";
            	itds = arrMMS200MI[intLvl].getField("ITDS"); if(itds==null) itds = "";
            	
            	if(itty.equals("INT")) { 
            		
            		if(!iintCamu.equals("")) {
            			// should only be one INT, don't call two, need to revisit this one...
            			boolean skip = true;
            			if( (itno.equals(existItno)) && (iintBano.equals(existBano)) ) skip = true; else skip = false;
            			//if(itno.equals(existItno)) skip = true; else skip = false;
            			existItno = itno;
            			existBano = iintBano;
            			//skip=false; //temp
            			
                            //row.createCell(intLvl).setCellValue(itno);    
                        	//row.createCell(intLvl+1).setCellValue(itty);
                        	//row.createCell(intLvl+2).setCellValue(itds);
            					json+="\"summ\": 1,";
                                json+="\"itno\": \"" + itno + "\",";
                                json+="\"itds\": \"" + itds + "\",";
                                json+="\"itty\": \"" + itty + "\",";
                                json+="\"bano\": \"" + iintBano + "\",";
                                json+="\"camu\": \"" + iintCamu + "\",";
                                
    	                    	json+="\"moridn25\": \"" + moridn25 + "\",";
    	                    	json+="\"moridl25\": \"" + moridl25 + "\",";
                                json+="\"bref\": \"" + bref + "\",";
                                json+="\"bre2\": \"" + bre2 + "\",";
                                json+="\"qty\": \"" + qty11 + "\",";
                                
                        if(!skip) {
                            //if( (!skip) && (! (intBano.equals(prevBano)) && (intCamu.equals(prevCamu)) ) ) {
                                json+="\"expanded\": true";
                                nextLvl(iintCamu, iintBano, intLvl+1, true); //recurse
            			} else {
            				// might do something to handle the skip here....
            				json+="\"leaf\": true,";
            				json+="\"expanded\": false";
            			}
            		}         
            	} else if( (itty.equals("PPK")) || (itty.equals("SPK")) || (itty.equals("RMT")) ) { // do PO lookup

            		json+="\"moridn25\": \"" + moridn25 + "\",";
                	json+="\"moridl25\": \"" + moridl25 + "\",";
                        json+="\"itno\": \"" + itno + "\",";
                        json+="\"itty\": \"" + itty + "\",";
                        json+="\"itds\": \"" + itds + "\",";
                        json+="\"bano\": \"" + iintBano + "\",";
                        json+="\"camu\": \"" + iintCamu + "\",";
                        json+="\"bref\": \"" + bref + "\",";
                        json+="\"bre2\": \"" + bre2 + "\",";
                        json+="\"qty\": \"" + qty11 + "\",";

                	if( (!iintCamu.equals("")) || (!iintBano.equals("")) ) {
                		System.out.println("camu " + iintCamu + " Lot " + iintBano);
            		
	            		if(arrMWS070MI25[intLvl]==null) {
	            			//System.out.println("25 is null, should only do this once...");
		            		arrMWS070MI25[intLvl] = new MvxBean();
		                	arrMWS070MI25[intLvl].setSystem(system);
		                	arrMWS070MI25[intLvl].setPort(port);
		                	arrMWS070MI25[intLvl].setCompany(cono);
		                	arrMWS070MI25[intLvl].setUsername(m3user);
		                	arrMWS070MI25[intLvl].setPassword(m3pass);
	            		}
	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	                		
	            		String poType = "25";  
	            		arrMWS070MI25[intLvl].setField("TTYP", poType); // MO Receipt QI
	            		// kw temp make sure put this back in!
	            		// can it work with just CAMU (not LOT, capture reclasses?)
	                    //arrMWS070MI25[intLvl].setField("BANO", iintBano);
	            	if(!iintCamu.equals("")) 
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	            	else
	            		arrMWS070MI25[intLvl].setField("BANO", iintBano);

	            	 arrMWS070MI25[intLvl].runProgram("LstTransBalID");
	                    
	
	            	 if(iintCamu.equals("")) {
		            	 // only check recursion if there's a BANO and no CAMU
	                    if(!arrMWS070MI25[intLvl].nextRow()) {
	                    	//iintCamu = ""; //blank out camu for search...
	                    	// need to do the ref lookup and recurse somehow here...
	                    	arrMWS070MI25[intLvl].setField("TTYP", "97"); // reclassification
		                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
		                    //arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
		                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
		                    if(arrMWS070MI25[intLvl].nextRow()) {
		                    	String rftx97 = (String)arrMWS070MI25[intLvl].getField("RFTX"); if(rftx97==null) rftx97 = "";
		                    	rftx97 = rftx97.trim().replaceAll(" +", " ");
		                    	//System.out.println("rftxx is " + rftx);
		                    	String[] rftxBits = rftx97.split(" ");
		                    	if(rftxBits.length>2) iintBano = rftxBits[1]; 
		                    	json+="\"origBano\": \"" + iintBano + "\",";
		                    	
		                    	arrMWS070MI25[intLvl].setInitialise("MWS070MI");
		                    	arrMWS070MI25[intLvl].setField("TTYP", poType);
		                    	arrMWS070MI25[intLvl].setField("BANO", iintBano);
			                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
		                    }
	            	 }
	            	 }

	                    
		                    
	                    if(arrMWS070MI25[intLvl].nextRow()) {
	                    	System.out.println("We're in,..");
	                    	String ridn25 = arrMWS070MI25[intLvl].getField("RIDN"); if(ridn25==null) ridn25 = "";
	                    	String ridl25 = arrMWS070MI25[intLvl].getField("RIDL"); if(ridl25==null) ridl25 = "";
	                    	String ridnl25 = ""; if(!ridn25.equals("")) ridnl25 = ridn25 + "/" + ridl25;
	                    	String trqt25 = arrMWS070MI25[intLvl].getField("TRQT"); if(trqt25==null) trqt25 = ""; trqt25 = trqt25.replace(".0000", "");
	                    	String unms25 = arrMWS070MI25[intLvl].getField("UNMS"); if(unms25==null) unms25 = "";
	                    	String qty25 = ""; if(!trqt25.equals("")) qty25 = trqt25 + " " + unms25;
	                    	
	                    	System.out.println("We're in, ridn " + ridn25);
	                    	//row.createCell(intLvl+5).setCellValue(ridnl25);
	                    	//row.createCell(intLvl+6).setCellValue(qty25);
	                    	
	                    	json+="\"ridnl\": \"" + ridnl25 + "\",";
				            json+="\"poqty\": \"" + qty25 + "\",";
	                    	
	                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
	                    	arrMWS070MI25[intLvl].setField("CONO", cono);
	                    	arrMWS070MI25[intLvl].setField("PUNO", ridn25);
	                    	arrMWS070MI25[intLvl].setField("PNLI", ridl25);
	                    	if(arrMWS070MI25[intLvl].runProgram("GetLine").startsWith("OK")) {
	                    		String suno25 = arrMWS070MI25[intLvl].getField("SUNO"); if(suno25==null) suno25 = "";
	                    		String whlo25 = arrMWS070MI25[intLvl].getField("WHLO"); if(whlo25==null) whlo25 = "";
	                    		if(!suno25.equals("")) {
	                    			//row.createCell(intLvl+7).setCellValue(suno25);	                    			
	    				            json+="\"suno\": \"" + suno25 + "\",";
	    				            json+="\"powhs\": \"" + whlo25 + "\",";
	                    			arrMWS070MI25[intLvl].changeProg("CRS620MI");
	                    			arrMWS070MI25[intLvl].setField("CONO", cono);
	                    			arrMWS070MI25[intLvl].setField("SUNO", suno25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetBasicData").startsWith("OK")) {
	                    				String sunm25 = arrMWS070MI25[intLvl].getField("SUNM"); if(sunm25==null) sunm25 = "";
	                    				//row.createCell(intLvl+8).setCellValue(sunm25);
		    				            json+="\"sunm\": \"" + sunm25 + "\",";
	                    			}
	                    		}
	                    		
	                    		if(!whlo25.equals("")) {
	                    			arrMWS070MI25[intLvl].changeProg("MMS005MI");
	                    			arrMWS070MI25[intLvl].setField("WHLO", whlo25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetWarehouse").startsWith("OK")) {
	                    				String whnm = arrMWS070MI25[intLvl].getField("WHNM"); if(whnm==null) whnm = "";
	                    				json+="\"powhs\": \"" + whlo25 + " " + whnm + "\",";
	                    			}
	                    		}
	                    		
	                    		// get the PO receipt transactions...
                    			arrMWS070MI25[intLvl].setInitialise("MWS070MI");
                    			arrMWS070MI25[intLvl].setField("TTYP", "25");
                    			arrMWS070MI25[intLvl].setField("RIDN", ridn25);
                    			arrMWS070MI25[intLvl].setField("RIDL", ridl25);
                    			// todo kw - do we need the ITNO?
                    			arrMWS070MI25[intLvl].setField("ITNO", itno);
                    			arrMWS070MI25[intLvl].runProgram("LstTransByOrder");
                    			boolean isTherePODat = false;
                    			if(arrMWS070MI25[intLvl].nextRow()) { // this should be a WHILE for multiple receipts, revisit TODO kw
                    				isTherePODat = true;
                    				json+="\"podat\": \"" + arrMWS070MI25[intLvl].getField("TRDT") + " " + arrMWS070MI25[intLvl].getField("TRTM") + "\",";
                    			}
                    			
                    			// get the PO receipt transactions without item (reclassed...)
                    			if(!isTherePODat) {
	                    			arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	                    			arrMWS070MI25[intLvl].setField("TTYP", "25");
	                    			arrMWS070MI25[intLvl].setField("RIDN", ridn25);
	                    			arrMWS070MI25[intLvl].setField("RIDL", ridl25);
	                    			// todo kw - do we need the ITNO?
	                    			//arrMWS070MI25[intLvl].setField("ITNO", itno);
	                    			arrMWS070MI25[intLvl].runProgram("LstTransByOrder");
	                    			if(arrMWS070MI25[intLvl].nextRow()) { // this should be a WHILE for multiple receipts, revisit TODO kw
	                    				json+="\"podat\": \"" + arrMWS070MI25[intLvl].getField("TRDT") + " " + arrMWS070MI25[intLvl].getField("TRTM") + "\",";
	                    			}
                    			}
                    			
	                    	}
	                    } else System.out.println(" Got no next row!?");
                	}
                	
                    json+="\"leaf\": true,";
                    json+="\"expanded\": false"; // only INTs can carry on...
            	} else { // non INT and PPK, no need to look up PO source of product...	
                    //row.createCell(intLvl).setCellValue(itno);    
                    //row.createCell(intLvl+3).setCellValue(iintBano);
                    //row.createCell(intLvl+4).setCellValue(iintCamu);
                	//row.createCell(intLvl+1).setCellValue(itty);
                	//row.createCell(intLvl+2).setCellValue(itds);
            		
            		json+="\"moridn25\": \"" + moridn25 + "\",";
                	json+="\"moridl25\": \"" + moridl25 + "\",";
            		json+="\"bref\": \"" + bref + "\",";
                    json+="\"bre2\": \"" + bre2 + "\",";
                    json+="\"qty\": \"" + qty11 + "\",";
            		
                    json+="\"itno\": \"" + itno + "\",";
                    json+="\"itty\": \"" + itty + "\",";
                    json+="\"itds\": \"" + itds + "\",";
                    json+="\"bano\": \"" + iintBano + "\",";
                    json+="\"camu\": \"" + iintCamu + "\",";
                    json+="\"leaf\": true,";
                    json+="\"expanded\": false"; // only INTs can carry on...
            	}	
            
            } else {
            	//row.createCell(intLvl+1).setCellValue("Not found");
            }
            json+="},";	
        }
        
        if(json.endsWith(",")) if(json.length()>2) json = json.substring(0, json.length() - 1); // remove last comma..
        json+="] "; // close the item children...
        
        
        System.out.println("Closing? " + itno + " with itty " + itty);
        if(json.length()>2) json = json.substring(0, json.length() - 1); // remove last comma..
        if(itty.equals("INT")) { 
	        json+=" }]"; // close the children of the MO 
        } else {
        	json+=" }]"; // close the children of the MO 
	        //json+=" }]"; // close the MO...  
        }
        
        } 
        System.out.println("End levelxxx " + intLvl);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
