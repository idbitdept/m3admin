package com.ornua;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jdom.*;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * Servlet implementation class xmlM3API
 */
@WebServlet("/xmlM3API")
public class xmlM3API extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String minm;
	private String trnm;
	private String trds;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public xmlM3API() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub

	    minm = request.getParameter("minm"); if(minm==null) minm = "";
	    trds = request.getParameter("trds"); if(trds==null) trds = "";
	    trnm = request.getParameter("trnm"); if(trnm==null) trnm = "";
	    System.out.println(minm + " " + trnm + " " + trds);
	    MvxBean KevBean = new MvxBean();
	
	    HttpSession session = request.getSession(true);
	    String system = session.getAttribute("system").toString();
	    String cono = session.getAttribute("zdcono").toString();
	
	    String m3user = session.getAttribute("m3user").toString();
	    String m3pass = session.getAttribute("m3pass").toString();
	    
	    String uname = session.getAttribute("uname").toString();
	
	    int port = (Integer)session.getAttribute("port");
	
	    KevBean.setSystem(system);
	    KevBean.setPort(port);
	    KevBean.setCompany(cono);
	    KevBean.setUsername(m3user);
	    KevBean.setPassword(m3pass);
	
	    KevBean.setInitialise("MRS001MI");
	
	    KevBean.setField("MINM", minm);
	    KevBean.setField("TRNM", trnm);
	    KevBean.setField("TRTP", "I"); //input fields
	    KevBean.runProgram("LstFields");
	    
	    Element root;
	    Document doc;
	    //int id = 1;
	    
	    root = new Element("masterdata");
	   // root.setAttribute("m3api", "v13.4");
	    
	    Comment comment = new Comment("Exported by " + uname + " at " + Calendar.getInstance().getTime() + "m3 api version 13.4java");
		root.addContent(comment);
	    
	    Element reference = new Element("reference").setText("sphera reference");
	    Element program = new Element("program").setText(minm);
	    Element transaction = new Element("transaction").setText(trnm);
	    
	    root.addContent(reference);
	    root.addContent(program);
	    root.addContent(transaction);
	    
	    Element rec = new Element("record");
	    
	    String flnm = ""; String flds = "";
	    while(KevBean.nextRow()) {
	    	flnm = KevBean.getField("FLNM");  flnm = flnm.trim();
	    	flds = KevBean.getField("FLDS");  flds = flds.trim();
	    	rec.addContent(new Element(flnm).setText(flds));
	    }
	    root.addContent(rec);
	    doc = new Document(root);
	    
	    response.setContentType("text/xml");
	    response.setHeader("Content-Disposition", "attachment; filename=" + minm + "_" + trnm + ".xml");
	    
	    XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(doc, response.getOutputStream());	    
	    
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
