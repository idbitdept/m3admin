package com.ornua;

// if it WAS an INT but now its NON-INT1000
// get unique containers (say there's 2....)
// do for each container....yup thats possible. If one container, do as normal...
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.compress.utils.Lists;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import com.ornua.MvxBean;

/**
 * Servlet implementation class dalytrace
 */
// Kevin Woods March 2020
@WebServlet("/dalytraceExcel")
public class dalytraceExcel extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String camu;
	private XSSFWorkbook workbook;
	private static XSSFWorkbook sortedWorkbook;
	private Sheet sht;
	private CellStyle headerCellStyle;
	private int rowNum;
	
	private MvxBean[] arrMWS070MI;
	private MvxBean[] arrMMS200MI;
	private MvxBean[] arrMWS070MI25;
		
	private String system;
	private String cono;
	private String faci;
	private String m3user;
	private String m3pass;
	private int port;
	private int highLvl;
	
	private int lvlMultiplierAdd = 1;
	private int copyRowNum = 0;
	private int fifty50 = 0;
	
	
	final String[] hdrCols = {"Tree", "Item Type", "Item Desc", "Lot", "Container", "Lot Ref 1", "Lot Ref 2", "Issued to MO", 
			"Trans Date", "PO", "PO Qty", "Supplier No", "Supplier Name"};
	
	final String[] hdrCols2 = {"Level", "I/O", "MO NUMBER", "Order number", "LEVEL ITEM NUMBER", "F/G Description", 
			"RAW MATERIAL ITEM NUMBER", "Item Type", "Name", "Total Issued", "Status", "Lot number", 
			"Container", "Reference text", "Entry date", "Entry time", "Lot Ref 1", "Lot Ref 2",
			"Responsible", "Partner Number", "Partner Name", "Receipt date", "Receipt time"};
	// removed , "Comments"
	
    final static short[] cols = {IndexedColors.LIGHT_ORANGE.getIndex(),
    		IndexedColors.LIGHT_TURQUOISE.getIndex(), IndexedColors.GREY_25_PERCENT.getIndex(),
    		 IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex(),  IndexedColors.LIGHT_GREEN.getIndex(), 
    		IndexedColors.GREY_40_PERCENT.getIndex(), IndexedColors.LEMON_CHIFFON.getIndex(),
    		IndexedColors.LAVENDER.getIndex(), IndexedColors.LIGHT_BLUE.getIndex()
    		};
    
    private static String thisMO;
    private static int colPos = 0;

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dalytraceExcel() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub 
    	
        camu = request.getParameter("camu"); if(camu==null) camu = "";
        String itno = request.getParameter("itno"); if(itno==null) itno = "";
        String mo = request.getParameter("mo"); if(mo==null) mo = ""; if(mo.equalsIgnoreCase("Undefined")) mo = "";
        String bano = request.getParameter("bano"); if(bano==null) bano = "";

    	workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        sht = workbook.createSheet("Lot " + bano);
        
        int width = 15; // Where width is number of caracters 
        sht.setDefaultColumnWidth(width);

        // Lets style it up
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // and font
        headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        rowNum = 0; System.out.println("Row nummt	 is " + rowNum);
        Row hdrRow = sht.createRow(rowNum++);
        
        for(int i = 0; i < hdrCols2.length; i++) {
            Cell cell = hdrRow.createCell(i);
            cell.setCellValue(hdrCols2[i]); 
            cell.setCellStyle(headerCellStyle);
        }

	
    HttpSession session = request.getSession(true);
    system = session.getAttribute("system").toString();
    cono = session.getAttribute("zdcono").toString();
    faci = session.getAttribute("zdfaci").toString();
    m3user = session.getAttribute("m3user").toString();
    m3pass = session.getAttribute("m3pass").toString();
    port = (Integer)session.getAttribute("port");
	
    
    boolean justLot = false;
    boolean gotHeaderTrans = false; boolean skipNegs = false;
    // if bano is given (but not lot or MO) - do lookup on BANO, get all pallets, only recurse on last..
    //if((!bano.equals("")) && (camu.equals("")) ) {
    if(camu.equals("")) {
    	justLot = true;
    	MvxBean firstLot = new MvxBean();
    	firstLot.setSystem(system);
    	firstLot.setPort(port);
    	firstLot.setCompany(cono);
    	firstLot.setUsername(m3user);
    	firstLot.setPassword(m3pass);
    	
    	MvxBean firstLotDetail = new MvxBean();
    	firstLotDetail.setSystem(system);
    	firstLotDetail.setPort(port);
    	firstLotDetail.setCompany(cono);
    	firstLotDetail.setUsername(m3user);
    	firstLotDetail.setPassword(m3pass);
    	firstLotDetail.setInitialise("MDBREADMI");
    	
    	MvxBean firstLotItem = new MvxBean();
    	firstLotItem.setSystem(system);
    	firstLotItem.setPort(port);
    	firstLotItem.setCompany(cono);
    	firstLotItem.setUsername(m3user);
    	firstLotItem.setPassword(m3pass);
    	firstLotItem.setInitialise("MMS200MI");
    	
    	int numPals = 0;
    	String ttyp = "17";
    	firstLot.setInitialise("MWS070MI");
    	firstLot.setField("TTYP", ttyp);
    	firstLot.setField("BANO", bano);
    	firstLot.setField("CAMU", camu);
    	firstLot.setField("RIDN", mo);
    	firstLot.runProgram("LstTransBalID");
    	while(firstLot.nextRow()) numPals++;
    	System.out.println("Type 17 is " + numPals);
    	if(numPals>0) skipNegs = true;
    	
    	if(numPals==0) {
    		ttyp = "11";
    		firstLot.setField("TTYP", ttyp);
    		System.out.println(" type 11 hedadder ");
    		firstLot.setField("BANO", bano);
        	firstLot.setField("RIDN", mo);
        	firstLot.runProgram("LstTransBalID");
        	while(firstLot.nextRow()) numPals++;
        	
    	}
    	
    	if(numPals==0) {
    		ttyp = "13";
    		firstLot.setField("TTYP", ttyp);
    		System.out.println(" type 13 hedader ");
    		firstLot.setField("BANO", bano);
        	firstLot.setField("RIDN", mo);
        	firstLot.runProgram("LstTransBalID");
        	while(firstLot.nextRow()) numPals++;	
    	}
    	
    	if(numPals==0) {
    		ttyp = "10";
    		firstLot.setField("TTYP", ttyp);
    		System.out.println(" type 10 hedader ");
    		firstLot.setField("BANO", bano);
    		firstLot.setField("CAMU", camu);
        	firstLot.setField("RIDN", mo);
        	firstLot.runProgram("LstTransBalID");
        	while(firstLot.nextRow()) numPals++;     	
    	}
    	
    	firstLot.setInitialise("MWS070MI");
    	firstLot.setField("TTYP", ttyp);
    	firstLot.setField("BANO", bano);
    	firstLot.setField("RIDN", mo);
    	firstLot.runProgram("LstTransBalID");

    	int w=0;
    	lvlMultiplierAdd = 1;
    	while(firstLot.nextRow()) {
    		String trqt = firstLot.getField("TRQT"); double dtrqt = 0.0;
    		try {
    			dtrqt = Double.parseDouble(trqt);
    		} catch (Exception e) {
    			;
    		}
    		System.out.println("Skip dfnegs  " + skipNegs + " trqt " + dtrqt);
    		w++;
    		if(skipNegs && (dtrqt>0) ) {
    		gotHeaderTrans = true;
    		camu = firstLot.getField("CAMU");
    		w++;
    		// add the excel row here for level -1
    		
            Row rowCamu = sht.createRow(rowNum++);
     
            itno = firstLot.getField("ITNO"); if(itno==null) itno = "";
            rowCamu.createCell(0).setCellValue((1 * 10000) + (lvlMultiplierAdd++)); // is -1 really
            rowCamu.createCell(1).setCellValue("Output");
            rowCamu.createCell(2).setCellValue(firstLot.getField("RIDN"));
            rowCamu.createCell(4).setCellValue(itno);
            rowCamu.createCell(5).setCellValue(firstLot.getField("ITDS"));
            
            firstLotItem.setField("CONO", cono);
            firstLotItem.setField("ITNO", firstLot.getField("ITNO"));
            if(firstLotItem.runProgram("GetItmBasic").startsWith("OK")) {
            	rowCamu.createCell(7).setCellValue(firstLotItem.getField("ITTY"));
            	//itds = arrMMS200MI[intLvl].getField("ITDS"); if(itds==null) itds = "";
            } else
            	rowCamu.createCell(7).setCellValue("Item type");
            	
            	
            rowCamu.createCell(6).setCellValue(" "); //todo whats this? ???
            
            rowCamu.createCell(8).setCellValue(" "); // ???
            rowCamu.createCell(9).setCellValue(""); // Total issued
            rowCamu.createCell(10).setCellValue(firstLot.getField("STAS"));
            rowCamu.createCell(11).setCellValue(firstLot.getField("BANO"));
            rowCamu.createCell(12).setCellValue(firstLot.getField("CAMU"));
            rowCamu.createCell(13).setCellValue(firstLot.getField("RFTX"));
            rowCamu.createCell(14).setCellValue(""); //Entry date
            rowCamu.createCell(15).setCellValue(""); //Entry time
            rowCamu.createCell(16).setCellValue(firstLot.getField("BREF"));
            rowCamu.createCell(17).setCellValue(firstLot.getField("BRE2"));
            rowCamu.createCell(18).setCellValue(""); //responsible
            rowCamu.createCell(3).setCellValue(firstLot.getField("RIDN"));
            rowCamu.createCell(19).setCellValue(""); //Partner number
            rowCamu.createCell(20).setCellValue(""); //Partner name
            rowCamu.createCell(21).setCellValue(""); //Receipt date
            rowCamu.createCell(22).setCellValue(""); //Receipt time
            //rowCamu.createCell(23).setCellValue("FINISHED GOODS DETAILS");
            
            // now lets get the detail transaction detail for each level 1 finished goods pallet

            firstLotDetail.setInitialise("MDBREADMI");
            firstLotDetail.setField("CAMU", firstLot.getField("CAMU"));
            firstLotDetail.setField("ITNO", firstLot.getField("ITNO"));
            System.out.println("in pallet " + firstLot.getField("CAMU"));
            firstLotDetail.runProgram("LstMITTRAU4K1");
            while(firstLotDetail.nextRow()) {
            	
            	if(!firstLot.getField("CAMU").trim().equals(firstLotDetail.getField("CAMU").trim())) break;
            	System.out.println("in detail of palletxyyyx " + firstLot.getField("CAMU"));
            	
            	trqt = firstLotDetail.getField("TRQT"); if(trqt==null) trqt = ""; if(trqt.equals("")) trqt = "0";
            	
            	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
                String ftrqt = trqt;
                
                try {
                	ftrqt = decimalFormat.format(Double.valueOf(trqt));
                } catch (Exception e){
                	; // nothing to do...
                }
                
            	String unms = firstLotDetail.getField("UNMS"); if(unms==null) unms = "";
            	String resp = firstLotDetail.getField("RESP"); if(resp==null) resp = "";
            	
            	String fldRidn = firstLotDetail.getField("RIDN"); if(fldRidn==null) fldRidn = "";
            	String fldRidl = firstLotDetail.getField("RIDL"); if(fldRidl==null) fldRidl = "";
            	
            	String qty = ftrqt + " " + unms;
            	String ttid = firstLotDetail.getField("TTID"); if(ttid==null) ttid = "";
            	String trtp = firstLotDetail.getField("TRTP"); if(trtp==null) trtp = "";
            	ttyp = firstLotDetail.getField("TTYP"); if(ttyp==null) ttyp = "";
            	String transTypeFull = ttid + " " + trtp + " " + ttyp;
            	String rscd = firstLotDetail.getField("RSCD"); if(rscd==null) rscd = "";
            	if(!rscd.equals("")) rscd = " (Reason: " + rscd + ")";
            	transTypeFull+= rscd;
            	
            	Row rowCamuDetail = sht.createRow(rowNum++);
            	String trdt = firstLotDetail.getField("TRDT"); if(trdt==null) trdt = "";
            	String trtm = firstLotDetail.getField("TRTM"); if(trtm==null) trtm = "";
            	String rftx = firstLotDetail.getField("RFTX"); if(rftx==null) rftx = "";
            	String stas = firstLotDetail.getField("STAS"); if(stas==null) stas = "";
            	
            	rowCamuDetail.createCell(0).setCellValue((1 * 10000) + (lvlMultiplierAdd++));
            	rowCamuDetail.createCell(1).setCellValue("Output");
            	rowCamuDetail.createCell(8).setCellValue(transTypeFull);
            	rowCamuDetail.createCell(9).setCellValue(qty); //qty
            	rowCamuDetail.createCell(10).setCellValue(stas); //status
            	rowCamuDetail.createCell(11).setCellValue(firstLot.getField("BANO"));
            	rowCamuDetail.createCell(12).setCellValue(firstLot.getField("CAMU")); // keep these for sorting...
            	rowCamuDetail.createCell(13).setCellValue(rftx); //rftx
             	rowCamuDetail.createCell(14).setCellValue(trdt); //Entry date
            	rowCamuDetail.createCell(15).setCellValue(trtm); //Entry time           	
            	rowCamuDetail.createCell(18).setCellValue(resp); //resp
            	
            	// copy to problem with blanks.. watch...
            	rowCamuDetail.createCell(2).setCellValue(" "); 
            	rowCamuDetail.createCell(3).setCellValue(" "); 
            	rowCamuDetail.createCell(4).setCellValue(" "); 
            	rowCamuDetail.createCell(5).setCellValue(" "); 
            	rowCamuDetail.createCell(6).setCellValue(" "); 
            	rowCamuDetail.createCell(7).setCellValue(" ");
            	//rowCamuDetail.createCell(11).setCellValue(" "); 
            	//rowCamuDetail.createCell(12).setCellValue(" ");
            	rowCamuDetail.createCell(16).setCellValue(" "); 
            	rowCamuDetail.createCell(17).setCellValue(" "); 
            	rowCamuDetail.createCell(19).setCellValue(" "); 
            	rowCamuDetail.createCell(20).setCellValue(" "); 
            	rowCamuDetail.createCell(21).setCellValue(""); //Receipt date
            	rowCamuDetail.createCell(22).setCellValue(""); //Receipt time
            	//rowCamuDetail.createCell(23).setCellValue(" "); 
            	
            	// get customer details...
            	System.out.println("TTid is " + ttid);
            	if(ttid.equals("OID")) {
            		System.out.println("TTID is OID");
            		
            		MvxBean OIS100MIBean = new MvxBean();
            		OIS100MIBean.setSystem(system);
            		OIS100MIBean.setPort(port);
            		OIS100MIBean.setCompany(cono);
            		OIS100MIBean.setUsername(m3user);
            		OIS100MIBean.setPassword(m3pass);
            		
            		OIS100MIBean.setInitialise("OIS100MI");	
            		OIS100MIBean.setField("CONO", cono);
            		OIS100MIBean.setField("ORNO", fldRidn);
            		OIS100MIBean.runProgram("GetHead");
            		String cuno = OIS100MIBean.getField("CUNO");
            		String cuor = OIS100MIBean.getField("CUOR");
            		String oref = OIS100MIBean.getField("OREF");
            		String rldt = OIS100MIBean.getField("RLDT");
            		
            		OIS100MIBean.setField("CONO", cono);
            		OIS100MIBean.setField("ORNO", fldRidn);
            		OIS100MIBean.setField("PONR", fldRidl);
            		OIS100MIBean.runProgram("GetLine");
            		String whlo = OIS100MIBean.getField("WHLO");
            		String orqt = OIS100MIBean.getField("ORQT");
            		String alun = OIS100MIBean.getField("ALUN");

            		OIS100MIBean.setInitialise("CRS610MI");
            		OIS100MIBean.setField("CONO", cono);
            		OIS100MIBean.setField("CUNO", cuno);
            		OIS100MIBean.runProgram("GetBasicData");
            		String cunm = OIS100MIBean.getField("CUNM"); if(cunm==null) cunm = ""; if(cunm.equals("")) cunm = " ";
            		rowCamuDetail.createCell(20).setCellValue(cuno + " - " + cunm);
            		rowCamuDetail.createCell(19).setCellValue("CO: " + fldRidn + "/" + fldRidl + "(" + orqt + " "  + alun + " out of " + whlo + ")");
            	}
            }
    	}
    	}
    } 
    
	arrMWS070MI = new MvxBean[10];
	arrMMS200MI = new MvxBean[10]; //todo recursion, could use linked list...
	arrMWS070MI25 = new MvxBean[10];

	
	if(gotHeaderTrans) nextLvl(camu, bano, 2, false, null, 0, itno); // blank bano to start...    KW hold off on this for now...
    // take out blank bano to start...
	
    System.out.println("High level is " + highLvl);

    for(int i = 0; i < 1; i++) sht.autoSizeColumn(i);
    
    
    
    
    
    
    
    
    sht.setAutoFilter(new CellRangeAddress(0, 0, 0, hdrCols2.length));
    sht.createFreezePane(0, 1);
    
    boolean sorting = true;
    int lastRow = sht.getLastRowNum();
    
    
    Sheet originalSheet = workbook.getSheetAt(0);
    //Map<String, Row> sortedRowsMap = new TreeMap<>();
    Map<Double, Row> sortedRowsMap = new TreeMap<>();
    Row headerRow = originalSheet.getRow(0);

    Iterator<Row> rowIterator = originalSheet.rowIterator();
    // skip header row as we saved it already
    rowIterator.next();
    // sort the remaining rows
    int i = 0;
    while(rowIterator.hasNext()) {
    	i++;
        Row row = rowIterator.next();
        //sortedRowsMap.put(row.getCell(0).getStringCellValue(), row);
        sortedRowsMap.put(row.getCell(0).getNumericCellValue(), row);
    }
    System.out.println("i is " + i);
    //System.out.println(sortedRowsMap.values());

    // Create a new workbook
    sortedWorkbook = new XSSFWorkbook();
        Sheet sortedSheet = sortedWorkbook.createSheet(originalSheet.getSheetName());

        // Copy all the sorted rows to the new workbook
        // - header first
        copyRowNum = 0;
        Row newRow = sortedSheet.createRow(0);
        copyRowToRow(headerRow, newRow,copyRowNum++);
        // then other rows, from row 1 up (not row 0)
        int rowIndex = 1;
        System.out.println(sortedRowsMap.size());
        for(Row row : sortedRowsMap.values()) {
            newRow = sortedSheet.createRow(rowIndex);
            copyRowToRow(row, newRow, copyRowNum++);
            rowIndex++;
        }
        
        for(int g = 0; g < hdrCols2.length; g++) sortedSheet.autoSizeColumn(g);
        sortedSheet.setAutoFilter(new CellRangeAddress(0, 0, 0, hdrCols2.length));
        sortedSheet.createFreezePane(0, 1); 
        
        
        
        
        
        
        

    response.setContentType("application/vnd.ms-excel");
    response.setHeader("Content-Disposition", "attachment; filename=" + bano + ".xlsx");
    // kw todo could also email this to user based on MNS150 GetUserData (CRS111 Get) email address (or SSO)...
    
    //workbook.write(response.getOutputStream());
    workbook.close();
    
    
    sortedWorkbook.write(response.getOutputStream());
    sortedWorkbook.close();
  }
    


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
    private void nextLvl(String intCamu, String intBano, int intLvl, boolean intType, Row currRow, int addNumz, String rawMatItno)  { // recursion...	
    	System.out.println("Camu: " + intCamu + " Lvl: " + intLvl + " row " + rowNum + " Lot: " + intBano);
    	int addNums = 0;
    	lvlMultiplierAdd+=101 + addNumz + fifty50;
    	fifty50+=50;
    	// if its an INT type, update that currRow with the new MO....
    	
    	if(intLvl>highLvl) highLvl = intLvl;
   	
    	arrMWS070MI[intLvl] = new MvxBean();
    	arrMWS070MI[intLvl].setSystem(system);
    	arrMWS070MI[intLvl].setPort(port);
    	arrMWS070MI[intLvl].setCompany(cono);
    	arrMWS070MI[intLvl].setUsername(m3user);
    	arrMWS070MI[intLvl].setPassword(m3pass);
    	arrMWS070MI[intLvl].setInitialise("MWS070MI");
  
        String ridn = ""; String finishedItno = "";
        // 17 is qi receipt
        // 10 is a normal receipt with a qi inspection
        // technical want an inspection on blends
        boolean ok17 = false; boolean ok10 = false; boolean ok13 = false;
        arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("CAMU", intCamu);
        arrMWS070MI[intLvl].setField("BANO", intBano);
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        
        String prevBano = ""; String prevBref = ""; String prevBre2 = ""; String dat = "";
        while(arrMWS070MI[intLvl].nextRow()) { //doesnt matter if multiples returned here...	
        	//System.out.println("Type 17");
        	ok17 = true;
        	ridn = arrMWS070MI[intLvl].getField("RIDN");
        	prevBano = arrMWS070MI[intLvl].getField("BANO");
        	prevBref = arrMWS070MI[intLvl].getField("BREF");
        	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
        	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
        }
        
        // try 17 without lot
        
        if(!ok17) {
        	System.out.println("Type 17 take 2 setting " + intCamu + " " + intBano);
        	//arrMWS070MI[intLvl].setInitialise("MWS070MI");
        	arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
            //arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok17 = true;
            	System.out.println("did we find a NEXT next row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }
        
        if(!ok17) {
        	System.out.println("Type 10");
        	arrMWS070MI[intLvl].setField("TTYP", "10"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok10 = true;
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }
        
        if(!ok17 && !ok10) {
        	System.out.println("Type 13 cakes, setting " + intCamu + " " + intBano);
        	arrMWS070MI[intLvl].setField("TTYP", "13"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ok13 = true;
            	System.out.println("did we find a next row for " + intBano);
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
            	prevBano = arrMWS070MI[intLvl].getField("BANO");
            	prevBref = arrMWS070MI[intLvl].getField("BREF");
            	prevBre2 = arrMWS070MI[intLvl].getField("BRE2");
            }
        }

        String itno = "";
        
        if( ok10 || ok17 || ok13 ) {
        arrMMS200MI[intLvl] = new MvxBean(); // get item type and desc
        arrMMS200MI[intLvl].setSystem(system);
        arrMMS200MI[intLvl].setPort(port);
        arrMMS200MI[intLvl].setCompany(cono);
        arrMMS200MI[intLvl].setUsername(m3user);
        arrMMS200MI[intLvl].setPassword(m3pass);
        arrMMS200MI[intLvl].setInitialise("MMS200MI");
        
        arrMWS070MI[intLvl].setField("TTYP", "11"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("RIDN", ridn);
        

        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        
        String existItno = ""; // dont send INTs twice (we get ++ INT records if they have diff bano/camu)
        String existBano = "";
        String itty = ""; String itds = "";
        
        while(arrMWS070MI[intLvl].nextRow()) { //

        	//Row row = null;
        	itno = arrMWS070MI[intLvl].getField("ITNO");
            String iintCamu = arrMWS070MI[intLvl].getField("CAMU"); if(iintCamu==null) iintCamu = "";
            String iintBano = arrMWS070MI[intLvl].getField("BANO"); if(iintBano==null) iintBano = "";
            String bref = arrMWS070MI[intLvl].getField("BREF"); if(bref==null) bref = "";
            String bre2 = arrMWS070MI[intLvl].getField("BRE2"); if(bre2==null) bre2 = "";
            String trqt = arrMWS070MI[intLvl].getField("TRQT"); if(trqt==null) trqt = "";
            String unms = arrMWS070MI[intLvl].getField("UNMS"); if(unms==null) unms = "";
            String stas = arrMWS070MI[intLvl].getField("STAS"); if(stas==null) stas = "";
            String rftx = arrMWS070MI[intLvl].getField("RFTX"); if(rftx==null) rftx = "";
            String resp = arrMWS070MI[intLvl].getField("RESP"); if(resp==null) resp = "";
            
            String moridn25 = arrMWS070MI[intLvl].getField("RIDN"); if(moridn25==null) moridn25 = "";
            String moridl25 = arrMWS070MI[intLvl].getField("RIDL"); if(moridl25==null) moridl25 = "";
            
            String ridnl25 = ""; String sunm25 = "";
            
            String qty11 = ""; 
            if(!trqt.equals("")) { 
            	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
                String ftrqt = decimalFormat.format(Double.valueOf(trqt));
            	qty11 = ftrqt + " " + unms;  
            }

         // get item type from MMS200MI/LstComponent (agreed with BDaly switch from PDS002MI LstComponents as BOM may have changed)
            arrMMS200MI[intLvl].setField("CONO", cono);
            arrMMS200MI[intLvl].setField("ITNO", itno);
            
            
            if(arrMMS200MI[intLvl].runProgram("GetItmBasic").startsWith("OK")) {
            	itty = arrMMS200MI[intLvl].getField("ITTY"); if(itty==null) itty = "";
            	itds = arrMMS200MI[intLvl].getField("ITDS"); if(itds==null) itds = "";
            	
            	
            	if(currRow!=null) { // kit of the blend?
            		currRow.createCell(3).setCellValue(moridn25);
            		//currRow.createCell(6).setCellValue(itno); // kit
            		//currRow.createCell(8).setCellValue(itds);
            		//currRow.createCell(0).setCellValue("Level " + intLvl);
            	} 
            	
            	
            	if( (itty.equals("PPK")) || (itty.equals("SPK")) || (itty.equals("RMT")) ) { // do PO lookup           		

                	if(!iintCamu.equals("")) {
            		
	            		if(arrMWS070MI25[intLvl]==null) {
	            			System.out.println("25 is null, should only do this once...");
		            		arrMWS070MI25[intLvl] = new MvxBean();
		                	arrMWS070MI25[intLvl].setSystem(system);
		                	arrMWS070MI25[intLvl].setPort(port);
		                	arrMWS070MI25[intLvl].setCompany(cono);
		                	arrMWS070MI25[intLvl].setUsername(m3user);
		                	arrMWS070MI25[intLvl].setPassword(m3pass);
	            		}
	            		
	            		String poType = "25";  
	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	            		arrMWS070MI25[intLvl].setField("TTYP", poType); // MO Receipt QI
	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");  
	                		
	            		if(arrMWS070MI25[intLvl].nextRow()) { 
	                    } else {
		            		arrMWS070MI25[intLvl].setField("TTYP", poType); // MO Receipt QI
		                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
		                    //arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
		                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
		                    if(arrMWS070MI25[intLvl].nextRow()) {
		                    	iintCamu = ""; //blank out camu for search...
		                    } else {
		                    	// need to do the ref lookup and recurse somehow here...
		                    	arrMWS070MI25[intLvl].setField("TTYP", "97"); // reclassification
			                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
			                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
			                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
			                    if(arrMWS070MI25[intLvl].nextRow()) {
			                    	//System.out.println("PO match level 44 container " + iintCamu);
			                    	String rftx97 = (String)arrMWS070MI25[intLvl].getField("RFTX"); if(rftx97==null) rftx97 = "";
			                    	rftx97 = rftx97.trim().replaceAll(" +", " ");
			                    	//System.out.println("rftxx is " + rftx);
			                    	String[] rftxBits = rftx97.split(" ");
			                    	if(rftxBits.length>2) iintBano = rftxBits[1]; 
			                    	//json+="\"origBano\": \"" + iintBano + "\",";
			                    	/*
			                    	String poRidnCheck = (String)arrMWS070MI25[intLvl].getField("RIDN");
			                    	poRidnCheck = poRidnCheck.trim();
			                    	System.out.println("Check PO " + poRidnCheck);
			                    	
			                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
			                    	arrMWS070MI25[intLvl].setField("CONO", cono);
			                    	arrMWS070MI25[intLvl].setField("PUNO", poRidnCheck);
			                    	if(arrMWS070MI25[intLvl].runProgram("GetHeadBasic").startsWith("OK")) {
			                    		System.out.println(" found poRidnCheck");
			                    		poFound = true;
			                    		json+="\"origBano\": \"" + iintBano + "\",";
			                    	}
				                    */
			                    }
		                    }
	                    }
	                    
	                    //if we get a response from the lookup, do this.arrMMS200MI..
                    	arrMWS070MI25[intLvl].setInitialise("MWS070MI");
                    	arrMWS070MI25[intLvl].setField("TTYP", "25");
	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
	                    
	                    if(arrMWS070MI25[intLvl].nextRow()) {
	                    	String ridn25 = arrMWS070MI25[intLvl].getField("RIDN"); if(ridn25==null) ridn25 = "";
	                    	String ridl25 = arrMWS070MI25[intLvl].getField("RIDL"); if(ridl25==null) ridl25 = "";
	                    	ridnl25 = ""; if(!ridn25.equals("")) ridnl25 = ridn25 + "/" + ridl25;
	                    	String trqt25 = arrMWS070MI25[intLvl].getField("TRQT"); if(trqt25==null) trqt25 = "";
	                    	String unms25 = arrMWS070MI25[intLvl].getField("UNMS"); if(unms25==null) unms25 = "";
	                    	String qty25 = ""; if(!trqt25.equals("")) qty25 = trqt25 + " " + unms25;             	
	                    	
	                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
	                    	arrMWS070MI25[intLvl].setField("CONO", cono);
	                    	arrMWS070MI25[intLvl].setField("PUNO", ridn25);
	                    	arrMWS070MI25[intLvl].setField("PNLI", ridl25);
	                    	if(arrMWS070MI25[intLvl].runProgram("GetLine").startsWith("OK")) {
	                    		String suno25 = arrMWS070MI25[intLvl].getField("SUNO"); if(suno25==null) suno25 = "";
	                    		String whlo25 = arrMWS070MI25[intLvl].getField("WHLO"); if(whlo25==null) whlo25 = "";
	                    		if(!suno25.equals("")) {
	                    			arrMWS070MI25[intLvl].changeProg("CRS620MI");
	                    			arrMWS070MI25[intLvl].setField("CONO", cono);
	                    			arrMWS070MI25[intLvl].setField("SUNO", suno25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetBasicData").startsWith("OK")) {
	                    				sunm25 = arrMWS070MI25[intLvl].getField("SUNM"); if(sunm25==null) sunm25 = "";
	                    			}
	                    		}
	                    		
	                    		if(!whlo25.equals("")) {
	                    			arrMWS070MI25[intLvl].changeProg("MMS005MI");
	                    			arrMWS070MI25[intLvl].setField("WHLO", whlo25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetWarehouse").startsWith("OK")) {
	                    				String whnm = arrMWS070MI25[intLvl].getField("WHNM"); if(whnm==null) whnm = "";	      
	                    			}
	                    		}
	                    		
	                    		// get the PO receipt transactions...
                    			arrMWS070MI25[intLvl].setInitialise("MWS070MI");
                    			arrMWS070MI25[intLvl].setField("TTYP", "25");
                    			arrMWS070MI25[intLvl].setField("RIDN", ridn25);
                    			arrMWS070MI25[intLvl].setField("RIDL", ridl25);
                    			arrMWS070MI25[intLvl].setField("ITNO", itno);
                    			arrMWS070MI25[intLvl].runProgram("LstTransByOrder");
                    			if(arrMWS070MI25[intLvl].nextRow()) { // this should be a WHILE for multiple receipts, revisit TODO kw
                    				//json+="\"podat\": \"" + arrMWS070MI25[intLvl].getField("TRDT") + " " + arrMWS070MI25[intLvl].getField("TRTM") + "\",";
                    			}
	                    	}
	                    }        
                	}

                 // do the first level PPK || RMT excel stuff now...
                    
                    Row rowCamu = sht.createRow(rowNum++); // add 400 for non-INT	                    
                    rowCamu.createCell(0).setCellValue(    ((intLvl-1) * 10000) + 400  + lvlMultiplierAdd++);
                    rowCamu.createCell(1).setCellValue("Inputs");
                    rowCamu.createCell(2).setCellValue(ridn);
                    rowCamu.createCell(3).setCellValue(moridn25);
                    rowCamu.createCell(4).setCellValue(itno); // parent item
                    rowCamu.createCell(5).setCellValue(itds);
                    rowCamu.createCell(6).setCellValue(rawMatItno); //raw mat item no
                    rowCamu.createCell(7).setCellValue(itty);                            	                            		                    
                    rowCamu.createCell(8).setCellValue(" "); //???
                    rowCamu.createCell(9).setCellValue(qty11); // Total issued
                    rowCamu.createCell(10).setCellValue(stas);
                    rowCamu.createCell(11).setCellValue(iintBano);
                    rowCamu.createCell(12).setCellValue(iintCamu);
                    rowCamu.createCell(13).setCellValue(rftx);
                    rowCamu.createCell(14).setCellValue(""); //Entry date
                    rowCamu.createCell(15).setCellValue(""); //Entry time
                    rowCamu.createCell(16).setCellValue(bref);
                    rowCamu.createCell(17).setCellValue(bre2);
                    rowCamu.createCell(18).setCellValue(resp); //responsible                         
                    rowCamu.createCell(19).setCellValue(ridnl25); //Partner number
                    rowCamu.createCell(20).setCellValue(sunm25); //Partner name
                    
                    
                    System.out.println("Lts get time o the  NON INT");
        			if(!iintCamu.equals("")) {
        				System.out.println("IINT camu not blankT");
	            		if(arrMWS070MI25[intLvl]==null) {
		            		arrMWS070MI25[intLvl] = new MvxBean();
		                	arrMWS070MI25[intLvl].setSystem(system);
		                	arrMWS070MI25[intLvl].setPort(port);
		                	arrMWS070MI25[intLvl].setCompany(cono);
		                	arrMWS070MI25[intLvl].setUsername(m3user);
		                	arrMWS070MI25[intLvl].setPassword(m3pass);
	            		}
	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	                		
	            		arrMWS070MI25[intLvl].setField("TTYP", "11"); // MO Receipt QI
	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	                    
	                    arrMWS070MI25[intLvl].setField("RIDN", moridn25);
	                    arrMWS070MI25[intLvl].setField("RIDL", moridl25);    	                    
	                    
	                    System.out.println("Run lst Trans with " + iintBano + " " + iintCamu+" " + moridn25 + " " + moridl25);
	                    arrMWS070MI25[intLvl].runProgram("LstTransByOrder");
	                    if(arrMWS070MI25[intLvl].nextRow()) {
	                    	rowCamu.createCell(14).setCellValue(arrMWS070MI25[intLvl].getField("TRDT")); //Entry date
	                        rowCamu.createCell(15).setCellValue(arrMWS070MI25[intLvl].getField("TRTM")); //Entry time
	                    }
        			}
        			
                    if(!ridnl25.equals("")) {
	                    rowCamu.createCell(21).setCellValue(arrMWS070MI25[intLvl].getField("TRDT")); //Receipt date
	                    rowCamu.createCell(22).setCellValue(arrMWS070MI25[intLvl].getField("TRTM")); //Receipt time
                    }
                    //rowCamu.createCell(23).setCellValue("Raw Materials Issued to FG MO");	                    
                    // 1st level PPK || SPK || RMT excel stuff finished...
            		
            	} else if(itty.equals("INT")) { 
            		
            		if(!iintCamu.equals("")) {
            			// should only be one INT, don't call two, need to revisit this one...
            			boolean skip = true;
            			if(itno.equals(existItno)) addNums+= 80;
            			if( (itno.equals(existItno)) && (iintBano.equals(existBano)) ) skip = true; else skip = false;
            			existItno = itno;
            			existBano = iintBano;
            			//skip=false; //temp
            			
            			Row rowCamu = sht.createRow(rowNum++);  
            			// just in case these end up blank...
                        rowCamu.createCell(14).setCellValue(""); //Entry date
                        rowCamu.createCell(15).setCellValue(""); //Entry time
            			// get the time for INTs
            			System.out.println("Lts get time o the INT");
            			if(!iintCamu.equals("")) {
            				System.out.println("IINT camu not blankT");
    	            		if(arrMWS070MI25[intLvl]==null) {
    		            		arrMWS070MI25[intLvl] = new MvxBean();
    		                	arrMWS070MI25[intLvl].setSystem(system);
    		                	arrMWS070MI25[intLvl].setPort(port);
    		                	arrMWS070MI25[intLvl].setCompany(cono);
    		                	arrMWS070MI25[intLvl].setUsername(m3user);
    		                	arrMWS070MI25[intLvl].setPassword(m3pass);
    	            		}
    	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
    	                		
    	            		arrMWS070MI25[intLvl].setField("TTYP", "11"); // MO Receipt QI
    	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
    	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
    	                    
    	                    arrMWS070MI25[intLvl].setField("RIDN", moridn25);
    	                    arrMWS070MI25[intLvl].setField("RIDL", moridl25);    	                    
    	                    
    	                    System.out.println("Run lst Trans with " + iintBano + " " + iintCamu+" " + moridn25 + " " + moridl25);
    	                    arrMWS070MI25[intLvl].runProgram("LstTransByOrder");
    	                    if(arrMWS070MI25[intLvl].nextRow()) {
    	                    	rowCamu.createCell(14).setCellValue(arrMWS070MI25[intLvl].getField("TRDT")); //Entry date
    	                        rowCamu.createCell(15).setCellValue(arrMWS070MI25[intLvl].getField("TRTM")); //Entry time
    	                    }
            			}
            			// end get the time for INTs

            			
                     // do the first level INT excel stuff now...
                                             
                        //rowCamu.createCell(0).setCellValue( (intLvl * 10000) + (lvlMultiplierAdd++) );
                        rowCamu.createCell(0).setCellValue( ((intLvl-1) * 10000) + (lvlMultiplierAdd++) );
                        rowCamu.createCell(1).setCellValue("Inputs");
                        rowCamu.createCell(2).setCellValue(ridn);
                        rowCamu.createCell(3).setCellValue(moridn25);
                        rowCamu.createCell(4).setCellValue(itno); // this item
                        rowCamu.createCell(5).setCellValue(itds);
                        rowCamu.createCell(6).setCellValue(finishedItno); //raw mat item no
                        //format yellow
                        CellStyle cstyle = workbook.createCellStyle();   
                        cstyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());  
                        cstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND); 	                        
                        Cell cItty = rowCamu.createCell(7);
                        cItty.setCellValue(itty);
                        cItty.setCellStyle(cstyle);
                        //end format
                        rowCamu.createCell(8).setCellValue(" "); // ???
                        rowCamu.createCell(9).setCellValue(qty11); // Total issued
                        rowCamu.createCell(10).setCellValue(stas);
                        rowCamu.createCell(11).setCellValue(iintBano);
                        rowCamu.createCell(12).setCellValue(iintCamu);
                        rowCamu.createCell(13).setCellValue(rftx);

                        rowCamu.createCell(16).setCellValue(bref);
                        rowCamu.createCell(17).setCellValue(bre2);
                        rowCamu.createCell(18).setCellValue(resp); //responsible                         
                        rowCamu.createCell(19).setCellValue(""); //Partner number
                        rowCamu.createCell(20).setCellValue(""); //Partner name
                        rowCamu.createCell(21).setCellValue(""); //Receipt date
                        rowCamu.createCell(22).setCellValue(""); //Receipt time
                        //rowCamu.createCell(23).setCellValue("Link Bin Details");
                        
                        // 1st level INT excel stuff finished...
                        if(!skip) {
                        	lvlMultiplierAdd = 0;
                        	nextLvl(iintCamu, iintBano, intLvl+1, true, rowCamu, addNums, itno);  
                        }
            		}         
            	} else { // non INT and PPK, no need to look up PO source of product...	
            		//
            	}	
            	
                
            } else {
            	//row.createCell(intLvl+1).setCellValue("Not found");
            }
        
        }
        }
        
        System.out.println("End levelx " + intLvl);
    }
    
    // Utility method to copy rows
    private static void copyRowToRow(Row row, Row newRow, int cpyRowNum) {
        Iterator<Cell> cellIterator = row.cellIterator();
        int cellIndex = 0; int iCellVal = 0; boolean inputs = false; boolean changeMO = false;
        while(cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            Cell newCell = newRow.createCell(cellIndex);
            String cellVal = "";
            double dCellVal = 0.0;
            
            
            if( (cellIndex==0) && (cpyRowNum!=0) ) {
            	try {
	            	dCellVal = cell.getNumericCellValue();
	            	cellVal = Double.toString(dCellVal);
            	} catch (Exception e) {
            		System.err.println("Cake " + e);
            		cellVal = cell.getStringCellValue();
            	}
                
                cellVal = cellVal.substring(0,1);
                iCellVal = Integer.parseInt(cellVal);
                iCellVal-=2;
                cellVal = Integer.toString(iCellVal);
            } else
            	cellVal = cell.getStringCellValue();
            
            if(cellIndex==1) {
            	if(cellVal.equals("Inputs")) 
            		inputs = true; 
            	else inputs = false;
            } else if(cellIndex==2) {
            	if(!cellVal.equals(thisMO)) 
            		changeMO = true; 
            	else changeMO = false;
            	thisMO = cellVal;
            	System.out.println(changeMO + " " + thisMO);
            	if(changeMO) {
            		if(colPos >= (cols.length-2)) colPos=0; else colPos++;
            		System.out.println("In change icellval " + iCellVal + " POS " + colPos);
            	}
            }
  
        
            if(cellVal.equals("INT")) {
	            //format yellow
	            CellStyle cstyle = sortedWorkbook.createCellStyle();   
	            cstyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());  
	            cstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND); 	                        
	            newCell.setCellStyle(cstyle);
            }  else if(inputs) {
            	CellStyle cstyle = sortedWorkbook.createCellStyle();            	

	            cstyle.setFillForegroundColor(cols[(colPos)]);  
	            cstyle.setFillPattern(FillPatternType.SOLID_FOREGROUND); 	                        
	            newCell.setCellStyle(cstyle);
            }
            //end format
            
            newCell.setCellValue(cellVal);
            cellIndex++;
        }
    }
}
