package com.ornua;

// if it WAS an INT but now its NON-INT
// get unique containers (say there's 2....)
// do for each container....yup thats possible. If one container, do as normal...
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ornua.MvxBean;

/**
 * Servlet implementation class dalytrace
 */
// Kevin Woods March 2020
@WebServlet("/dalytraceJson2")
public class dalytraceJson2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String camu;
	private int rowNum;
	
	private MvxBean[] arrMWS070MI;
	private MvxBean[] arrMMS200MI;
	private MvxBean[] arrMWS070MI25;
		
	private String system;
	private String cono;
	private String faci;
	private String m3user;
	private String m3pass;
	private int port;

	private String json;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public dalytraceJson2() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
    * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
    */
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // TODO Auto-generated method stub 
    	PrintWriter out = response.getWriter();
    	response.setContentType("text/html;charset=utf-8");  
    	
   rowNum = 0;
    camu = request.getParameter("camu"); if(camu==null) camu = "";
    
    json = "";
	json+="{";
	
	json+="\"success\": true,";
    json+="\"text\": \".\",";
    json+="\"children\": [";
        json+="{";
            json+="\"itno\": \"Pallet: " + camu + "\",";
            //todo could also get some detail on this pallet (if in stock?) e.g. itno, bref, bre2 etc...
            json+="\"expanded\": true,";
    
	arrMWS070MI = new MvxBean[10];
	arrMMS200MI = new MvxBean[10]; //todo recursion, could use linked list...
	arrMWS070MI25 = new MvxBean[10];
	
    HttpSession session = request.getSession(true);
    system = session.getAttribute("system").toString();
    cono = session.getAttribute("zdcono").toString();
    faci = session.getAttribute("zdfaci").toString();
    m3user = session.getAttribute("m3user").toString();
    m3pass = session.getAttribute("m3pass").toString();
    port = (Integer)session.getAttribute("port");

    nextLvl(camu, "", 0); // blank bano to start...    
    
    if(json.length()>2) json = json.substring(0, json.length() - 4);
			//json+="  }  ";
		//json+="  ]  ";
    json+="  }  ";
    out.print(json); out.flush();
  }
    
    private void nextLvl(String intCamu, String intBano, int intLvl) { // recursion...	
    	
    	System.out.println("Camu: " + intCamu + " Lvl: " + intLvl + " row " + rowNum + " Lot: " + intBano);
   	
    	arrMWS070MI[intLvl] = new MvxBean();
    	arrMWS070MI[intLvl].setSystem(system);
    	arrMWS070MI[intLvl].setPort(port);
    	arrMWS070MI[intLvl].setCompany(cono);
    	arrMWS070MI[intLvl].setUsername(m3user);
    	arrMWS070MI[intLvl].setPassword(m3pass);
    	arrMWS070MI[intLvl].setInitialise("MWS070MI");
  
        String ridn = ""; String finishedItno = "";
        // 17 is qi receipt
        // 10 is a normal receipt with a qi inspection
        // technical want an inspection on blends
        boolean ok17 = false;
        arrMWS070MI[intLvl].setField("TTYP", "17"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("CAMU", intCamu);
        arrMWS070MI[intLvl].setField("BANO", intBano);
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        
        while(arrMWS070MI[intLvl].nextRow()) { //doesnt matter if multiples returned here...	
        	//System.out.println("Type 17");
        	ok17 = true;
        	ridn = arrMWS070MI[intLvl].getField("RIDN");
        	finishedItno = arrMWS070MI[intLvl].getField("ITNO"); 
            //row.createCell(intLvl).setCellValue(ridn);
        }
        
        if(!ok17) {
        	System.out.println("Type 10");
        	arrMWS070MI[intLvl].setField("TTYP", "10"); // MO Receipt QI
            arrMWS070MI[intLvl].setField("CAMU", intCamu);
            arrMWS070MI[intLvl].setField("BANO", intBano);
            arrMWS070MI[intLvl].runProgram("LstTransBalID");           

            while(arrMWS070MI[intLvl].nextRow()) {
            	ridn = arrMWS070MI[intLvl].getField("RIDN"); 
            	finishedItno = arrMWS070MI[intLvl].getField("ITNO");
                //row.createCell(intLvl).setCellValue(ridn);
            }
        }
        
    	json+="\"children\": [";
        json+="{";
            json+="\"itno\": \"MO: " + ridn + "\",";
            json+="\"expanded\": true,";
    		json+="\"children\": [";
            // MO should always have children...
        //rowNum++;
        String itno = "";
        
        arrMMS200MI[intLvl] = new MvxBean(); // get item type and desc
        arrMMS200MI[intLvl].setSystem(system);
        arrMMS200MI[intLvl].setPort(port);
        arrMMS200MI[intLvl].setCompany(cono);
        arrMMS200MI[intLvl].setUsername(m3user);
        arrMMS200MI[intLvl].setPassword(m3pass);
        arrMMS200MI[intLvl].setInitialise("MMS200MI");
        
        arrMWS070MI[intLvl].setField("TTYP", "11"); // MO Receipt QI
        arrMWS070MI[intLvl].setField("RIDN", ridn);
        
        // try this...
        //arrMWS070MI[intLvl].setField("BANO", intBano);
        //arrMWS070MI[intLvl].setField("CAMU", intCamu);
        //
        
        arrMWS070MI[intLvl].runProgram("LstTransBalID");
        //arrMWS070MI[intLvl].runProgram("LstTransByOrder");
        //order gives back duplicates and different stuff, need to check...
        //System.out.println("11: " + ridn);
        
        String existItno = ""; // dont send INTs twice (we get ++ INT records if they have diff bano/camu)
        String itty = ""; String itds = "";
        while(arrMWS070MI[intLvl].nextRow()) { //
        	
        	json+="{"; 

        	//Row row = null;
        	itno = arrMWS070MI[intLvl].getField("ITNO");
            String iintCamu = arrMWS070MI[intLvl].getField("CAMU"); if(iintCamu==null) iintCamu = "";
            String iintBano = arrMWS070MI[intLvl].getField("BANO"); if(iintBano==null) iintBano = "";
            String bref = arrMWS070MI[intLvl].getField("BREF"); if(bref==null) bref = "";
            String bre2 = arrMWS070MI[intLvl].getField("BRE2"); if(bre2==null) bre2 = "";
            String trqt = arrMWS070MI[intLvl].getField("TRQT"); if(trqt==null) trqt = "";
            String unms = arrMWS070MI[intLvl].getField("UNMS"); if(unms==null) unms = "";
            
            String moridn25 = arrMWS070MI[intLvl].getField("RIDN"); if(moridn25==null) moridn25 = "";
            String moridl25 = arrMWS070MI[intLvl].getField("RIDL"); if(moridl25==null) moridl25 = "";
            
            String qty11 = ""; 
            if(!trqt.equals("")) { 
            	DecimalFormat decimalFormat = new DecimalFormat("0.#####");
                String ftrqt = decimalFormat.format(Double.valueOf(trqt));
            	qty11 = ftrqt + " " + unms;  
            }

         // get item type from MMS200MI/LstComponent (agreed with BDaly switch from PDS002MI LstComponents as BOM may have changed)
            arrMMS200MI[intLvl].setField("CONO", cono);
            arrMMS200MI[intLvl].setField("ITNO", itno);
            if(arrMMS200MI[intLvl].runProgram("GetItmBasic").startsWith("OK")) {
            	itty = arrMMS200MI[intLvl].getField("ITTY"); if(itty==null) itty = "";
            	itds = arrMMS200MI[intLvl].getField("ITDS"); if(itds==null) itds = "";
            	
            	if(itty.equals("INT")) { 
            		
            		if(!iintCamu.equals("")) {
            			// should only be one INT, don't call two, need to revisit this one...
            			boolean skip = true;
            			if(itno.equals(existItno)) skip = true; else skip = false;
            			existItno = itno;
            			if(!skip) {
                            //row.createCell(intLvl).setCellValue(itno);    
                        	//row.createCell(intLvl+1).setCellValue(itty);
                        	//row.createCell(intLvl+2).setCellValue(itds);
            				
                                json+="\"itno\": \"" + itno + "\",";
                                json+="\"itty\": \"" + itty + "\",";
                                json+="\"bano\": \"" + iintBano + "\",";
                                json+="\"camu\": \"" + iintCamu + "\",";
                                
    	                    	json+="\"moridn25\": \"" + moridn25 + "\",";
    	                    	json+="\"moridl25\": \"" + moridl25 + "\",";
                                
                                json+="\"bref\": \"" + bref + "\",";
                                json+="\"bre2\": \"" + bre2 + "\",";
                                json+="\"qty\": \"" + qty11 + "\",";
                                
                                json+="\"expanded\": true,";
            				nextLvl(iintCamu, iintBano, intLvl+1); //recurse
            			}
            		}         
            	} else if( (itty.equals("PPK")) || (itty.equals("SPK")) ) { // do PO lookup
            		
                    //row.createCell(intLvl).setCellValue(itno);    
                    //row.createCell(intLvl+3).setCellValue(iintBano);
                    //row.createCell(intLvl+4).setCellValue(iintCamu);
                	//row.createCell(intLvl+1).setCellValue(itty);
                	//row.createCell(intLvl+2).setCellValue(itds);

            		json+="\"moridn25\": \"" + moridn25 + "\",";
                	json+="\"moridl25\": \"" + moridl25 + "\",";
                        json+="\"itno\": \"" + itno + "\",";
                        json+="\"itty\": \"" + itty + "\",";
                        json+="\"itds\": \"" + itds + "\",";
                        json+="\"bano\": \"" + iintBano + "\",";
                        json+="\"camu\": \"" + iintCamu + "\",";
                        json+="\"bref\": \"" + bref + "\",";
                        json+="\"bre2\": \"" + bre2 + "\",";
                        json+="\"qty\": \"" + qty11 + "\",";

                	if(!iintCamu.equals("")) {
            		
	            		if(arrMWS070MI25[intLvl]==null) {
	            			System.out.println("25 is null, should only do this once...");
		            		arrMWS070MI25[intLvl] = new MvxBean();
		                	arrMWS070MI25[intLvl].setSystem(system);
		                	arrMWS070MI25[intLvl].setPort(port);
		                	arrMWS070MI25[intLvl].setCompany(cono);
		                	arrMWS070MI25[intLvl].setUsername(m3user);
		                	arrMWS070MI25[intLvl].setPassword(m3pass);
	            		}
	            		arrMWS070MI25[intLvl].setInitialise("MWS070MI");
	                		
	            		arrMWS070MI25[intLvl].setField("TTYP", "25"); // MO Receipt QI
	                    arrMWS070MI25[intLvl].setField("BANO", iintBano);
	                    arrMWS070MI25[intLvl].setField("CAMU", iintCamu);
	                    arrMWS070MI25[intLvl].runProgram("LstTransBalID");
	                    if(arrMWS070MI25[intLvl].nextRow()) {
	                    	String ridn25 = arrMWS070MI25[intLvl].getField("RIDN"); if(ridn25==null) ridn25 = "";
	                    	String ridl25 = arrMWS070MI25[intLvl].getField("RIDL"); if(ridl25==null) ridl25 = "";
	                    	String ridnl25 = ""; if(!ridn25.equals("")) ridnl25 = ridn25 + "/" + ridl25;
	                    	String trqt25 = arrMWS070MI25[intLvl].getField("TRQT"); if(trqt25==null) trqt25 = "";
	                    	String unms25 = arrMWS070MI25[intLvl].getField("UNMS"); if(unms25==null) unms25 = "";
	                    	String qty25 = ""; if(!trqt25.equals("")) qty25 = trqt25 + " " + unms25;
	                    	
	                    	//row.createCell(intLvl+5).setCellValue(ridnl25);
	                    	//row.createCell(intLvl+6).setCellValue(qty25);
	                    	
	                    	json+="\"ridnl\": \"" + ridnl25 + "\",";
				            json+="\"poqty\": \"" + qty25 + "\",";
	                    	
	                    	arrMWS070MI25[intLvl].changeProg("PPS001MI");
	                    	arrMWS070MI25[intLvl].setField("CONO", cono);
	                    	arrMWS070MI25[intLvl].setField("PUNO", ridn25);
	                    	if(arrMWS070MI25[intLvl].runProgram("GetHeadBasic").startsWith("OK")) {
	                    		String suno25 = arrMWS070MI25[intLvl].getField("SUNO"); if(suno25==null) suno25 = "";
	                    		if(!suno25.equals("")) {
	                    			//row.createCell(intLvl+7).setCellValue(suno25);	                    			
	    				            json+="\"suno\": \"" + suno25 + "\",";
	    	                        
	                    			arrMWS070MI25[intLvl].changeProg("CRS620MI");
	                    			arrMWS070MI25[intLvl].setField("CONO", cono);
	                    			arrMWS070MI25[intLvl].setField("SUNO", suno25);
	                    			if(arrMWS070MI25[intLvl].runProgram("GetBasicData").startsWith("OK")) {
	                    				String sunm25 = arrMWS070MI25[intLvl].getField("SUNM"); if(sunm25==null) sunm25 = "";
	                    				//row.createCell(intLvl+8).setCellValue(sunm25);
		    				            json+="\"sunm\": \"" + sunm25 + "\",";
	                    			}
	                    		}
	                    	}
	                    }
                	}
                	
                    json+="\"leaf\": true,";
                    json+="\"expanded\": false"; // only INTs can carry on...
            	} else { // non INT and PPK, no need to look up PO source of product...	
                    //row.createCell(intLvl).setCellValue(itno);    
                    //row.createCell(intLvl+3).setCellValue(iintBano);
                    //row.createCell(intLvl+4).setCellValue(iintCamu);
                	//row.createCell(intLvl+1).setCellValue(itty);
                	//row.createCell(intLvl+2).setCellValue(itds);
            		
            		json+="\"moridn25\": \"" + moridn25 + "\",";
                	json+="\"moridl25\": \"" + moridl25 + "\",";
            		
            		json+="\"bref\": \"" + bref + "\",";
                    json+="\"bre2\": \"" + bre2 + "\",";
                    json+="\"qty\": \"" + qty11 + "\",";
            		
                    json+="\"itno\": \"" + itno + "\",";
                    json+="\"itty\": \"" + itty + "\",";
                    json+="\"itds\": \"" + itds + "\",";
                    json+="\"bano\": \"" + iintBano + "\",";
                    json+="\"camu\": \"" + iintCamu + "\",";
                    json+="\"leaf\": true,";
                    json+="\"expanded\": false"; // only INTs can carry on...
            	}	
            
            } else {
            	//row.createCell(intLvl+1).setCellValue("Not found");
            }
            json+="},";	
        }
        
        if(json.length()>2) json = json.substring(0, json.length() - 1);
        if((json.length()>2) && (json.endsWith("{}")))
        	json = json.substring(0, json.length() - 2);
        
        if((json.length()>2) && (json.endsWith("{}, ")))
        	json = json.substring(0, json.length() - 4);
        
        json+="]"; // close the item children...
        
        if((json.length()>2) && (json.endsWith("{}")))
        	json = json.substring(0, json.length() - 2);
        
        json+="}]"; // close the MO (check if more than one? TODO)
        
        if((json.length()>2) && (json.endsWith("{}")))
        	json = json.substring(0, json.length() - 2);
        
        //if(json.length()>2) json = json.substring(0, json.length() - 1);
        json+="}]"; // close the INT item (might need to check if there is one? TODO)
        
        if((json.length()>2) && (json.endsWith("{}")))
        	json = json.substring(0, json.length() - 2);

        
        //if(json.length()>2) json = json.substring(0, json.length() - 1);
        //json+="]";
        System.out.println("End levelx " + intLvl);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
